@if(!empty($viewData['group']) AND !empty($viewData['group']->id))
    <form action="{{ route('updateGroup',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id] )}}" role="form" method="post" class="smart-form currentForm">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        @else
            <form action="{{ route('storeGroup') }}" method="post" class="smart-form currentForm">
                {{ csrf_field() }}
                @endif

                <input name="survey_id" type="hidden" value="<?php echo $viewData['survey']->id; ?>">
                <?php
                    if(!empty($viewData['group']->id)){
                ?>
                    <input name="group_id" type="hidden" value="<?php echo $viewData['group']->id; ?>">
                <?php
                    }
                ?>

                <fieldset>
                    <section>
                        <label class="label">
                            <strong>
                                Titulo
                            </strong>
                        </label>
                        <label class="input">
                            <input type="text"
                                   name="txtTitle"
                                   class="input-sm" required
                                   value="<?php echo !empty($viewData['group']->name) ? $viewData['group']->name : ""; ?>">
                        </label>
                    </section>

                    <section>
                        <label class="label">
                            <strong>
                                Descripción
                            </strong>
                        </label>
                        <label class="textarea">
                                    <textarea rows="3" id="txtDescription" name="txADescription"
                                              class="custom-scroll smCkEditor"><?php echo !empty($viewData['group']->description) ? $viewData['group']->description : ""; ?></textarea>
                        </label>
                    </section>
                </fieldset>
                <input type="submit" value="Enviar" class="hideElement" />
            </form>