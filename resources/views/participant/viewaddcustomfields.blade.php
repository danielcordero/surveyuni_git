@extends('layouts.app')
@section('content')
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Gestionar participantes de la encuesta
                    </strong>
                </h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">

                        @include('participant.elements.topBottons')

                        <div class="row">
                            <div class="col-md-12">
                                <ul class="demo-btns pull-right">
                                    <li>
                                        <button type="button" id="btnSave" class="btn btn-primary openMainModal">
                                            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                                        </button>
                                    </li>
                                    <li>
                                        <a href="{{ route('participants.config',['surveyId'=>$viewData['survey']->id]) }}"
                                           class="btn btn-danger">
                                            Regresar
                                            <span class="glyphicon glyphicon-arrow-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            Agregar campos personalizados
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <form action="{{ route('participants.createcustomfields',['surveyId'=>$viewData['survey']->id]) }}"
                              method="post"
                              id="frmAddCustomFields"
                              class=""
                              novalidate="novalidate">
                            {{ csrf_field() }}

                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <div class="jarviswidget jarviswidget-color-green"
                                         data-widget-editbutton="false"
                                         data-widget-custombutton="false"
                                         data-widget-deletebutton="false"
                                         data-widget-sortable="false"
                                         data-widget-collapsed="false"
                                         data-widget-fullscreenbutton="false"
                                         data-widget-attstyle="jarviswidget-color-green">

                                        <header role="heading">
                                            <h2>
                                                <strong>
                                                    Campos personalizados
                                                </strong>
                                                <i class="fa fa-file-text-o"></i>
                                            </h2>
                                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                        </header>
                                        <!-- widget div-->
                                        <div role="content">

                                            <!-- widget edit box -->
                                            <div class="jarviswidget-editbox">
                                                <!-- This area used as dropdown edit box -->
                                            </div>
                                            <!-- end widget edit box -->

                                            <!-- widget content -->
                                            <div class="widget-body no-padding smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-md-12">
                                                            <a class="btn btn-primary btnForce addCustomField">
                                                                Agregar nuevos campos personalizados
                                                                <i class="fa fa-fw fa-plus"></i>
                                                            </a>
                                                        </section>
                                                    </div>
                                                </fieldset>

                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-md-12">
                                                            <div class="alert alert-info" role="alert">
                                                                <strong></strong>
                                                                <span style="font-weight:bold;">
                                                                    Condiciones:
                                                                </span>
                                                                <br>
                                                                <strong>
                                                                    a. No se aceptan campos duplicados.
                                                                </strong>
                                                                <br>
                                                                <strong>
                                                                    b. Agregue una palabra por campo.
                                                                </strong>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </fieldset>

                                                <div class="">
                                                    <fieldset>
                                                        <div class="panel panel-default">
                                                            <table class="table"
                                                                   id="tblCustomFields"
                                                                   data-toggle="dynamicRow">
                                                                <thead>
                                                                <tr>
                                                                    <th>Posición</th>
                                                                    <th>Código</th>
                                                                    <th>Requirido</th>
                                                                    <th>Campo</th>
                                                                    <th>Acción</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                if(!empty($viewData['customFields'])){
                                                                $customFields = $viewData['customFields'];
                                                                foreach ($customFields as $key => $value){
                                                                ?>
                                                                <tr id="<?php echo $key; ?>">
                                                                    <td>
                            <span class="cursorMove">
                                <i class="fa fa-bars fa-2x cursorMove" aria-hidden="true"></i>
                            </span>
                                                                    </td>
                                                                    <td>
                                                                        <?php echo $key; ?>
                                                                    </td>
                                                                    <td>
                                                                        <span class="onoffswitch">
                                                                                <input type="checkbox"
                                                                                       name="data[mandatoryfield][<?php echo $key; ?>]"
                                                                                       class="onoffswitch-checkbox"
                                                                                       id="mandatoryfield_<?php echo $key; ?>"
                                                                                       <?php if(!empty($value->isRequired)){
                                                                                            echo "checked=''";
                                                                                       } ?>>
                                                                                <label class="onoffswitch-label" for="mandatoryfield_<?php echo $key; ?>">
                                                                            <span class="onoffswitch-inner"
                                                                                  data-swchon-text="ON" data-swchoff-text="OFF"></span>
                                                                            <span class="onoffswitch-switch"></span></label>
                                                                        </span>
                                                                    </td>
                                                                    <td>
                                                                        <label class="input fullWidth">
                                                                            <input id="customfields_<?php echo $key; ?>"
                                                                                   type="text"
                                                                                   class="input-sm fullWidth"
                                                                                   name="data[customfields][<?php echo $key; ?>]"
                                                                                   value="<?php echo $value->namefield; ?>" >
                                                                        </label>
                                                                    </td>
                                                                    <td>

                                                                        <a class="btn btn-info btn-xs addTr"
                                                                           role="button" data-toggle="tooltip"
                                                                           data-type-btn="addTr"
                                                                           data-placement="top"
                                                                           title="Agregar subpreguntas">
                                                                            <i class="fa fa-fw fa-plus"></i>
                                                                        </a>

                                                                        <a class="btn btn-danger btn-xs removeTr"
                                                                           role="button" data-toggle="tooltip"
                                                                           data-placement="top"
                                                                           data-type-btn="removeTr"
                                                                           title="Eliminar subpreguntas">
                                                                            <i class="fa fa-trash-o"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                }
                                                                }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden"
                                   value="{{$viewData['survey']->id}}"
                                   name="survey_id"
                                   id="survey_id"
                                   class="hideElement" />

                            <input type="submit"
                                   value="send"
                                   class="hideElement" />
                        </form>
                    </article>
                </div>
            </div>
        </section>
    </div>

    @push('scripts')
        <script src="<?php echo url('/') ?>/js/modules/AddEditParticipant.js"></script>
    @endpush

@stop