<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property int $sortorder
 * @property int $created_by
 * @property int $parent_id
 * @property string $created
 * @property string $modified
 */
class Group extends Model
{
    /**
     * @var array
     */
    Protected $primaryKey = "id";

    protected $fillable = ['survey_id','name', 'title', 'description', 'sortorder', 'created_by', 'parent_id', 'created_at', 'updated_at'];

    public function survey()
    {
        return $this->belongsTo('Survey','survey_id');
    }

    public function questions()
    {
        return $this->hasMany('App\Question','gid','id');
    }

}
