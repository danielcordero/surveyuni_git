<?php
use \App\CustomLib\Helper\HelperServices;

//echo "<pre>";
//print_r( $details );
//exit;

?>
<div id="tab-<?php echo $count; ?>" class="tab-pane fade in <?php echo empty($count) ? "active" : ""; ?>">
    <fieldset>
        <div class="form-group">
            <label class="col-md-3 control-label">
                <strong>
                    <?php
                        echo $details['subject'];
                    ?>

                </strong>
            </label>
            <div class="col-md-9">
                <input class="form-control"
                       name="<?php echo $details['field']['subject']; ?>"
                       id="<?php echo $details['field']['subject']; ?>"
                       placeholder="" value="<?php echo $viewData['surveysetting']->{$details['field']['subject']}; ?>" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">
                <strong>
                    <?php
                        echo $details['body'];
                    ?>
                    <!--Cuerpo de correo electrónico de invitación:-->
                </strong>

            </label>
            <div class="col-md-9">
                <label class="textarea">
                    <textarea
                            name="<?php echo $details['field']['body']; ?>"
                            id="<?php echo $details['field']['body']; ?>"
                            class="smCkEditor"><?php echo $viewData['surveysetting']->{$details['field']['body']}; ?></textarea>
                </label>
            </div>

        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">
                <strong>
                    Acciones:
                </strong>
            </label>
            <div class="col-md-9">
                <!--<a href=" {{route("email.validatetemplate",[
                    "surveyId" => $viewData['survey']->id,
                    "sa"=>"email",
                    "type"=>$tab])}}"
                   target="dialog"
                   title="<?php echo $details['title'] ?>" class="btn btn-default">
                    Validar expresiones
                </a>-->
                    <?php
//                    $details['default']['body']=( $tab == 'admin_detailed_notification') ? $details['default']['body'] : HelperServices::conditionalNewlineToBreak($details['default']['body'],true) ;
                    ?>

                    <input type="button"
                           class="btn btn-default btn btnResetTemplate"
                           value="Reiniciar está plantilla"
                           data-value='{{ $details["default"]["body"]  }}'
                           data-target="<?php echo $details['field']['body']; ?>">
            </div>
        </div>
    </fieldset>
</div>