<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;

class MultipleChoiceComments extends BaseQuestion{

    public function saveConfig( $data ){
        $jsonToSave = new \stdClass();
        $subQuestions = $data->subQuestions;
        foreach ($subQuestions as $key => $value){
            $subQuestions[$key] = $this->cleanData( $value );
        }

        $jsonToSave->anotherOption = !empty($data->anotherOption) ? "Y" : "N";
        $jsonToSave->subQuestions = $subQuestions;
        $question = Question::find( $data->question_id );
        $question->additionalData = json_encode( $jsonToSave );
        $question->save();
    }

    public function getConfig( $dataJson ){
        return json_decode( $dataJson, true );
    }

    public function validateAnswers( $partakerRequest ){

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message'] = "Esta pregunta es requerida";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest )
    {

        $answers = !empty($partakerRequest['answers']) ? $partakerRequest['answers'] : [];
        $subQuestions = $partakerRequest['subQuestions'];
        $subQuestions = explode("|",$subQuestions);
        $messages = [];

        $fail = false;
        $keepAnswered = [];
        $withoutAnswered = [];

        foreach ($subQuestions as $key => $value){

            if(in_array($value."_comment", array_keys($answers))){
                $answers[$value."_comment"] = $this->cleanData($answers[$value."_comment"]);

                if(!empty($answers[$value."_comment"])){
                    $keepAnswered[$value."_comment"] = trim($answers[$value."_comment"]);
                } else {
                    $withoutAnswered[] = $value."_comment";
//                    $messages[] = "Por favor ingrese números enteros";
                }
            } else {
                $withoutAnswered[] = $value."_comment";
            }
        }

        if(!empty($withoutAnswered) &&
            !empty($partakerRequest['mandatory']) &&
            $partakerRequest['mandatory'] == "Y" &&
            count($keepAnswered) < 1){
            $fail = true;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

        return $response;

//        $answers = !empty($partakerRequest['answers']) ? $partakerRequest['answers'] : [];
//        $subQuestions = $partakerRequest['subQuestions'];
//        $subQuestions = explode("|",$subQuestions);

        echo "<pre>";
        print_r($partakerRequest);
//        echo "<pre>";
//        print_r($subQuestions);
        exit;


        $fail = false;
        $keepAnswered = [];

        if(empty($partakerRequest)){
            $fail = true;
        } else {
            foreach ($partakerRequest as $key => $value){
                $value = $this->cleanData($value);
                if(!empty($value) && !empty($partakerRequest[$key.'_comment'])){
                    $keepAnswered[$key] = $value;
                }
            }
        }

        if(empty($keepAnswered)){
            $fail = true;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;

        echo "<pre>";
        print_r( $response );
        exit;

        return $response;
    }

}
?>