<?php
    use \App\CustomLib\TypeQuestions\BaseQuestion;
    use \App\CustomLib\Services\SurveyPublic;
    $surveyPublic = new SurveyPublic;
    $statusProgressBar = $surveyPublic->getProgressBarStatus($viewData);
?>

<?php $__env->startSection('title',$viewData['config']['survey']->title); ?>
<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('survey.public.questions.littlepartial.requiredQuestionModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <form id="frmSurvey" autocomplete="off"
          action="<?php echo e(route('public.index',['surveyId'=> $viewData['config']['survey']->id ])); ?>"
          method="post">
        <?php echo e(csrf_field()); ?>

        <?php if($surveyPublic->isModePreview()): ?>
            <div class="row">
                <br>
                <div class="col-md-12 text-center titleSurvey">
                    <div class="alert alert-warning alert-dismissible fade in alert-dismissible"
                         role="alert">
                        <h3>
                            Está encuesta actualmente no está activa. Usted no será capaz de guardar sus respuestas.
                        </h3>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if($surveyPublic->isVisiblePogressBar($viewData['config']['survey'])): ?>
            <div class="row">
                <br>
                <div class="col-md-12">
                    <div class="progress">
                        <div class="progress-bar bg-color-blue"
                             aria-valuetransitiongoal="<?php echo $statusProgressBar; ?>"
                             style="width: <?php echo $statusProgressBar; ?>%;" aria-valuenow="<?php echo $statusProgressBar; ?>"><?php echo $statusProgressBar; ?>%</div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-12 text-center">
                <h1>
                    <?php echo $viewData['groupInfo']->name; ?>

                </h1>
            </div>
        </div>
        <?php
            $questionsType = config('custom.questionsTypeTemplate');
            $allQuestionsId = "";
        ?>
        <?php
            //$resultQuestion['fail']
            if(!empty($viewData['isThereAWrong'])){
        ?>
            <?php $__env->startPush('scripts'); ?>
            <script>
                //console.log(" There you go ");
                $('#requiredQuestionModal').modal('show');
            </script>
            <?php $__env->stopPush(); ?>
        <?php
            }
            //$isThereError = false;
        ?>
        <?php $__currentLoopData = $viewData['groupInfo']->questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
                $questionType = $questionsType[$question->type];
                $baseQuestion = new BaseQuestion;
                $config = $baseQuestion->getConfigBase($questionType, $question->additionalData);
                $template = 'survey.public.questions.' . $questionType;
                $resultQuestion = [];
                if(!empty($viewData['responses'])){
                    if(!empty($viewData['responses'][$question->id])){
                        $resultQuestion = $viewData['responses'][$question->id];
                        //if(!empty($resultQuestion['fail']) && !$isThereError){
                            //$isThereError = true;
                            ?>
                            <?php $__env->startPush('scripts'); ?>
                                <script>
//                                    console.log(" There you go ");
                                    //$('#requiredQuestionModal').modal('show');
                                </script>
                            <?php $__env->stopPush(); ?>
                            <?php
                        //}
                    }
                }
            ?>
            <?php if(!empty($question->hide) && $question->hide == 'N'): ?>
                <?php
                    $allQuestionsId = $allQuestionsId . '|' . 'question_'.$question->id;
                ?>
                <?php echo $__env->make($template, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <input type="hidden"
               name="all_answers"
               id="all_answers"
               value="<?php echo trim($allQuestionsId, '|'); ?>">

        <!-- PRESENT THE NAVIGATOR -->
        <div class="row btnsNavBar">
            <div class="col-md-6 col-xs-6 text-left">
                <?php if($surveyPublic->isVisibleNavBack($viewData['config']['survey'])): ?>
                    <?php if(!empty($viewData['currentGroup'])): ?>
                        <button type="submit"
                                name="btnNavBack"
                                value="true"
                                class="btn btn-danger btn-lg">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                            Anterior
                        </button>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <div class="col-md-6 col-xs-6 text-right">
                <button type="submit"
                        name="btnNavNext"
                        value="true"
                        class="btn btn-danger btn-lg">
                    Siguiente <i class="glyphicon glyphicon-chevron-right"></i>
                </button>
            </div>
        </div>

        <input name="current_group"
               type="hidden"
               value="<?php echo $viewData['currentGroup']; ?>">

        <input name="survey_id"
               type="hidden"
               value="<?php echo $viewData['config']['survey']->id; ?>">

        <?php if(!empty($viewData['isRunning'])): ?>
            <input name="is_running"
                   type="hidden"
                   value="<?php echo $viewData['isRunning']; ?>">
        <?php endif; ?>
    </form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('survey.public.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>