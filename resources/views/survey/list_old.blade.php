@extends('layouts.app')
@section('content')

@push('scripts')
   <script src="<?php echo url('/') ?>/js/views/surveyModule.js"></script>
@endpush

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      <span class="glyphicon glyphicon-th-list"></span>
      <span>Lista de encuestas</span>
   </h1>
</section>

<!-- Main content -->
<section class="content">
   <!-- Default box -->
   <div class="box box-primary">
      <div class="box-header with-border">
         <h3 class="box-title inlineBlock">
            <strong>
               Lista de encuestas
            </strong>
         </h3>
         <a href="{{ route('survey.create') }}"  
               class="btn btn-primary pull-right">
                Crear encuesta
         </a>
      </div>
      <div class="box-body">
         <table id="" 
            class="display table table-hover_c fullWidth table-striped table-hover hoverTable hoverActionTable">
            <thead>
               <tr>
                  <th>Identificador de la encuesta</th>
                  <th>Estado</th>
                  <th>Titulo</th>
                  <th>Fecha Creado</th>
                  <th>Propetario/a</th>
                  <th>Acciones</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  foreach($viewData['surveys'] as $survey){  
                  ?>
               <tr role="row" 
                  class="odd cursorPointer">
                  <td class="sorting_1">
                     <strong>
                        <?= $survey->id; ?>   
                     </strong>
                  </td>
                  <td>
                     @if($survey->active =='N')
                        <a href="/index.php?r=admin/survey/sa/view/surveyid/485439" class="survey-state" data-toggle="tooltip" title="" data-original-title="Inactiva"><span class="fa fa-stop text-warning"></span>
                        </a>
                     @else
                        <a href="/index.php?r=admin/survey/sa/view/surveyid/636129" class="survey-state" data-toggle="tooltip" title="" data-original-title="Activa">
                           <span class="fa fa-play text-success"></span>
                        </a>
                     @endif
                  </td>
                  <td>
                     <strong>
                        <?= $survey->title; ?>   
                     </strong>
                     
                  </td>
                  <td>
                     <strong>
                        <?= $survey->created_at;  ?>   
                     </strong>
                  </td>
                  <td>
                     <strong>
                        122   
                     </strong>
                  </td>
                  <td>

                     <a href="{{ route('survey.show',[$survey->id])}}" 
                        class="btn btn-default_c  btn-xs" 
                        role="button" 
                        data-toggle="tooltip" 
                        data-placement= "top" 
                        title="Editar ">
                        <i class="fa fa-pencil ">
                        </i>
                     </a>
                  
                     <a href="{{ route('questions',[$survey->id])}}"
                        class="btn btn-default_c  btn-xs" 
                        role="button" 
                        data-toggle="tooltip" 
                        data-placement= "top" 
                        title="Agregar preguntas">
                        <i class="fa fa-fw fa-gear"></i>
                     </a>
                     
                     <a href="#" 
                        class="btn btn-danger btn-xs" 
                        role="button" 
                        data-toggle="tooltip" 
                        data-placement= "top" 
                        title="Eliminar">
                        <i class="fa fa-trash ">
                        </i>
                     </a>
                     
                  </td>
               </tr>
               <?php        
                  }
               ?>
            </tbody>
         </table>
         <?php
            echo str_replace('/?', '?', $viewData['surveys']->render() );
         ?>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
         <!-- Footer -->
      </div>
      <!-- /.box-footer-->
   </div>
   <!-- /.box -->
</section>
<!-- /.content -->
@stop