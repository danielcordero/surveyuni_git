<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('survey.elements.newEdit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <?php $__env->startPush('scripts'); ?>

    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/SurveyModule.js"></script>

    <?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>