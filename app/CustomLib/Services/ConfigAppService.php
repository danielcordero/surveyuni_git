<?php

namespace App\CustomLib\Services;

use App\SettingsGlobal;

class ConfigAppService
{
    public static function setGlobalSetting($settingname, $settingvalue)
    {
        if ($record = SettingsGlobal::find($settingname))
        {
            $settingvalue = !empty($settingvalue) ? $settingvalue : self::getDefaultValue($settingvalue);
            $record->stg_value = trim($settingvalue);
            $record->save();
        }
        else
        {
            $record = new SettingsGlobal;
            $record->stg_name = trim($settingname);
            $record->stg_value = ($settingvalue);
            $record->save();
        }
    }

    public static function getDefaultValue($nameProperty)
    {
        $defaultConfig = config('globalconfig');
        if(!empty($defaultConfig[$nameProperty])){
            return $defaultConfig[$nameProperty];
        }
        return "";
    }

    public static function getGlobalSetting($settingname)
    {
        $dbValue = SettingsGlobal::find($settingname);
        if(!empty($dbValue)){
            $dbValue = $dbValue->stg_value;

        } else {

            $dbValue = self::getDefaultValue($settingname);
            if(empty($dbValue))
            {
                $dbValue = SettingsGlobal::find($settingname);
                if(empty($dbValue)){
                    config("globalconfig.{$settingname}", null);
                    $dbValue = '';
                }
                else
                {
                    $dbValue = $dbValue->stg_value;
                }
                if(!empty(self::getDefaultValue($settingname))){
                    self::setGlobalSetting($settingname, self::getDefaultValue($settingname));
                    $dbValue =  self::getDefaultValue($settingname);
                }
            }
        }
        return $dbValue;
    }

    public static function getFullConfig()
    {
        return SettingsGlobal::all();
    }
}
?>