<?php
//        $questionsType = config('custom.questionsTypeTemplate');
//        echo "<pre>";
//        print_r( $questionsType[$viewData['question']->type] );
//        print_r( $viewData['question']->type );
//        exit;

?>
@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Configuración de la pregunta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
        @include('survey.questions.elements.topBarBreadcrumbs')
        <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-3">
                    @include('survey.questions.elements.treeGroupQuestions')
                </div>
                <div class="col-sm-9">

                    <form class="currentForm"
                          action="{{ route('storeConfigQuestion') }}"
                          method="post">
                            {{ csrf_field() }}

                            <article class="well">

                                @if(empty($viewData['config']))

                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <ul class="demo-btns">
                                                <li>
                                                    <a href="{{ route('editQuestion',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id,'questionId'=>$viewData['question']->id])}}"
                                                       class="btn btn-primary">
                                                        <i class="fa fa-pencil"></i>
                                                        Editar pregunta
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                @else
                                    @include('survey.questions.config.topActions')
                                @endif

                                <div class="inbox-info-bar">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h2>
                                                <strong>
                                                    Configuración de la pregunta
                                                </strong>
                                            </h2>
                                        </div>
                                    </div>

                                </div>
                                <br>
                                <?php
                                    $questionsType = config('custom.questionsTypeTemplate');
                                    $template = 'survey.questions.config.' . $questionsType[$viewData['question']->type];
                                ?>
                                @include($template)
                            </article>

                        <input type="hidden"
                               id="question_type"
                               name="question_type"
                               value="<?php echo $questionsType[$viewData['question']->type] ?>">

                        <input type="hidden"
                               id="survey_id"
                               name="survey_id"
                               value="<?php echo $viewData['survey']->id; ?>">

                        <input type="hidden"
                               id="group_id"
                               name="group_id"
                               value="<?php echo $viewData['group']->id; ?>">

                        <input type="hidden"
                               id="question_id"
                               name="question_id"
                               value="<?php echo $viewData['question']->id; ?>">

                        <input type="submit"
                               value="send"
                               class="hideElement" />
                    </form>
                </div>
            </div>
        </section>
    </div>


    @push('scripts')
        <script src="<?php echo url('/') ?>/js/modules/config/configQuestion.js"></script>
    @endpush
@stop