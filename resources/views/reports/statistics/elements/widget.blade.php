<article class="col-md-4">
    <div class="jarviswidget jarviswidget-initialHeight"
         id="barChartwidget<?php echo $widgetData->elementId; ?>"
         data-widget-colorbutton="false"
         data-widget-fullscreenbutton="false"
         data-widget-editbutton="false"
         data-widget-sortable="false"
         data-widget-deletebutton="false">
        <header style="padding-left: 8px;" class="headerWidget">
            <h5>
                <strong>
                    <?php
                        echo strip_tags($widgetData->titleElement);
                    ?>
                </strong>
                @if(!empty($widgetData->subTitleElement))
                    [<strong>
                        <?php
                        echo $widgetData->subTitleElement;
                        ?>
                    </strong>]
                @endif
            </h5>
        </header>
        <div>
            <div class="jarviswidget-editbox">
                <input class="form-control" type="text">
            </div>
            <div class="widget-body">
                @if(!empty($questionsWithoutChart) && in_array($question->type,$questionsWithoutChart))
                    <div class="alert alert-warning"
                         style="margin-bottom: 363px;">
                        No hay un gráfico simple para este tipo de pregunta
                    </div>
                @else
                    <div id="barChart<?php echo $widgetData->elementId; ?>"></div>
                @endif
            </div>
        </div>
    </div>
</article>


<!--
        code of reference
        <article class="col-md-4">
        <div class="jarviswidget"
             id="barChartwidget<?php //echo $question->id.'_'.$key; ?>"
             data-widget-colorbutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-editbutton="false"
             data-widget-sortable="false"
             data-widget-deletebutton="false">
            <header>
                <h2>
                    <strong>
                        <?php
//echo strip_tags($question->title);
?>
</strong>
</h2>
<h2>
[<strong>
                        <?php
//echo strip_tags($value);
?>
</strong>]
</h2>
</header>
<div>
<div class="jarviswidget-editbox">
<input class="form-control" type="text">
</div>
<div class="widget-body">

<div id="barChart<?php //echo $question->id.'_'.$key; ?>"></div>
                </div>
            </div>
        </div>
    </article>
    -->