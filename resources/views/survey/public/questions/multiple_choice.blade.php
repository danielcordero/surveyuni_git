<?php

//echo "<pre>";
//print_r("I'm here");
//exit;

$allQuestions = "";
//    echo "<pre>";
////    if(!empty($resultQuestion)){
////
////    }
//    print_r( $resultQuestion['keepAnswered']['anotherInput'] );
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- multiple_choice -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body smart-form"
                     id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                        </div>
                    @endif

                    <fieldset>

                        <section>
                            <?php if($question->has_condition == "Y") {
                            ?>
                            @include("survey.public.questions.conditions.yes_no")
                            <?php
                            }?>
                        </section>

                        <section id="bodyQuestion-<?php echo $question->id; ?>" class="<?php echo $question->has_condition == "Y" ? ' hideElement ' : ''; ?>">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table adjustTableContent">
                                            <tbody>
                                            <?php
                                            foreach ($config["subQuestions"] as $key => $value){
                                            $allQuestions = $allQuestions . '|' .$key;
                                            ?>
                                            <tr>
                                                <td><label class="checkbox inlineBlock">
                                                        <input type="checkbox"
                                                               id="question_<?php echo $question->id; ?>[answers'[<?php echo $key; ?>]"
                                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                               value="<?php echo $key; ?>"
                                                               <?php
                                                                   if(!empty($resultQuestion['keepAnswered']) &&
                                                                       !empty($resultQuestion['keepAnswered'][$key])){
                                                                        echo "checked";
                                                                   }
                                                                   ?>>
                                                        <i></i>
                                                        <span class="label <?php
                                                        if(!empty($resultQuestion['withoutAnswered']) &&
                                                            in_array($key, $resultQuestion['withoutAnswered']) &&
                                                            $question->mandatory == "Y" &&
                                                            !empty($resultQuestion['fail'])){
                                                            echo "labelCommonError";
                                                        }
                                                        ?>"><?php echo $value; ?>
                                                        </span>
                                                        <!--<strong>
                                                            <?php //echo $value; ?>
                                                        </strong>-->
                                                    </label>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                            @if(!empty($config["anotherOption"]) && $config["anotherOption"] == "Y")
                                                <tr>
                                                    <td >
                                                        <label class="checkbox inlineBlock">
                                                            <input type="checkbox"
                                                                   id="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                                   name="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                                   <?php if(!empty($resultQuestion['keepAnswered']) && !empty($resultQuestion['keepAnswered']['anotherInput'])){ ?> <?php echo "checked"; ?> <?php } ?>>
                                                            <i></i><strong>
                                                                Otro:
                                                            </strong></label>
                                                        <label class="input inlineBlock">
                                                            <input type="text"
                                                                   id="question_<?php echo $question->id; ?>[answers][anotherInput]"
                                                                   name="question_<?php echo $question->id; ?>[answers][anotherInput]"
                                                                   class="input-sm"
                                                                   value="<?php if(!empty($resultQuestion['keepAnswered']) && !empty($resultQuestion['keepAnswered']['anotherInput'])){ ?> <?php echo $resultQuestion['keepAnswered']['anotherInput']; ?> <?php } ?>">
                                                        </label>
                                                    </td>

                                                    <td>

                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                </div>
                @include('survey.public.questions.littlepartial.help')
                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[answered]"
                       id="question_<?php echo $question->id; ?>[answered]"
                       value="0">

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[type]"
                       id="question_<?php echo $question->id; ?>[<?php echo 0; ?>]"
                       value="<?php echo $questionType ; ?>">

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[questionId]"
                       id="question_<?php echo $question->id; ?>[questionId]"
                       value="<?php echo $question->id ; ?>">

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[subQuestions]"
                       id="question_<?php echo $question->id; ?>[subQuestions]"
                       value="<?php echo trim($allQuestions, '|'); ?>">

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[mandatory]"
                       id="question_<?php echo $question->id; ?>[mandatory]"
                       value="<?php echo $question->mandatory ; ?>">

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[groupId]"
                       id="question_<?php echo $question->id; ?>[groupId]"
                       value="<?php echo $question->group->id; ?>">
                <!-- end widget content -->
            </div>
            <!-- end widget div -->
        </div>
    </article>
</div>

@push('scripts')
<script>
    var hasCondition = {!! "'" . $question->has_condition . "'"!!}
    var questionId = {!! "'" . $question->id . "'"!!};
    if(hasCondition == 'Y'){
        surveyRuntime.handleYesNoCondition(questionId);
    }
    surveyRuntime.loadConfig($("#<?php echo $question->id.'_question :input';  ?>"));
</script>
@endpush