<ul id="myTab1" class="nav nav-tabs bordered">
    <li class="active">
        <a href="#s1" data-toggle="tab" aria-expanded="true">
            <i class="fa fa-fw fa-lg fa-gear"></i>
            Básica
        </a>
    </li>
    <li class="">
        <a href="#s2" data-toggle="tab" aria-expanded="false">
            <i class="fa fa-fw fa-lg fa-gear"></i>
            Avanzada
        </a>
    </li>
</ul>