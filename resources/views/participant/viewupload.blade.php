<?php
//    echo "<pre>";
//    print_r( $viewData['survey']->customFields );
//    exit;
?>

@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Gestionar participantes de la encuesta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        @include('participant.elements.topBottons')

                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            Importar participantes en la encuesta desde archivo CSV
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <div class="widget-body no-padding">
                                    <form class="smart-form" method="post" enctype="multipart/form-data"
                                          action="{{ route('participants.upload',['surveyId'=>$viewData['survey']->id] )}}">
                                        {{ csrf_field() }}
                                        <fieldset>
                                            <section>
                                                <label class="label">
                                                    <strong>
                                                        Escoja el archivo CSV para subir:
                                                    </strong>
                                                </label>
                                                <div class="input input-file">
                                                    <input required="required"
                                                           accept=".csv"
                                                           type="file" value=""
                                                           name="csv_file"
                                                           id="file">
                                                </div>
                                            </section>

                                        </fieldset>
                                        <footer>
                                            <button type="submit"
                                                    class="btn btn-primary pull-left">
                                                <i class="fa fa-upload"></i>
                                                Subir
                                            </button>

                                            <a href="{{ route('participants.viewaddcustomfields',['surveyId'=>$viewData['survey']->id]) }}"
                                               class="btn btn-default txt-color-white bg-color-greenDark pull-left">
                                                <i class="fa fa-gear"></i>
                                                Administar campos personalizados
                                            </a>


                                        </footer>

                                        <input type="hidden"
                                               name="survey_id"
                                               id="survey_id"
                                               value="{{$viewData['survey']->id}}">
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <?php
                                    if(!empty($viewData['survey']->customFields)){
                                ?>
                                    <div class="alert alert-success" role="alert">
                                        <strong>
                                            Está encuesta tiene configurado los siguientes campos adicionales:
                                        </strong><br>

                                        <div>
                                            <ul>
                                                <?php
                                                    $customFields = $viewData['survey']->customFields;
                                                    foreach ($customFields as $key => $value){
                                                ?>
                                                    <li>
                                                        <?php
                                                            echo $value->namefield;
                                                            if(!empty($value->isRequired)){
                                                        ?>
                                                            <strong>
                                                                Requerido
                                                            </strong>
                                                        <?php
                                                            }
                                                        ?>
                                                    </li>
                                                <?php
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php
                                    }
                                ?>

                                <div class="alert alert-info" role="alert">
                                    <strong>Formato de entrada CSV</strong><br>
                                    <p>
                                        El archivo debe ser CSV estándar (delimitado por puntos y comas) y con comillas dobles para enmarcar los valores (configuración por defecto de los ficheros CSV en OpenOffice.org y MS Excel). La primera línea debe contener los nombres de los campos. Los campos no tienen por qué estar en un orden específico.
                                    </p>
                                    <span style="font-weight:bold;">Campos obligatorios:</span>
                                        firstname, lastname, email

                                    <br>
                                </div>


                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
@stop