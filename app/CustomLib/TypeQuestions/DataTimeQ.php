<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;

class DataTimeQ extends BaseQuestion{

    public function saveConfig( $data ){

    }

    public function getConfig( $dataJson ){

    }

    public function validateAnswers( $partakerRequest ){

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message']= "Esta pregunta es requerida";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest ){

//        echo "<pre>";
//        print_r( $partakerRequest );
//        ///exit;

        $currentFormatDate = "/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/";
        $messages = $keepAnswered = $withoutAnswered = [];
        $fail = false;
        $answers =  $partakerRequest['answers'];
        $date = $answers['date'];
        $date = $this->cleanData($date);
        if((empty($date) &&
                !empty($partakerRequest['mandatory']) &&
                $partakerRequest['mandatory'] == "Y") ||
            !preg_match($currentFormatDate, $date)){
            echo $date;

            $messages[] = "Por favor ingrese una fecha valida";
            $fail = true;
        } else {
            $keepAnswered['date'] = $date;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

        return $response;
    }
}
?>