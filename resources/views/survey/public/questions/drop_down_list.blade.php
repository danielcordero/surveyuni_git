<div class="row">
    <!-- NEW WIDGET START -->
    <!-- drop_down_list -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>
                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <br>
                    <section>
                        <label class="select">
                            <select class="input-sm"
                                    name="question_<?php echo $question->id; ?>[answers]">
                                <option value="">Elija una opción</option>
                                <?php
                                    if(!empty($config["subQuestions"])){
                                foreach ($config["subQuestions"] as $key => $value){
                                ?>
                                    <option value="{{$key}}" <?php
                                        if(!empty($resultQuestion['keepAnswered'])){
                                            if($resultQuestion['keepAnswered'] == $key){
                                                echo "selected";
                                            }
                                        }
                                        ?>>
                                        <?php
                                            echo $value;
                                        ?>
                                    </option>
                                <?php
                                    }
                                    }
                                ?>
                            </select>
                            <i></i>
                        </label>
                    </section>


                </div>

                @include('survey.public.questions.littlepartial.help')

                @include('survey.public.questions.commonHidden')

                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>