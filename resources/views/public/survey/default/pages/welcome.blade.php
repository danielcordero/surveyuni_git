@extends('public.survey.default.main')
@section('content')
	@include('public.survey.default.partial.header')
	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
					<?= $viewData['survey']->title; ?>	
				</div>
				<div class="col-md-12">
					<?= $viewData['survey']->description; ?>
				</div>
			</div>
		</div>
	</div>
	@include('public.survey.default.partial.footer')
@stop