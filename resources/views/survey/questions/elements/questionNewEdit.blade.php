@if(!empty($viewData['question']) && !empty($viewData['question']->id))
    <form action="{{ route('updateQuestion',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id,'questionId'=>$viewData['question']->id] )}}"
          role="form" method="post" class="currentForm">
        {{ method_field('PATCH') }}
 @else
     <form class="currentForm"
           action="{{ route('storeQuestion') }}"
           method="post">
  @endif
    {{ csrf_field() }}
    <div class="col-sm-9">
        <article class="well">
            <div class="row">
                <div class="col-sm-8">
                    <div class="jarviswidget"
                         id="wid-id-1" data-widget-editbutton="false"
                         data-widget-custombutton="false"
                         data-widget-deletebutton="false"
                         data-widget-sortable="false"
                         data-widget-collapsed="false"
                         data-widget-fullscreenbutton="false">

                        <header role="heading">
                            <h2>
                                <strong>
                                    Agregar pregunta
                                </strong>
                            </h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body smart-form">
                                <input name="survey_id" type="hidden"
                                       value="<?php echo $viewData['survey']->id; ?>">

                                <fieldset>
                                    <section>
                                        <label class="label">
                                            <h3>
                                                <strong>
                                                    Pregunta
                                                </strong>
                                            </h3>
                                        </label>
                                        <label class="textarea">
                                             <textarea rows="3"
                                                       id="txtQuestion"
                                                       name="txtQuestion"
                                                       class="custom-scroll smCkEditor"><?php echo !empty($viewData['question']) ? $viewData['question']->title : ""; ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">
                                            <h3>
                                                <strong>
                                                    Ayuda
                                                </strong>
                                            </h3>
                                        </label>
                                        <label class="textarea">
                                            <textarea rows="3"
                                                      id="txtHelpQuestion"
                                                      name="txtHelpQuestion"
                                                      class="custom-scroll smCkEditor"><?php echo !empty($viewData['question']) ? $viewData['question']->help : ""; ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">
                                            <h3>
                                                <strong>
                                                    Pregunta para si o no
                                                </strong>
                                            </h3>
                                        </label>
                                        <label class="textarea">
                                            <textarea rows="3"
                                                      id="txtQuestionYesNo"
                                                      name="txtQuestionYesNo"
                                                      class="custom-scroll smCkEditor"><?php echo !empty($viewData['question']) ? $viewData['question']->question_yes_no : ""; ?></textarea>
                                        </label>
                                    </section>

                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="jarviswidget"
                         id="wid-id-1" data-widget-editbutton="false"
                         data-widget-custombutton="false"
                         data-widget-deletebutton="false"
                         data-widget-sortable="false"
                         data-widget-collapsed="false"
                         data-widget-fullscreenbutton="false">

                        <header role="heading">
                            <h2>
                                <strong>
                                    Opciones generales
                                </strong>
                            </h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>

                        <div role="content">
                            <div class="jarviswidget-editbox">
                            </div>
                            <div class="widget-body smart-form">
                                <fieldset>
                                    <div class="row">
                                        <section>
                                            <label class="label">
                                                <strong>
                                                    Tipo de pregunta
                                                </strong>
                                            </label>
                                            <label class="select">
                                                <div>
                                                    <input type="hidden"
                                                           id="questionType"
                                                           name="questionType"
                                                           class=""
                                                           value="<?php
                                                                   echo !empty($viewData['question']->type) ? $viewData['question']->type : "OM";
                                                           ?>"
                                                           style="margin-bottom: 12px;">
                                                    <div id="question_type_button"
                                                         class="btn-group fullWidth">
                                                        <button type="button" data-toggle="dropdown" id="btnSelectQuestion"
                                                                aria-haspopup="true" aria-expanded="false"
                                                                class="btn btn-success dropdown-toggle fullWidth"
                                                                style="height: 29px;border-radius: 0;">
                                                                            <span class="buttontext">
                                                                                <?php
                                                                                    if(!empty($viewData['question']->type)){
                                                                                        $questionsType = config('custom.questionsType');
                                                                                        echo $questionsType[$viewData['question']->type];
                                                                                    } else {
                                                                                        echo "Opción múltiple";
                                                                                    }

                                                                                ?>
                                                                            </span>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <span class="caret"></span></button>
                                                        <ul class="dropdown-menu" style="z-index: 1000;" id="selectQuestion">

                                                            <small>Preguntas de opción múltiple</small>
                                                            <li><a href="#" data-value="OM"
                                                                   class="questionType">Opción múltiple</a>
                                                            </li>
                                                            <li><a href="#" data-value="OMC"
                                                                   class="questionType">Opción múltiple con comentarios</a></li>
                                                            <li role="separator" class="divider"></li>

                                                            <small>Matrices</small>
                                                            <li><a href="#" data-value="A"
                                                                   class="questionType">Matriz</a></li>
                                                            <li><a href="#" data-value="A10"
                                                                   class="questionType">Matriz (Elegir del 1 al 10)</a></li>
                                                            <li><a href="#" data-value="A5"
                                                                   class="questionType">Matriz (Elegir del 1 al 5)</a></li>
                                                            <li><a href="#" data-value="AN"
                                                                class="questionType">Matriz (Números)</a>
                                                            </li>
                                                            <li><a href="#" data-value="AYN"
                                                                   class="questionType">Matriz (Sí/No/No sé)</a></li>
                                                            <li><a href="#" data-value="AISD"
                                                                   class="questionType">Matriz (Aumentar/Mismo/Disminuir)</a></li>
                                                            <li><a href="#" data-value="AT"
                                                                class="questionType">Matriz (Textos)</a>
                                                            </li>
                                                            {{--<li><a href="#" data-value="1"--}}
                                                            {{--class="questionType">Matriz de doble--}}
                                                            {{--eje</a></li>--}}
                                                            <li><a href="#" data-value="AC"
                                                                   class="questionType">Matriz en
                                                                    columnas</a></li>
                                                            <li role="separator" class="divider"></li>

                                                            <small>Preguntas de máscara</small>
                                                            {{--<li><a href="#" data-value="*"--}}
                                                            {{--class="questionType">Ecuación</a></li>--}}
                                                            <li><a href="#" data-value="N"
                                                                   class="questionType">Entrada Numérica</a>
                                                            </li>
                                                            <li><a href="#" data-value="NM"
                                                                   class="questionType">Entrada Numérica
                                                                    Múltiple</a></li>
                                                            <li><a href="#" data-value="DT"
                                                                   class="questionType">Fecha/tiempo</a>
                                                            </li>
                                                            <li><a href="#" data-value="T"
                                                                   class="questionType">Género</a></li>
                                                            {{--<li><a href="#" data-value="ST"--}}
                                                                   {{--class="questionType">Mostrar texto</a>--}}
                                                            {{--</li>--}}
                                                            <li><a href="#" data-value="R"
                                                            class="questionType">Clasificaión</a></li>
                                                            {{--<li><a href="#" data-value="I"--}}
                                                            {{--class="questionType">Selección de--}}
                                                            {{--idioma</a></li>--}}
                                                            {{--<li><a href="#" data-value="|"--}}
                                                            {{--class="questionType">Subir ficheros</a>--}}
                                                            {{--</li>--}}
                                                            <li><a href="#" data-value="YN"
                                                                   class="questionType">Sí/No</a></li>
                                                            <li role="separator" class="divider"></li>
                                                            <small>Preguntas de respuesta única</small>
                                                            <li><a href="#" data-value="C5"
                                                                   class="questionType">Elegir del 1 al
                                                                    5</a></li>
                                                            <li><a href="#" data-value="DDL" class="questionType">
                                                                    Lista(Desplegable)</a>
                                                            </li>
                                                            <li><a href="#" data-value="LR"
                                                                   class="questionType">Lista (Radio)</a>
                                                            </li>
                                                            <li><a href="#" data-value="LRC"
                                                                   class="questionType">Lista (botones
                                                                    radiales) con comentarios</a></li>
                                                            <li role="separator" class="divider"></li>
                                                            <small>Preguntas de texto</small>
                                                            <li><a href="#" data-value="TFS"
                                                                   class="questionType">Texto libre (línea)</a></li>
                                                            <li><a href="#" data-value="TFB" active=""
                                                                   class="questionType">Texto libre (párrafo)</a></li>
                                                            <li><a href="#" data-value="TFP"
                                                                   class="questionType">Texto libre (varios párrafos)</a></li>
                                                            <li><a href="#" data-value="TS"
                                                                   class="questionType">Varios textos cortos</a></li>
                                                            <li role="separator" class="divider"></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </label>
                                        </section>


                                        @if(empty($viewData['question']->id))
                                            <section>
                                                <label class="label">
                                                    <strong>
                                                        Grupo de pregunta
                                                    </strong>
                                                </label>
                                                <label class="select">
                                                    <select name="groupsQuestion" id="groupsQuestion" required>
                                                        <option value=""
                                                                selected=""
                                                                disabled="">Seleccione un grupo de pregunta
                                                        </option>
                                                        <?php
                                                        if(!empty($viewData['survey']->groups)){
                                                        $groups = $viewData['survey']->groups;
                                                        foreach ($groups as $group){
                                                        ?>
                                                        <option
                                                                value="<?php echo $group->id; ?>"
                                                        <?php
                                                            if(!empty($viewData['question'])){
                                                                $groupParent = $viewData['question']->group->id;
                                                                if($groupParent == $group->id){
                                                                    echo "selected";
                                                                }
                                                            }
                                                            ?>>
                                                            {{ $group->name }}
                                                        </option>
                                                        <?php
                                                        }
                                                        }
                                                        ?>
                                                    </select>
                                                    <i></i>
                                                </label>
                                            </section>
                                        @endif

                                        <section>
                                            <label class="label">
                                                <strong>
                                                    Posición
                                                </strong>
                                            </label>
                                            <label id="slSectionQuestions"  class="select">

                                                <select name="positionQ" required id="positionQ">
                                                    <option value=""
                                                            selected=""
                                                            disabled="">Elegir una posición
                                                    </option>
                                                    <?php
                                                        if(!empty($viewData['group'])){
                                                    ?>
                                                    <?php
                                                        $questions = $viewData['group']->questions;
                                                        if(!empty($questions)){
                                                            foreach ($questions as $question){
                                                    ?>
                                                        <option value="<?php echo $question->id; ?>"
                                                            <?php
                                                                if($question->id == $viewData['question']->id){
                                                                    echo "selected";
                                                                }
                                                                ?> >
                                                                <?php
                                                                    if($question->id != $viewData['question']->id){
                                                                        echo "Intercambiar con";
                                                                    }
                                                                ?>
                                                            (<?php
                                                                echo $question->sortorder;
                                                            ?>)
                                                            <?php
                                                                echo strip_tags($question->title);
                                                            ?>
                                                        </option>
                                                    <?php
                                                            }
                                                        } else {
                                                    ?>
                                                        <option value="F">
                                                            Al inicio de la lista
                                                        </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select> <i></i> </label>
                                        </section>

                                    </div>


                                </fieldset>
                            </div>
                        </div>

                    </div>
                    <div class="jarviswidget"
                         id="wid-id-3"
                         data-widget-editbutton="false"
                         data-widget-custombutton="false"
                         data-widget-deletebutton="false"
                         data-widget-sortable="false"
                         data-widget-collapsed="false"
                         data-widget-fullscreenbutton="false">

                        <header role="heading">
                            <h2>
                                <strong>
                                    Reglas
                                </strong>
                            </h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body smart-form">
                                <fieldset>
                                    <div class="row">
                                        <section>
                                            <label class="label">
                                                <strong>
                                                    Ocultar
                                                </strong>
                                            </label>
                                            <span class="onoffswitch">
                                                <input type="checkbox"
                                                       name="hideQuestion"
                                                       class="onoffswitch-checkbox"
                                                       id="hideQuestion"
                                                       <?php
                                                               if(!empty($viewData['question']) &&
                                                                   $viewData['question']->hide == "Y"){
                                                                   echo "checked";
                                                               }
                                                           ?>
                                                >
                                                <label class="onoffswitch-label"
                                                       for="hideQuestion">
                                                    <span class="onoffswitch-inner"
                                                          data-swchon-text="ON"
                                                          data-swchoff-text="OFF"></span>
                                                    <span class="onoffswitch-switch"></span></label>
                                            </span>
                                        </section>

                                        <section>
                                            <label class="label">
                                                <strong>
                                                    Obligatorio
                                                </strong>
                                            </label>
                                            <span class="onoffswitch">
                                                <input type="checkbox"
                                                       name="mandatoryQuestion"
                                                       class="onoffswitch-checkbox"
                                                       id="mandatoryQuestion"
                                                    <?php
                                                        if(!empty($viewData['question']) &&
                                                            $viewData['question']->mandatory == "Y"){
                                                            echo "checked";
                                                        }
                                                    ?>>
                                                <label class="onoffswitch-label"
                                                       for="mandatoryQuestion">
                                                    <span class="onoffswitch-inner"
                                                          data-swchon-text="ON"
                                                          data-swchoff-text="OFF"></span>
                                                    <span class="onoffswitch-switch"></span></label>
                                            </span>
                                        </section>

                                        <section>
                                            <label class="label">
                                                <strong>
                                                    Mostar ayuda
                                                </strong>
                                            </label>
                                            <span class="onoffswitch">
                                                <input type="checkbox"
                                                       name="showHelpQuestion"
                                                       class="onoffswitch-checkbox"
                                                       id="showHelpQuestion"
                                                <?php
                                                    if(!empty($viewData['question']) &&
                                                        $viewData['question']->show_help == "Y"){
                                                        echo "checked";
                                                    }
                                                    ?>>
                                                <label class="onoffswitch-label"
                                                       for="showHelpQuestion">
                                                    <span class="onoffswitch-inner"
                                                          data-swchon-text="ON"
                                                          data-swchoff-text="OFF"></span>
                                                    <span class="onoffswitch-switch"></span></label>
                                            </span>
                                        </section>

                                        <section>
                                            <label class="label">
                                                <strong>
                                                    Ancho completo
                                                </strong>
                                            </label>
                                            <span class="onoffswitch">
                                                <input type="checkbox"
                                                       name="fullWidth"
                                                       class="onoffswitch-checkbox"
                                                       id="fullWidth"
                                                       <?php
                                                           if(!empty($viewData['question']) &&
                                                               !empty($viewData['question']->full_width) &&
                                                               $viewData['question']->full_width == "Y"){
                                                               echo "checked";
                                                           } else if(!empty($viewData['question']) &&
                                                               !empty($viewData['question']->full_width) &&
                                                               $viewData['question']->full_width == "N"){
                                                               echo "";
                                                           } else {
                                                               echo "checked";
                                                           }
                                                           ?>
                                                >
                                                <label class="onoffswitch-label"
                                                       for="fullWidth">
                                                    <span class="onoffswitch-inner"
                                                          data-swchon-text="ON"
                                                          data-swchoff-text="OFF"></span>
                                                    <span class="onoffswitch-switch"></span></label>
                                            </span>
                                        </section>

                                        <section>
                                            <label class="label">
                                                <strong>
                                                    Mostrar condicion pregunta
                                                </strong>
                                            </label>
                                            <span class="onoffswitch">
                                                <input type="checkbox"
                                                       name="hasCondition"
                                                       class="onoffswitch-checkbox"
                                                       id="hasCondition"
                                                       <?php
                                                           if(!empty($viewData['question']) &&
                                                               !empty($viewData['question']->has_condition) &&
                                                               $viewData['question']->has_condition == "Y"){
                                                               echo "checked";
                                                           } else if(!empty($viewData['question']) &&
                                                               !empty($viewData['question']->has_condition) &&
                                                               $viewData['question']->has_condition == "N"){
                                                               echo "";
                                                           } else {
                                                               echo "";
                                                           }
                                                           ?>
                                                >
                                                <label class="onoffswitch-label"
                                                       for="hasCondition">
                                                    <span class="onoffswitch-inner"
                                                          data-swchon-text="ON"
                                                          data-swchoff-text="OFF"></span>
                                                    <span class="onoffswitch-switch"></span></label>
                                            </span>
                                        </section>

                                    </div>
                                </fieldset>
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->
                    </div>
                </div>
            </div>
        </article>
    </div>
    <input type="submit"
           value="send"
           class="hideElement" />

    <?php
        if(!empty($viewData['question'])){
     ?>
         <input type="hidden"
                name="question_id"
                id="question_id"
                value="<?php echo $viewData['question']->id; ?>">

         <input type="hidden"
                name="preview_group_id"
                value="<?php echo $viewData['question']->group->id; ?>">
     <?php
        }
     ?>
</form>