<?php


namespace App\CustomLib\Services;

@ob_start();
session_start();

use App\CustomLib\Helper\HelperServices;
use App\CustomLib\Services\ParticipantService;
use App\{Survey,Participant};

class SurveyPublic
{
    public function getInfo($surveyId)
    {
        return Survey::with([
            'groups' => function ($q) {
                $q->orderBy('sortorder', 'asc');
            },
            'groups.questions' => function ($q) {
                $q->orderBy('sortorder', 'asc');
                $q->where('hide', 'N');
            }
        ])->find($surveyId);
    }

    public function getConfig($surveyId)
    {
        $config = [];
        $surveyInfo = $this->getInfo($surveyId);
        $settings = $this->getSettings($surveyInfo->additionalData);
        if (!empty($settings)) {
            foreach ($settings as $key => $value) {
                $surveyInfo->{$key} = $value;
            }
        }
        $config['survey'] = $surveyInfo;
        $config['groupsIds'] = $this->getGroupIds($surveyInfo->groups);
        //$config['questionsIds'] = $this->getAllQuestionsIds( $surveyInfo->groups );
        $config['questionsIds'] = $this->getQuestionsAttr($surveyInfo->groups);
        $config['questionsTitle'] = $this->getQuestionsAttr($surveyInfo->groups, "title");
        $config['isPreviewMode'] = $this->isModePreview();

        return $config;
    }

    public function checkIfAnswerTofollowSurvey( $config , $request )
    {
        if($config['survey']->askifwantfollowsurvey &&
            $config['survey']->askifwantfollowsurvey == "Y"){
            if($request->askifwantfollowsurvey == "no"){
                return false;
            }
        }
        return true;
    }

    public function getSettings( $settings ){
        return json_decode($settings);
    }

    public  function getGroupIds( $groups ){
        return $groups->pluck("id")->toArray();
    }

    public  function getQuestionsIds( $questions ){
        return $questions->pluck("id")->toArray();
    }

    public function getQuestionsAttr( $groups , $attr = "id" )
    {
        $allQuestionsAttr = [];
        foreach ($groups as $group){
            $questionIds = $this->getQuestionAttr($group->questions, $attr);
            $allQuestionsAttr = array_merge($allQuestionsAttr, $questionIds);
        }
        return $allQuestionsAttr;
    }

    public function getQuestionAttr( $questions , $attr)
    {
        return $questions->pluck($attr)->toArray();
    }

    public function getAllQuestionsIds( $groups ){
        $allQuestionsIds = [];
        foreach ($groups as $group){
            $questionIds = $this->getQuestionsIds($group->questions);
            $allQuestionsIds = array_merge($allQuestionsIds, $questionIds);
        }
        return $allQuestionsIds;
    }

    public function isValidToken($queryParamenters){

        $isValid = true;
        $surveyId = $queryParamenters->surveyId;
        $token = $queryParamenters->token;

//        $token = HelperServices::sanitize_paranoid_string($token);
        $surveyId = HelperServices::sanitize_int($surveyId);

        $parameters = [
            'surveyId' => $surveyId,
            'token' => $token,
        ];

//        echo "<pre>";
//        print_r( $parameters );

        $query = "SELECT * FROM survey_participant WHERE survey_id = :surveyId AND token = :token";
        $result = \DB::select( \DB::raw($query), $parameters);

//        print_r( $result );
//
//        exit;

        if(empty($result)){
            return false;
        }
        $result = (array)$result[0];
        $this->saveSessionParticipant($result);
        return $isValid;
    }



    public function IsAuthenticatedParticipant()
    {
        $participant = $this->getSessionParticipant();
        return !empty($participant) ? true : false;
    }

    public function getParticipantByToken($queryParamenters)
    {
        $surveyId = $queryParamenters->surveyId;
        $token = $queryParamenters->token;

        $token = HelperServices::sanitize_paranoid_string($token);
        $surveyId = HelperServices::sanitize_int($surveyId);

        $parameters = [
            'surveyId' => $surveyId,
            'token' => $token,
        ];
        $query = "SELECT * FROM survey_participant WHERE survey_id = :surveyId AND token = :token";
        $result = \DB::select( \DB::raw($query), $parameters);

        if(empty($result)){
             return [];
        }

        $result = (array)$result[0];
        $this->saveSessionParticipant($result);
        return $result;
    }

    public function saveSessionParticipant( $participant )
    {
        $_SESSION['participant'] = [];
        $_SESSION['participant'] = $participant;
    }

    public function getSessionParticipant()
    {
        return !empty($_SESSION['participant']) ? $_SESSION['participant'] : [];
    }

    public function checkQSParameter($paramater)
    {
        if(!empty($paramater['mode'])){
            $mode = $paramater['mode'];
            $mode = HelperServices::sanitize_xss_string($mode);
            switch ($mode){
                case "preview":
                    $_SESSION['previewMode'] = true;
                    break;
            }
        }
    }

    public function isModePreview()
    {
        return !empty($_SESSION['previewMode']);
    }

    public function isVisiblePogressBar($survey){

        if((!empty($survey->showprogress) &&
            $survey->showprogress == "Y")){
            return true;
        }
        return false;
    }

    public function showQuestionaAskifwantfallowsurvey($survey){
        var_dump($survey);
        if((!empty($survey->askifwantfallowsurvey) &&
            $survey->askifwantfallowsurvey == "Y")){
            return true;
        }
        return false;
    }

    public function isVisibleNavBack($survey){
        if((!empty($survey->allowprev) &&
            $survey->allowprev == "Y")){
            return true;
        }
        return false;
    }

    public function isActive($survey)
    {
        if((!empty($survey['config']['survey']->active) &&
            $survey['config']['survey']->active == "Y")){
            return true;
        }
        return false;
    }

    public function isAccesible($survey){
        $isAccesible = false;
        $participant = $this->getSessionParticipant();

        if($this->isActive($survey)){
            $isAccesible = true;
        }

        if(!empty($participant)){
            $isAccesible = true;
        }

        if($this->isModePreview()){
            $isAccesible = true;
        }

        return $isAccesible;
    }

    public function isCompleted($request)
    {
        $completed = false;
        $participant = $this->getParticipantByToken($request);

        if(!empty($participant) && $participant['completed'] == 'Y'){
            $completed = true;
        }

        return $completed;
    }

    public function isFirstGroup($currentIndexGroup)
    {
        $isFirstGroup = false;
        $currentIndexGroup = $currentIndexGroup - 1;
        if($currentIndexGroup == 0){
            $isFirstGroup = true;
        }
        return $isFirstGroup;
    }

    public function isEndGroup($currentIndexGroup, $allGroups)
    {
        //Old logic
        //if(empty($config['groupsIds'][$currentGroup])){
        //return view('survey.public.end')
        //->with("viewData",$viewData);
        //}

        $isEndGroup = false;
        $countGroups = count($allGroups);
        if($currentIndexGroup == $countGroups - 1){
            $isEndGroup = true;
        }
        return $isEndGroup;
    }

    public function isNavBack($request)
    {
        $isNavBack = false;
        if($request->has('btnNavBack')){
            $isNavBack = true;
        }
        return $isNavBack;
    }

    public function getNextViewToshow($request)
    {
        $currentView = $request->has('currentView') ? $request->currentView : "";
        $newCurrentView = "";
        switch ($currentView){
            case "accesstokenview":
                $newCurrentView = "survey.public.start";
                break;
        }
        return $newCurrentView;
    }

    public function isTheFirstGroup($request)
    {
        $currentView = $request->has('currentView') ? $request->currentView : "";
        return !empty($currentView);
    }

    public function getProgressBarStatus($survey)
    {
        $currentGroup = !empty($survey['currentGroup']) ? $survey['currentGroup'] : 0;
        $statusProgressBar = $currentGroup / count($survey['config']['groupsIds']);
        return $statusProgressBar * 100;
    }

    public function setCompletedByParticipant()
    {
        $participant = $this->getSessionParticipant();
        if(!empty($participant)){
            ParticipantService::setSurveyCompleted($participant['id']);
            $_SESSION['participant'] = [];
        }
    }

    public function checkParameter($request, $_GS)
    {
        if(!empty($_GS)){
            if(array_key_exists('token',$_GS)){
                $token = !empty($_GS['token']) ? $_GS['token'] : "";
                $token = HelperServices::sanitize_xss_string($token);
                $request->token = $token;
                $this->getParticipantByToken($request);
            }

            if(array_key_exists('mode',$_GS)){
                $mode = $_GS['mode'];
                $mode = HelperServices::sanitize_xss_string($mode);
                switch ($mode){
                    case "preview":
                        $_SESSION['previewMode'] = true;
                        break;
                }
            }
        }
    }

    public function hasAccess($survey)
    {
        $hasAccess = false;
        $participant = $this->getSessionParticipant();
        if(!empty($participant) &&
            !empty($survey->active) &&
            $survey->active == "Y"){

            $hasAccess = true;
        }
        return $hasAccess;
    }
    // This method returns the survey status
    // If user has been completed the survey return true
    // otherwise ret
    public function hasBeenCompleted()
    {
        $completed = false;
        $participant = $this->getSessionParticipant();

        if(!empty($participant) && $participant['completed'] == 'Y'){
            $completed = true;
        }
        return $completed;
    }

    public function hasExpired($survey)
    {
        $hasExpired = true;
        $currentDate = strtotime(date("Y-m-d"));
        if(!empty($survey->startdate) && !empty($survey->expiresdate)){
            $startdate = strtotime(date($survey->startdate));
            $expiresdate = strtotime(date($survey->expiresdate));
            if($currentDate >= $startdate && $currentDate <= $expiresdate){
                $hasExpired = false;
            }
        } else {
            $hasExpired = false;
        }
        return $hasExpired;
    }
}