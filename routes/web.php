<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// echo bcrypt("test");
// exit;

/* Login */
// Route::get('/login','LoginController@login');
Route::get('/',function(){
	return redirect()->action('Auth\LoginController@login');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function(){
	

	/***MANAGE USERS***/
	Route::get('/admin', 'HomeController@index');

	Route::get('/createuser', 'HomeController@createuser')->name("createuser");
	Route::post('/storeUser', 'HomeController@storeUser')->name("storeUser");
	Route::get('/adminuser', 'HomeController@adminuser')->name("adminuser");

    Route::post('/user-delete/{userId?}','AdminController@destroy')
            ->name("user.delete");


    Route::get('/addedit/users/{userId?}',
        'AdminController@addeditusersview')
        ->name("admin.addeditusersview");

    Route::post('/addedit/users/createuser',
        'AdminController@createuser')
        ->name("admin.createuser");

    Route::match(['put', 'patch'], '/addedit/users/updateuser',
        'AdminController@updateuser')
        ->name("admin.updateuser");

	//user.delete

	/***MANAGE ROLES***/
	Route::post('/setroles', 
		'AdminController@setroles')->name("setroles");
	Route::get('/manageroles/{userId?}', 
		'AdminController@manageroles')->name("manageroles");

	/***AJAX GENERIC REQUEST***/
	Route::post('/storeUserAjax', 'AjaxController@storeUser')->name("storeUserAjax");    
});