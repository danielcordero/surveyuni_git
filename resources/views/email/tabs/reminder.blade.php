<div id="reminder" class="tab-pane fade">
    <fieldset>
        <div class="form-group">
            <label class="col-md-2 control-label">
                <strong>
                    Asunto del correo electrónico de recordatorio:
                </strong>
            </label>
            <div class="col-md-10">
                <input class="form-control"
                       name="email_reminder_subj"
                       id="email_reminder_subj"
                       placeholder="" type="text">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label">
                <strong>
                    Cuerpo de correo electrónico de recordatorio:
                </strong>

            </label>
            <div class="col-md-10">
                <label class="textarea">
                    <textarea
                            name="email_reminder"
                            id="email_reminder"
                            class="smCkEditor"></textarea>
                </label>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-2 control-label">
                <strong>
                    Acciones:
                </strong>
            </label>
            <div class="col-md-10">
                <button class="btn btn-default">Validar expresiones</button>
                <button class="btn btn-default">Resetear esta plantilla</button>
            </div>
        </div>
    </fieldset>
</div>