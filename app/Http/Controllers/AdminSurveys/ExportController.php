<?php
/**
 * Created by PhpStorm.
 * User: ccordero
 * Date: 3/23/2018
 * Time: 1:49 PM
 */

namespace App\Http\Controllers\AdminSurveys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\GenericMessages;
use App\CustomLib\Services\{SurveyPublic,SurveyService,SurveySettingsServices,ParticipantService};
use App\CustomLib\Helper\HelperServices;
use App\Survey;
use App\SurveySettings;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware('CheckExistResource');
    }

    public function exportviewdetail(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyId = HelperServices::sanitize_int( $surveyId );
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['survey']->amountSent = ParticipantService::getAmountSent( $surveyId );
        $viewData['survey']->amountCompleted = ParticipantService::getAmountCompleted( $surveyId );

        return view('export.viewdetail')
            ->with('viewData',$viewData);
    }

    public function exportData()
    {
    }
}