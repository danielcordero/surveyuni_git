<?php


use \App\CustomLib\Services\ConfigAppService;

$emailsmtpssl = ConfigAppService::getGlobalSetting('emailsmtpssl');
$emailsmtpdebug = ConfigAppService::getGlobalSetting('emailsmtpdebug');
//echo $emailsmtpssl;
//exit;
?>


<?php $__env->startSection('content'); ?>

    <section id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa-fw fa fa-puzzle-piece"></i>
                    <strong>
                        Configuración global de la aplicación
                    </strong>
                </h1>
            </div>
        </div>

        <section id="widget-grid" class="">
            <div class="well">
                <hr>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#backup">
                            Exportar base de datos
                        </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#emailsetting">
                            Configuración de correo
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="backup" class="tab-pane fade in active">

                        <div class="jarviswidget"
                             data-widget-editbutton="false" data-widget-custombutton="false"
                             data-widget-deletebutton="false" data-widget-sortable="false"
                             data-widget-collapsed="false" data-widget-fullscreenbutton="false" role="widget">

                            <header role="heading">
                                <h2>
                                    <strong>
                                        Exportar base de datos
                                    </strong>
                                    <i class="fa fa-database"></i>
                                </h2>
                            </header>

                            <!-- widget div-->
                            <div class="widget-body">

                                <form action="<?php echo e(route('admin.viewconfigglobal')); ?>"
                                      role="form" method="post" id="frmSurvey">
                                    <?php echo e(csrf_field()); ?>


                                    <div class="form-horizontal">
                                        <fieldset>

                                            <?php
                                                if(!empty($viewData['response']) && !empty($viewData['response']->failExport)){
                                            ?>
                                                <div class="alert alert-danger callout callout-danger text-center"
                                                     role="alert">
                                                    <button type="button"
                                                            class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <strong>
                                                        <?php echo e($viewData['response']->failMailMessage); ?>

                                                    </strong>
                                                </div>
                                            <?php
                                                }
                                            ?>

                                            <?php
                                                if(!empty($viewData['response']) && !empty($viewData['response']->failMail)){
                                            ?>
                                            <div class="alert alert-danger callout callout-danger text-center"
                                                 role="alert">
                                                <button type="button"
                                                        class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <strong>
                                                    No se pudo respaldar el backup en el correo del administrador, revise las credenciales del correo
                                                </strong>
                                            </div>
                                            <?php
                                                }
                                            ?>

                                            <!-- welcome screen -->
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="showwelcome" style="padding-top: 3px;">
                                                    <strong>
                                                        Guardar en el correo del admin:
                                                    </strong>
                                                    <!--Show welcome screen:-->
                                                </label>
                                                <div class="col-sm-7">
                                                <span class="onoffswitch">
                                                        <input type="checkbox" name="saveitonemail" class="onoffswitch-checkbox" id="ytshowwelcome" checked="">
                                                        <label class="onoffswitch-label" for="ytshowwelcome">
                                                    <span class="onoffswitch-inner" data-swchon-text="ON" data-swchoff-text="OFF"></span>
                                                    <span class="onoffswitch-switch"></span></label>
                                                </span>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <button type="submit" class="btn btn-default btn-lg btn-block bg-color-darken txt-color-white">
                                        Exportar base de datos
                                        <span class="glyphicon glyphicon-cloud-download"></span>
                                    </button>
                                </form>

                            </div>
                        </div>

                    </div>
                    <div id="emailsetting" class="tab-pane">

                        <form action="<?php echo e(route('admin.saveconfigglobal')); ?>"
                              role="form" method="post" id="frmSurvey">
                            <?php echo e(csrf_field()); ?>


                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-right">
                                        <button type="submit"
                                                class="btn btn-primary">
                                            <span class="glyphicon glyphicon-plus"></span> Guardar
                                        </button>
                                        <a class="btn btn-default"
                                           href="<?php echo e(route('survey.index')); ?>">
                                            Cancelar
                                        </a>
                                    </p>
                                </div>
                            </div>

                            <div id="emailsetting">
                                <div class="jarviswidget" data-widget-editbutton="false" data-widget-custombutton="false"
                                     data-widget-deletebutton="false" data-widget-sortable="false"
                                     data-widget-collapsed="false" data-widget-fullscreenbutton="false" role="widget">

                                    <header role="heading">
                                        <h2>
                                            <strong>
                                                Configuración del correo
                                            </strong>
                                            <span class="glyphicon glyphicon-send"></span>
                                        </h2>

                                    </header>

                                    <!-- widget div-->
                                    <div class="widget-body">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class=" control-label" for="siteadminemail">
                                                            <strong>
                                                                Correo electrónico predeterminado del administrador del
                                                                sitio:
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <input class="form-control"
                                                                   type="email"
                                                                   size="50"
                                                                   id="siteadminemail"
                                                                   name="siteadminemail"
                                                                   value="<?php echo e(ConfigAppService::getGlobalSetting('siteadminemail')); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="  control-label" for="siteadminname">
                                                            <strong>
                                                                Nombre del administrador:
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <input class="form-control"
                                                                   type="text" size="50"
                                                                   id="siteadminname"
                                                                   name="siteadminname"
                                                                   value="<?php echo e(ConfigAppService::getGlobalSetting('siteadminname')); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="  control-label" for="emailmethod">
                                                            <strong>
                                                                Método del correo
                                                            </strong>

                                                        </label>
                                                        <div class="">
                                                            <div class="btn-group" id="emailmethod"
                                                                 data-toggle="buttons">
                                                                <!--<label class="btn btn-default">
                                                                    <input name="emailmethod"
                                                                           id="emailmethod_opt1"
                                                                           value="mail" type="radio">
                                                                    PHP
                                                                </label>-->
                                                                <label class="btn btn-default active">
                                                                    <input name="emailmethod"
                                                                           id="emailmethod_opt2"
                                                                           value="smtp"
                                                                           checked="checked"
                                                                           type="radio">SMTP
                                                                </label>
                                                                <!--<label class="btn btn-default">
                                                                    <input name="emailmethod"
                                                                           id="emailmethod_opt3"
                                                                           value="sendmail"
                                                                           type="radio">Sendmail
                                                                </label>-->
                                                                <!--<label class="btn btn-default">
                                                                    <input name="emailmethod"
                                                                           id="emailmethod_opt4"
                                                                           value="qmail"
                                                                           type="radio">qmail
                                                                </label>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="  control-label" for="emailsmtphost">
                                                            <strong>
                                                                SMTP host:
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <input class="form-control" type="text" size="50"
                                                                   id="emailsmtphost"
                                                                   name="emailsmtphost" value="<?php echo e(ConfigAppService::getGlobalSetting('emailsmtphost')); ?>">
                                                            <span class="hint">
                                                                Introduzca su nombre de host y puerto,, e.g.: my.smtp.com:25
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="  control-label" for="emailsmtpuser">
                                                            <strong>
                                                                Nombre de usuario SMTP:
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <input class="form-control" type="text" size="50"
                                                                   id="emailsmtpuser"
                                                                   name="emailsmtpuser" value="<?php echo e(ConfigAppService::getGlobalSetting('emailsmtpuser')); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="  control-label" for="emailsmtppassword">
                                                            <strong>
                                                                Contraseña SMTP:
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <input class="form-control" type="password"
                                                                   autocomplete="off"
                                                                   size="50" id="emailsmtppassword"
                                                                   name="emailsmtppassword"
                                                                   value="somepassword">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="  control-label" for="emailsmtpssl">
                                                            <strong>
                                                                Encriptación SMTP
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <div class="btn-group" id="emailsmtpssl"
                                                                 data-toggle="buttons">
                                                                <label class="btn btn-default <?php if(empty($emailsmtpssl)){
                                                                    echo "active";
                                                                } ?>">
                                                                    <input
                                                                            name="emailsmtpssl"
                                                                            id="emailsmtpssl_opt1"
                                                                            value=""
                                                                            type="radio"
                                                                    <?php
                                                                        if(empty($emailsmtpssl)){
                                                                            echo "checked";
                                                                        }
                                                                        ?>
                                                                    >Apagado</label>
                                                                <label class="btn btn-default <?php if(!empty($emailsmtpssl) && $emailsmtpssl == 'ssl'){
                                                                    echo "active";
                                                                } ?> "><input
                                                                            name="emailsmtpssl"
                                                                            id="emailsmtpssl_opt2"
                                                                            value="ssl" type="radio"
                                                                    <?php
                                                                        if(!empty($emailsmtpssl) && $emailsmtpssl == 'ssl'){
                                                                            echo "checked";
                                                                        }
                                                                        ?>
                                                                    >SSL
                                                                </label>
                                                                <label class="btn btn-default <?php if(!empty($emailsmtpssl) && $emailsmtpssl == 'tls'){
                                                                    echo "active";
                                                                } ?>"><input
                                                                            name="emailsmtpssl"
                                                                            id="emailsmtpssl_opt3"
                                                                            value="tls" type="radio" <?php
                                                                        if(!empty($emailsmtpssl) && $emailsmtpssl == 'TLS'){
                                                                            echo "checked";
                                                                        }
                                                                        ?>>TLS</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="  control-label" for="emailsmtpdebug">
                                                            <strong>
                                                                Modo debug SMTP:
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <div class="btn-group" id="emailsmtpdebug"
                                                                 data-toggle="buttons">
                                                                <label class="btn btn-default <?php if(empty($emailsmtpdebug)){
                                                                    echo "active";
                                                                } ?>">
                                                                    <input
                                                                            name="emailsmtpdebug"
                                                                            id="emailsmtpdebug_opt1"
                                                                            type="radio"
                                                                            value="0"
                                                                    <?php
                                                                        if(empty($emailsmtpdebug)){
                                                                            echo "checked";
                                                                        }
                                                                        ?>
                                                                    >
                                                                    Apagado
                                                                </label>
                                                                <label class="btn btn-default <?php if(!empty($emailsmtpdebug) && $emailsmtpdebug == '1'){
                                                                    echo "active";
                                                                } ?>"><input
                                                                            name="emailsmtpdebug"
                                                                            id="emailsmtpdebug_opt2"
                                                                            value="1" type="radio"
                                                                    <?php
                                                                        if(!empty($emailsmtpdebug) && $emailsmtpdebug == '1'){
                                                                            echo "checked";
                                                                        }
                                                                        ?>
                                                                    >Errores encendido</label>
                                                                <label class="btn btn-default <?php if(!empty($emailsmtpdebug) && $emailsmtpdebug == '2'){
                                                                    echo "active";
                                                                } ?>"><input
                                                                            name="emailsmtpdebug"
                                                                            id="emailsmtpdebug_opt3"
                                                                            value="2" type="radio"
                                                                    <?php

                                                                        if(!empty($emailsmtpdebug) && $emailsmtpdebug == '2'){
                                                                            echo "checked";
                                                                        }
                                                                        ?>
                                                                    >Siempre</label>
                                                            </div>

                                                            <br>&nbsp;
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="  control-label" for="siteadminname">
                                                            <strong>
                                                                Tamaño del lote de correo electrónico:
                                                            </strong>
                                                        </label>
                                                        <div class="">
                                                            <input class="form-control"
                                                                   type="text" size="5"
                                                                   id="maxemails"
                                                                   name="maxemails" value="<?php echo e(ConfigAppService::getGlobalSetting('maxemails')); ?>">
                                                        </div>
                                                    </div>
                                                    <!--<div class="form-group">
                                                        <label class="  control-label" for="maxemails">Email batch size:</label>
                                                        <div class="">
                                                            <input class="form-control" type="text" size="5" id="maxemails"
                                                                   name="maxemails" value="1">
                                                        </div>
                                                    </div>
                                                    <p>Note: Demo mode is activated. Marked (*) settings can't be changed.</p>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="hide"></button>
                        </form>
                    </div>
                </div>

            </div>
        </section>
    </section>

    <?php $__env->startPush('scripts'); ?>
    <script src="<?php echo url('/') ?>/js/modules/ConfigAppModule.js"></script>
    <script>
        ConfigAppModule.init();
    </script>
    <?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>