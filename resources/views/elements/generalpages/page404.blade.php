{{-- \resources\views\errors\401.blade.php --}}
@extends('layouts.app')
@section('content')
    <div class="col-sm-12">
        <div class="text-center error-box">
            <h1 class="error-text-2 bounceInDown animated"> Error 404 <span class="particle particle--c"></span><span class="particle particle--a"></span><span class="particle particle--b"></span></h1>
            <h2 class="font-xl"><strong><i class="fa fa-fw fa-warning fa-lg text-warning"></i> Pagina <u>no</u> encontrada</strong></h2>
            <br>
            @if(!empty($viewData['message']))
                <h2 class="font-xl">
                    <strong class="text-danger">
                        <?php
                        echo $viewData['message'];
                        ?>
                    </strong>
                </h2>
            @endif
        </div>
    </div>
@endsection