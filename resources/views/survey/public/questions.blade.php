<?php
    use \App\CustomLib\TypeQuestions\BaseQuestion;
    use \App\CustomLib\Services\SurveyPublic;
    $surveyPublic = new SurveyPublic;
    $statusProgressBar = $surveyPublic->getProgressBarStatus($viewData);
?>
@extends('survey.public.layout')
@section('title',$viewData['config']['survey']->title)
@section('content')
    @include('survey.public.questions.littlepartial.requiredQuestionModal')
    <form id="frmSurvey" autocomplete="off"
          action="{{ route('public.index',['surveyId'=> $viewData['config']['survey']->id ]) }}"
          method="post">
        {{ csrf_field() }}
        @if($surveyPublic->isModePreview())
            <div class="row">
                <br>
                <div class="col-md-12 text-center titleSurvey">
                    <div class="alert alert-warning alert-dismissible fade in alert-dismissible"
                         role="alert">
                        <h3>
                            Está encuesta actualmente no está activa. Usted no será capaz de guardar sus respuestas.
                        </h3>
                    </div>
                </div>
            </div>
        @endif

        @if($surveyPublic->isVisiblePogressBar($viewData['config']['survey']))
            <div class="row">
                <br>
                <div class="col-md-12">
                    <div class="progress">
                        <div class="progress-bar bg-color-blue"
                             aria-valuetransitiongoal="<?php echo $statusProgressBar; ?>"
                             style="width: <?php echo $statusProgressBar; ?>%;" aria-valuenow="<?php echo $statusProgressBar; ?>"><?php echo $statusProgressBar; ?>%</div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-md-12 text-center">
                <h1>
                    {!! $viewData['groupInfo']->name !!}
                </h1>
            </div>
        </div>
        <?php
            $questionsType = config('custom.questionsTypeTemplate');
            $allQuestionsId = "";
        ?>
        <?php
            //$resultQuestion['fail']
            if(!empty($viewData['isThereAWrong'])){
        ?>
            @push('scripts')
            <script>
                //console.log(" There you go ");
                $('#requiredQuestionModal').modal('show');
            </script>
            @endpush
        <?php
            }
            //$isThereError = false;
        ?>
        @foreach($viewData['groupInfo']->questions as $question)
            <?php
                $questionType = $questionsType[$question->type];
                $baseQuestion = new BaseQuestion;
                $config = $baseQuestion->getConfigBase($questionType, $question->additionalData);
                $template = 'survey.public.questions.' . $questionType;
                $resultQuestion = [];
                if(!empty($viewData['responses'])){
                    if(!empty($viewData['responses'][$question->id])){
                        $resultQuestion = $viewData['responses'][$question->id];
                        //if(!empty($resultQuestion['fail']) && !$isThereError){
                            //$isThereError = true;
                            ?>
                            @push('scripts')
                                <script>
//                                    console.log(" There you go ");
                                    //$('#requiredQuestionModal').modal('show');
                                </script>
                            @endpush
                            <?php
                        //}
                    }
                }
            ?>
            @if(!empty($question->hide) && $question->hide == 'N')
                <?php
                    $allQuestionsId = $allQuestionsId . '|' . 'question_'.$question->id;
                ?>
                @include($template)
            @endif
        @endforeach

        <input type="hidden"
               name="all_answers"
               id="all_answers"
               value="<?php echo trim($allQuestionsId, '|'); ?>">

        <!-- PRESENT THE NAVIGATOR -->
        <div class="row btnsNavBar">
            <div class="col-md-6 col-xs-6 text-left">
                @if($surveyPublic->isVisibleNavBack($viewData['config']['survey']))
                    @if(!empty($viewData['currentGroup']))
                        <button type="submit"
                                name="btnNavBack"
                                value="true"
                                class="btn btn-danger btn-lg">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                            Anterior
                        </button>
                    @endif
                @endif
            </div>

            <div class="col-md-6 col-xs-6 text-right">
                <button type="submit"
                        name="btnNavNext"
                        value="true"
                        class="btn btn-danger btn-lg">
                    Siguiente <i class="glyphicon glyphicon-chevron-right"></i>
                </button>
            </div>
        </div>

        <input name="current_group"
               type="hidden"
               value="<?php echo $viewData['currentGroup']; ?>">

        <input name="survey_id"
               type="hidden"
               value="<?php echo $viewData['config']['survey']->id; ?>">

        @if(!empty($viewData['isRunning']))
            <input name="is_running"
                   type="hidden"
                   value="<?php echo $viewData['isRunning']; ?>">
        @endif
    </form>
@stop