<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;

class Title extends BaseQuestion{

    public function saveConfig( $data ){

    }

    public function getConfig( $dataJson ){

    }

    public function validateAnswers( $partakerRequest ){

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message']= "Esta pregunta es requerida";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest ){

        $answers = !empty($partakerRequest['answers']) ? $partakerRequest['answers'] : [];
        $messages = [];

        $fail = false;
        $keepAnswered = [];
        $withoutAnswered = [];

        if(empty($answers) &&
            !empty($partakerRequest['mandatory']) &&
            $partakerRequest['mandatory'] == "Y"){
            $fail = true;
        } else {
            $keepAnswered = $answers;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

//        echo "<pre>";
//        print_r( $response );
//        print_r( $partakerRequest );
//        exit;

        return $response;
    }

}
?>