<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

// routes/web.php
Route::group(['prefix' => 'admin'], function () {

    Route::get('/viewindex',
        'AdminSurveys\SurveyController@viewindex')
        ->name("admin.viewindex");

//    Route::get('/viewconfigglobal',
//        'AdminSurveys\SurveyController@viewconfigglobal')
//        ->name("admin.viewconfigglobal");

    Route::resource('survey', 'AdminSurveys\SurveyController');

    Route::match(['GET','POST'],'/lists/survey',
        'AdminSurveys\SurveyController@lists')
        ->name("surveyList");

    Route::get('/survey/{surveyId?}/gs',
        'AdminSurveys\QuestionController@groupQ')
        ->name("questionGroup");

    Route::get('/survey/{surveyId?}/edit/gs/{groupId?}',
        'AdminSurveys\QuestionController@editGroup')
        ->name("editGroup");

    Route::get('/survey/{surveyId?}/gs/{groupId?}',
        'AdminSurveys\QuestionController@showDetailGroup')
        ->name("groupDetail");

    Route::post('/survey/{surveyId?}/config/gs/{groupId?}',
        'AdminSurveys\GroupController@saveConfig')
        ->name("saveConfigGroup");

    Route::match(['put', 'patch'], '/survey/{surveyId?}/gs/{groupId?}',
        'AdminSurveys\QuestionController@updateGroup')
        ->name("updateGroup");

    Route::get('/survey/{surveyId?}/q',
        'AdminSurveys\QuestionController@index')
        ->name("question");

    Route::get('/survey/{surveyId?}/gs/{groupId?}/q/{questionId?}',
        'AdminSurveys\QuestionController@showDetail')
        ->name("questionDetail");

    Route::get('/survey/{surveyId?}/gs/{groupId?}/edit/q/{questionId?}',
        'AdminSurveys\QuestionController@edit')
        ->name("editQuestion");

    Route::match(['put', 'patch'], '/survey/{surveyId?}/gs/{groupId?}/edit/q/{questionId?}',
        'AdminSurveys\QuestionController@update')
        ->name("updateQuestion");

    Route::get('/survey/{surveyId?}/gs/{groupId?}/config/q/{questionId?}',
        'AdminSurveys\ConfigQuestionController@config')
            ->name("configQuestion");

    Route::post('config/question/{questionId?}',
        'AdminSurveys\ConfigQuestionController@store')
            ->name("storeConfigQuestion");

    Route::get('/survey/view/{surveyId?}',
        'AdminSurveys\QuestionController@showDetailSurvey')
            ->name("surveyDetail");

    Route::post('/survey/saveconfig/{surveyId?}',
        'AdminSurveys\QuestionController@storeSurveyConfig')
            ->name("storeSurveyConfig");

    Route::post('/group', 'AdminSurveys\QuestionController@storeGroup')
            ->name("storeGroup");

    Route::post('/question', 'AdminSurveys\QuestionController@storeQuestion')
            ->name("storeQuestion");

    Route::post('/survey-delete/{surveyId?}',
        'AdminSurveys\SurveyController@destroy')
        ->name("surveyDelete");

    Route::delete('/question-delete/{questionId?}',
        'AdminSurveys\QuestionController@destroy')
        ->name("questionDelete");

    Route::delete('/groups/{groupId?}',
        'AdminSurveys\GroupController@destroy')
            ->name("groupDelete");

    Route::get('/survey/{surveyId?}/viewactivesurvey',
        'AdminSurveys\SurveyController@viewactivesurvey')
        ->name("survey.viewactivesurvey");

    Route::post('/survey/{surveyId?}/saveactivesurvey',
        'AdminSurveys\SurveyController@saveactivesurvey')
        ->name("survey.saveactivesurvey");

    Route::get('/survey/{surveyId?}/redirectpreviewmode',
        'AdminSurveys\SurveyController@redirectpreviewmode')
        ->name("survey.redirectpreviewmode");

    Route::get('/survey/{surveyId}/savedesactivesurvey',
        'AdminSurveys\SurveyController@savedesactivesurvey')
        ->name("survey.savedesactivesurvey");

    Route::get('/survey/{surveyId?}/gs/{groupId?}/preview',
        'AdminSurveys\GroupController@viewPreview')
        ->name("groupviewpreviewpage");

    Route::get('/survey/{surveyId?}/gs/{groupId?}/q/{questionId?}/preview',
        'AdminSurveys\QuestionController@viewPreview')
        ->name("questionviewpreviewpage");

    Route::get('/survey/{surveyId?}/exportviewdetail',
        'AdminSurveys\ExportController@exportviewdetail')
        ->name("survey.exportviewdetail");

    Route::post('/survey/{surveyId?}/clonesu',
        'AdminSurveys\SurveyController@clonesu')
        ->name("survey.clonesu");

});