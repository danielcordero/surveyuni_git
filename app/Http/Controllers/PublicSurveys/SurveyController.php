<?php

namespace App\Http\Controllers\PublicSurveys;

use App\CustomLib\TypeQuestions\BaseQuestion;
use App\CustomLib\Services\{SurveyPublic,GroupService};
use App\CustomLib\Helper\HelperServices;
use App\CustomLib\TypeQuestions\HandlerStatusSurvey;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Survey;
use Session;

class SurveyController extends Controller
{
    public function index(Request $request)
    {
        if($request->isMethod('post')){

            $surveyId = (int)$request->surveyId;
            $surveyId = HelperServices::sanitize_int($surveyId);
            $surveyPublic = new SurveyPublic();
            $config = $surveyPublic->getConfig( $surveyId );
            $viewData['config'] = $config;

            if($surveyPublic->isAccesible($viewData)){
                $participantInfo = $surveyPublic->getSessionParticipant();
                if(!$surveyPublic->isModePreview() && empty($participantInfo)){
                    $participant = $surveyPublic->getSessionParticipant();

                    if(!$surveyPublic->isValidToken($request) && empty($participant)){
                        $viewData['wrongtoken'] = true;
                        return view('survey.public.accesstoken')
                            ->with("viewData",$viewData);
                    }

                    $participantInfo = $surveyPublic->getParticipantByToken( $request );
                    if(!empty($participantInfo)){
                        $surveyPublic->saveSessionParticipant( $participantInfo );
                    }
                }
            }

            if(!$surveyPublic->checkIfAnswerTofollowSurvey( $config , $request)){

                return view('survey.public.end')
                    ->with("viewData",$viewData);
            }

            $groupSurvey = new GroupService();
            $currentGroup = (int)$request->current_group;

            $nextView = $surveyPublic->getNextViewToshow($request);
            if(!empty($nextView)){
                return view($nextView)
                    ->with("viewData",$viewData);
            }

            if($surveyPublic->isNavBack($request)){

                $currentGroup = $currentGroup - 1;
                $participantInfo = $surveyPublic->getSessionParticipant();
                if(!empty($participantInfo)){
                    $request->participantId = $participantInfo['id'];
                }
                $handlerStatusSurvey = new HandlerStatusSurvey();
                $surveyStatus = $handlerStatusSurvey->getBySurvey( $request, $viewData);
                if(!empty($surveyStatus) && !$surveyPublic->isModePreview()){

                    $groupId = $config['groupsIds'][$currentGroup];
                    $groupInfo = $groupSurvey->getById($groupId);
                    $viewData['currentGroup'] = $currentGroup;
                    $viewData['groupInfo'] = $groupInfo;
                    $viewData['isRunning'] = true;
                    $viewData['responses'] = $surveyStatus['answers'];
                } else {
                    $groupId = $config['groupsIds'][$currentGroup];
                    $groupInfo = $groupSurvey->getById($groupId);
                    $viewData['currentGroup'] = $currentGroup;
                    $viewData['groupInfo'] = $groupInfo;
                }

            } else {
                $baseQuestion = new BaseQuestion;
                $currentView = $request->has('currentView') ? $request->currentView : "";
                if($currentView != "startview"){
                    $responses = $baseQuestion->startToAnswer($request);
                }

                //keep current page if have not completed the questions required
                if(!empty($responses['isThereAWrong'])){
                    $viewData['responses'] = $responses['questionsFail'];
                    $viewData['isThereAWrong'] = $responses['isThereAWrong'];
                    $currentGroup = $currentGroup - 1;
                }

                if($surveyPublic->isTheFirstGroup($request)){
                    $currentGroup = 0;
                } else {
                    $currentGroup = $currentGroup + 1;
                }

                if(empty($config['groupsIds'][$currentGroup])){
                    $surveyPublic->setCompletedByParticipant();
                    return view('survey.public.end')
                        ->with("viewData",$viewData);
                }


                $participantInfo = $surveyPublic->getSessionParticipant();
                if(!empty($participantInfo)){
                    $request->participantId = $participantInfo['id'];
                }

                $handlerStatusSurvey = new HandlerStatusSurvey();
                $surveyStatus = $handlerStatusSurvey->getBySurvey($request, $viewData);
                if(!empty($surveyStatus) && !$surveyPublic->isModePreview() ){

                    $groupId = $config['groupsIds'][$currentGroup];
                    $groupInfo = $groupSurvey->getById($groupId);
                    $viewData['currentGroup'] = $currentGroup;
                    $viewData['groupInfo'] = $groupInfo;
                    $viewData['isRunning'] = true;
                    $viewData['responses'] = $surveyStatus['answers'];
                } else {

                    $groupId = $config['groupsIds'][$currentGroup];
                    $groupInfo = $groupSurvey->getById($groupId);
                    $viewData['currentGroup'] = $currentGroup;
                    $viewData['groupInfo'] = $groupInfo;
                }
            }

            return view('survey.public.questions')
                ->with("viewData",$viewData);

        }

        if($request->isMethod('get')){

            $surveyId = (int)$request->surveyId;
            $surveyId = HelperServices::sanitize_int($surveyId);

//            echo "I'm here";
//            exit;

            if(empty($surveyId) ||
               empty(Survey::where("id",$surveyId)->first())){
                return view('survey.public.inaccessible');
            }

            $surveyPublic = new SurveyPublic();
            $config = $surveyPublic->getConfig( $surveyId );
            $viewData['config'] = $config;
            $surveyPublic->checkParameter($request, $_GET);

            if($surveyPublic->isModePreview()){
//                session_destroy();
                return view('survey.public.start')
                    ->with("viewData",$viewData);
//                return view('survey.public.accesstoken')
//                    ->with("viewData",$viewData);
            }

            if($surveyPublic->hasExpired($viewData['config']['survey'])){
                return view('survey.public.inaccessible')
                    ->with("viewData",$viewData);
            }

            if(!$surveyPublic->hasAccess($viewData['config']['survey'])){
                return view('survey.public.inaccessible')
                    ->with("viewData",$viewData);
            }

            if($surveyPublic->hasBeenCompleted()){
                return view('survey.public.completed')
                    ->with("viewData",$viewData);
            }

            $handlerStatusSurvey = new HandlerStatusSurvey();
            $participant = $surveyPublic->getSessionParticipant();

            $groupSurvey = new GroupService();
            if(!empty($participant)){
                $request->participantId = $participant['id'];
            }

            $surveyStatus = $handlerStatusSurvey->getBySurvey($request, $viewData);
            if(!empty($surveyStatus)){
                $currentGroup = $surveyStatus['currentGroup'];
                $groupId = $config['groupsIds'][$currentGroup];
                $groupInfo = $groupSurvey->getById($groupId);
                $viewData['currentGroup'] = $currentGroup;
                $viewData['groupInfo'] = $groupInfo;
                $viewData['isRunning'] = true;
                $viewData['responses'] = $surveyStatus['answers'];
                return view('survey.public.questions')
                    ->with("viewData",$viewData);
            }

            if(!empty($participant)){
                return view('survey.public.start')
                    ->with("viewData",$viewData);
            }

            $viewData['wrongtoken'] = true;
            return view('survey.public.accesstoken')
                ->with("viewData",$viewData);

        }
    }

    public function infoapp()
    {
        return view('survey.public.welcome');
    }


    public function testtwig()
    {
        require_once(app_path().'/../vendor/autoload.php');

        for ($i = 0; $i< 2000; $i++){

            $loader = new \Twig_Loader_Array(array(
                'index.html' => '',
            ));
            $twig = new \Twig_Environment($loader);

            echo $twig->render('index.html', array('name' => 'Fabien'));
        }

        exit;
    }


}
