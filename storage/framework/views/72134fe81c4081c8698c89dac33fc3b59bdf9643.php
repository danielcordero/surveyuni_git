<?php
    $testSurveys = config('custom.testSurvey');
?>
<ul class="demo-btns">
    <?php if(in_array($viewData['survey']->id,$testSurveys)): ?>
        <li>
            <a href="<?php echo e(route('survey.redirectpreviewmode',[$viewData['survey']->id])); ?>"
               class="btn bg-color-greenDark txt-color-white" target="_blank">
                <i class="fa fa-eye"></i>
                Vista previa de la encuesta
            </a>
        </li>
    <?php else: ?>
        <?php if(!empty($viewData['survey']->active) &&
        $viewData['survey']->active == "Y"): ?>
            <li>
                <a href="<?php echo e(route('survey.savedesactivesurvey',[$viewData['survey']->id])); ?>"
                   class="btn btn-danger"
                   rel="tooltip"
                   data-placement="bottom" data-original-title="Detener está encuesta">
                    <i class="fa fa-stop"></i>
                    Detener está encuesta
                </a>
            </li>
        <?php else: ?>

            <li>
                <a href="<?php echo e(route('survey.viewactivesurvey',[$viewData['survey']->id])); ?>"
                   class="btn btn-primary">
                    <i class="fa fa-gear"></i>
                    <i class="fa fa-binoculars" aria-hidden="true"></i>
                    Activar encuesta
                </a>
            </li>
            <li>
                <a href="<?php echo e(route('survey.show',[$viewData['survey']->id])); ?>"
                   class="btn btn-primary">
                    <i class="fa fa-pencil"></i>
                    Editar encuesta
                </a>
            </li>

        <?php endif; ?>

        <li>
            <a href="<?php echo e(route('survey.redirectpreviewmode',[$viewData['survey']->id])); ?>"
               class="btn bg-color-greenDark txt-color-white" target="_blank">
                <i class="fa fa-eye"></i>
                Vista previa de la encuesta
            </a>
        </li>
        <li>
            <a href="<?php echo e(route('participants.config',[$viewData['survey']->id])); ?>"
               class="btn bg-color-greenDark txt-color-white">
                <i class="fa fa-user"></i>
                Participantes de la encuesta
            </a>
        </li>

        <?php if(!empty($viewData['survey']->active) &&
        $viewData['survey']->active == "N"): ?>
            <li>
                <button
                        data-toggle="modal"
                        data-target="#mdConfirm"
                        class="btn btn-danger btnDelete"
                        role="button"
                        data-toggle="tooltip"
                        data-placement="top"
                        data-question-id="<?php echo e($viewData['survey']->id); ?>"
                        title="Eliminar">
                    <i class="fa fa-trash-o">
                    </i>
                    Eliminar encuesta
                </button>

                <form method="post" role="form" id="<?php echo "frDelete_" . $viewData['survey']->id; ?>"
                      class="hideElement <?php echo "frDelete_" . $viewData['survey']->id; ?>"
                      action="<?php echo e(route('surveyDelete',[$viewData['survey']->id])); ?>">
                    <!--method_field('DELETE')-->
                    <div class="form-group">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    </div>
                    <input type="submit" value="Enviar"  />
                </form>
            </li>
        <?php endif; ?>

        <li>
            <a href="<?php echo e(route('surveystatistics.view',[$viewData['survey']->id])); ?>"
               class="btn bg-color-orange txt-color-white">
                <i class="fa fa-eye"></i>
                Ver respuestas
            </a>
        </li>

    <?php endif; ?>
</ul>