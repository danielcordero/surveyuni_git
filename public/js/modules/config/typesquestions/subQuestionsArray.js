/**
 * Created by cesar on 12/29/2017.
 */
var subQuestionsArray = new function () {
    var that = this;
    this.questionId = 0;

    this.init = function(qId){
        this.questionId = qId;
        this.configTable();
    };

    this.configTable = function(){
        var config = {
            row:{
                keyRow:this.questionId+"_r",
                fieldName:"subQuestionsR"
            },
            column:{
                keyRow:this.questionId+"_c",
                fieldName:"subQuestionsC"
            }
        };

        $("#subQuestionsR").dynamicRow( config.row ).on("atleastone.us.jquery.dynamicRow",
            function (e)
        {
            commonModule.showPopupGeneric("error","Al menos debe haber una fila");
        });

        $("#subQuestionsC").dynamicRow( config.column ).on("atleastone.us.jquery.dynamicRow",
            function (e)
            {
                commonModule.showPopupGeneric("error","Al menos debe haber una columna");
            });
    };
};