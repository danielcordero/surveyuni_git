<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?php echo url('/') ?>/components/template/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

<!-- Theme style -->
<link rel="stylesheet" href="<?php echo url('/') ?>/components/template/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo url('/') ?>/components/template/dist/css/skins/_all-skins.min.css">

<link rel="stylesheet" href="<?php echo url('/') ?>/components/styles/general.css">

<link rel="stylesheet" href="<?php echo url('/') ?>/components/styles/base.css">