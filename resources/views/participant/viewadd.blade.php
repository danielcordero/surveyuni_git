@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Gestionar participantes de la encuesta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">

                        @include('participant.elements.topBottons')

                        <div class="row">
                            <div class="col-md-12">
                                <ul class="demo-btns pull-right">
                                    <li>
                                        <button type="button" id="btnSave" class="btn btn-primary openMainModal">
                                            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                                        </button>
                                    </li>
                                    <li>
                                        <a href="{{ route('participants.config',['surveyId'=>$viewData['survey']->id]) }}"
                                           class="btn btn-danger">
                                            Cancelar
                                            <span class="glyphicon glyphicon-arrow-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            Agregar participante a la encuesta
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <form action="{{  route('participants.addEdit',['surveyId'=>$viewData['survey']->id])  }}"
                              method="post"
                              id="frmParticipanNewEdit"
                              class=""
                              novalidate="novalidate">
                            {{ csrf_field() }}

                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <div class="jarviswidget jarviswidget-color-greenLight"
                                         data-widget-editbutton="false"
                                         data-widget-custombutton="false"
                                         data-widget-deletebutton="false"
                                         data-widget-sortable="false"
                                         data-widget-collapsed="false"
                                         data-widget-fullscreenbutton="false"
                                         data-widget-attstyle="jarviswidget-color-greenLight">

                                        <header role="heading">
                                            <h2>
                                                <strong>
                                                    Participantes
                                                </strong>
                                                <i class="fa fa-user"></i>
                                            </h2>
                                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                        </header>
                                        <!-- widget div-->
                                        <div role="content">

                                            <!-- widget edit box -->
                                            <div class="jarviswidget-editbox">
                                                <!-- This area used as dropdown edit box -->

                                            </div>
                                            <!-- end widget edit box -->

                                            <!-- widget content -->
                                            <div class="widget-body no-padding smart-form">
                                                <fieldset>
                                                    <div class="row">
                                                        <section class="col col-md-12">
                                                            <label class="label">
                                                                <strong>
                                                                    E-mail:
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-envelope-o"></i>
                                                                <input type="email"
                                                                       name="pemail"
                                                                       id="pemail" value="{{ old('pemail') }}">
                                                            </label>
                                                        </section>
                                                    </div>
                                                    <div class="row">
                                                        <section class="col col-6">
                                                            <label class="label">
                                                                <strong>
                                                                    Nombre:
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input type="text"
                                                                       name="pfirstname"
                                                                       placeholder="Nombre"
                                                                       value="{{ old('pfirstname') }}">
                                                            </label>
                                                        </section>
                                                        <section class="col col-6">
                                                            <label class="label">
                                                                <strong>
                                                                    Apellido:
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input type="text"
                                                                       name="plastname"
                                                                       placeholder="Apellido"
                                                                       value="{{ old('plastname') }}">
                                                            </label>
                                                        </section>
                                                    </div>

                                                    <!--<div class="row">
                                                        <section class="col col-6">
                                                            <label class="label">
                                                                <strong>
                                                                    Fecha válida desde:
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text"
                                                                       name="pvdatefrom"
                                                                       id="pvdatefrom"
                                                                       placeholder="Fecha válida desde"
                                                                       readonly
                                                                >
                                                            </label>
                                                        </section>
                                                        <section class="col col-6">
                                                            <label class="label">
                                                                <strong>
                                                                    Fecha válida hasta:
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text"
                                                                       name="pvdateuntil"
                                                                       id="pvdateuntil"
                                                                       placeholder="Fecha válida hasta"
                                                                       readonly
                                                                >
                                                            </label>
                                                        </section>
                                                    </div>-->
                                                </fieldset>

                                                <input type="hidden"
                                                       value="{{$viewData['survey']->id}}"
                                                       name="survey_id"
                                                       id="survey_id"
                                                       class="hideElement" />

                                                <input type="submit"
                                                       value="send"
                                                       class="hideElement" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </article>
                </div>
            </div>
        </section>
    </div>

    @push('scripts')
    <script src="<?php echo url('/') ?>/js/modules/AddEditParticipant.js"></script>
    @endpush

@stop