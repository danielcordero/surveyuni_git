/**
 * Created by cesar on 1/30/2018.
 */
'use strict';
var EmailModule = new function(){
    //Private variable
    var that = this;
    this.init = function(){
        this.loadckEditorElement();
        this.loadEventHandler();
        this.elementDialog();
    };

    this.elementDialog = function(){
        var iframe = $('<iframe class="iframeDialog" id="dialog" allowfullscreen></iframe>');
        var dialog = $("<div></div>").append(iframe).appendTo("body").dialog({
            autoOpen: false,
            modal:false,
            resizable:true,
            width:"60%",
            height: $(window).height() * 0.6,
            close: function(){
                iframe.attr("src","");
            }
        });

        $(document).on("click",'a[target=dialog]', function(event){
            event.preventDefault();
            debugger;

            var src = $(this).attr("href");
            var title = $(this).attr("title");
            iframe.attr({
                src:src
            });
            dialog.dialog("option","title",title);
            dialog.dialog("open");
        });

    };

    this.loadEventHandler = function(){
        $("#btnSave").on("click", function(e){
            $("#frmEmailTemplates").trigger('submit');
        });

        $('.btnResetTemplate').on('click', function(e) {
            e.preventDefault;
            var newval = $(this).attr('data-value');
            var targetId = $(this).attr('data-target');
            var target = $('#'+targetId);
            $(target).val(newval);
            try{
                var oMyEditor = CKEDITOR.instances[targetId];
                oMyEditor.setData(newval);
                //that.updateCKeditor($(this).attr('data-target'),newval);
            }
            catch(err) {}
        });
    };

    this.updateCKeditor = function(fieldname,value)
    {
        var mypopup= editorwindowsHash[fieldname];
        if (mypopup)
        {
            var oMyEditor = mypopup.CKEDITOR.instances['MyTextarea'];
            if (oMyEditor) {oMyEditor.setData(value);}
            mypopup.focus();
        }
        else
        {
            var oMyEditor = CKEDITOR.instances[fieldname];
            oMyEditor.setData(value);
        }
    }

    this.loadckEditorElement = function(){

        var ckeditorOptions = {};
        var idElement = "";
        var surveyId = $("#survey_id");
        $(".smCkEditor").each(function(index, element){
            // ckeditorOptions.LimeReplacementFieldsType = element).attr("id")
            idElement = $(element).attr("id");
            ckeditorOptions.LimeReplacementFieldsSID = surveyId.val();
            ckeditorOptions.LimeReplacementFieldsAction = "editemailtemplates";
            ckeditorOptions.LimeReplacementFieldsType = idElement;
            $("#"+idElement).initcKeditor(ckeditorOptions);
        });
    };
};