<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('elements.modals.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('elements.modals.confirmcopy', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    <!-- END RIBBON -->
    <!-- MAIN CONTENT -->
    <div id="content">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <!-- PAGE HEADER -->
                    <strong>
                        <i class="fa-fw fa fa-puzzle-piece"></i>
                        Lista de encuesta
                    </strong>
                </h1>
            </div>
        </div>
        <!-- end row -->

        <section id="widget-grid" class="">

            <div class="row">
                <div class="col-md-12">
                    <div class="jarviswidget jarviswidget-color-greenLight"
                         data-widget-editbutton="false"
                         data-widget-custombutton="false"
                         data-widget-deletebutton="false"
                         data-widget-sortable="false"
                         data-widget-collapsed="false"
                         data-widget-fullscreenbutton="false"
                         data-widget-attstyle="jarviswidget-color-greenLight">

                        <header role="heading">
                            <h2>
                                <strong>
                                    Filtro de encuestas
                                </strong>
                                <i class="fa fa-filter"></i>
                            </h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <div class="widget-body no-padding">
                                <form id="frmFilterSurvey"
                                      class="smart-form"
                                      method="GET"
                                      action="<?php echo e(route('survey.index')); ?>">
                                    <fieldset>
                                        <div class="row">
                                            <section class="col col-2">
                                                <label class="label">
                                                    <strong>
                                                        Encuesta Id
                                                    </strong>
                                                </label>
                                                <label class="input">
                                                    <input type="text"
                                                           name="fsurveyId"
                                                           placeholder="Escriba ids separados por coma"
                                                           value="<?php echo e(request()->input('fsurveyId')); ?>">
                                                </label>
                                            </section>

                                            <section class="col col-2">
                                                <label class="label">
                                                    <strong>
                                                        Fecha de inicio
                                                    </strong>
                                                </label>

                                                <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                    <i class="icon-append fa fa-calendar"></i>
                                                    <input type="text"
                                                           name="fdateStart"
                                                           id="fdateStart"
                                                           placeholder="Fecha de inicio"
                                                           value="<?php echo e(request()->input('fdateStart')); ?>"
                                                           readonly>
                                                </label>
                                            </section>

                                            <section class="col col-2">
                                                <label class="label">
                                                    <strong>
                                                        Fecha de fin
                                                    </strong>
                                                </label>

                                                <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                    <i class="icon-append fa fa-calendar"></i>
                                                    <input type="text"
                                                           name="fdateEnd"
                                                           id="fdateEnd"
                                                           placeholder="Fecha de final"
                                                           value="<?php echo e(request()->input('fdateEnd')); ?>"
                                                           readonly>
                                                </label>
                                            </section>

                                            <section class="col col-2">
                                                <label class="label">
                                                    <strong>
                                                        Título
                                                    </strong>
                                                </label>
                                                <label class="input">
                                                    <input type="text"
                                                           name="ftitle"
                                                           placeholder="Título"
                                                           value="<?php echo e(request()->input('ftitle')); ?>">
                                                </label>
                                            </section>
                                            <!--<section class="col col-2">
                                                <label class="label">
                                                    <strong>
                                                        Correo del propetario
                                                    </strong>
                                                </label>
                                                <label class="input">
                                                    <input type="email"
                                                           name="femail"
                                                           placeholder="Correo del propetario"
                                                           value="<?php echo e(request()->input('femail')); ?>">
                                                </label>
                                            </section>-->
                                            <section class="col col-2">
                                                <label class="label">
                                                    <strong>
                                                        Estado
                                                    </strong>
                                                </label>
                                                <label class="select">
                                                    <select name="fStatus">
                                                        <option value="" selected>Elija una opción</option>
                                                        <option value="N" >Inactiva</option>
                                                        <option value="Y">Activa</option>
                                                    </select> <i></i> </label>
                                            </section>

                                        </div>
                                    </fieldset>

                                    <footer class="">
                                        <button type="submit" class="btn btn-primary pull-left">
                                            <i class="fa fa-search"></i>
                                            Mostrar resultados
                                        </button>
                                        <button id="btnResetForm"
                                                type="button"
                                                class="btn btn-default pull-left bg-color-greenDark txt-color-white">
                                            Limpiar formulario
                                        </button>
                                    </footer>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget well jarviswidget-sortable" id="wid-id-1" role="widget">
                        <header role="heading">
                            <div class="jarviswidget-ctrls" role="menu"><a href="javascript:void(0);"
                                                                           class="button-icon jarviswidget-edit-btn"
                                                                           rel="tooltip" title=""
                                                                           data-placement="bottom"
                                                                           data-original-title="Edit"><i
                                            class="fa fa-cog "></i></a> <a href="javascript:void(0);"
                                                                           class="button-icon jarviswidget-toggle-btn"
                                                                           rel="tooltip" title=""
                                                                           data-placement="bottom"
                                                                           data-original-title="Collapse"><i
                                            class="fa fa-minus "></i></a> <a href="javascript:void(0);"
                                                                             class="button-icon jarviswidget-fullscreen-btn"
                                                                             rel="tooltip" title=""
                                                                             data-placement="bottom"
                                                                             data-original-title="Fullscreen"><i
                                            class="fa fa-expand "></i></a> <a href="javascript:void(0);"
                                                                              class="button-icon jarviswidget-delete-btn"
                                                                              rel="tooltip" title=""
                                                                              data-placement="bottom"
                                                                              data-original-title="Delete"><i
                                            class="fa fa-times"></i></a></div>
                            <div class="widget-toolbar" role="menu">
                                <a data-toggle="dropdown" class="dropdown-toggle color-box selector"
                                   href="javascript:void(0);"></a>
                                <ul class="dropdown-menu arrow-box-up-right color-select pull-right">
                                    <li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green"
                                              rel="tooltip" data-placement="left"
                                              data-original-title="Green Grass"></span>
                                    </li>
                                    <li><span class="bg-color-greenDark"
                                              data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip"
                                              data-placement="top" data-original-title="Dark Green"></span></li>
                                    <li><span class="bg-color-greenLight"
                                              data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip"
                                              data-placement="top" data-original-title="Light Green"></span></li>
                                    <li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple"
                                              rel="tooltip" data-placement="top" data-original-title="Purple"></span>
                                    </li>
                                    <li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta"
                                              rel="tooltip" data-placement="top" data-original-title="Magenta"></span>
                                    </li>
                                    <li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink"
                                              rel="tooltip" data-placement="right" data-original-title="Pink"></span>
                                    </li>
                                    <li><span class="bg-color-pinkDark"
                                              data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip"
                                              data-placement="left" data-original-title="Fade Pink"></span></li>
                                    <li><span class="bg-color-blueLight"
                                              data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip"
                                              data-placement="top" data-original-title="Light Blue"></span></li>
                                    <li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal"
                                              rel="tooltip" data-placement="top" data-original-title="Teal"></span></li>
                                    <li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue"
                                              rel="tooltip" data-placement="top"
                                              data-original-title="Ocean Blue"></span></li>
                                    <li><span class="bg-color-blueDark"
                                              data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip"
                                              data-placement="top" data-original-title="Night Sky"></span></li>
                                    <li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken"
                                              rel="tooltip" data-placement="right" data-original-title="Night"></span>
                                    </li>
                                    <li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow"
                                              rel="tooltip" data-placement="left"
                                              data-original-title="Day Light"></span></li>
                                    <li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange"
                                              rel="tooltip" data-placement="bottom" data-original-title="Orange"></span>
                                    </li>
                                    <li><span class="bg-color-orangeDark"
                                              data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip"
                                              data-placement="bottom" data-original-title="Dark Orange"></span></li>
                                    <li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red"
                                              rel="tooltip" data-placement="bottom"
                                              data-original-title="Red Rose"></span></li>
                                    <li><span class="bg-color-redLight"
                                              data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip"
                                              data-placement="bottom" data-original-title="Light Red"></span></li>
                                    <li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white"
                                              rel="tooltip" data-placement="right" data-original-title="Purity"></span>
                                    </li>
                                    <li><a href="javascript:void(0);" class="jarviswidget-remove-colors"
                                           data-widget-setstyle="" rel="tooltip" data-placement="bottom"
                                           data-original-title="Reset widget color to default">Remove</a></li>
                                </ul>
                            </div>
                            <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                            <h2>Widget Title </h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>
                        <!-- widget div-->
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                <input class="form-control" type="text">
                            </div>
                            <!-- end widget edit box -->
                            <!-- widget content -->
                            <div class="widget-body no-padding">
                                <div id="example_wrapper" class="dataTables_wrapper form-inline no-footer">
                                    <div class="dt-toolbar">
                                        <div class="col-xs-12 col-sm-6">
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="dataTables_length" id="example_length">
                                                <label>
                                                    <a href="<?php echo e(route('survey.create')); ?>"
                                                       class="btn btn-primary pull-right">
                                                        <i class="fa fa-plus"></i>
                                                        Crear encuesta
                                                    </a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <table id="example"
                                           class="display projects-table table table-striped table-bordered table-hover dataTable no-footer"
                                           cellspacing="0" width="100%" role="grid" aria-describedby="example_info"
                                           style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th class="details-control sorting_disabled" rowspan="1" colspan="1"
                                                aria-label="" style="width: 43px;">Id
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Status: activate to sort column ascending"
                                                style="width: 104px;">Estado
                                            </th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Projects: activate to sort column ascending"
                                                style="width: 429px;" aria-sort="ascending">Titulo
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label=" EST: activate to sort column ascending"
                                                style="width: 126px;"><i
                                                        class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>
                                                Propetario/a
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label=" Starts: activate to sort column ascending"
                                                style="width: 123px;"><i
                                                        class="fa fa-fw fa-calendar text-muted hidden-md hidden-sm hidden-xs"></i>
                                                Fecha de creación
                                            </th>
                                            <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                colspan="1" aria-label="Tracker: activate to sort column ascending"
                                                style="width: 102px;">Acciones
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $testSurveys = config('custom.testSurvey');
                                        foreach($viewData['surveys'] as $survey){
                                        ?>
                                        <tr role="row" class="odd">
                                            <td class="">
                                                <strong>
                                                    <?php
                                                    echo $survey->id;
                                                    ?>
                                                </strong>
                                            </td>
                                            <td>
                                                <?php if($survey->active =='N'): ?>
                                                    <span class="label label-primary">
                                                        INACTIVA
                                                    </span>
                                                <?php else: ?>
                                                    <span class="label label-success" rel="tooltip"
                                                          data-placement="bottom"
                                                          data-original-title="Esta encuesta está activa">
                                                        ACTIVA
                                                    </span>
                                                <?php endif; ?>
                                            </td>
                                            <td class="sorting_1">
                                                <strong>
                                                    <?php echo e($survey->title); ?>

                                                </strong>
                                            </td>
                                            <td>
                                                <strong>
                                                    <?php
                                                    echo Auth::user()->name . '-' . Auth::user()->email;
                                                    ?>
                                                </strong>
                                                
                                                
                                                
                                            </td>

                                            <td>
                                                <strong>
                                                    <?php
                                                    echo  date("Y-m-d",strtotime($survey->created_at));
                                                    ?>
                                                </strong>
                                            </td>
                                            <td>

                                                <?php if(!in_array($survey->id,$testSurveys)): ?>
                                                    <a href="<?php echo e(route('survey.show',[$survey->id])); ?>"
                                                       class="btn btn-info btn-xs"
                                                       role="button"
                                                       rel="tooltip" data-placement="top"
                                                       data-original-title="Editar">
                                                        <i class="fa fa-pencil ">
                                                        </i>
                                                    </a>
                                                <?php endif; ?>

                                                <a href="<?php echo e(route('surveyDetail',[$survey->id])); ?>"
                                                   class="btn btn-info btn-xs"
                                                   role="button"
                                                   rel="tooltip" data-placement="top"
                                                   data-original-title="Agregar preguntas">
                                                    <i class="fa fa-gear fa-spin"></i>
                                                </a>

                                                <button
                                                        data-toggle="modal"
                                                        data-target="#mdConfirmClone"
                                                        class="btn btn-primary btn-xs btnCloneSurvey"
                                                        role="button"
                                                        rel="tooltip" data-placement="top"
                                                        data-placement="top"
                                                        data-survey-id="<?php echo e($survey->id); ?>"
                                                        title="Clonar encuesta">
                                                    <i class="fa fa-files-o" aria-hidden="true"></i>
                                                </button>

                                                <form method="post"  id="<?php echo "frClone_" . $survey->id; ?>"
                                                      class="hideElement"
                                                      action="<?php echo e(route('survey.clonesu',[$survey->id])); ?>">
                                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                    <input type="hidden" name="surveyId" value="<?php echo e($survey->id); ?>">
                                                    <input type="submit" value="Enviar"  />
                                                </form>

                                                <?php if(!in_array($survey->id,$testSurveys)): ?>
                                                        <button
                                                                data-toggle="modal"
                                                                data-target="#mdConfirm"
                                                                class="btn btn-danger btn-xs btnDelete"
                                                                role="button"
                                                                data-placement="top"
                                                                data-survey-id="<?php echo e($survey->id); ?>"
                                                                title="Eliminar">
                                                            <i class="fa fa-trash-o">
                                                            </i>
                                                        </button>

                                                        <form method="post"  id="<?php echo "frDelete_" . $survey->id; ?>"
                                                              class="hideElement"
                                                              action="<?php echo e(route('surveyDelete',[$survey->id])); ?>">
                                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                            <input type="submit" value="Enviar"  />
                                                        </form>
                                                <?php endif; ?>

                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                    <div class="dt-toolbar-footer">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="dataTables_info" id="example_info" role="status"
                                                 aria-live="polite">Mostrando <span class="txt-color-darken">
                                    <?php echo e($viewData['surveys']->firstItem()); ?>

                                 </span> a <span class="txt-color-darken"> <?php echo e($viewData['surveys']->lastItem()); ?></span>
                                                de <span class="text-primary"><?php echo e($viewData['surveys']->total()); ?></span>
                                                entries
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="dataTables_paginate paging_simple_numbers"
                                                 id="example_paginate">
                                                <?php
                                                echo $viewData['surveys']->render();
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <!-- end row -->

        </section>
        <!-- end widget grid -->
    </div>
    <?php $__env->startPush('scripts'); ?>
        <script src="<?php echo url('/') ?>/js/modules/SurveyModule.js"></script>
    <?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>