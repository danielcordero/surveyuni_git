<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 11/15/2017
 * Time: 3:10 AM
 */

return [
    'questionsType' =>[
        'OM' => 'Opción múltiple',
        'OMC' => 'Opción múltiple con comentarios',
        'TFS' => 'Texto libre (línea)',
        'TFB' => 'Texto libre (párrafo)',
        'TFP' => 'Texto libre (varios párrafos)',
        'TS' => 'Varios textos cortos',
        'A' => 'Matriz',
        'A10' => 'Matriz (Elegir del 1 al 10)',
        'A5' => 'Matriz (Elegir del 1 al 5)',
        'AYN' => 'Matriz (Sí/No/No sé)',
        'AC' => 'Matriz en columnas',
        'AISD' => 'Matriz (Aumentar/Mismo/Disminuir)',
        'AN'=>'Matriz (Números)',
        'AT'=>'Matriz (Texto)',
        'N' => 'Entrada Numérica',
        'NM' => 'Entrada Numérica Múltiple',
        'DT' => 'Fecha/tiempo',
        'T' => 'Género',
        'ST' => 'Mostrar texto',
        'YN' => 'Sí/No',
        'C5' => 'Elegir del 1 al 5',
        'LR' => 'Lista (Radio)',
        'DDL' => 'Lista (Desplebagle)',
        'LRC' => 'Lista (botones radiales) con comentarios',
        'R'=>'Clasificaión'
    ],

    'questionsTypeTemplate' =>[
        'TFS' => 'free_text_line',
        'TFB' => 'free_text_paragraph',
        'TFP' => 'free_text_several_paragraph',
        'TS' => 'free_text_several_line',
        'OM' => 'multiple_choice',
        'OMC' => 'multiple_choice_comments',
        'A' => 'array',
        'A10' => 'array10',
        'A5' => 'array5',
        'AYN' => 'array_yes_no',
        'AC' => 'array_column',
        'AISD'=> 'array_i_s_d',
        'AN'=>'array_number',
        'AT'=>'array_text',
        'N' => 'numeric_input',
        'NM' => 'numeric_input_several',
        'DT' => 'datatime',
        'T' => 'title',
        'ST' => 'show_text',
        'YN' => 'yes_no',
        'C5' => 'choice5',
        'LR' => 'list_radio',
        'LRC' => 'list_radio_comments',
        'DDL' => 'drop_down_list',
        'R'=>'ranking'
    ],

    'testSurvey' =>[7],
];


