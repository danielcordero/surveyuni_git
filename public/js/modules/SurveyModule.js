'use strict';
var SurveyModule = {};
(function(){
	//Private variable
	var that = this;
	this.init = function(){
		this.handlerEvent();
		this.loadElement();
	};

	this.loadElement = function(){
        //switch(globalValues.action){

        if($("#sortorderGroup").length){
            $("#sortorderGroup").find("tbody").sortable();
        }

        this.loadElementDates();
        this.initckEditors();
    };

	this.initckEditors = function () {
	    var ckeditorOptions = {};
	    var idElement = "";
	    var surveyId = $("#survey_id");
        $(".smCkEditor").each(function(index, element){
            // ckeditorOptions.LimeReplacementFieldsType = element).attr("id")
            idElement = $(element).attr("id");
            if(surveyId && surveyId.length){
                ckeditorOptions.LimeReplacementFieldsSID = surveyId.val();
                ckeditorOptions.LimeReplacementFieldsAction = "newsurvey";
            } else {
                ckeditorOptions.LimeReplacementFieldsAction = "newsurvey";
            }
            ckeditorOptions.LimeReplacementFieldsType = idElement;
            $("#"+idElement).initcKeditor(ckeditorOptions);
        });
    };

    this.loadElementDates = function(){

        if($('#startdate').length){
            $('#startdate').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
        }

        if($('#expires').length){
            $('#expires').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
        }

    };

	this.handlerEvent = function(){
        var isValidForm = true;
		$("#btnSave").on("click", function(e){

            switch(globalValues.action){
                case "create":
                case "show":
                    isValidForm = that.validaForm();
                    break;
            }
            if(isValidForm){
                $("#frmSurvey").find('[type="submit"]').trigger("click");
            }
            return false;
		});

		$("#frmSurvey").submit(function(e){});


        $('#mdConfirmClone').on('show.bs.modal', function(e) {
            console.log($(e.relatedTarget).data('survey-id'));
            // debugger;
            var currentSurvey = $(e.relatedTarget).data('survey-id');
            //$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            $(this).find('.btn-ok').on("click", function(e){
                e.preventDefault();
                $("#frClone_" + currentSurvey).find('[type="submit"]').trigger("click");
            });
        });


        $('#mdConfirm').on('show.bs.modal', function(e) {
        	// console.log($(e.relatedTarget).data('survey-id'));
        	// debugger;
			var currentSurvey = $(e.relatedTarget).data('survey-id');
            //$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            $(this).find('.btn-ok').on("click", function(e){
                e.preventDefault();
                $("#frDelete_" + currentSurvey).find('[type="submit"]').trigger("click");
			});
        });

        if($('#fdateStart').length > 0){
            $('#fdateStart').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
		}

        if($('#fdateEnd').length > 0){
            $('#fdateEnd').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
        }

        $("#btnResetForm").on("click", function(event){
            commonModule.cleanForm($("#frmFilterSurvey")[0]);
            return false;
        });

        /*Clear fields of dates*/
        $(".btnClearDate").on("click", function(e){
            $("#"+$(this).data("type-date")).val("");
        });
    };

	this.validaForm = function(){
        var fieldDateStart = $("#startdate").val();
        var fieldDateEnd = $("#expires").val();
        var isValid = true;

        if(fieldDateStart != "" || fieldDateEnd != ""){
            if(!commonModule.isDateStartLow(fieldDateStart, fieldDateEnd)){
                commonModule.showPopupGeneric("error","La fecha de inicio debe ser menor que la fecha de fin");
                isValid = false;
            }
        }

        return isValid;

    };

	$(document).ready(function(){
		that.init();
	});

}).call(SurveyModule);