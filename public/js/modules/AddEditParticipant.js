/**
 * Created by cesar on 1/19/2018.
 */
'use strict';
var AddEditParticipant = new function(){
    var that = this;
    this.currentModalId = 0;
    this.surveyId = "";
    this.maxSq = 0;
    this.sqsAll = [];
    this.templates = {
        sortaTable: _.template('<tr id="<%= sqId %>">' +
            '<td>' +
            '<span class="cursorMove">' +
            '<i class="fa fa-bars fa-2x cursorMove" aria-hidden="true"></i>' +
            '</span>' +
            '</td>' +
            '<td><%= sqId %></td>' +
            '<td>' +
            '<label class="onoffswitch">' +
            '<input type="checkbox" name="data[mandatoryfield][<%= sqId %>]" class="onoffswitch-checkbox" id="mandatoryfield_<%= sqId %>">' +
            '<label class="onoffswitch-label" for="mandatoryfield_<%= sqId %>">' +
            '<span class="onoffswitch-inner"  data-swchon-text="ON" data-swchoff-text="OFF"></span>' +
            '<span class="onoffswitch-switch"></span></label>' +
            '</span>' +
            '</td>' +
            '<td>' +
            '<label class="input fullWidth">' +
            '<input type="text" id="customfields_<%= sqId %>" class="input-sm fullWidth" name="data[customfields][<%= sqId %>]">' +
            '</label>' +
            '</td>' +
            '<td>' +
            '<a class="btn btn-info btn-xs addTr"' +
            'data-toggle="tooltip"' +
            'data-type-btn="addTr"' +
            'data-placement="top"' +
            'title="Agregar subpreguntas">' +
            '<i class="fa fa-fw fa-plus"></i>' +
            '</a>' +
            '<a class="btn btn-danger btn-xs removeTr" style="margin-left: 4px;"' +
            'data-toggle="tooltip"' +
            'data-placement="top"' +
            'data-type-btn="removeTr"' +
            'title="Eliminar subpreguntas">' +
            '<i class="fa fa-trash-o"></i>' +
            '</a>' +
            '</td>' +
            '</tr>')
    };

    this.init = function(){
        switch(globalValues.action){
            case "listAll":
                this.loadElementViewAll();
                break;
            case "viewadd":
                this.loadElement();
                break;
            case "viewaddcustomfields":
                this.initSortTable();
                break;
        }
    };

    this.initSortTable = function() {
        this.surveyId = $("#survey_id").val();
        var
            nameTable = "tblCustomFields",
            table = $("#" + nameTable),
            tbody = table.find("tbody"),
            rows = tbody.find("tr");

        // if(rows.length < 1){
        //     tbody.append(this.templates.sortaTable({sqId: 1 + '_' + that.surveyId}));
        // }

        if(rows.length > 0){
            rows.each(function(index, el){
                var i = $(el).attr("id").split("_")[0];
                that.sqsAll.push(i);
            });
            that.maxSq = rows.length;
        }

        tbody.sortable();
        //Action tr
        this.actionBtnOverTable(tbody);

        $("#btnSave").on("click", function(e){
            $("#frmAddCustomFields").trigger('submit');
        });
    };

    this.actionBtnOverTable = function(tbody){
        var dataSqId = "",
            typeBtn,
            parrentTr;

        this.sqsAll.push(that.maxSq);

        $(".addCustomField").on("click",function(e){
            that.maxSq = that.maxSq + 1;
            that.sqsAll.push(that.maxSq);
            tbody.append (that.templates.sortaTable({sqId: that.maxSq + '_' + that.surveyId}));
        });

        tbody.on("click", ".addTr,.removeTr", function () {
            typeBtn = $(this).data("type-btn");
            parrentTr = $(this).parent().parent();

            if(typeBtn === "addTr"){
                that.maxSq = that.maxSq + 1;
                that.sqsAll.push(that.maxSq);
                parrentTr.after(that.templates.sortaTable({sqId: that.maxSq + '_' + that.surveyId}));
            }

            if(typeBtn === "removeTr"){
                // if(tbody.find("tr").length == 1){
                //     commonModule.showPopupGeneric("error","Al menos debe haber una subpregunta");
                //     return;
                // }
                dataSqId = parrentTr.attr("id");
                that.sqsAll = commonModule.rmElementInArray(that.sqsAll, dataSqId.split("_")[0]);
                parrentTr.closest('tr').remove();
                that.reorderValueTr(dataSqId.split("_")[0]);
            }
        });
    };

    this.reorderValueTr = function(fromsqId) {
        // debugger;
        var highsElement = [],
            lesserElement = [],
            highsElementCopy = [],
            surveyIdItem = "";

        _.each(this.sqsAll, function (el) {
            (fromsqId < el) ? highsElement.push(el) : lesserElement.push(el);
        });

        highsElementCopy = highsElement;
        highsElement = _.map(highsElement, function (el) {
            return el - 1;
        });
        this.sqsAll = _.union(highsElement, lesserElement);
        this.maxSq = _.max(this.sqsAll);

        highsElementCopy = _.map(highsElementCopy, function (el) {
            return "#" + el + "_" + that.surveyId;
        });
        highsElementCopy = highsElementCopy.join(",");
        $(highsElementCopy).each(function (i, el) {
            /*
            * Also do changes on view and template javascript
            * */

            $("#mandatoryfield_"+ $(el).attr('id'))
                .attr("name","data[mandatoryfield]["+highsElement[i] +"_"+ that.surveyId+"]")
                .attr("id","mandatoryfield_"+highsElement[i] +"_"+ that.surveyId);

            $("#customfields_"+ $(el).attr('id'))
                .attr("name","data[customfields]["+highsElement[i] +"_"+ that.surveyId+"]")
                .attr("id","customfields_"+highsElement[i] +"_"+ that.surveyId);

            // debugger;
            $(el).attr("id", highsElement[i] + "_" + that.surveyId);
            $(el).children('td').eq(1).html(highsElement[i] + "_" + that.surveyId);


        });
    };

    this.loadElementViewAll = function(){

        $('#frmParticipanNewEditModal_'+ this.currentModalId).submit(function(event) {
            debugger;
            // console.log(commonModule.formToObject('#frmParticipanNewEditModal_'+ this.currentModalId));
            return false;
        });

        // $('#mdAddEdit').on('show.bs.modal', function(e) {
        //     var surveyParticipantId = $(e.relatedTarget).data('survey-participant-id');
        //     var parameters = {}, requestHandler;
        //     var formName = "frmParticipanNewEditModal";
        //     parameters.participantId = surveyParticipantId;
        //     parameters.surveyId = $("#survey_id").val();
        //     requestHandler = ajaxRequest.getParticipantSurvey(parameters);
        //     requestHandler.then(function(response){
        //         commonModule.cleanForm($('#'+formName)[0]);
        //         $('#'+formName+' input[name=pfirstname]').val(response.firstname);
        //         $('#'+formName+'  input[name=plastname]').val(response.lastname);
        //         $('#'+formName+'  input[name=pvdatefrom]').val(response.validfrom);
        //         $('#'+formName+'  input[name=pvdateuntil]').val(response.validuntil);
        //         $('#'+formName+'  input[name=pemail]').val(response.email);
        //         $('#'+formName+'  input[name=pvdatefrom]').val(response.validfrom);
        //         $('#'+formName+'  input[name=pid]').val(surveyParticipantId);
        //     });
        //
        //     // $('#'+formName).on("submit", function(e){
        //     //     e.preventDefault();
        //     //     console.log("heytyyyyyyyyy");
        //     // });
        //
        //     // $(this).find('.btn-ok').on("click", function(e){
        //     //     console.log("Heyyyyyyyyyyy");
        //     //     e.preventDefault();
        //     //     //console.log($('#'+formName)[0]);
        //     // });
        // });
        //
        //
        // $("#mdAddEdit").on('hidden.bs.modal', function () {
        //     console.log("Modal des");
        //     $(this).data('bs.modal', null);
        // });
    };

    this.setCurrentModalId = function(modalId){
        this.currentModalId = modalId;
        this.prepareBehaviorModal();
        // console.log(this.currentModalId);
    };

    this.prepareBehaviorModal = function(){
        var currentForm = '#frmParticipanNewEditModal_'+ this.currentModalId;
        var infoForm = {}, requestHandler = {};
        $(currentForm).submit(function(event) {
            infoForm = commonModule.formToObject(currentForm);
            requestHandler = ajaxRequest.updateOneSurveyParticipant(infoForm);
            requestHandler.then(function(response){
                if(response && response.result){
                    location.reload();
                }
            });
            return false;
        });

        $("#mdAddEdit_"+ this.currentModalId).on('hidden.bs.modal', function () {
            console.log("Modal des");
            $(this).data('bs.modal', null);
        });
    };

    this.loadElement = function(){
        $("#btnSave").on("click", function(e){
            $("#frmParticipanNewEdit").trigger('submit');
        });
        this.loadFormElements();
    };

    this.loadFormElements = function(){

        if($('#pvdatefrom').length){
            $('#pvdatefrom').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
        }

        if($('#pvdateuntil').length > 0){
            $('#pvdateuntil').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
        }
    };

    $(document).ready(function(){
        that.init();
    });
};