<!-- Modal -->
<div class="modal fade" id="requiredQuestionModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">
                    Preguntas requeridas
                </h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center text-danger">
                    <strong>
                        Una o varias preguntas requeridas no han sido respondida. Usted no puede proceder hasta estas preguntas se han completadas.
                    </strong>
                </h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Cerrar
                </button>
            </div>
        </div>

    </div>
</div>