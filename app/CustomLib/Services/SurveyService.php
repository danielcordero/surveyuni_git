<?php

namespace App\CustomLib\Services;

use App\CustomLib\Helper\HelperServices;

use App\{Survey,Group,Question,SurveySettings};
use App\CustomLib\Libraries\Date_Time_Converter;
use Illuminate\Support\Facades\Route;
use Auth;

class SurveyService
{
    public  $data = [];

    public  function __construct()
    {
        $group = [];
        $question = [];
        $parameters = Route::current()->parameters();
        if(!empty($parameters['surveyId'])){
            $surveyId = (int)$parameters['surveyId'];
            $surveyInfo = Survey::with([
                'groups' => function($q){
                    $q->orderBy('sortorder', 'asc');
                },
                'groups.questions' => function($q){
                    $q->orderBy('sortorder', 'asc');
                }
            ])->find($surveyId);
            $this->data['surveyInfo'] = $surveyInfo;

            if(!empty($parameters['groupId'])){
                $group = Group::find($parameters['groupId']);
            }

            if(!empty($parameters['questionId'])){
                $question = Question::find($parameters['questionId']);
            }
            $this->data['survey'] = $surveyInfo;
            $this->data['group'] = $group;
            $this->data['question'] = $question;

        } else {
            $this->data = [];
        }
    }

    public  function getData(){
        return !empty($this->data) ? $this->data : [];
    }

    public function filter($filter)
    {
        //$surveyFilter = $survey->newQuery();
        $surveyFilter = Survey::query();
        $currentUser = Auth::user();
        if(empty($currentUser->is_super_admin)){
            $surveyFilter->where('user_id',$currentUser->id);
        }

        if ($filter->has('fsurveyId') && !empty($filter->fsurveyId)) {

            $surveyIdstrings = explode(",",$filter->fsurveyId);
            $surveyIdstrings = array_filter($surveyIdstrings);
            $surveyIdstrings = array_map(array(new HelperServices,'sanitize_int'), $surveyIdstrings);
            $surveyIdstrings = array_filter($surveyIdstrings); //Removes empty or equivalent values from array
            $surveyFilter->whereIn('id', $surveyIdstrings);
        }

        if ($filter->has('ftitle') && !empty($filter->ftitle)) {
            $surveyFilter->where('title','like' , '%' . $filter->input('ftitle') . '%');
        }

//        if ($filter->has('femail')) {
//            $surveyFilter->where('id', $filter->input('femail'));
//        }

        if ($filter->has('fStatus') && !empty($filter->fStatus)) {
            $surveyFilter->where('active', $filter->input('fStatus'));
        }

        if (($filter->has('fdateStart') && !empty($filter->fdateStart)) ||
            ($filter->has('fdateEnd') || !empty($filter->fdateEnd))) {
            $dateStart = $filter->fdateStart;
            $fdateEnd = $filter->fdateEnd;
            if(!HelperServices::checkFormatDateYMD($dateStart) &&
                HelperServices::checkFormatDateYMD($fdateEnd)){
                $dateStart = $fdateEnd;
            }

            if(!HelperServices::checkFormatDateYMD($fdateEnd) &&
                HelperServices::checkFormatDateYMD($dateStart)){
                $fdateEnd = $dateStart;
            }

            if(HelperServices::checkFormatDateYMD($fdateEnd) &&
                HelperServices::checkFormatDateYMD($dateStart)){

                //$surveyFilter->whereRaw('DATE(created_at) = ?', $filter->input('fdateCrated'));
                $surveyFilter->whereBetween('created_at',[$dateStart, $fdateEnd]);
            }
        }

        $surveyFilter->orderBy('created_at', 'DESC');

        return $surveyFilter;
    }

    public static function saveConfig( $surveyId, $allData )
    {

        $data = $allData->toArray();
        $config = new \stdClass();
        $config->url = $data['url'];
        $config->urldescription = html_entity_decode($data['urldescrip'],ENT_QUOTES, "UTF-8");
        $config->admin = !empty($data['admin']) ? $data['admin'] : "";
        $config->template = !empty($data['template']) ? $data['template'] : "";
        $config->allowprev = !empty($data['allowprev']) ? 'Y' : 'N';
        $config->showprogress = !empty($data['showprogress']) ? 'Y' : 'N';
        $config->printanswers = !empty($data['printanswers']) ? 'Y' : 'N';
        $config->showxquestions = !empty($data['showxquestions']) ?'Y':'N';
        $config->showgroupinfo = !empty($data['showgroupinfo']) ? $data['showgroupinfo'] : "";
        $config->showqnumcode = !empty($data['showqnumcode']) ? $data['showqnumcode'] : "";
        $config->showwelcome = !empty($data['showwelcome']) ? 'Y' : 'N';
        $config->listpublic = !empty($data['listpublic']) ? 'Y' : 'N';
        $config->showindexquestions = !empty($data['showindexquestions']) ? 'Y' : 'N';
        $config->usecookie = !empty($data['usecookie'])?'Y':'N';
        $config->surveyaccess = !empty($data['usecaptcha_surveyaccess'])?'Y':'N';
        $config->askifwantfollowsurvey = !empty($data['askifwantfollowsurvey'])?'Y':'N';

        //$config->usecaptcha = self::transcribeCaptchaOptions( $data );
        if (trim($data['startdate']) != '' && HelperServices::checkFormatDateYMD($data['startdate'])){
//            $converter = new Date_Time_Converter($data['startdate'], "Y-m-d" . ' H:i:s');
//            $sStartDate = $converter->convert("Y-m-d H:i:s");
            $config->startdate = $data['startdate'];
        }

        if (trim($data['expires']) != '' && HelperServices::checkFormatDateYMD($data['expires'])){
//            $converter = new Date_Time_Converter($data['expires'], "Y-m-d" . ' H:i:s');
//            $sExpiryDate = $converter->convert("Y-m-d H:i:s");
            $config->expiresdate = $data['expires'];
        }

//        echo "<pre>";
//        print_r($config);
//        exit;

        $survey = Survey::find($surveyId);
        $survey->additionalData = json_encode($config);

        return $survey->save();
    }

    public static function transcribeCaptchaOptions($data) {
        $surveyaccess = !empty($data['usecaptcha_surveyaccess']) ? $data['usecaptcha_surveyaccess'] : "";
        $registration = !empty($data['usecaptcha_registration']) ? $data['usecaptcha_registration'] : "";
        $saveandload = !empty($data['usecaptcha_saveandload']) ? $data['usecaptcha_saveandload'] : "";

        if ($surveyaccess && $registration && $saveandload)
        {
            return 'A';
        }
        elseif ($surveyaccess && $registration)
        {
            return 'B';
        }
        elseif ($surveyaccess && $saveandload)
        {
            return 'C';
        }
        elseif ($registration && $saveandload)
        {
            return 'D';
        }
        elseif ($surveyaccess)
        {
            return 'X';
        }
        elseif ($registration)
        {
            return 'R';
        }
        elseif ($saveandload)
        {
            return 'S';
        }

        return 'N';
    }

    public static function getInfoBydId($surveyId)
    {
        $surveyInfo = Survey::find($surveyId);
        $config = self::getConfigData($surveyInfo->additionalData);
        if(!empty($config)){
            foreach ($config as $key => $value){
                $surveyInfo->{$key} = $value;
            }
        }
        return $surveyInfo;
    }

    public static function getConfigData($config){
        return json_decode($config);
    }

    public static function processResponse( $question, $participants, $arraySubQuestion = null )
    {
        $response = [];
        foreach ($participants as $participant) {
            $statusSurvey = json_decode($participant->additionalData, true);
            $statusQuestion = self::getFullStatusInSurvey($statusSurvey['answers'], $question->id);
            $response[$question->title] = $statusQuestion['keepAnswered'];
        }
        return $response;
    }

    public static function getFullStatusInSurvey($surveyStatus , $surveyId )
    {
        return !empty($surveyStatus[$surveyId]) ? $surveyStatus[$surveyId] :[];
    }

    public static function getSeveralByLimit( $limitL = 0, $limitH = 2)
    {
        //SELECT * FROM orders WHERE 1=1  ORDER BY id DESC LIMIT 0,30
        //$query = "SELECT * FROM surveys WHERE active = 'Y' AND all_invitation_sent = '0' AND first_block_sent = '1' ORDER BY id DESC LIMIT $limitL,$limitH";
//        $query = "SELECT * FROM surveys WHERE active = 'Y' AND all_invitation_sent = '0' AND first_block_sent = '1' AND Id NOT IN (7) ORDER BY id DESC LIMIT $limitL,$limitH";

        $query = "SELECT * FROM surveys WHERE active = 'Y' AND all_invitation_sent = '0' AND first_block_sent = '1' AND Id NOT IN (7) ORDER BY id DESC LIMIT $limitL,$limitH";

        $result = \DB::select( $query );
        if(!empty($result)){
            $result = array_map(function ($value) {
                return (array)$value;
            }, $result);
        }

        echo "<pre>";
        print_r( $result );
        exit;

        return $result;
    }

    public static function setAllInvitationSent( $surveyId )
    {
        $query = "UPDATE surveys
            SET all_invitation_sent = '1' 
            WHERE id = {$surveyId}";

        return \DB::statement($query);
    }

    public static function setNotAllInvitationSent( $surveyId )
    {
        $query = "UPDATE surveys
            SET all_invitation_sent = '0' 
            WHERE id = {$surveyId}";

        return \DB::statement($query);
    }

    public static function setFirstBlockSent( $surveyId )
    {
        $query = "UPDATE surveys
            SET first_block_sent = '1'
            WHERE id = {$surveyId}";

        return \DB::statement($query);
    }

    public static function getFullConfig( $surveyId ){
        $fullConfig = [];
        $parameters['surveyId'] = $surveyId;
        $query = "SELECT * FROM surveys AS s 
                  INNER JOIN surveys_settings AS ss ON s.id = ss.survey_survey_id 
                  WHERE s.id = :surveyId LIMIT 1";
        $result = \DB::select( \DB::raw($query), $parameters);

        $fullConfig['survey'] = $result;

        $query = "SELECT * FROM groups AS g 
                  WHERE g.survey_id = :surveyId";

        $result = \DB::select( \DB::raw($query), $parameters);
        $fullConfig['groups'] = $result;

        $groupIds = array_column($result,"id");
        $groupQuestions = [];

        for ($i = 0; $i < count($groupIds); $i++ ){
            $parameters['groupId'] = $groupIds[$i];
            $query = "SELECT * FROM questions AS q
                  WHERE q.gid = :groupId AND sid = :surveyId";
            $result = \DB::select( \DB::raw($query), $parameters);
            $groupQuestions[$groupIds[$i]] = [];
            if(!empty( $result )){
                $groupQuestions[$groupIds[$i]] = $result;
            }
        }

        $fullConfig['groupQuestions'] = $groupQuestions;
        return $fullConfig;
    }

}