<?php

namespace App\CustomLib\TypeQuestions;

class FreeTextLine extends BaseQuestion{

    public function saveConfig( $data ){
    }

    public function getConfig( $dataJson ){

    }

    public function validateAnswers( $partakerRequest ){

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message']= "Esta pregunta es requerida";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest ){

        $messages = $keepAnswered = $withoutAnswered = [];
        $fail = false;
        $answers =  $partakerRequest['answers'];
        $answers = $this->cleanData($answers);
        if((empty($answers) &&
                !empty($partakerRequest['mandatory']) &&
                $partakerRequest['mandatory'] == "Y")){
            $fail = true;
        } else {
            $keepAnswered = $answers;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

        return $response;
    }

}
?>