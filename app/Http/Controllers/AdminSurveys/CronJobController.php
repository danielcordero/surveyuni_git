<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 3/30/2018
 * Time: 5:15 PM
 */
namespace App\Http\Controllers\AdminSurveys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\Services\CronJobServices;
use Illuminate\Support\Facades\Log;
use App\CustomLib\Helper\FileLogger;

class CronJobController extends Controller
{
    public function servinginvitation()
    {
        if(!empty($_GET['execute'])){
            CronJobServices::sendInvetations();
            $level = 'DEBUG';
            $logger = new FileLogger('cron_job', $level);
            $message = 'Cron Job Running date time' . date("Y-m-d H:i:s");
            $logger->addRecord($level, $message);
        }
    }
}