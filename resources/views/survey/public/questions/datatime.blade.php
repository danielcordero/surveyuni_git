<div class="row">
    <!-- NEW WIDGET START -->
    <!-- datetime -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <fieldset>
                        <div class="row">
                            <div class="col-md-6 col-xs-6 smart-form">
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input type="text"
                                           name="question_<?php echo $question->id; ?>[answers][date]"
                                           placeholder="Fecha"
                                           class="date"
                                           data-dateformat="dd/mm/yy"
                                           readonly
                                           id="<?php echo $question->id; ?>_date"
                                            value="<?php if(!empty($resultQuestion['keepAnswered']['date'])){ ?>{{$resultQuestion['keepAnswered']['date']}}<?php } ?>">
                                </label>
                            </div>

                        </div>
                    </fieldset>

                </div>

                @include('survey.public.questions.littlepartial.help')
                @include('survey.public.questions.commonHidden')

                <!-- end widget content -->
            </div>
            <!-- end widget div -->
        </div>
    </article>
</div>

@push('scripts')
<script>

    $('#<?php echo $question->id ."_date" ?>').datepicker({
        dateFormat : 'dd/mm/yy',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });

    $('#<?php echo $question->id ."_timepicker" ?>').timepicker();

</script>
@endpush