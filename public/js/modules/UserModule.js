'use strict';
var UserModule = new function(){
	var that = this;
	this.currentModal = {};
	// this.messages = {
	// 	USERCREATED: "Usuario creado exitosamente"
	// };
	// this.forms = {
	// 	createNewUser: "createuser"
	// };
	this.init = function(){
		this.handlerEvent();
        this.loadElementDates();
	};

	this.handlerEvent = function(){
        // this.currentModal = $("#btnModal").manageModal({
			// view:'myModal',
			// onSubmit:that.saveUser
        // });
        //
        $('#mdConfirm').on('show.bs.modal', function(e) {
            var currentUser = $(e.relatedTarget).data('user-id');
            $(this).find('.btn-ok').on("click", function(e){
                e.preventDefault();
                console.log("#frDelete_" + currentUser);
                $("#frDelete_" + currentUser).find('[type="submit"]').trigger("click");
            });
        });
        //
        // $("#btnResetForm").on("click", function(event){
        //     commonModule.cleanForm($("#frmFilterUser")[0]);
        //     return false;
        // });
        this.handlerFormEvent();
	};

	this.handlerFormEvent = function(){
        var isValidForm = true;
        $("#btnSave").on("click", function(e){
            console.log(" test te ");
            console.log(globalValues.action);
            $("#frmNewEditUser").find('[type="submit"]').trigger("click");
            // switch(globalValues.action){
            //     case "create":
            //     case "show":
            //         isValidForm = that.validaForm();
            //         break;
            // }
            // if(isValidForm){
            //     $("#frmSurvey").find('[type="submit"]').trigger("click");
            // }
            return false;
        });

        // $("#frmSurvey").submit(function(e){});
        //
        // $('#mdConfirm').on('show.bs.modal', function(e) {
        //     // console.log($(e.relatedTarget).data('survey-id'));
        //     // debugger;
        //     var currentSurvey = $(e.relatedTarget).data('survey-id');
        //     //$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        //     $(this).find('.btn-ok').on("click", function(e){
        //         e.preventDefault();
        //         $("#frDelete_" + currentSurvey).find('[type="submit"]').trigger("click");
        //     });
        // });
    };

	this.saveUser = function( userData ){
		var requestHandler = ajaxRequest.registerUser(userData);
		requestHandler.then(function(response){
			location.reload();
			console.log(response);
		});
	};

    this.loadElementDates = function(){
        if($('#fdateStart').length > 0){
            $('#fdateStart').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
        }

        if($('#fdateEnd').length > 0){
            $('#fdateEnd').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {}
            });
        }
    };

	$(document).ready(function(){
		that.init();
		// console.log("Loaded");
	});
};