<?php
    use \App\CustomLib\Services\ConfigAppService;
    $adminEmail = ConfigAppService::getGlobalSetting("siteadminemail");
?>

<?php if(!empty($viewData['config'])): ?>
    <?php $__env->startSection('title',$viewData['config']['survey']->title); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="block_error">
                    <div>
                        <h2>
                            En este momento usted no tiene acceso a está encuesta, por favor contactarse con el administrador!
                            <a href="mailto:<?php echo e($adminEmail); ?>"><?php echo e($adminEmail); ?></a>
                        </h2>
                        <h3>
                            Los motivos podrían ser los siguientes:
                        </h3>
                        <ul>
                            <li>
                                <strong>
                                    La encuesta no existe
                                </strong>
                            </li>
                            <li>
                                <strong>
                                    La encuesta no está activa
                                </strong>
                            </li>
                            <li>
                                <strong>
                                    No tiene permiso para acceder a está encuesta
                                </strong>
                            </li>
                            <li>
                                <strong>
                                    La encuesta ha expirado
                                </strong>
                            </li>
                            <li>
                                <strong>
                                    Usted ya completo está encuesta
                                </strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('survey.public.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>