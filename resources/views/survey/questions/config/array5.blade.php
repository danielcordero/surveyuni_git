<div class="inbox-info-bar">
    <div class="row">
        <div class="col-sm-12">
            <h3>
                <strong>
                    Editar subpreguntas
                </strong>
            </h3>
        </div>
    </div>
</div>

<div class="row">
    <br>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <table class="table" id="tbSortOrder">
                <thead>
                <tr>
                    <th>Posición</th>
                    <th>Código</th>
                    <th>Subpregunta</th>
                    <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($viewData['config']['subQuestions'])){
                $subQs = $viewData['config']['subQuestions'];
                foreach ($subQs as $key => $value){
                ?>
                <tr id="<?php echo $key; ?>">
                    <td>
                        <span class="cursorMove">
                            <i class="fa fa-bars fa-2x cursorMove" aria-hidden="true"></i>
                        </span>
                    </td>
                    <td>
                        <?php echo $key; ?>
                    </td>
                    <td>
                        <label class="input fullWidth">
                            <input type="text"
                                   class="input-sm fullWidth"
                                   name="subQuestions[<?php echo $key; ?>]"
                                   value="<?php echo $value; ?>" >
                        </label>
                    </td>
                    <td>

                        <a class="btn btn-info btn-xs addTr"
                           role="button" data-toggle="tooltip"
                           data-type-btn="addTr"
                           data-placement="top"
                           title="Agregar subpreguntas">
                            <i class="fa fa-fw fa-plus"></i>
                        </a>

                        <a class="btn btn-danger btn-xs removeTr"
                           role="button" data-toggle="tooltip"
                           data-placement="top"
                           data-type-btn="removeTr"
                           title="Eliminar subpreguntas">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>
                </tr>
                <?php
                }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

@push('scripts')
<script src="<?php echo url('/') ?>/js/modules/config/typesquestions/subQuestions.js"></script>
@endpush