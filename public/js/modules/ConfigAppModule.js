/**
 * Created by cesar on 2/6/2018.
 */
'use strict';
var ConfigAppModule = new function(){
    //Private variable
    var that = this;
    this.init = function(){
        this.handlerEvent();
    };

    this.handlerEvent = function(){
        $("#btnSave").on("click", function(e){
            //$("#frmSurvey").trigger('submit');
            $("#frmSurvey").find('[type="submit"]').trigger("click");
        });
    };
};

