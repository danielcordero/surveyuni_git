var commonModule = {};
(function(){
	'use strict';
    //Private variable
	var that = this;
	this.genericMessage = {
		ERRORSERVER : "Hubo un error en el servidor",
		UNAUTHORIZED : "No tiene permiso para realizar está acción",
		REQUESTFAIL : "La solictud no fue realizada con exito",
	};

	this.configPopupByStatus = {
		422:{
			title:that.genericMessage.REQUESTFAIL,
			color:"#5C1000",
			iconSmall : "fa fa-thumbs-down bounce animated",
			timeout : 4000
		}
	};

	this.arrayToObject = function(array){
		var o = {};
		var a = array;
		$.each(a, function(){
			if(o[this.name] !== undefined){
				if(!o[this.name].push){
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '')
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	this.formToObject = function(form){
		form = $(form).serializeArray();
		return this.arrayToObject(form);
	};

	this.objectToUL = function(data){

		var list = "";
		var item = "";
		if(data.errors){

            $.each(data.errors, function(key, value){
                item += "<li>" + "<strong>" + key + "<strong> " + value + "</li>";
            });

		} else {
            $.each(data, function(key, value){
                item += "<li>" + "<strong>" + key + "<strong> " + value + "</li>";
            });
		}

		list = '<ul>'+ item +'</ul>';
		return list;
	};

	this.notifyMessage = function(statusCode, data){

		var config = {};
		if(that.configPopupByStatus[statusCode]){
			config = that.configPopupByStatus[statusCode];
		}

		switch(statusCode){
			case 401:
                $.smallBox({
                    title : this.genericMessage.UNAUTHORIZED,
                    //content : this.genericMessage.UNAUTHORIZED,
                    color : "#5C1000",
                    icon : "swing animated"
                });
			break;
			case 422:
				$.smallBox({
					title : config.title,
					content : that.objectToUL(data),
					color : config.color,
					iconSmall : config.iconSmall,
					timeout : config.timeout
				});
			break;
			case 500:
				// toastr.error(this.genericMessage.ERRORSERVER);
			break;
		}
	};

	this.showPopupGeneric = function (type, message) {
        switch(type){
            case "error":

                $.smallBox({
                    // title : config.title,
                    content : message,
                    color : "#5C1000",
                    iconSmall : "fa fa-thumbs-down bounce animated",
                    timeout : 10000
                });

                // $.smallBox({
                //     title : message,
                //     color : "#5C1000",
                //     icon : "swing animated"
                // });
                break;
            case "success":
                $.smallBox({
                    // title : config.title,
                    // content : message,
                    // color : config.color,
                    // iconSmall : config.iconSmall,
                    // timeout : config.timeout
                    content : message,
                    color : "#275b89",
                    iconSmall : "fa fa-thumbs-up bounce animated",
                    timeout : 10000
                });
                break;
            case 500:
                // toastr.error(this.genericMessage.ERRORSERVER);
                break;
        }
    };

	this.openModalConfirmation = function(element, customConfig, fcClose, fcConfirm){
		var config = {
                autoOpen : false,
                width : 600,
                resizable : false,
                modal : true,
                title : "<div class='widget-header'><h4><i class='fa fa-warning'></i> Empty the recycle bin?</h4></div>",
                buttons : [{
                    html : "<i class='fa fa-trash-o'></i>&nbsp; Delete all items",
                    "class" : "btn btn-danger",
                    click : function() {
                        $(this).dialog("close");
                    	if(fcClose){
                            fcClose();
						}
                    }
                }, {
                    html : "<i class='fa fa-times'></i>&nbsp; Cancel",
                    "class" : "btn btn-default",
                    click : function() {
                        $(this).dialog("close");
                        if(fcConfirm){
                            fcConfirm();
						}
                    }
                }]
            };

		if(customConfig){
            config = $.extend({}, config, customConfig);
        }

        $(element).dialog(config);
	};

	this.rmElementInArray = function(array, element){

        array = $.grep(array, function(value) {
            return value != element;
        });
        return array;
	};

	this.cleanForm = function(form){

		var fiel_type, i = 0, elements = form.elements;
        form.reset();

        for(i=0; i<elements.length; i++ ){

            fiel_type = elements[i].type.toLowerCase();
            switch (fiel_type){

				case "text":
                case "password":
                case "textarea":
                case "hidden":
                case "number":
                case "email":
                    elements[i].value = "";
                    break;
				case "radio":
				case "checkbox":
					if(elements[i].checked){
                        elements[i].checked = false;
					}
					break;
				case "select_one":
				case "select-multi":
					elements[i].selectedIndex = -1;
					break;
				default:
					break;
			}
		}
	};

    this.isDateStartLow = function(start, end){
        var isDateStartLow = false;
        if(new Date(end) >= new Date(start)){
            isDateStartLow = true;
        }
        return isDateStartLow;
    };

}).call(commonModule);