<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production-plugins.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-skins.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-rtl.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/demo.min.css">
<!-- <link rel="shortcut icon" href="<?php echo url('/') ?>/img/favicon/favicon.ico" type="image/x-icon"> -->
<!-- <link rel="icon" href="<?php echo url('/') ?>/img/favicon/favicon.ico" type="image/x-icon"> -->
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
<link rel="apple-touch-icon" href="<?php echo url('/') ?>/img/splash/sptouch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo url('/') ?>/img/splash/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo url('/') ?>/img/splash/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo url('/') ?>/img/splash/touch-icon-ipad-retina.png">

<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/helperClass.css">

<link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/mystyle.css">

<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-startup-image" href="<?php echo url('/') ?>/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image" href="<?php echo url('/') ?>/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image" href="<?php echo url('/') ?>/img/splash/iphone.png" media="screen and (max-device-width: 320px)">