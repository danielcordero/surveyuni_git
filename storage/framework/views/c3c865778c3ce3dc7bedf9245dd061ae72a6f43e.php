<?php
    use \App\CustomLib\Services\ConfigAppService;
    $adminEmail = ConfigAppService::getGlobalSetting("siteadminemail");
?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <title> Bienvenido </title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="icon" href="<?php echo e(asset('img/favicon.ico')); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicon.ico')); ?>" type="image/x-icon">

    <link rel="icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-rtl.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/mystyle.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/demo.min.css">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="<?php echo url('/') ?>/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="<?php echo url('/') ?>/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="<?php echo url('/') ?>/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

</head>
<body class="hold-transition login-page">

<div class="container-fluid">
    <div class="row bg-primary header">
        <div class="hidden-xs col-md-2">
            <a href="http://www.uni.edu.ni" target="_blank" class="logo-UNI">
                <img src="https://si.uni.edu.ni/NotasUNI/imgs/logo_uni_89.png" alt="UNI"></a>
        </div>
        <div class="col-md-8 text-center header-text">
            <h1 class="noMarginTopBottom">Universidad Nacional de Ingeniería</h1>
            <!-- <h2>Maestría en Gestión de Tecnologías de Información y Comunicación (GTIC)</h2> -->
            <h3 class="noMarginTopBottom">Sistema de gestion de encuestas online</h3>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 titleSurvey">
            <br>
            <div class="well">

                <h1>
                    Bienvenido al sistema de encuesta
                </h1>
                <br>
                <h2>
                    Por favor contacte al administrador <strong>( <?php echo e($adminEmail); ?> )</strong> para asistencia adicional.
                </h2>
                <br>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada dapibus nulla, nec porttitor ante interdum ut. Nullam laoreet mauris vel consectetur interdum. Proin at libero sapien. Proin sit amet tincidunt orci. Etiam commodo mauris nisl, ac pretium libero pellentesque in. Praesent vitae justo imperdiet, convallis justo nec, molestie quam. Morbi luctus consequat libero vitae placerat. Pellentesque id varius quam. Morbi volutpat tortor quis est congue semper. Proin in quam quam. Nulla ac suscipit lorem, finibus bibendum eros.

                    Suspendisse dapibus ullamcorper faucibus. Etiam imperdiet ornare ex, quis ullamcorper lorem feugiat eget. Pellentesque id laoreet nisi, vel malesuada felis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla facilisi. Morbi dictum pharetra massa, vel ornare ex eleifend id. Aenean sollicitudin pharetra tellus, quis pulvinar neque accumsan et.

                    Nullam arcu nisi, scelerisque finibus purus id, ultricies condimentum diam. Sed mollis nunc est, non rhoncus orci laoreet eu. Integer vel rutrum magna. Cras mattis ipsum a dictum vehicula. Cras suscipit pulvinar suscipit. Nunc porta fermentum sagittis. Proin nec ultricies nunc. Sed a neque lacinia, finibus elit ac, commodo nunc. Quisque tincidunt tincidunt quam non mollis.

                    Vivamus at diam viverra, commodo metus a, rutrum erat. Quisque imperdiet sapien purus, sit amet auctor risus ultrices eleifend. Phasellus a rutrum eros. Donec sapien lorem, malesuada rhoncus velit at, venenatis fermentum lectus. Donec eu ante ut nisl dignissim venenatis ut malesuada lectus. Fusce arcu ante, scelerisque et arcu at, interdum vulputate dolor. Cras purus nunc, ornare sed posuere id, ultrices sed odio. Cras condimentum, massa eget mollis rutrum, augue massa rutrum ante, luctus bibendum augue tortor sit amet lectus. Nunc rhoncus tellus vel tortor auctor convallis. Duis ut vulputate quam.

                    Mauris id volutpat orci. Etiam fermentum sapien ut massa aliquam pulvinar. Donec interdum elit sit amet sapien tincidunt, id imperdiet metus tristique. Ut non odio convallis, pellentesque sapien nec, faucibus justo. Suspendisse dapibus placerat nisi, nec finibus nulla elementum in. Etiam in pellentesque massa, a vestibulum sapien. Cras cursus elit nec odio facilisis, sit amet porttitor lacus posuere. Nam consequat tempor nunc sagittis interdum.
                </p>
            </div>

        </div>
    </div>
</div>


<div class="container-fluid overContent-fixedBottom fullWidth">
    <div class="row bg-primary footer">
        <div class="hidden-xs col-md-2">
        </div>
        <div class="col-md-8 text-center header-text">
            <p class="noMarginTopBottom">
                Universidad Nacional de Ingeniería (UNI)
            </p>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="js/libs/jquery-2.1.1.min.js"><\/script>');
    }
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo url('/') ?>/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo url('/') ?>/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<!-- BOOTSTRAP JS -->
<script src="<?php echo url('/') ?>/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo url('/') ?>/js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo url('/') ?>/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo url('/') ?>/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="<?php echo url('/') ?>/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo url('/') ?>/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo url('/') ?>/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo url('/') ?>/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo url('/') ?>/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo url('/') ?>/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo url('/') ?>/js/plugin/fastclick/fastclick.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
<script src="<?php echo url('/') ?>/js/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="<?php echo url('/') ?>/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo url('/') ?>/js/speech/voicecommand.min.js"></script>

<!-- SmartChat UI : plugin -->
<script src="<?php echo url('/') ?>/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="<?php echo url('/') ?>/js/smart-chat-ui/smart.chat.manager.min.js"></script>

<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script>-->

<script type="text/javascript">

    $(document).ready(function() {
        pageSetUp();
    })

</script>

</body>
</html>