<?php
//    echo "<pre>";
//    echo "";
//    print_r($viewData['survey']->startdate);
//    exit;
?>

<div class="row">
    <div class="col-md-6">
        <div class="jarviswidget"
             data-widget-editbutton="false"
             data-widget-custombutton="false"
             data-widget-deletebutton="false"
             data-widget-sortable="false"
             data-widget-collapsed="false"
             data-widget-fullscreenbutton="false">

            <header role="heading">
                <h2>
                    <strong>
                        Texto generales
                    </strong>
                </h2>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
            </header>
            <div role="content">
                <div class="jarviswidget-editbox">
                </div>
                <div class="widget-body">
                    <div class="widget-body smart-form">
                        <fieldset>
                            <section>
                                <label class="label">
                                    <small class=" superset  text-danger asterisk fa fa-asterisk small "
                                           aria-hidden="true">
                                    </small>
                                    <strong>
                                        Titulo:
                                    </strong>
                                </label>
                                <label class="input">
                                    <input type="text"
                                           name="txtTitle" class="input-sm"
                                           value="<?php echo !empty($viewData['survey']->title) ? $viewData['survey']->title : ""; ?>" required>
                                </label>
                            </section>
                            <section>
                                <label class="label">
                                    <strong>
                                        Descripción:
                                    </strong>
                                </label>
                                <label class="textarea">
                                <textarea
                                        name="txADescription"
                                        id="txADescription"
                                        class="smCkEditor"><?php echo !empty($viewData['survey']->description) ? $viewData['survey']->description : ""; ?></textarea>

                                </label>
                            </section>
                        </fieldset>
                        <fieldset>
                            <section>
                                <label class="label">
                                    <strong>
                                        Mensaje de bienvenida:
                                    </strong>
                                </label>
                                <label class="textarea">
                                    <textarea
                                            name="txAWelcomeMessage"
                                            id="txAWelcomeMessage"
                                            class="smCkEditor"><?php echo !empty($viewData['survey']->welcometext) ? $viewData['survey']->welcometext : "";?></textarea>

                                </label>
                            </section>
                            <section>
                                <label class="label">
                                    <strong>
                                        Mensaje de despedida:
                                    </strong>
                                </label>
                                <label class="textarea">
                                    <textarea
                                            name="txAEndMessage"
                                            id="txAEndMessage"
                                            class="smCkEditor"><?php echo !empty($viewData['survey']->endtext) ? $viewData['survey']->endtext : "";?></textarea>
                                </label>
                            </section>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="jarviswidget"
             data-widget-editbutton="false"
             data-widget-custombutton="false"
             data-widget-deletebutton="false"
             data-widget-sortable="false"
             data-widget-collapsed="false"
             data-widget-fullscreenbutton="false">

            <header role="heading">
                <h2>
                    <strong>
                        Link de interés

                        <a href="#"
                           class=""
                           role="button" rel="tooltip"
                           data-placement="top"
                           data-original-title="Muestra un link para más información de la encuesta">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </strong>
                </h2>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
            </header>

            <!-- widget div-->
            <div role="content">
                <div class="widget-body">

                    <div class="form-horizontal">
                        <!-- General options -->
                        <!-- End URL -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="url">
                                <strong>
                                    URL Final:

                                    <a href="#"
                                       class=""
                                       role="button" rel="tooltip"
                                       data-placement="top"
                                       data-original-title="Es un link que muestre información de interés de la encuesta">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                </strong>
                            </label>
                            <div class="col-sm-9">
                                <input type="text"
                                       class="form-control"
                                       id="url"
                                       name="url"
                                       value="<?php echo !empty($viewData['survey']->url) ? $viewData['survey']->url : '';?>"
                                       placeholder="http://ejemplo.com (página externa ó youtube)">
                            </div>
                        </div>

                        <!-- URL description -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="urldescrip">
                                <strong>
                                    Descripción URL :

                                    <a href="#"
                                       class=""
                                       role="button" rel="tooltip"
                                       data-placement="top"
                                       data-original-title="Es una descripción del link agregado">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                </strong>
                            </label>
                            <div class="col-sm-9">
                                <input type="text"
                                       maxlength="255"
                                       size="50"
                                       id="urldescrip"
                                       name="urldescrip"
                                       value="<?php echo !empty($viewData['survey']->urldescription) ? $viewData['survey']->urldescription : '';?>"
                                       class="form-control"
                                       placeholder="Descripción del link">
                            </div>
                        </div>

                        <!-- Date format
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="dateformat">Date format:</label>
                            <div class="col-sm-9">
                                <select id="dateformat" size="1" class="form-control" name="dateformat">
                                    <option value="1">dd.mm.yyyy</option>
                                    <option value="2">dd-mm-yyyy</option>
                                    <option value="3">yyyy.mm.dd</option>
                                    <option value="4">d.m.yyyy</option>
                                    <option value="5">dd/mm/yyyy</option>
                                    <option value="6">yyyy-mm-dd</option>
                                    <option value="7">yyyy/mm/dd</option>
                                    <option value="8">d/m/yyyy</option>
                                    <option value="9" selected="selected">mm-dd-yyyy</option>
                                    <option value="10">mm.dd.yyyy</option>
                                    <option value="11">mm/dd/yyyy</option>
                                    <option value="12">d-m-yyyy</option>
                                </select>        </div>
                        </div>-->

                        <!-- Decimal mark
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="numberformat">Decimal mark:</label>
                            <div class="col-sm-9">
                                <select id="numberformat" size="1" class="form-control" name="numberformat">
                                    <option value="0" selected="selected">Dot (.)</option>
                                    <option value="1">Comma (,)</option>
                                </select>        </div>
                        </div>-->

                        <!-- Administrator -->
                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label" for="admin">
                                <strong>
                                    Administrator:
                                </strong>
                            </label>
                            <div class="col-sm-9">
                                <input type="text"
                                       size="50"
                                       id="admin"
                                       name="admin"
                                       class="form-control"
                                       value="<?php echo !empty($viewData['survey']->admin) ? $viewData['survey']->admin : '';?>">
                            </div>
                        </div>-->

                    <!-- Admin email
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="adminemail">Admin email:</label>
                            <div class="col-sm-9">
                                <input type="email" size="30" class="form-control" id="adminemail" name="adminemail" value="your-email@example.net">
                            </div>
                        </div>
                        -->

                    <!-- Bounce Email
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="bounce_email">Bounce email:</label>
                            <div class="col-sm-9">
                                <input type="email" size="50" class="form-control" id="bounce_email" name="bounce_email" value="your-email@example.net">
                            </div>
                        </div>
                        -->

                        <!-- Format
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="format">Format:</label>
                            <div class="col-sm-9">
                                <div class="btn-group" id="format" data-toggle="buttons">
                                    <label class="btn btn-default"><input name="format" id="format_opt1" value="S" type="radio">Question by question</label>
                                    <label class="btn btn-default active"><input name="format" id="format_opt2" value="G" checked="checked" type="radio">Group by group</label>
                                    <label class="btn btn-default"><input name="format" id="format_opt3" value="A" type="radio">All in one</label>
                                </div>
                            </div>
                        </div>
                        -->
                        <!-- Template -->
                        <!--<div class="form-group">
                            <label class="col-sm-3 control-label" for="template">
                                <strong>
                                    Plantilla:
                                </strong>
                            </label>
                            <div class="col-sm-5">
                                <select id="template" class="form-control" name="template">
                                    <option value="default" selected="selected">default</option>
                                    <option value="news_paper">news_paper</option>
                                    <option value="ubuntu_orange">ubuntu_orange</option>
                                </select>
                            </div>
                            <div class="col-sm-4 template-img">
                                <img class="img-responsive" alt="Template preview image" id="preview"
                                     src="/monografia/limesurvey/templates/default/preview.png">
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>

        <div class="jarviswidget"
             data-widget-editbutton="false"
             data-widget-custombutton="false"
             data-widget-deletebutton="false"
             data-widget-sortable="false"
             data-widget-collapsed="false"
             data-widget-fullscreenbutton="false">

            <header role="heading">
                <h2>
                    <strong>
                        Presentación y navegación
                    </strong>
                </h2>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
            </header>

            <!-- widget div-->
            <div role="content">
                <div class="widget-body">

                    <div class="form-horizontal">
                        <fieldset>
                            <!-- welcome screen -->
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="showwelcome">
                                    <strong>
                                        Mostrar página de bienvenida:

                                        <a href="#"
                                           class=""
                                           role="button" rel="tooltip"
                                           data-placement="top"
                                           data-original-title="Habilita la opción de mostrar pagina de bienvenida">
                                            <i class="fa fa-question-circle"></i>
                                        </a>

                                    </strong>
                                    <!--Show welcome screen:-->
                                </label>
                                <div class="col-sm-6">
                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="showwelcome"
                                                   class="onoffswitch-checkbox"
                                                   id="ytshowwelcome"
                                            <?php
                                                if(!empty($viewData['survey']) &&
                                                    !empty($viewData['survey']->showwelcome) &&
                                                    $viewData['survey']->showwelcome == "Y"){
                                                    echo "checked";
                                                } else if(!empty($viewData['survey']) &&
                                                    !empty($viewData['survey']->showwelcome) &&
                                                    $viewData['survey']->showwelcome == "N"){
                                                    echo "";
                                                } else {
                                                    echo "checked";
                                                }

                                                ?>>
                                            <label class="onoffswitch-label"
                                                   for="ytshowwelcome">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>
                                </div>
                            </div>

                            <!-- Navigation delay
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="navigationdelay">Navigation delay
                                    (seconds):</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" value="0" name="navigationdelay"
                                           id="navigationdelay" size="12" maxlength="2"
                                           onkeypress="return goodchars(event,'0123456789')">
                                </div>
                            </div>-->

                            <!-- Show [<< Prev] button -->
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="allowprev">
                                    <strong>
                                        Permitir navegacion hacia atrás:

                                        <a href="#"
                                           class=""
                                           role="button" rel="tooltip"
                                           data-placement="top"
                                           data-original-title="Habilita la opción de navegar entre las preguntas">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </strong>
                                </label>
                                <div class="col-sm-6">

                                    <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="allowprev"
                                               class="onoffswitch-checkbox"
                                               id="ytallowprev"
                                            <?php
                                                if(!empty($viewData['survey']) && !empty($viewData['survey']->allowprev) &&
                                                    $viewData['survey']->allowprev == "Y"){
                                                    echo "checked";
                                                } else if(!empty($viewData['survey']) &&
                                                    !empty($viewData['survey']->allowprev) &&
                                                    $viewData['survey']->allowprev == "N"){
                                                    echo "";
                                                } else {
                                                    echo "checked";
                                                }
                                                ?>>
                                        <label class="onoffswitch-label"
                                               for="ytallowprev">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>
                                </div>
                            </div>

                            <!-- Show question index
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="questionindex">Show question index / allow
                                    jumping:</label>
                                <div class="col-sm-7">

                                    <div class="btn-group" id="questionindex" data-toggle="buttons">
                                        <label class="btn btn-default active"><input name="questionindex"
                                                                                     id="questionindex_opt1" value="0"
                                                                                     checked="checked" type="radio">Disabled</label>
                                        <label class="btn btn-default"><input name="questionindex"
                                                                              id="questionindex_opt2" value="1"
                                                                              type="radio">Incremental</label>
                                        <label class="btn btn-default"><input name="questionindex"
                                                                              id="questionindex_opt3" value="2"
                                                                              type="radio">Full</label>
                                    </div>
                                </div>
                            </div>-->

                            <!-- Keyboard-less operation
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="nokeyboard">Show on-screen keyboard:</label>
                                <div class="col-sm-7">
                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="nokeyboard"
                                                   class="onoffswitch-checkbox"
                                                   id="ytnokeyboard">
                                            <label class="onoffswitch-label"
                                                   for="ytnokeyboard">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>
                                </div>
                            </div>-->

                            <!-- Show progress bar -->
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="showprogress">
                                    <strong>
                                        Mostrar barra de progreso:

                                        <a href="#"
                                           class=""
                                           role="button" rel="tooltip"
                                           data-placement="top"
                                           data-original-title="Habilita la opción de mostrar el estado de las preguntas">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </strong>
                                </label>
                                <div class="col-sm-6">
                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="showprogress"
                                                   class="onoffswitch-checkbox"
                                                   id="ytshowprogress" <?php
                                                if(!empty($viewData['survey']) && !empty($viewData['survey']->showprogress) &&
                                                    $viewData['survey']->showprogress == "Y"){
                                                    echo "checked";
                                                } else if(!empty($viewData['survey']) &&
                                                    !empty($viewData['survey']->showprogress) &&
                                                    $viewData['survey']->showprogress == "N"){
                                                    echo "";
                                                } else {
                                                    echo "checked";
                                                }
                                                ?>>
                                            <label class="onoffswitch-label"
                                                   for="ytshowprogress">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>

                                </div>
                            </div>

                            <!-- Participants may print answers
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="printanswers">
                                    <strong>
                                        Permitir imprimir las preguntas:
                                    </strong>
                                </label>
                                <div class="col-sm-7">

                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="printanswers"
                                                   class="onoffswitch-checkbox"
                                                   id="ytprintanswers" <?php
//                                                if(!empty($viewData['survey']) &&
//                                                    $viewData['survey']->printanswers == "Y"){
//                                                    echo "checked";
//                                                }
                                                ?>>
                                            <label class="onoffswitch-label"
                                                   for="ytprintanswers">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>
                                </div>
                            </div>
                            -->

                            <!-- Public statistics
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="publicstatistics">Public statistics:</label>
                                <div class="col-sm-7">

                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="publicstatistics"
                                                   class="onoffswitch-checkbox"
                                                   id="ytpublicstatistics">
                                            <label class="onoffswitch-label"
                                                   for="ytpublicstatistics">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>

                                </div>
                            </div>-->

                            <!-- Show graphs in public statistics
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="publicgraphs">Show graphs in public
                                    statistics:</label>
                                <div class="col-sm-7">

                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="publicgraphs"
                                                   class="onoffswitch-checkbox"
                                                   id="ytpublicgraphs">
                                            <label class="onoffswitch-label"
                                                   for="ytpublicgraphs">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>

                                </div>
                            </div>-->

                            <!-- Automatically load URL
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="autoredirect">Automatically load URL when
                                    survey complete:</label>
                                <div class="col-sm-7">

                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="autoredirect"
                                                   class="onoffswitch-checkbox"
                                                   id="ytautoredirect">
                                            <label class="onoffswitch-label"
                                                   for="ytautoredirect">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>

                                </div>
                            </div>-->

                            <!-- showxquestions -->

                            <!-- Show "There are X questions in this survey" -->
                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="showxquestions">
                                    <strong>
                                        Mostrar "Hay x preguntas en esta encuesta":
                                        <a href="#"
                                           class=""
                                           role="button" rel="tooltip"
                                           data-placement="top"
                                           data-original-title="Habilita la opción de mostrar la cantidad de pregunta en la encuesta">
                                            <i class="fa fa-question-circle"></i>
                                        </a>

                                    </strong>
                                </label>
                                <div class="col-sm-6">

                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="showxquestions"
                                                   class="onoffswitch-checkbox"
                                                   id="ytshowxquestions" <?php
                                                if(!empty($viewData['survey']) &&
                                                    !empty($viewData['survey']->showxquestions)){
                                                    if($viewData['survey']->showxquestions == "Y"){
                                                        echo "checked";
                                                    } else {
                                                        echo "";
                                                    }
                                                } else {
                                                    echo "checked";
                                                }
                                                ?>>
                                            <label class="onoffswitch-label"
                                                   for="ytshowxquestions">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-6 control-label" for="showindexquestions">
                                    <strong>
                                        Mostrar índice de la pregunta:

                                        <a href="#"
                                           class=""
                                           role="button" rel="tooltip"
                                           data-placement="top"
                                           data-original-title="Habilita la opción de mostrar el número de pregunta en la encuesta">
                                            <i class="fa fa-question-circle"></i>
                                        </a>

                                    </strong>
                                    <!--Show "There are X questionsin this survey":-->
                                </label>
                                <div class="col-sm-6">

                                    <span class="onoffswitch">
                                            <input type="checkbox"
                                                   name="showindexquestions"
                                                   class="onoffswitch-checkbox"
                                                   id="showindexquestions" <?php
                                                if(!empty($viewData['survey']) &&
                                                   !empty($viewData['survey']->showindexquestions) &&
                                                    $viewData['survey']->showindexquestions == "Y"){
                                                    echo "checked";
                                                } else if(!empty($viewData['survey']) &&
                                                    !empty($viewData['survey']->showindexquestions) &&
                                                    $viewData['survey']->showindexquestions == "N"){
                                                    echo "";
                                                } else {
                                                    echo "checked";
                                                }
                                                ?>>
                                            <label class="onoffswitch-label"
                                                   for="showindexquestions">
                                        <span class="onoffswitch-inner"
                                              data-swchon-text="ON"
                                              data-swchoff-text="OFF"></span>
                                        <span class="onoffswitch-switch"></span></label>
                                    </span>

                                </div>
                            </div>


                            <!-- Show group name and/or group description -->
                            <!--<div class="form-group">
                                <label class="col-sm-5 control-label" for="showgroupinfo">
                                    <strong>
                                        Mostrar nombre de grupo y/o descripción del grupo
                                    </strong>
                                </label>
                                <div class="col-sm-7">
                                    <select id="showgroupinfo" name="showgroupinfo" class="form-control">
                                        <option value="B" selected="selected">Mostrar ambos</option>
                                        <option value="N">Mostrar nombre del grupo solamente</option>
                                        <option value="D">Mostrar descripción del grupo solamente</option>
                                        <option value="X">Ocultar ambos</option>
                                    </select>
                                </div>
                            </div>-->


                            <!-- Show question number and/or code -->
                            <!--<div class="form-group">
                                <label class="col-sm-5 control-label" for="showqnumcode">
                                    <strong>
                                        Mostrar número de pregunta y/o codigo
                                        Show question number and/or code:
                                    </strong>
                                </label>
                                <div class="col-sm-7">
                                    <select class="form-control" id="showqnumcode" name="showqnumcode">
                                        <option value="B">Mostrar ambos</option>
                                        <option value="N">Mostrar número de la pregunta solamente</option>
                                        <option value="C">Mostrar código de la pregunta solamente</option>
                                        <option value="X" selected="selected">
                                            Ocultar ambos
                                        </option>
                                    </select>
                                </div>
                            </div>-->


                            <!-- Show "No answer"
                            <div class="form-group">
                                <label class="col-sm-5 control-label" for="dis_shownoanswer">Show "No answer":</label>
                                <div class="col-sm-7">
                                    <input type="hidden" name="shownoanswer" id="shownoanswer" value="Y">
                                    <input class="form-control" type="text" name="dis_shownoanswer"
                                           id="dis_shownoanswer" disabled="disabled"
                                           value="On (Forced by the system administrator)">
                                </div>
                            </div>-->

                        </fieldset>

                    </div>
                </div>
            </div>
        </div>

        <div class="jarviswidget"
             data-widget-editbutton="false"
             data-widget-custombutton="false"
             data-widget-deletebutton="false"
             data-widget-sortable="false"
             data-widget-collapsed="false"
             data-widget-fullscreenbutton="false">

            <header role="heading">
                <h2>
                    <strong>
                        Publicación y control de acceso:
                        <a href="#"
                           class=""
                           role="button" rel="tooltip"
                           data-placement="top"
                           data-original-title="Define el intervalo de tiempo que la encuesta estará activa">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </strong>
                </h2>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
            </header>

            <!-- widget div-->
            <div role="content">
                <div class="widget-body">
                    <div class="form-horizontal">
                        <!-- List survey publicly
                        <div class="form-group">
                            <label class="col-sm-6 control-label" for="public">
                                <strong>
                                    Enumerar la encuesta públicamente:
                                </strong>

                            </label>
                            <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="listpublic"
                                               class="onoffswitch-checkbox"
                                               id="ytlistpublic" <?php
//                                            if(!empty($viewData['survey']) &&
//                                                $viewData['survey']->listpublic == "Y"){
//                                                echo "checked";
//                                            }
                                            ?>>
                                        <label class="onoffswitch-label"
                                               for="ytlistpublic">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                            </div>
                        </div>
                        -->

                        <!-- Start date/time -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="startdate">
                                <strong>
                                    Inicio de la encuesta:
                                </strong>
                                <!--Start date/time:-->
                            </label>
                            <div class="col-sm-7 has-feedback">
                                <div id="startdate_datetimepicker"
                                        class="input-group date">
                                    <input id="startdate"
                                           class="form-control"
                                           type="text"
                                           name="startdate" readonly value="<?php
                                    if(!empty($viewData['survey']) &&
                                        !empty($viewData['survey']->startdate)){
                                        echo $viewData['survey']->startdate;
                                    }
                                    ?>">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>

                            <div class="col-sm-2 has-feedback">
                                <a class="btn btn-info btnClearDate"
                                   data-type-date="startdate"
                                   href="javascript:void(0);">
                                    Limpiar
                                </a>
                            </div>
                        </div>

                        <!-- Expiry date/time -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="expires">
                                <strong>
                                    <!--Fecha / hora de caducidad:-->
                                    Fecha de caducidad:
                                </strong>
                            </label>
                            <div class="col-sm-7 has-feedback">
                                <div id="expires_datetimepicker" class="input-group date">
                                    <input id="expires"
                                             class="form-control"
                                             type="text" value="<?php
                                    if(!empty($viewData['survey']) &&
                                        !empty($viewData['survey']->expiresdate)){
                                        echo $viewData['survey']->expiresdate;
                                    }
                                    ?>"
                                             name="expires" readonly>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-sm-2 has-feedback">
                                <a class="btn btn-info btnClearDate"
                                   data-type-date="expires"
                                   href="javascript:void(0);">
                                    Limpiar
                                </a>
                            </div>
                        </div>

                        <!-- Set cookie to prevent repeated participation -->
                        <!--<div class="form-group">
                            <label class="col-sm-6 control-label" for="usecookie">
                                <strong>
                                    Configure la cookie para evitar la participación repetida:
                                </strong>

                            </label>
                            <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="usecookie"
                                               class="onoffswitch-checkbox"
                                               id="ytusecookie" <?php
                                            if(!empty($viewData['survey']) && !empty($viewData['survey']->usecookie) &&
                                                $viewData['survey']->usecookie == "Y"){
                                                echo "checked";
                                            }
                                            ?>>
                                        <label class="onoffswitch-label"
                                               for="ytusecookie">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>
                            </div>
                        </div>-->

                        <!-- Use CAPTCHA for survey access -->
                        <!--<div class="form-group">
                            <label class="col-sm-6 control-label" for="usecaptcha_surveyaccess">
                                <strong>
                                    Use CAPTCHA para el acceso a la encuesta:
                                </strong>
                            </label>
                            <div class="col-sm-6">
                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="usecaptcha_surveyaccess"
                                               class="onoffswitch-checkbox"
                                               id="ytusecaptcha_surveyaccess" <?php
                                            if(!empty($viewData['survey']) && !empty($viewData['survey']->surveyaccess) &&
                                                $viewData['survey']->surveyaccess == "Y"){
                                                echo "checked";
                                            }
                                            ?>>
                                        <label class="onoffswitch-label"
                                               for="ytusecaptcha_surveyaccess">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                            </div>
                        </div>-->

                        <!-- Use CAPTCHA for registration
                        <div class="form-group">
                            <label class="col-sm-6 control-label" for="usecaptcha_registration">
                                <strong>
                                    Use CAPTCHA para el registro:
                                </strong>
                            </label>
                            <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="usecaptcha_registration"
                                               class="onoffswitch-checkbox"
                                               id="ytusecaptcha_registration">
                                        <label class="onoffswitch-label"
                                               for="ytusecaptcha_registration">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                            </div>
                        </div>-->

                        <!-- Use CAPTCHA for save and load
                        <div class="form-group">
                            <label class="col-sm-6 control-label" for="usecaptcha_saveandload">
                                <strong>
                                    Use CAPTCHA para guardar y cargar:
                                </strong>
                            </label>
                            <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="usecaptcha_saveandload"
                                               class="onoffswitch-checkbox"
                                               id="ytusecaptcha_saveandload">
                                        <label class="onoffswitch-label"
                                               for="ytusecaptcha_saveandload">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>