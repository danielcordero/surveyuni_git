<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $survey_survey_id
 * @property string $survey_title
 * @property string $survey_description
 * @property string $survey_welcometext
 * @property string $survey_endtext
 * @property string $survey_url
 * @property string $survey_urldescription
 * @property string $survey_email_invite_subj
 * @property string $survey_email_invite
 * @property string $survey_email_remind_subj
 * @property string $survey_email_remind
 * @property string $survey_email_register_subj
 * @property string $survey_email_register
 * @property string $survey_email_confirm_subj
 * @property string $survey_email_confirm
 * @property string $survey_attributecaptions
 * @property string $email_admin_notification_subj
 * @property string $email_admin_notification
 * @property string $email_admin_responses_subj
 * @property string $email_admin_responses
 */
class SurveySettings extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    public $timestamps = false;

    protected $table = 'surveys_settings';

    /**
     * @var array
     */
    protected $fillable = ['survey_survey_id', 'survey_title', 'survey_description', 'survey_welcometext', 'survey_endtext', 'survey_url', 'survey_urldescription', 'survey_email_invite_subj', 'survey_email_invite', 'survey_email_remind_subj', 'survey_email_remind', 'survey_email_register_subj', 'survey_email_register', 'survey_email_confirm_subj', 'survey_email_confirm', 'survey_attributecaptions', 'email_admin_notification_subj', 'email_admin_notification', 'email_admin_responses_subj', 'email_admin_responses'];

    public function survey()
    {
        return $this->belongTo('App\Survey');
    }
}
