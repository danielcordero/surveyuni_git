<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>My first survey</title>
	
	 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	@include('public.survey.default.partial.css')
</head>
<body>
	@yield('content')
	
	@include('public.globalValues')
	@include('public.survey.default.partial.js')
</body>
</html>