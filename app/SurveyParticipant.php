<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $survey_id
 * @property int $participant_id
 * @property string $created
 * @property string $modified
 */
class SurveyParticipant extends Model
{
    public $timestamps = false;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'survey_participant';

    /**
     * @var array
     */
    protected $fillable = ['additionalData','created', 'modified'];

}
