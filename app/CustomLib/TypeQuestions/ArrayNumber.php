<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;

class ArrayNumber extends BaseQuestion{

    public function saveConfig( $data ){
        $jsonToSave = new \stdClass();
        $subQuestionsR = $data->subQuestionsR;
        $subQuestionsC = $data->subQuestionsC;

        foreach ($subQuestionsR as $key => $value){
            $subQuestionsR[$key] = $this->cleanData( $value );
        }

        foreach ($subQuestionsC as $key => $value){
            $subQuestionsC[$key] = $this->cleanData( $value );
        }

        $jsonToSave->subQuestionsR = $subQuestionsR;
        $jsonToSave->subQuestionsC = $subQuestionsC;

        $question = Question::find( $data->question_id );
        $question->additionalData = json_encode( $jsonToSave );
        $question->save();
    }

    public function getConfig( $dataJson ){
        return json_decode( $dataJson, true );
    }

    public function validateAnswers( $partakerRequest ){

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message']= "Esta pregunta es requerida";
            $response['messages'][] = "Por favor complete todas las partes";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest ){

        $answers = !empty($partakerRequest['answers']) ? $partakerRequest['answers'] : [];
        $subQuestions = $partakerRequest['subQuestions'];
        $subQuestions = explode("|",$subQuestions);
        $messages = [];

        $fail = false;
        $keepAnswered = [];
        $withoutAnswered = [];

        foreach ($subQuestions as $key => $value){
            if(in_array($value, array_keys($answers))){
                $answers[$value] = $this->cleanData( $answers[$value] );
                if(!empty($answers[$value])){
                    $keepAnswered[$value] = $answers[$value];
                } else {
                    $withoutAnswered[] = $value;
                }
            } else {
                $withoutAnswered[] = $value;
            }
        }

        if(!empty($withoutAnswered) &&
            !empty($partakerRequest['mandatory']) &&
            $partakerRequest['mandatory'] == "Y"){
            $fail = true;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

//        echo "<pre>";
//        print_r( $partakerRequest );
//        exit;

        return $response;
    }

}
?>