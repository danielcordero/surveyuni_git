<?php

namespace App\CustomLib\Services;

use App\{SettingsGlobal};

class EmailService
{
    public static function getTabStructure($aDefaultTexts)
    {
        return array(
            'invitation' => array(
                'title' => "invitación",
                'subject' => "Asunto del correo electrónico de invitación:",
                'body' => "Cuerpo de correo electrónico de invitación:",
                'attachments' => "Invitation attachments:",
                'field' => array(
                    'subject' => 'survey_email_invite_subj',
                    'body' => 'survey_email_invite'
                ),
                'default' => array(
                    'subject' => $aDefaultTexts['invitation_subject'],
                    'body' => $aDefaultTexts['invitation']
                )
            ),
            'reminder' => array(
                'title' => "Recordatorio",
                'subject' => "Recordatorio del asunto del correo electrónico:",
                'body' => "Recordatorio cuerpo del correo electrónico:",
                'attachments' => "Archivos adjuntos de recordatorio:",
                'field' => array(
                    'subject' => 'survey_email_remind_subj',
                    'body' => 'survey_email_remind'
                ),
                'default' => array(
                    'subject' => $aDefaultTexts['reminder_subject'],
                    'body' => $aDefaultTexts['reminder']
                )
            ),
            'confirmation' => array(
                'title' => "Confirmación",
                'subject' => "Correo electrónico de confirmación:",
                'body' => "Cuerpo de correo electrónico de confirmación:",
                'attachments' => "Confirmation attachments:",
                'field' => array(
                    'subject' => 'survey_email_confirm_subj',
                    'body' => 'survey_email_confirm'
                ),
                'default' => array(
                    'subject' => $aDefaultTexts['confirmation_subject'],
                    'body' => $aDefaultTexts['confirmation']
                )
            ),
//            'registration' => array(
//                'title' => "Registration",
//                'subject' => "Registration email subject:",
//                'body' => "Registration email body:",
//                'attachments' => "Registration attachments:",
//                'field' => array(
//                    'subject' => 'surveyls_email_register_subj',
//                    'body' => 'surveyls_email_register'
//                ),
//                'default' => array(
//                    'subject' => $aDefaultTexts['registration_subject'],
//                    'body' => $aDefaultTexts['registration']
//                )
//            ),
//            'admin_notification' => array(
//                'title' => "Basic admin notification",
//                'subject' => "Basic admin notification subject:",
//                'body' => "Basic admin notification email body:",
//                'attachments' => "Basic notification attachments:",
//                'field' => array(
//                    'subject' => 'email_admin_notification_subj',
//                    'body' => 'email_admin_notification'
//                ),
//                'default' => array(
//                    'subject' => $aDefaultTexts['admin_notification_subject'],
//                    'body' => $aDefaultTexts['admin_notification']
//                )
//            ),
//            'admin_detailed_notification' => array(
//                'title' => "Detailed admin notification",
//                'subject' => "Detailed admin notification subject:",
//                'body' => "Detailed admin notification email body:",
//                'attachments' => "Detailed notification attachments:",
//                'field' => array(
//                    'subject' => 'email_admin_responses_subj',
//                    'body' => 'email_admin_responses'
//                ),
//                'default' => array(
//                    'subject' => $aDefaultTexts['admin_detailed_notification_subject'],
//                    'body' => $aDefaultTexts['admin_detailed_notification']
//                )
//            )
        );
    }

    public static function sendInvitation( $parameters )
    {

        require_once(app_path().'/ThirdParty/phpmailer/PHPMailerAutoload.php');
        require_once(app_path().'/ThirdParty/html2text/src/Html2Text.php');

        $resultEmail = [];
        //Credential SMTP
        $credentialSMTP = [];
        $credentialSMTP['Username'] = SettingsGlobal::find("siteadminemail")->stg_value;
        $credentialSMTP['Password'] = SettingsGlobal::find("emailsmtppassword")->stg_value;

        $mail = new \PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
//        $mail->SMTPOptions = array(
//            'ssl' => array(
//                'verify_peer' => false,
//                'verify_peer_name' => false,
//                'allow_self_signed' => true
//            )
//        );

//        echo "<pre>";
//        print_r( $credentialSMTP );
//        exit;
        $credentialSMTP['Username'] = "limesurveyuni@gmail.com";
        $credentialSMTP['Password'] = "limesurveyuni2018";

        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 4; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        //$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->SMTPSecure = "ssl";
        //$mail->Port = 587; // or 587
        $mail->Port = "465";
        $mail->IsHTML(true);
        $mail->Username = $credentialSMTP['Username'];
        $mail->Password = $credentialSMTP['Password'];
        $mail->SetFrom($credentialSMTP['Username']);

        $mail->addAddress($parameters['emailParticipant']);
        $mail->Subject = $parameters['subject'];
        $mail->Body = $parameters['messageHtml'];
        $html = new \Html2Text($parameters['messageHtml']);
        $mail->AltBody = $html->getText();

        ob_start();
        $maildebug = '<li>'. 'SMTP debug output:'.'</li><pre>'.strip_tags(ob_get_contents()).'</pre>';
        if(!$mail->Send()) {
            $resultEmail['success'] = false;
            $resultEmail['message'] = $mail->ErrorInfo;
        } else{
            $resultEmail['success'] = true;
            $resultEmail['message'] = "";
        }
        ob_end_clean();

        return $resultEmail;
    }
}
?>