<?php
//        echo $viewData['survey']->id;
//        echo "<br>";
//        echo $viewData['group']->id;
//        exit;
$testSurveys = config('custom.testSurvey');
?>

@extends('layouts.app')
@section('content')

    @include('elements.modals.confirm')

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Detalle de grupo de preguntas
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">

            @include('survey.questions.elements.topBarBreadcrumbs')
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-3">
                    @include('survey.questions.elements.treeGroupQuestions')
                </div>
                <div class="col-sm-9">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <ul class="demo-btns">

                                            @if(!in_array($viewData['survey']->id,$testSurveys))
                                                @if(!empty($viewData['survey']->active) &&
                                                $viewData['survey']->active == "N")
                                                    <li>
                                                        <a href="{{ route('editGroup',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id])}}"
                                                           class="btn btn-primary">
                                                            <i class="fa fa-pencil"></i>
                                                            Editar grupo
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <button
                                                                data-toggle="modal"
                                                                data-target="#mdConfirm"
                                                                class="btn btn-danger btnDelete"
                                                                role="button"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                data-question-id="{{ $viewData['group']->id }}"
                                                                title="Eliminar">
                                                            <i class="fa fa-trash-o">
                                                            </i>
                                                            Eliminar group
                                                        </button>

                                                        <form method="post"
                                                              role="form"
                                                              id="<?php echo "frDelete_" . $viewData['group']->id; ?>"
                                                              class="hideElement <?php echo "frDelete_" . $viewData['group']->id; ?>"
                                                              action="{{ route('groupDelete',[$viewData['group']->id])}}">
                                                            {{ method_field('DELETE') }}
                                                            <div class="form-group">
                                                                <input type="hidden"
                                                                       name="_token"
                                                                       value="{{ csrf_token() }}">
                                                            </div>
                                                            <input
                                                                    type="submit"
                                                                    value="Enviar"/>
                                                        </form>
                                                    </li>
                                                @endif
                                            @endif
                                            <li>
                                                <a href="{{ route('survey.redirectpreviewmode',[$viewData['survey']->id])}}"
                                                   class="btn bg-color-orange txt-color-white" target="_blank">
                                                    <i class="fa fa-eye"></i>
                                                    Vista previa encuesta
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('groupviewpreviewpage',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id])}}"
                                                   target="_blank"
                                                   class="btn bg-color-blueDark txt-color-white">
                                                    <i class="fa fa-eye"></i>
                                                    Vista previa grupo
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            Sección resumen
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Título
                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php echo strip_tags($viewData['group']->name); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Descripción
                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php echo strip_tags($viewData['group']->description); ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                    <form action="{{ route('saveConfigGroup',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id] )}}"
                          role="form" method="post" class="currentForm">
                        {{ csrf_field() }}
                        <article class="well">
                            <div class="inbox-info-bar">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h3>
                                            <strong>
                                                Ordenar preguntas
                                            </strong>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <br>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="table-responsive">

                                            <table class="table table-striped" id="sortorderQuestions">
                                                <thead>
                                                <tr>
                                                    <th>Posición</th>
                                                    <th>Código</th>
                                                    <th>Pregunta</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if(!empty($viewData['group']->questions)){
                                                $questions = $viewData['group']->questions;
                                                $loopI = 0;
                                                foreach ($questions as $question){
                                                $loopI = $loopI + 1;
                                                ?>
                                                <tr id="<?php echo $question->id; ?>">
                                                    <td>
                                                        <input type="hidden"
                                                               value="{{$question->id}}"
                                                               name="questions[<?php echo $question->id; ?>]">
                                                        <span class="cursorMove">
                                                            <i class="fa fa-bars fa-2x cursorMove" aria-hidden="true"></i>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <?php echo $question->id; ?>
                                                    </td>
                                                    <td>
                                                        {!! $question->title !!}
                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                }
                                                ?>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <input type="hidden"
                               value="{{$viewData['group']->id}}"
                               name="group_id">
                        <input type="submit" value="Enviar" class="hideElement" />
                        <input type="hidden"
                               value="{{$viewData['survey']->id}}"
                               name="survey_id">
                        <input type="submit" value="Enviar" class="hideElement" />
                    </form>
                </div>
            </div>
        </section>
    </div>


    @push('scripts')
    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/QuestionsModule.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/GroupsModule.js"></script>
    @endpush
@stop