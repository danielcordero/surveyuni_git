@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Gestionar participantes de la encuesta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-danger pull-right"
                                   href="{{ route("participants.config" , ['surveyId'=>$viewData['survey']->id]) }}">
                                    <i class="fa fa-sign-out"></i>
                                    Cerrar
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <div class="well message-box message-box-error text-center">
                                    @if(!empty($viewData['headerFaild']))
                                        <h2 class="text-danger">
                                            <strong>
                                                El archivo no tiene las cabeceras requiridas
                                            </strong>
                                        </h2>
                                    @endif

                                    @if(empty($viewData['aTokenListArray']) ||
                                        $viewData['iRecordImported'] == 0)
                                        <h2 class="text-danger">
                                            <strong>
                                                Error al cargar algunos registros
                                            </strong>
                                        </h2>
                                    @else
                                        <h2 class="text-success">
                                            <h2 class="text-success">
                                                <strong>
                                                    El archivo CSV se ha cargado exitosamente
                                                </strong>
                                            </h2>
                                        </h2>
                                    @endif
                                    <p>
                                    </p><ul class="list-unstyled">
                                        <li>{{$viewData['iRecordCount']}} regiistros en el CSV</li>
                                        <li>{{$viewData['iRecordOk']}} registros que cumplen la cantidad minima de registros</li>
                                        <li>{{$viewData['iRecordImported']}} registros importados</li>
                                    </ul>
                                    <p></p>

                                    @if(!empty($viewData['aDuplicateList']) ||
                                        !empty($viewData['aInvalidEmailList']))
                                            <h2 class="text-warning">
                                                <strong>Advertencias</strong>
                                            </h2>
                                            <p>
                                            </p>
                                            <ul class="list-unstyled">
                                                @if(!empty($viewData['aDuplicateList']))
                                                    <li>
                                                        {{count($viewData['aDuplicateList'])}}
                                                        Registror duplicados eliminados
                                                        [<a href="#"
                                                            onclick="$('#duplicateslist').toggle();">List</a>]
                                                        <div class="badtokenlist well"
                                                             id="duplicateslist"
                                                             style="display: none;">
                                                            <ul class="list-unstyled">
                                                                <?php foreach($viewData['aDuplicateList'] as $value){
                                                                    ?>
                                                                    <li>
                                                                        {{$value}}
                                                                    </li>
                                                                <?php
                                                                } ?>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                @endif

                                                @if(!empty($viewData['aInvalidEmailList']))
                                                        <li>
                                                            {{count($viewData['aInvalidEmailList'])}}
                                                            Registror con inválidos correos eliminados
                                                            [<a href="#"
                                                                onclick="$('#aInvalidEmailList').toggle();">List</a>]
                                                            <div class="badtokenlist well"
                                                                 id="aInvalidEmailList"
                                                                 style="display: none;">
                                                                <ul class="list-unstyled">
                                                                    <?php foreach($viewData['aInvalidEmailList'] as $value){
                                                                    ?>
                                                                    <li>
                                                                        {{$value}}
                                                                    </li>
                                                                    <?php
                                                                    } ?>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                @endif
                                            </ul>

                                            <p></p>
                                            <br>
                                            <p>
                                                <a href="{{ route("participants.listAll" , ['surveyId'=>$viewData['survey']->id]) }}"
                                                   class="btn btn-large btn-primary"
                                                   type="button"
                                                   value="Browse participants">
                                                    Ver participantes agregados
                                                </a>
                                            </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
@stop