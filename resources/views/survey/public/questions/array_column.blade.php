<?php
$allQuestions = "";
//    echo "<pre>";
//    print_r($resultQuestion);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- array_column -->
    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>
                <!-- widget content -->
                <div class="widget-body smart-form">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Fila / Columna</th>
                                <?php
                                foreach ($config["subQuestionsC"] as $key => $value){
                                    $allQuestions = $allQuestions . '|' .$key;
                                ?>
                                <th class="text-center">
                                    <span class="label <?php
                                    if(!empty($resultQuestion['withoutAnswered']) &&
                                        in_array($key, $resultQuestion['withoutAnswered'])
                                        && $question->mandatory == "Y"){
                                        echo "labelCommonError";
                                    }
                                    ?>">
                                        <?php
                                    echo $value;
                                    ?>
                                        </span>
                                </th>
                                <?php
                                }
                                ?>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($config["subQuestionsR"] as $key1 => $value1){
                            ?>
                            <tr>
                                <th>
                                    <?php
                                    echo $value1;
                                    ?>
                                </th>
                                <?php
                                foreach ($config["subQuestionsC"] as $key2 => $value2){

                                ?>
                                <td class="text-center">
                                    <label class="radio inline">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key2; ?>]"
                                               value="<?php echo $key1; ?>" <?php
                                            if(!empty($resultQuestion['keepAnswered']) &&
                                                !empty($resultQuestion['keepAnswered'][$key2])){
                                                if($resultQuestion['keepAnswered'][$key2] == $key1){
                                                    echo "checked";
                                                }
                                            }
                                            ?>>
                                        <i></i><strong>
                                        </strong>
                                    </label>
                                </td>
                                <?php
                                }
                                ?>
                            </tr>
                            <?php
                            }
                            ?>
                            <?php
                            if(!empty($question->mandatory) && $question->mandatory == "N"){
                            ?>
                            <tr>
                                <th class="">
                                    Sin respuesta
                                </th>

                                <?php
                                foreach ($config["subQuestionsC"] as $key2 => $value2){
                                ?>
                                <td class="text-center">
                                    <label class="radio inline">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key2; ?>]"
                                               value="notanswer" <?php
                                            if(!empty($resultQuestion)){
                                                if(!empty($resultQuestion['keepAnswered']) &&
                                                    !empty($resultQuestion['keepAnswered'][$key2])){
                                                    if($resultQuestion['keepAnswered'][$key2] == 'notanswer'){
                                                        echo "checked";
                                                    }
                                                }
                                            } else {
                                                echo "checked";
                                            }
                                            ?>>
                                        <i></i><strong>
                                        </strong>
                                    </label>
                                </td>
                                <?php
                                }
                                ?>

                            </tr>

                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                @include('survey.public.questions.littlepartial.help')

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[subQuestions]"
                       id="question_<?php echo $question->id; ?>[subQuestions]"
                       value="<?php echo trim($allQuestions, '|'); ?>">
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>