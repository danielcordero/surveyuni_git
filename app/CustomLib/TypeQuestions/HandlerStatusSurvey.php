<?php

namespace App\CustomLib\TypeQuestions;
use App\SurveyParticipant;


class HandlerStatusSurvey{

    public function saveBySurvey($answers, $partakerRequest)
    {
        $paramters = ['id' => $partakerRequest->participantId];
        $surveyParticipanAnswers = SurveyParticipant::where($paramters)->first();

        if(!empty($surveyParticipanAnswers)){
            $answersSaved = json_decode($surveyParticipanAnswers->additionalData, true);
            $answersSavedAll = [];

            /* Old way
             * if(!empty($answersSaved['answers'])){
                $answersSavedAll = $answersSaved['answers'];
                foreach ($answers as $key => $value){
                    $answersSavedAll[$key] = $value;
                }
            } else {
                foreach ($answers as $key => $value){
                    $answersSavedAll[$key] = $value;
                }
            }*/
            if(!empty($answersSaved['answers'])){
                $answersSavedAll = $answersSaved['answers'];
            }

            foreach ($answers as $key => $value){
                $answersSavedAll[$key] = $value;
            }

            $statusSurvey = new \stdClass();
            $statusSurvey->currentGroup = $partakerRequest->current_group;
            $statusSurvey->answers = $answersSavedAll;

            $surveyParticipanAnswers->additionalData = json_encode( $statusSurvey );
            $surveyParticipanAnswers->save();
        }
//        else {
//            $statusSurvey = new \stdClass();
//            $statusSurvey->currentGroup = $partakerRequest->current_group;
//            $statusSurvey->answers = $answers;
//
//            $surveyParticipanAnswers = new SurveyParticipanAnswers();
//            $surveyParticipanAnswers->survey_id = 7;
//            $surveyParticipanAnswers->participant_id = 170;
//            $surveyParticipanAnswers->additionalData = json_encode($statusSurvey);
//            $surveyParticipanAnswers->save();
//        }

    }

    public function getBySurvey( $requestParticipant, $configSurvey )
    {
        $answers = [];
        $paramters = ['id' => $requestParticipant->participantId];

        $surveyParticipanAnswers = SurveyParticipant::where($paramters)->first();
        if(!empty($surveyParticipanAnswers)){
            $answers = json_decode( $surveyParticipanAnswers->additionalData, true );
        }

        if(!empty($answers['answers']) &&
           !empty($configSurvey['config']['questionsIds'])){
            $surveyQuestionsId = $configSurvey['config']['questionsIds'];
            $questionsId = array_keys($answers['answers']);

            if($questionsId != $surveyQuestionsId){
                $answersQuestions = $answers['answers'];
                $answersQuestions = $this->diffQuestions($answersQuestions, $surveyQuestionsId);
                $answers['answers'] = $answersQuestions;
                $surveyParticipanAnswers->additionalData = json_encode( $answers );
                $surveyParticipanAnswers->save();
            }
        }
        return $answers;
    }

    public function diffQuestions( $statusQuestions , $questionsId )
    {
        $newQuestions = [];
        foreach ($statusQuestions as $key => $value)
        {
            if(in_array($key, $questionsId))
            {
                $newQuestions[$key] = $value;
            }
        }
        return $newQuestions;
    }
}
?>