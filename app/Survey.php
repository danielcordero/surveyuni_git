<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Group;

/**
 * @property int $id
 * @property int $user_id
 * @property string $active
 * @property string $title
 * @property string $description
 * @property string $welcometext
 * @property string $endtext
 * @property string $url
 * @property string $urldescription
 * @property string $email_invite_subj
 * @property string $email_invite
 * @property string $email_remind_subj
 * @property string $email_remind
 * @property string $email_register_subj
 * @property string $email_register
 * @property string $email_confirm_subj
 * @property string $email_confirm
 * @property int $dateformat
 * @property string $attributecaptions
 * @property string $email_admin_notification_subj
 * @property string $email_admin_notification
 * @property string $email_admin_responses_subj
 * @property string $email_admin_responses
 * @property string $created_at
 * @property string $updated_at
 */
class Survey extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'active', 'title', 'description', 'welcometext', 'endtext', 'url', 'urldescription', 'email_invite_subj', 'email_invite', 'email_remind_subj', 'email_remind', 'email_register_subj', 'email_register', 'email_confirm_subj', 'email_confirm', 'dateformat', 'attributecaptions', 'email_admin_notification_subj', 'email_admin_notification', 'email_admin_responses_subj', 'email_admin_responses', 'additionalData','all_invitation_sent','first_block_sent','created_at', 'updated_at'];

    public function groups() {
        return $this->hasMany('App\Group','survey_id','id');
    }


    public function participants() {
        return $this->belongsToMany('App\Participant',
            'survey_participant',
            'survey_id',
            'participant_id')
                ->withPivot('id',"firstname","lastname","sent","remindersent","remindercount","completed",'token','validfrom','validuntil','customFields');
    }

    public function participantsCount()
    {
        return $this->belongsToMany('App\Participant','survey_participant')
            ->selectRaw('count(participants.id) as aggregate')
            ->groupBy('pivot_participant_id');
    }

    // accessor for easier fetching the count
    public function getParticipantsCountAttribute()
    {
        if ( ! array_key_exists('participantsCount', $this->relations)) $this->load('participantsCount');

        $related = $this->getRelation('participantsCount')->first();

        return ($related) ? $related->aggregate : 0;
    }

    public function surveysetting()
    {
        return $this->hasOne('App\SurveySettings','survey_survey_id','id');
    }

}
