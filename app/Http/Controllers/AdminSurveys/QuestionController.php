<?php

namespace App\Http\Controllers\AdminSurveys;

use Illuminate\Support\Facades\Route;
use App\CustomLib\Services\SurveyService;
use App\CustomLib\Helper\HelperServices;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\GenericMessages;
use App\CustomLib\Services\{QuestionService,GroupService};
use App\Survey;
use App\Group;
use App\Question;
use Exception;
use Session;


class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckOwnSurvey');
        $this->middleware('CheckExistResource');
    }

    public function index(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyInfo = Survey::find( $surveyId );
        $viewData['survey'] = $surveyInfo;
        return view('survey.questions.createQuestion')
                    ->with('viewData',$viewData);
    }

    public function groupQ(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyInfo = Survey::find( $surveyId );
        $viewData['survey'] = $surveyInfo;
        return view('survey.questions.createGroup')
                    ->with('viewData',$viewData);
    }

    public function storeGroup(Request $request)
    {
        $surveyId = (int)$request->survey_id;
        $surveyInfo = Survey::with([
            'groups'
        ])->find($surveyId);

        $group = new Group();
        $group->survey_id = $surveyId;
        $group->name = $request->txtTitle;
        $group->title = $request->txtTitle;
        $group->description = $request->txADescription;
        $group->sortorder = $surveyInfo->groups->count() + 1;
        $group->created_by =\Auth::user()->id;
        $lastId = $group->save();

        Session::flash('success', GenericMessages::$all['successOperation']);
        return redirect()->route('questionGroup',['surveyId'=>$request->survey_id]);
    }

    public function updateGroup(Request $request, $id)
    {
        $groupId = (int)$request->group_id;
        $group = Group::find($groupId);
        $group->name = $request->txtTitle;
        $group->title = $request->txtTitle;
        $group->description = $request->txADescription;
        $group->save();

        Session::flash('success', GenericMessages::$all['successOperation']);

        $parameters = [
            'surveyId'=>$request->survey_id,
            'groupId'=>$request->group_id
        ];
        return redirect()->route('groupDetail',$parameters);
    }

    public function editGroup(Request $request){

        $surveyId = (int)$request->surveyId;
        $groupId = (int)$request->groupId;

        $surveyInfo = Survey::find( $surveyId );
        $groupInfo = Group::find( $groupId );

        $viewData['survey'] = $surveyInfo;
        $viewData['group'] = $groupInfo;
        return view('survey.questions.createGroup')->with('viewData',$viewData);
    }

    public function storeQuestion(Request $request){

        $success = true;

        DB::beginTransaction();
        try {

            $groupId = (int)$request->groupsQuestion;
            $questions = QuestionService::getByGroupId($groupId, true);

            if(!empty($request->positionQ) && $request->positionQ == "S"){
                $orderN = 1;
            }

            if(!empty($request->positionQ) && $request->positionQ == "E"){
                $orderN = count($questions) + 1;
            }

            if(empty($orderN)){
                $questionToSwapId = (int)$request->positionQ;
                $questionToSwap = Question::find($questionToSwapId);
                $orderN = $questionToSwap->sortorder;
                $questionToSwap->sortorder = count($questions) + 1;
                $questionToSwap->save();
            }

            $question = new Question();
            $question->sid = (int)$request->survey_id;
            $question->gid = $groupId;
            $question->type = $request->questionType;
            $question->title = $request->txtQuestion;
            $question->help = $request->txtHelpQuestion;
            $question->question_yes_no = $request->txtQuestionYesNo;

            $question->sortorder = $orderN;
            $question->mandatory = !empty($request->mandatoryQuestion) ? "Y" : "N";
            $question->hide = !empty($request->hideQuestion) ? "Y" : "N";
            $question->show_help = !empty($request->showHelpQuestion) ? "Y" : "N";
            $question->full_width = !empty($request->fullWidth) ? "Y" : "N";
            $question->has_condition = !empty($request->hasCondition) ? "Y" : "N";
            $question->created_by =\Auth::user()->id;
            $question->save();
            $lastId = $question->id;

//            echo "<pre>";
//            print_r( Question::find( $lastId )->toArray() );
//            exit;

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }

        $parameters = [
            'surveyId'=>(int)$request->survey_id,
            'groupId'=>$groupId,
            'questionId'=>$lastId,
        ];
        return redirect()->route('configQuestion',$parameters);
    }

    public function showDetailGroup(Request $request){

        $surveyId = (int)$request->surveyId;
        $groupId = (int)$request->groupId;
        $surveyInfo = Survey::find( $surveyId );
        //$groupInfo = Group::with('') find($groupId);

        $groupInfo = Group::with([
            'questions' => function($q){
                $q->orderBy('sortorder', 'asc');
            }
        ])->find($groupId);

        $viewData['survey'] = $surveyInfo;
        $viewData['group'] = $groupInfo;

        return view('survey.questions.detailGroup')->with('viewData',$viewData);
    }

    public function showDetail(Request $request){

        $surveyId = (int)$request->surveyId;
        $groupId = (int)$request->groupId;
        $questionId = (int)$request->questionId;
        $surveyInfo = Survey::find( $surveyId );
        $groupInfo = Group::find($groupId);
        $questionInfo = Question::with('group')->find($questionId);
        $viewData['survey'] = $surveyInfo;
        $viewData['group'] = $groupInfo;
        $viewData['question'] = $questionInfo;

//        echo "<pre>";
//        echo "I'm here";
//        exit;

        return view('survey.questions.detailQuestion')
                ->with('viewData',$viewData);
    }

    public function showDetailSurvey(Request $request){

        $surveyId = (int)$request->surveyId;
        $surveyInfo = Survey::with([
            'groups' => function($q){
                $q->orderBy('sortorder', 'asc');
            },
            'groups.questions' => function($q){

            }
        ])->find($surveyId);

        $totalQuestions = 0;
        $totalGroup = 0;
        foreach ($surveyInfo->groups as $group){
            $totalGroup++;
            foreach ($group->questions as $question){
                $totalQuestions++;
            }
        }

        $settings = json_decode($surveyInfo->additionalData);
        if(!empty($settings)){
            foreach ($settings as $key => $value){
                $surveyInfo->{$key} = $value;
            }
        }

        $surveyInfo->totalQuestions = $totalQuestions;
        $surveyInfo->totalGroups = $totalGroup;
        $viewData['survey'] = $surveyInfo;

        return view('survey.questions.detailSurvey')->with('viewData',$viewData);
    }

    public function edit(Request $request){

        $surveyId = (int)$request->surveyId;
        $groupId = (int)$request->groupId;
        $questionId = (int)$request->questionId;

        $surveyInfo = Survey::find( $surveyId );
        $groupInfo = Group::with([
            'questions' => function($q) {
                $q->orderBy('sortorder', 'asc');
            }
        ])->find($groupId);
        $questionInfo = Question::with('group')->find($questionId);

        $viewData['survey'] = $surveyInfo;
        $viewData['group'] = $groupInfo;
        $viewData['question'] = $questionInfo;

        return view('survey.questions.createQuestion')->with('viewData',$viewData);
    }

    public function update(Request $request){

        $success = true;
        DB::beginTransaction();
        try {
            $previewGroup = (int)$request->preview_group_id;
            $newGroupId = (int)$request->groupId;
            $questionId = (int)$request->questionId;
            $question = Question::find($questionId);
            $sortOrder = $question->sortorder;

            $groupQuestions = QuestionService::getByGroupId($newGroupId, true);
            $firsQuestion = $groupQuestions[0];
            $lastQuestion = $groupQuestions[count($groupQuestions) - 1]; //Get last item

            if(!empty($request->positionQ) && is_int((int)$request->positionQ)){
                $swapQuestion = Question::find($request->positionQ);
                $sortOrder = $swapQuestion->sortorder;
                $swapQuestion->sortorder = $question->sortorder;
                $swapQuestion->save();
            }
            //        echo $request->positionQ;
            //        exit;

            //        if($sortOrder != $firsQuestion['sortorder']
            //            && $sortOrder != $lastQuestion['sortorder']){
            //            $getLessQuestions = true;
            //            $sumOrder = true;
            //            $sortOrder = 1;
            //            //Add it to the end
            //            if(!empty($request->positionQ) && $request->positionQ == "E"){
            //                $getLessQuestions = false;
            //                $sumOrder = false;
            //                $sortOrder = count($groupQuestions);
            //            }
            //            $questionCondition = QuestionService::getByOrderCondition($newGroupId, $question->sortorder, $getLessQuestions, true);
            //            QuestionService::changeSortOrder($questionCondition, $sumOrder);
            //        }

            //        if(!empty($request->positionQ) && $request->positionQ == "E"){
            //            $questionCondition = QuestionService::getByOrderCondition($newGroupId, $question->sortorder, false, true);
            //            $sortOrder = count($groupQuestions);
            //            $sumOrder = false;
            //            QuestionService::changeSortOrder($questionCondition, $sumOrder);
            //        }
            if(!empty($request->positionQ) && $request->positionQ == "F"){
                $questionCondition = QuestionService::getByOrderCondition($newGroupId, $question->sortorder, true, true);
                $sortOrder = 1;
                $sumOrder = true;
                QuestionService::changeSortOrder($questionCondition, $sumOrder);
            }
            //        if($previewGroup == $newGroupId){
            ////            QuestionService::getByOrderCondition($newGroupId,);
            //        }
            $question->type = $request->questionType;
            $question->title = $request->txtQuestion;
            $question->help = $request->txtHelpQuestion;
            $question->question_yes_no = $request->txtQuestionYesNo;
            $question->sortorder = $sortOrder;
            $question->mandatory = !empty($request->mandatoryQuestion) ? "Y" : "N";
            $question->hide = !empty($request->hideQuestion) ? "Y" : "N";
            $question->show_help = !empty($request->showHelpQuestion) ? "Y" : "N";
            $question->full_width = !empty($request->fullWidth) ? "Y" : "N";
            $question->has_condition = !empty($request->hasCondition) ? "Y" : "N";
            $question->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }


        $parameters = [
            'surveyId'=>$request->surveyId,
            'groupId'=>$request->groupId,
            'questionId'=>$request->questionId
        ];
        return redirect()->route('configQuestion',$parameters);
    }

    public function destroy( $questionId )
    {
        $success = true;
        \DB::beginTransaction();
        try {
            $questionId = (int)$questionId;
            $questionInfo = Question::find( $questionId );
            $groupId = (int)$questionInfo->gid;
            $questions = QuestionService::getByOrderCondition( $groupId, $questionInfo->sortorder, false);
            $groupInfo = Group::find( $groupId );

            if(!empty($questions)){
                if(!QuestionService::changeSortOrder($questions, false)){
                    throw new Exception();
                }
            }
            $questionInfo->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }

        $parameters = [
            'surveyId'=>$groupInfo->survey_id,
            'groupId'=>$groupId
        ];

        return redirect()->route('groupDetail',$parameters);
    }

    public function storeSurveyConfig(Request $request)
    {

        $success = true;
        \DB::beginTransaction();
        try{
            if(!empty($request->groups)){
                $groups = $request->groups;
                $groupS = [];
                $groupsS = [];
                $counter = 0;
                foreach($groups as $key => $groupId)
                {
                    $counter = $counter + 1;
                    $groupS['id'] = $key;
                    $groupS['sortorder'] = $counter;
                    $groupsS[] = $groupS;
                }
                $result = HelperServices::updateBulk('groups',$groupsS,'id');
                if(empty($result)){
                    throw new Exception();
                }
            }
            \DB::commit();
        }catch (\Exception $e){
            \DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }

        $parameters = [
            'surveyId'=>$request->survey_id
        ];
        return redirect()->route('surveyDetail',$parameters);
    }

    public function viewPreview($surveyId, $groupId, $questionId)
    {
        $groupId = HelperServices::sanitize_int( $groupId );
        $groupService = new GroupService();
        $group = $groupService->getById( $groupId );
        $question = Question::find($questionId);
        $viewData['groupInfo'] = $group;
        $viewData['question'] = $question;

//        echo "<pre>";
//        print_r($viewData['question']->toArray());
//        exit;

        return view('survey.public.questionpreview')
            ->with('viewData',$viewData);
    }

    public function checkExistResource($parameters)
    {
        if(array_key_exists("surveyId",$parameters)){
            $where = [['id',$parameters['surveyId']]];
            $resource = Survey::where($where)->first();
            if(empty($resource)){
                return view('elements.generalpages.page404');
            }
        }

        if(array_key_exists("groupId",$parameters)){
            $where = [['id',$parameters['groupId']]];
            $resource = Group::where($where)->first();
            if(empty($resource)){
                return view('elements.generalpages.page404');
            }
        }

        if(array_key_exists("questionId",$parameters)){
            $where = [['id',$parameters['questionId']]];
            $resource = Question::where($where)->first();
            if(empty($resource)){
//                echo "test tete";
//                exit;
                return view('elements.generalpages.page404');
            }
        }
    }

}