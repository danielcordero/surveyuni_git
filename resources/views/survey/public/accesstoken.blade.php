<?php
    use \App\CustomLib\Services\SurveyPublic;
    $surveyPublic = new SurveyPublic;
?>
@extends('survey.public.layout')
@section('title',$viewData['config']['survey']->title)
@section('content')
    <form action="{{ route('public.index',
                ['surveyId'=> $viewData['config']['survey']->id ]) }}" method="post">

        <div class="row">
            <br>
            @if($surveyPublic->isModePreview())
                <div class="col-md-12 text-center titleSurvey">
                    <div class="alert alert-warning alert-dismissible fade in alert-dismissible"
                         role="alert">
                        <!--<button type="button"
                                data-dismiss="alert"
                                aria-label="Close" class="close">
                            <span aria-hidden="true">×</span></button>-->
                        <h3>
                            Está encuesta actualmente no está activa. Usted no será capaz de guardar sus respuestas.
                        </h3>
                    </div>
                </div>
            @endif

            <div class="col-md-12">
                <h2>
                    <strong>
                        Para participar en esta restringida encuesta, usted necesita un token válido.
                    </strong>
                </h2>
            </div>


            <div class="col-md-12">
                <div class="well container-fluid">
                    <p class="text-info">
                        <strong>
                            Si usted ha recibido un token, por favor ingreselo en caja de texto y click en continuar.
                        </strong>
                    </p>

                    @if(!empty($viewData['wrongtoken']))
                        <div class="alert alert-danger" role="alert">
                            El token que ha proporcionado no es válido o ya se ha utilizado.
                            <br>
                            Para más información, póngase en contacto con el Administrador <span class="mailto">
                                (<a href="mailto:your-email@example.net">your-email@example.net</a>)
                            </span>
                        </div>
                    @endif

                    <div class="form-token">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="loadname">
                                <small class=" superset  text-danger asterisk fa fa-asterisk small "
                                       aria-hidden="true">
                                </small>
                                Token:
                                <span class="sr-only text-danger asterisk ">
                                    ( Mandatory )
                                <span>
                                </span></span>
                            </label>
                            <div class="  load-survey-input input-cell  col-sm-7">
                                <input class="form-control"
                                       type="password"
                                       id="token"
                                       name="token" value="" required="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- PRESENT THE NAVIGATOR -->
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 text-right">
                <button type="submit"
                        name="currentView"
                        value="accesstokenview"
                        class="btn btn-success btn-lg">
                    Continuar <i class="glyphicon glyphicon-chevron-right"></i>
                </button>
            </div>
        </div>

        <input name="survey_id"
               type="hidden"
               value="<?php echo $viewData['config']['survey']->id; ?>">

        <input name="current_group"
               type="hidden"
               value="0">
    </form>
@stop