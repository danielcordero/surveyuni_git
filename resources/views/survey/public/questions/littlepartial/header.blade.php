<header class="headerWidget">
    <h2>
        @if($question->mandatory == "Y")
            <small class="text-danger fa fa-asterisk small redAsterisk"
                   aria-hidden="true">
            </small>
        @endif
        <strong>

            @if(!empty($viewData['config']['survey']->showindexquestions) &&
                $viewData['config']['survey']->showindexquestions == "Y")
                <?php
                    $indexQuestion = array_search($question->id, $viewData['config']['questionsIds']);
                    echo $indexQuestion + 1;
                ?>

            @endif

            <?php
                echo strip_tags($question->title);
            ?>
            <?php
//                $questionsType = config('custom.questionsType');
//                echo $questionsType[$question->type];
            ?>
        </strong>
    </h2>
</header>