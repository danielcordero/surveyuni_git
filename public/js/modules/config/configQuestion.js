/**
 * Created by cesar on 12/25/2017.
 */
var configQuestion = {};
(function(){
    //private variable
    var that = this;
    this.init = function(){
        loadByType();
    };
    
    function loadByType(){
        var typeQuestion = $("#question_type").val(),
            questionId = $("#question_id").val();

        switch(typeQuestion){
            case "multiple_choice":
            case "drop_down_list":
            case "numeric_input_several":
            case "list_radio":
            case "list_radio_comments":
            case "multiple_choice_comments":
            case "free_text_several_line":
            case "array10":
            case "array5":
            case "array_yes_no":
            case "array_i_s_d":
            case "ranking":
                subQuestions.init(questionId);
                break;
            case "array":
            case "array_column":
            case "array_number":
            case "array_text":
                subQuestionsArray.init(questionId);
                break;
        }

        $("#btnSave").on("click", function(e){
            // switch(typeQuestion){
            //     case "multiple_choice":
            //         multipleChoise.prepareConfigToSave($(".currentForm"));
            //         break;
            // }
            $(".currentForm").find('[type="submit"]').trigger("click");
        });
    }


    $(document).ready(function(){
        that.init();
    });

}).call(configQuestion);