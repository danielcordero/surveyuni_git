<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $sid
 * @property int $gid
 * @property string $type
 * @property string $title
 * @property string $help
 * @property string $mandatory
 * @property int $sortorder
 * @property string $additionalData
 * @property string $created_at
 * @property string $updated_at
 */
class Question extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['sid', 'gid', 'type', 'title', 'help','question_yes_no', 'mandatory','hide','show_help','full_width','has_condition','sortorder','created_by', 'additionalData', 'created_at', 'updated_at'];

    public function group()
    {
        return $this->belongsTo('App\Group','gid');
    }

}
