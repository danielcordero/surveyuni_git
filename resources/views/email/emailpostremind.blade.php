<?php
//    echo "<pre>";
//    print_r( $viewData['survey'] );
//    exit;
?>

@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Enviando recordatorio ...
                    </strong>
                </h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            <span class="fa fa-envelope-o"></span>
                                            Enviando recordatorio ...
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="jumbotron message-box">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="outputEmail">
                                                <?php echo $viewData['tokenoutput']; ?>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(!empty($viewData['isThereError']))
                                                <strong
                                                        class="result warning text-warning textRed">
                                                    Algunos correos electrónicos no se
                                                    enviaron porque el servidor no aceptó
                                                    los correos electrónicos o se produjo
                                                    algún otro error.
                                                </strong>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-12">
                                            <a href="{{ route("participants.config",[$viewData['survey']->id]) }}"
                                               class="btn btn-primary btn-lg">
                                                OK
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
    @push('scripts')

    @endpush
@stop