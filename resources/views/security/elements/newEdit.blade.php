<?php

//use \App\CustomLib\TypeQuestions\BaseQuestion;
//$helperHtml = new HelperHtml();
namespace App\CustomLib\Helper;
//use \App\CustomLib\Helper\HelperHtml;
use \stdClass;

//echo "<pre>";
//$helperHtml = new HelperHtml();
//print_r( $helperHtml );
//exit;


if (empty($viewData)) {
    $viewData['survey'] = new \stdClass();
}

?>
<section id="content">

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa-fw fa fa-puzzle-piece"></i>
                <strong>
                    <?php
                        echo !empty($viewData['user']) ? "Editar" : "Crear";
                    ?>
                    usuario
                </strong>
            </h1>
        </div>
    </div>

    <section id="widget-grid" class="">
        <div class="well">

            <div class="row">
                <div class="col-md-12">
                    <p class="text-right">
                        <button type="button" id="btnSave"
                                class="btn btn-primary openMainModal">
                            <i class="fa fa-save"></i> Guardar
                        </button>
                        <a class="btn btn-danger"
                           href="{{ route('adminuser') }}">
                            Cancelar
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </a>
                    </p>
                </div>
            </div>
            <hr>
            @if(!empty($viewData['user']) && !empty($viewData['user']->id))
                <form action="{{ route('admin.updateuser') }}"
                      role="form" method="post" id="frmNewEditUser">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    @else
                        <form action="{{ route('admin.createuser') }}"
                              method="post" id="frmNewEditUser" class="">
                            {{ csrf_field() }}
                            @endif

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="jarviswidget jarviswidget-color-greenLight"
                                         widget-attstyle="jarviswidget-color-greenLight"
                                         data-widget-editbutton="false"
                                         data-widget-custombutton="false"
                                         data-widget-deletebutton="false"
                                         data-widget-sortable="false"
                                         data-widget-collapsed="false"
                                         data-widget-fullscreenbutton="false">

                                        <header role="heading">
                                            <h2>
                                                <strong>
                                                    Información general del usuario
                                                </strong>
                                                <i class="fa fa-user"></i>
                                            </h2>
                                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                        </header>
                                        <div role="content">
                                            <div class="jarviswidget-editbox">
                                            </div>
                                            <div class="widget-body">
                                                <div class="widget-body smart-form">
                                                    <fieldset>
                                                        <section>
                                                            <label class="label">
                                                                <small class="superset text-danger asterisk fa fa-asterisk small "
                                                                       aria-hidden="true">
                                                                </small>
                                                                <strong>
                                                                    Nombre:
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input id="name"
                                                                       type="text"
                                                                       class="form-control"
                                                                       name="name"
                                                                       value="{{ !empty($viewData['user']) ? $viewData['user']->name : old('name') }}" required autofocus>
                                                            </label>
                                                        </section>

                                                        <section>
                                                            <label class="label">
                                                                <small class=" superset  text-danger asterisk fa fa-asterisk small "
                                                                       aria-hidden="true">
                                                                </small>
                                                                <strong>
                                                                    Dirección de correo:
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input id="email"
                                                                       type="email"
                                                                       class="form-control"
                                                                       name="email"
                                                                       value="{{ !empty($viewData['user']) ? $viewData['user']->email : old('email') }}" required>
                                                            </label>
                                                        </section>

                                                        <section>
                                                            <label class="label">
                                                                <small class=" superset  text-danger asterisk fa fa-asterisk small "
                                                                       aria-hidden="true">
                                                                </small>
                                                                <strong>
                                                                    Contraseña
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input id="password"
                                                                       type="password"
                                                                       class="form-control"
                                                                       name="password" required>
                                                            </label>
                                                        </section>

                                                        <section>
                                                            <label class="label">
                                                                <small class=" superset  text-danger asterisk fa fa-asterisk small "
                                                                       aria-hidden="true">
                                                                </small>
                                                                <strong>
                                                                    Confirmación de contraseña
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input id="password-confirm"
                                                                       type="password"
                                                                       class="form-control"
                                                                       name="password_confirmation" required>
                                                            </label>
                                                        </section>

                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            @if(!empty($viewData['user']) && !empty($viewData['user']->id))
                                <input type="hidden"
                                       name="user_id"
                                       id="user_id"
                                       value="{{$viewData['user']->id}}">
                            @endif
                            <button type="submit" class="hide"></button>
                        </form>
        </div>
    </section>
</section>
@push('scripts')

@endpush