<?php

namespace App\CustomLib;

class GenericMessages{
    public static $all = [
        'successOperation'=>'Acción realizada exitosamente',
        'failOperation'=>'Error al ejecutar la acción',
        'emailExistSurvey'=> 'Este correo existe en está encuesta',
        'invalidEmail'=> 'El email es invalido',
        'duplicateField'=> 'Algunos campos están repetidos',
        'invalidFile'=>'Por favor seleccione un archivo válido',
        'invalidHeaderFile'=>'Por favor revise los campos obligatorios del archivo',
        'userAddedSuccessfully'=>'El usuario se ha agregado correctamente',
        'userUpdatedSuccessfully'=>'El usuario se ha actualizado correctamente',
        'surveyclonedsuccessfully'=>'La encuesta se ha clonado exitasamente',
        'surveyclonedfailed'=>'Ha ocurrido un problema al clonar la encuesta'
    ];
}