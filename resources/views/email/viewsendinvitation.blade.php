<?php
//    echo "<pre>";
//    print_r( $viewData['survey'] );
//    exit;
?>

@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Enviar invitaciones
                    </strong>
                </h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-right">
                                    @if($viewData['survey']->active == "Y")
                                        <button type="button" id="btnSave"
                                                class="btn btn-primary">
                                            <i class="fa fa-send"></i>
                                            Enviar invitaciones
                                        </button>
                                    @endif
                                    <a class="btn btn-default"
                                       href="{{ route('participants.config', ['surveyId'=>$viewData['survey']->id]) }}">
                                        Cancelar
                                    </a>
                                </p>
                            </div>
                        </div>

                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            <span class="fa fa-envelope-o"></span>
                                            Enviar invitación
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            @if(!empty($viewData['survey']->active) &&
                                $viewData['survey']->active == "N")

                                <div class="col-md-12">
                                    <div class="jumbotron message-box-error">
                                        <h2 class="text-danger">
                                            <strong>
                                                ¡Advertencia!
                                            </strong>
                                        </h2>
                                        <p class="lead text-danger">
                                            Esta encuesta aún no se activó y por lo tanto sus participantes no podrán completar la encuesta.
                                        </p>
                                    </div>
                                </div>

                            @endif


                            <div class="col-md-12">
                                <form action="{{ route('email.sendinvitation',['surveyId'=>$viewData['survey']->id]) }}"
                                      method="post" id="frmEmailTemplates" class="">
                                    {{ csrf_field() }}
                                    <ul class="nav nav-tabs">
                                        <li role='presentation' class='active'><a  data-toggle='tab' href='#tab-0'>Invitación</a></li>

                                    </ul>

                                    <div class="tab-content tabsinner form-horizontal" id=''>

                                        <div id="tab-0"
                                             class="tab-pane fade in active">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        <strong>
                                                            Asunto
                                                        </strong>
                                                    </label>
                                                    <div class="col-md-10">
                                                        <input class="form-control"
                                                               name="survey_email_invite_subj"
                                                               id="survey_email_invite_subj"
                                                               placeholder=""
                                                               value="<?php echo $viewData['surveysetting']->survey_email_invite_subj; ?>"
                                                               type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                        <strong>
                                                            Mensaje
                                                            <!--Cuerpo de correo electrónico de invitación:-->
                                                        </strong>

                                                    </label>
                                                    <div class="col-md-10">
                                                        <label class="textarea">
                                                        <textarea rows="20"
                                                        name="survey_email_invite"
                                                        id="survey_email_invite"
                                                        class="smCkEditor"><?php echo $viewData['surveysetting']->survey_email_invite; ?></textarea>
                                                        </label>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>

                                    </div>

                                    <input type="hidden"
                                           name="survey_id"
                                           id="survey_id"
                                           value="<?php echo $viewData['survey']->id; ?>">
                                    <input type="hidden"
                                           id="surveyconfig_id"
                                           name="surveyconfig_id"
                                           value="<?php echo $viewData['surveysetting']->id; ?>">
                                </form>
                            </div>

                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
    @push('scripts')
    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/EmailModule.js"></script>
    <script>
        EmailModule.init();
    </script>
    @endpush
@stop