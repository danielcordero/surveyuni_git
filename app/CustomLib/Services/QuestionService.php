<?php

namespace App\CustomLib\Services;

use App\Group;
use App\Question;
use App\CustomLib\Helper\HelperServices;

class QuestionService
{
    public static function getByGroupId($groupId, $isArray, $isFirst = false){
        $group = Group::with([
            'questions' => function($q) use ($isFirst){
                if($isFirst){
                    $q->orderBy('sortorder', 'asc')->first();
                } else {
                    $q->orderBy('sortorder', 'asc');
                }

            }
        ])->find($groupId);

        if($isArray){
            $result = $group->questions->toArray();
        } else {
            $result = $group->questions;
        }

        return $result;
    }

    public static function getByOrderCondition($groupId, $order, $isLess = true ,$isArray = true){

        $group = Group::with([
            'questions' => function($q) use ($isLess, $order){
                if($isLess){
                    $q->where(
                        "questions.sortorder","<",$order
                    );
                } else {
                    $q->where(
                        "questions.sortorder",">",$order
                    );
                }
            }
        ])->find($groupId);

        if($isArray){
            $result = $group->questions->toArray();
        } else {
            $result = $group->questions;
        }
        return $result;
    }

    public static function changeSortOrder($questions, $isPlus = true){
        $newArrayQuestion = [];
        $newArrayQuestions = [];
        foreach($questions as $key => $question)
        {
            $sortOrder = $questions[$key]['sortorder'];
            if($isPlus){
                $sortOrder = $sortOrder + 1;
            } else {
                $sortOrder = $sortOrder - 1;
            }

            $questions[$key]['sortorder'] = $sortOrder;
            $newArrayQuestion['id'] = $questions[$key]['id'];
            $newArrayQuestion['sortorder'] = $sortOrder;
            array_push($newArrayQuestions, $newArrayQuestion);
        }

        return HelperServices::updateBulk('questions',$newArrayQuestions,'id');
    }
}

?>