<?php

namespace App\Http\Controllers\AdminSurveys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\Helper\HelperServices;

class ReplacementFieldsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('CheckPermission',['except'=>'setroles']);
    }

    public function index(Request $request)
    {

        $surveyid = intval($request->surveyid);
        $gid = intval($request->gid);
        $qid = intval($request->qid);
        $fieldtype = HelperServices::sanitize_xss_string($request->fieldtype);
        $action = HelperServices::sanitize_xss_string($request->action);
        list($replacementFields, $isInstertAnswerEnabled) = $this->_getReplacementFields($fieldtype, $surveyid);
        $countfields = count($replacementFields);
        $replFields = $replacementFields;
        return view('replacefields.replacefields',compact('countfields', 'replFields'));
    }

    private function _getReplacementFields($fieldtype, $surveyid)
    {
        $replFields = array();
        if(!$surveyid)
            return array($replFields, false);

        switch ($fieldtype)
        {
            case 'txADescription':
            case 'txAWelcomeMessage':
            case 'txAEndMessage':
            case 'edittitle': // for translation
            case 'editdescription': // for translation
            case 'editwelcome': // for translation
            case 'editend': // for translation
                $replFields[] = array('TOKEN:FIRSTNAME', "Nombre del participante");
                $replFields[] = array('TOKEN:LASTNAME', "Apellido del participante");
                $replFields[] = array('TOKEN:EMAIL', "Correo del participante");
                $replFields[] = array('EXPIRY', "Fecha de experiración de la encuesta");
                $replFields[] = array('ADMINNAME', "Nombre del administrador de la encuesta");
                $replFields[] = array('ADMINEMAIL', "Correo electronico del administrador de la encuesta");
                return array($replFields, false);

//            case 'email-admin_notification':
//            case 'email-admin_detailed_notification':
//                $replFields[] = array('RELOADURL', gT("Reload URL"));
//                $replFields[] = array('VIEWRESPONSEURL', gT("View response URL"));
//                $replFields[] = array('EDITRESPONSEURL', gT("Edit response URL"));
//                $replFields[] = array('STATISTICSURL', gT("Statistics URL"));
//                $replFields[] = array('TOKEN', gT("Token code for this participant"));
//                $replFields[] = array('TOKEN:FIRSTNAME', gT("First name from token"));
//                $replFields[] = array('TOKEN:LASTNAME', gT("Last name from token"));
//                $replFields[] = array('SURVEYNAME', gT("Name of the survey"));
//                $replFields[] = array('SURVEYDESCRIPTION', gT("Description of the survey"));
//                $attributes = getTokenFieldsAndNames($surveyid, true);
//                foreach ($attributes as $attributefield => $attributedescription)
//                {
//                    $replFields[] = array(strtoupper($attributefield), sprintf(gT("Token attribute: %s"), $attributedescription['description']));
//                }
//                $replFields[] = array('ADMINNAME', gT("Name of the survey administrator"));
//                $replFields[] = array('ADMINEMAIL', gT("Email address of the survey administrator"));
//                return array($replFields, false);
//
//            case 'email-admin-resp':
//                $replFields[] = array('RELOADURL', gT("Reload URL"));
//                $replFields[] = array('VIEWRESPONSEURL', gT("View response URL"));
//                $replFields[] = array('EDITRESPONSEURL', gT("Edit response URL"));
//                $replFields[] = array('STATISTICSURL', gT("Statistics URL"));
//                $replFields[] = array('ANSWERTABLE', gT("Answers from this response"));
//                $replFields[] = array('TOKEN', gT("Token code for this participant"));
//                $replFields[] = array('TOKEN:FIRSTNAME', gT("First name from token"));
//                $replFields[] = array('TOKEN:LASTNAME', gT("Last name from token"));
//                $replFields[] = array('SURVEYNAME', gT("Name of the survey"));
//                $replFields[] = array('SURVEYDESCRIPTION', gT("Description of the survey"));
//                $attributes = getTokenFieldsAndNames($surveyid, true);
//                foreach ($attributes as $attributefield => $attributedescription)
//                {
//                    $replFields[] = array(strtoupper($attributefield), sprintf(gT("Token attribute: %s"), $attributedescription['description']));
//                }
//                $replFields[] = array('ADMINNAME', gT("Name of the survey administrator"));
//                $replFields[] = array('ADMINEMAIL', gT("Email address of the survey administrator"));
//                return array($replFields, false);
//
            case 'survey_email_invite':
            case 'survey_email_remind':
                // these 2 fields are supported by email-inv and email-rem
                // but not email-reg for the moment
                $replFields[] = array('EMAIL', ("Email from the token"));
                $replFields[] = array('TOKEN', ("Token code for this participant"));
                $replFields[] = array('OPTOUTURL', ("URL for a respondent to opt-out of this survey"));
                $replFields[] = array('OPTINURL', ("URL for a respondent to opt-in to this survey"));
            case 'email-registration':
                $replFields[] = array('FIRSTNAME', ("First name from token"));
                $replFields[] = array('LASTNAME', ("Last name from token"));
                $replFields[] = array('SURVEYNAME', ("Name of the survey"));
                $replFields[] = array('SURVEYDESCRIPTION', ("Description of the survey"));
                $replFields[] = array('ADMINNAME', ("Name of the survey administrator"));
                $replFields[] = array('ADMINEMAIL', ("Email address of the survey administrator"));
                $replFields[] = array('SURVEYURL', ("URL of the survey"));
                $replFields[] = array('EXPIRY', ("Survey expiration date"));
                return array($replFields, false);
//            case 'group-desc':
//                $replFields[] = array('FIRSTNAME', ("First name from token"));
//                $replFields[] = array('LASTNAME', ("Last name from token"));
//                $replFields[] = array('EMAIL', ("Name of the survey"));
//                return array($replFields, false);

//
//            case 'email-confirmation':
//                $replFields[] = array('TOKEN', gT("Token code for this participant"));
//                $replFields[] = array('FIRSTNAME', gT("First name from token"));
//                $replFields[] = array('LASTNAME', gT("Last name from token"));
//                $replFields[] = array('SURVEYNAME', gT("Name of the survey"));
//                $replFields[] = array('SURVEYDESCRIPTION', gT("Description of the survey"));
//                $attributes = getTokenFieldsAndNames($surveyid, true);
//                foreach ($attributes as $attributefield => $attributedescription)
//                {
//                    $replFields[] = array(strtoupper($attributefield), sprintf(gT("Token attribute: %s"), $attributedescription['description']));
//                }
//                $replFields[] = array('ADMINNAME', gT("Name of the survey administrator"));
//                $replFields[] = array('ADMINEMAIL', gT("Email address of the survey administrator"));
//                $replFields[] = array('SURVEYURL', gT("URL of the survey"));
//                $replFields[] = array('EXPIRY', gT("Survey expiration date"));
//
//                // email-conf can accept insertans fields for non anonymous surveys
//                if (isset($surveyid)) {
//                    $surveyInfo = getSurveyInfo($surveyid);
//                    if ($surveyInfo['anonymized'] == "N") {
//                        return array($replFields, true);
//                    }
//                }
//                return array($replFields, false);
//
//            case 'group-desc':
//            case 'question-text':
//            case 'question-help':
//            case 'editgroup': // for translation
//            case 'editgroup_desc': // for translation
//            case 'editquestion': // for translation
//            case 'editquestion_help': // for translation
//                $replFields[] = array('TOKEN:FIRSTNAME', "First name from token");
//                $replFields[] = array('TOKEN:LASTNAME', "Last name from token");
//                $replFields[] = array('TOKEN:EMAIL', "Email from the token");
//                $replFields[] = array('SID', "This question's survey ID number");
//                $replFields[] = array('GID', "This question's group ID number");
//                $replFields[] = array('QID', "This question's question ID number");
//                $replFields[] = array('SGQ', "This question's SGQA code");
//                $attributes = getTokenFieldsAndNames($surveyid, true);
//                foreach ($attributes as $attributefield => $attributedescription)
//                {
//                    $replFields[] = array('TOKEN:' . strtoupper($attributefield), sprintf(gT("Token attribute: %s"), $attributedescription['description']));
//                }
//                $replFields[] = array('EXPIRY', gT("Survey expiration date"));
//            case 'editanswer':
//                return array($replFields, true);
//
//            case 'assessment-text':
//                $replFields[] = array('TOTAL', gT("Overall assessment score"));
//                $replFields[] = array('PERC', gT("Assessment group score"));
//                return array($replFields, false);
        }
    }

}