<?php



Form::macro('selectWeekDay', function ($fieldtype,$fieldname,$fieldtext, $surveyID=null,$gID=null,$qID=null,$action=null) {

    $htmlcode = '';
    $imgopts = '';
    $toolbarname = 'inline';
    $toolbaroption="";
    $sFileBrowserAvailable='';
    $htmlformatoption="";
    $oCKeditorVarName = "oCKeditor_".str_replace("-","_",$fieldname);

    if ( ($fieldtype == 'editanswer' ||
            $fieldtype == 'addanswer' ||
            $fieldtype == 'editlabel' ||
            $fieldtype == 'addlabel') && (preg_match("/^translate/",$action) == 0) )
    {
        $toolbaroption= ",toolbarStartupExpanded:true\n"
            .",toolbar:'popup'\n"
            .",toolbarCanCollapse:false\n";
    }
    else
    {
//            $ckeditexpandtoolbar = Yii::app()->getConfig('ckeditexpandtoolbar');
//            if (!isset($ckeditexpandtoolbar) ||  $ckeditexpandtoolbar == true)
//            {
//                $toolbaroption = ",toolbarStartupExpanded:true\n"
//                    .",toolbar:'inline'\n";
//            }
    }

    if ( $fieldtype == 'email-inv' ||
        $fieldtype == 'email-reg' ||
        $fieldtype == 'email-conf'||
        $fieldtype == 'email-admin-notification'||
        $fieldtype == 'email-admin-resp'||
        $fieldtype == 'email-rem' )
    {
        $htmlformatoption = ",fullPage:true\n";
    }

//        if ($surveyID=='')
//        {
//            $sFakeBrowserURL=Yii::app()->getController()->createUrl('admin/survey/sa/fakebrowser');
//            $sFileBrowserAvailable=",filebrowserBrowseUrl:'{$sFakeBrowserURL}'
//            ,filebrowserImageBrowseUrl:'{$sFakeBrowserURL}'
//            ,filebrowserFlashBrowseUrl:'{$sFakeBrowserURL}'
//            ,filebrowserUploadUrl:'{$sFakeBrowserURL}'
//            ,filebrowserImageUploadUrl:'{$sFakeBrowserURL}'
//            ,filebrowserFlashUploadUrl:'{$sFakeBrowserURL}'";
//        }

    $htmlcode .= ""
        . "<script type=\"text/javascript\">debugger;\n"
        . "$(document).ready(
        function(){ var $oCKeditorVarName = CKEDITOR.replace('$fieldname', {
        
        LimeReplacementFieldsType : \"".$fieldtype."\"
        ,LimeReplacementFieldsSID : \"".$surveyID."\"
        ,LimeReplacementFieldsGID : \"".$gID."\"
        ,LimeReplacementFieldsQID : \"".$qID."\"
        ,LimeReplacementFieldsType : \"".$fieldtype."\"
        ,LimeReplacementFieldsAction : \"".$action."\"
        ,LimeReplacementFieldsPath : \"".''."\"
        ,language:'"."'"
        . $sFileBrowserAvailable
        . $htmlformatoption
        . $toolbaroption
        ."});

CKEDITOR.editorConfig = function( config )
{
    config.uiColor = '#FFF';
};

        \$('#$fieldname').parents('ul:eq(0)').addClass('editor-parent');
        });";


    $htmlcode.= '</script>';

    return $htmlcode;

//    $days = [
//        'monday' => 'Monday',
//        'tuesday' => 'Tuesday',
//        'wednesday' => 'Wednesday',
//        'thursday' => 'Thursday',
//        'friday' => 'Friday',
//        'saturday' => 'Saturday',
//        'sunday' => 'Sunday',
//    ];
//    return Form::select('day', $days, null, ['class' => 'form-control']);
});