<?php

//use \App\CustomLib\TypeQuestions\BaseQuestion;
//$helperHtml = new HelperHtml();
namespace App\CustomLib\Helper;
//use \App\CustomLib\Helper\HelperHtml;
use \stdClass;

//echo "<pre>";
//$helperHtml = new HelperHtml();
//print_r( $helperHtml );
//exit;


if (empty($viewData)) {
    $viewData['survey'] = new \stdClass();
}

$existSurveyId = !empty($viewData['survey']) && !empty($viewData['survey']->id);

?>
<section id="content">

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa-fw fa fa-puzzle-piece"></i>
                <strong>
                    <?php
                    echo !empty($existSurveyId) ? "Editar" : "Crear";
                    ?>
                    encuesta (Configuración)
                </strong>
            </h1>
        </div>
    </div>

    <section id="widget-grid" class="">
        <div class="well">

            <div class="row">
                <div class="col-md-12">
                    <p class="text-right">
                        <button type="button" id="btnSave"
                                class="btn btn-primary openMainModal">
                            <i class="fa fa-save"></i> Guardar
                        </button>
                        <a class="btn btn-danger"
                           href="<?php echo e(route('survey.index')); ?>">
                            Cancelar
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </a>
                    </p>
                </div>
            </div>

            <hr>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Básico</a></li>
                <!--<li><a data-toggle="tab" href="#menu1">Avanzada</a></li>-->
            </ul>

            <?php if(!empty($viewData['survey']) && !empty($viewData['survey']->id)): ?>
                <form action="<?php echo e(route('survey.update',['surveyId'=>$viewData['survey']->id])); ?>"
                      role="form" method="post" id="frmSurvey">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('PATCH')); ?>

                    <?php else: ?>
                        <form action="<?php echo e(route('survey.store')); ?>"
                              method="post" id="frmSurvey" class="">
                            <?php echo e(csrf_field()); ?>

                            <?php endif; ?>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <br>
                        <?php echo $__env->make('survey.elements.tags.basicConfig', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <br>
                        
                    </div>
                </div><button type="submit" class="hide"></button>
                <?php if(!empty($viewData['survey']) && !empty($viewData['survey']->id)): ?>
                    <input type="hidden"
                           name="survey_id"
                           id="survey_id"
                           value="<?php echo e($viewData['survey']->id); ?>">
                <?php endif; ?>
            </form>
        </div>
    </section>
</section>
<?php $__env->startPush('scripts'); ?>

<?php $__env->stopPush(); ?>