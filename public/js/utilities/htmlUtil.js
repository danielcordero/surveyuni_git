/**
 * Created by cesar on 12/8/2017.
 */
var htmlUtil = (function(){

    function buildSelectOption(data, id, name ,classAttr, required){
        var newSelect = document.createElement('select');
        newSelect.id = id;
        newSelect.name = name;
        newSelect.className = classAttr;
        if(required){
            newSelect.setAttribute("required", "required");
        }

        data.forEach(function(element, index, selfData){
            var opt = document.createElement("option");
            opt.value= element.id;
            opt.innerHTML = element.title;
            newSelect.appendChild(opt);
        });
        return newSelect;
    };

    return {
        buildSelectOption:buildSelectOption
    };

})();