<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $viewArray = ['layouts.app',
            'survey.questions.configQuestion',
            'survey.questions.createGroup',
            'survey.questions.createQuestion',
            'survey.questions.configQuestion',
            'survey.questions.detailGroup',
            'survey.questions.detailSurvey'];

        app('view')->composer($viewArray,
            function ($view) {
            $action = app('request')->route()->getAction();
            $controller = class_basename($action['controller']);
            $contronllersQuestions = [
                'QuestionController',
                'ConfigQuestionController',
                'EmailController',
                'ParticipantController',
                'StatisticsController'
            ];
            $currentController = explode('@', $controller)[0];
            $currentAction = explode('@', $controller)[1];
            $viewDataApp = [];

            if(in_array($currentController, $contronllersQuestions)){

                $viewDataApp["showMenuQuestions"] = true;
                //$view->with('viewDataApp', $viewDataApp);
            }

            $submitBtnQuestion = [
                'editGroup','updateGroup',
                'editQuestion','updateQuestion',
                'configQuestion','storeGroup',
                'groupQ',
                'config',
                'showDetailGroup',
                'showDetailSurvey'];

            if(in_array($currentAction, $submitBtnQuestion)){
                $viewDataApp["btnSubmitQuestion"] = true;
                //$view->with('viewDataApp', $viewDataApp);
            }

            $view->with('viewDataApp', $viewDataApp);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
