<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;


class ArrayC extends BaseQuestion{

    use ArrayTrait;

    public function saveConfig( $data ){
        $jsonToSave = new \stdClass();
        $subQuestionsR = $data->subQuestionsR;
        $subQuestionsC = $data->subQuestionsC;

        foreach ($subQuestionsR as $key => $value){
            $subQuestionsR[$key] = $this->cleanData( $value );
        }

        foreach ($subQuestionsC as $key => $value){
            $subQuestionsC[$key] = $this->cleanData( $value );
        }

        $jsonToSave->subQuestionsR = $subQuestionsR;
        $jsonToSave->subQuestionsC = $subQuestionsC;

        $question = Question::find( $data->question_id );
        $question->additionalData = json_encode( $jsonToSave );
        $question->save();
    }

    public function getConfig( $dataJson ){
        return json_decode( $dataJson, true );
    }
}
?>