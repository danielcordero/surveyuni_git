<div class="inbox-info-bar">
    <div class="row">
        <div class="col-sm-12">
            <h3>
                <strong>
                    Editar subpreguntas
                </strong>
            </h3>
        </div>
    </div>
</div>

<div class="row">
    <br>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <table class="table" id="tbSortOrder">
                <thead>
                <tr>
                    <th>Posición</th>
                    <th>Código</th>
                    <th>Subpregunta</th>
                    <th>Acción</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@push('scripts')

@endpush