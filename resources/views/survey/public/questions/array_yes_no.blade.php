<?php
$allQuestions = "";
//    echo "<pre>";
//    print_r($resultQuestion);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- array_yes_no -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Fila / Columna</th>

                                <th class="text-center">
                                    Si
                                </th>
                                <th class="text-center">
                                    No
                                </th>
                                <th class="text-center">
                                    No sé
                                </th>
                                <?php
                                if(!empty($question->mandatory) && $question->mandatory == "N"){
                                ?>
                                <th class="text-center">
                                    Sin respuesta
                                </th>
                                <?php
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($config["subQuestions"] as $key => $value){
                            $allQuestions = $allQuestions . '|' .$key;
                            ?>
                            <tr>
                                <th>
                                    <span class="label <?php
                                    if(!empty($resultQuestion['withoutAnswered']) &&
                                        in_array($key, $resultQuestion['withoutAnswered'])){
                                        echo "labelCommonError";
                                    }
                                    ?>">
                                        <?php
                                        echo $value;
                                        ?>
                                    </span>
                                    <?php
                                    //echo $value;
                                    ?>
                                </th>
                                <td class="text-center">
                                    <label class="radio inline">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                value="yes" <?php
                                            if(!empty($resultQuestion['keepAnswered']) &&
                                                !empty($resultQuestion['keepAnswered'][$key])){
                                                if($resultQuestion['keepAnswered'][$key] == "yes"){
                                                    echo "checked";
                                                }
                                            }
                                            ?>>
                                        <i></i><strong>
                                        </strong>
                                    </label>
                                </td>
                                <td class="text-center">
                                    <label class="radio inline">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                value="no" <?php
                                            if(!empty($resultQuestion['keepAnswered']) &&
                                                !empty($resultQuestion['keepAnswered'][$key])){
                                                if($resultQuestion['keepAnswered'][$key] == "no"){
                                                    echo "checked";
                                                }
                                            }
                                            ?>>
                                        <i></i><strong>
                                        </strong>
                                    </label>
                                </td>
                                <td class="text-center">
                                    <label class="radio inline">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                value="uncertain" <?php
                                            if(!empty($resultQuestion['keepAnswered']) &&
                                                !empty($resultQuestion['keepAnswered'][$key])){
                                                if($resultQuestion['keepAnswered'][$key] == "uncertain"){
                                                    echo "checked";
                                                }
                                            }
                                            ?>>
                                        <i></i><strong>
                                        </strong>
                                    </label>
                                </td>
                                <?php
                                if(!empty($question->mandatory) && $question->mandatory == "N"){
                                ?>
                                <td class="text-center">
                                    <label class="radio inline">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                               value="notanswer" <?php
                                            if(!empty($resultQuestion)){
                                                if(!empty($resultQuestion['keepAnswered']) &&
                                                    !empty($resultQuestion['keepAnswered'][$key])){
                                                    if($resultQuestion['keepAnswered'][$key] == 'notanswer'){
                                                        echo "checked";
                                                    }
                                                }
                                            } else {
                                                echo "checked";
                                            }
                                            ?>>
                                        <i></i><strong>
                                        </strong>
                                    </label>
                                </td>
                                <?php
                                }
                                ?>
                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>



                </div>
                @include('survey.public.questions.littlepartial.help')

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[subQuestions]"
                       id="question_<?php echo $question->id; ?>[subQuestions]"
                       value="<?php echo trim($allQuestions, '|'); ?>">
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>