<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;
use App\CustomLib\Services\{SurveyPublic};

class BaseQuestion{

    public static $pathNamespace = "\\App\\CustomLib\\TypeQuestions\\";

    public function cleanData( $string ){
        return htmlentities($string, ENT_QUOTES, 'UTF-8', false);
    }

    public function getConfigBase( $type , $additionalData ){
        $instance = BaseQuestion::getInstance($type);
        $config = $instance->getConfig( $additionalData );
        return $config;
    }

    public function saveConfigBase($type , $data ){
        $instance = BaseQuestion::getInstance($type);
        return $instance->saveConfig( $data );
    }

    public static function getInstance( $type ){

        switch ($type){
            case "multiple_choice":
                $class = BaseQuestion::$pathNamespace . "MultipleChoice";
                break;
            case "multiple_choice_comments":
                $class = BaseQuestion::$pathNamespace . "MultipleChoiceComments";
                break;
            case "choice5":
                $class = BaseQuestion::$pathNamespace . "Choice1to5";
                break;
            case "drop_down_list":
                $class = BaseQuestion::$pathNamespace . "DropDownList";
                break;
            case "numeric_input":
                $class = BaseQuestion::$pathNamespace . "NumericInput";
                break;
            case "numeric_input_several":
                $class = BaseQuestion::$pathNamespace . "NumericInputSeveral";
                break;
            case "datatime":
                $class = BaseQuestion::$pathNamespace . "DataTimeQ";
                break;
            case "title":
                $class = BaseQuestion::$pathNamespace . "Title";
                break;
            case "yes_no":
                $class = BaseQuestion::$pathNamespace . "YesNo";
                break;
            case "list_radio":
                $class = BaseQuestion::$pathNamespace . "ListRadio";
                break;
            case "list_radio_comments":
                $class = BaseQuestion::$pathNamespace . "ListRadioComments";
                break;
            case "free_text_line":
                $class = BaseQuestion::$pathNamespace . "FreeTextLine";
                break;
            case "free_text_paragraph":
                $class = BaseQuestion::$pathNamespace . "FreeTextParagraph";
                break;
            case "free_text_several_paragraph":
                $class = BaseQuestion::$pathNamespace . "FreeTextSeveralParagraph";
                break;
            case "free_text_several_line":
                $class = BaseQuestion::$pathNamespace . "FreeTextSeveralLine";
                break;
            case "array":
                $class = BaseQuestion::$pathNamespace . "ArrayC";
                break;
            case "array10":
                $class = BaseQuestion::$pathNamespace . "Array10";
                break;
            case "array5":
                $class = BaseQuestion::$pathNamespace . "Array5";
                break;
            case "array_yes_no":
                $class = BaseQuestion::$pathNamespace . "ArrayYesNo";
                break;
            case "array_column":
                $class = BaseQuestion::$pathNamespace . "ArrayColumn";
                break;
            case "array_i_s_d":
                $class = BaseQuestion::$pathNamespace . "ArrayIncreaseSameDecrease";
                break;
            case "array_number":
                $class = BaseQuestion::$pathNamespace . "ArrayNumber";
                break;
            case "array_text":
                $class = BaseQuestion::$pathNamespace . "ArrayText";
                break;
            case "ranking":
                $class = BaseQuestion::$pathNamespace . "Ranking";
                break;
        }
        return new $class();
    }

    public function startToAnswer( $partakerRequest ){

        $allAnswers = $partakerRequest->all_answers;
        $allAnswers = explode( '|' , $allAnswers);
        $questionsAnswers = [];
        $isThereAWrong = false;
        $result = [];

        foreach ($allAnswers as $key => $value){
            if(!empty($partakerRequest->{$value})){
                $questionToCheck = (array)$partakerRequest->{$value};
                $instance = self::getInstance($questionToCheck['type']);
                $response = $instance->validateAnswers($questionToCheck);
                if(!empty($response['fail'])){
                    $isThereAWrong = true;
                } else {
                    //Save status of survey
                }
                $questionsAnswers[$questionToCheck['questionId']] = $response;
            }
        }

        $surveyPublic = new SurveyPublic();
        $participantInfo = $surveyPublic->getSessionParticipant();
        if(!$surveyPublic->isModePreview() && !empty($participantInfo)){
            $partakerRequest->participantId = $participantInfo['id'];
            $handlerStatusSurvey = new HandlerStatusSurvey();
            $handlerStatusSurvey->saveBySurvey($questionsAnswers, $partakerRequest, $allAnswers);
        }

        $result['isThereAWrong'] = $isThereAWrong;
        $result['questionsFail'] = $questionsAnswers;

        return $result;
    }

    public function isMandatory( $response ){
        $isMandatory = false;
        if(!empty($response['mandatory']) &&
            $response['mandatory'] == "Y"){
            $isMandatory = true;
        }
        return $isMandatory;
    }

}
?>