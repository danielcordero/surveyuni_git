<label class="select">

    <select name="positionQ" required id="positionQ">
        <option value=""
                selected=""
                disabled="">Elegir una posición
        </option>
        <?php
        if(!empty($viewData['group']->id)){
        ?>
        <?php
        $questions = $viewData['group']->questions;
        if(!empty($questions->toArray())){
        foreach ($questions as $question){
        ?>
        <option value="<?php echo $question->id; ?>"
        <?php

            if (!empty($viewData['question']) &&
                $question->id == $viewData['question']->id) {
                echo "selected";
            }
            ?> >
            (<?php
            echo $question->sortorder;
            ?>)
            <?php
            if (!empty($viewData['question'])
            && $question->id != $viewData['question']->id) {
                echo "Intercambiar con";
            }
            if(empty($viewData['question'])){
                echo "Intercambiar con";
            }
            ?>
            <?php
            echo strip_tags($question->title);
            ?>
        </option>
        <?php
        }

        if(empty($viewData['question']->id)){
            ?>
        <option value="E">
            Al final de la lista
        </option>
            <?php
        }

        } else {
        ?>
        <option value="S">
            Al inicio de la lista
        </option>
        <?php
        }
        }
        ?>
    </select> <i></i>
</label>