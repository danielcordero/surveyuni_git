<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\{Survey,Group,Question};
use App\CustomLib\Helper\HelperServices;

class CheckExistResource
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $parameters = Route::current()->parameters();
        $viewData = [];

        if(array_key_exists("surveyId",$parameters)){
            $surveyId = $parameters['surveyId'];
            $surveyId = HelperServices::sanitize_int($surveyId);
            $parametersQ = ['surveyId' => $surveyId];
            $result = $this->executeQuery("surveys","surveyId",$parametersQ);
            $viewData['message'] = "Encuesta no encontrada";
            if(empty($result)){
                return response(view('elements.generalpages.page404',["viewData"=>$viewData]));
            }
        }

        if(array_key_exists("groupId",$parameters)){
            $groupId = $parameters['groupId'];
            $groupId = HelperServices::sanitize_int($groupId);
            $parametersQ = ['groupId' => $groupId];
            $result = $this->executeQuery("groups","groupId",$parametersQ);
            $viewData['message'] = "Grupo de pregunta no encontrada";
            if(empty($result)){
                return response(view('elements.generalpages.page404',["viewData"=>$viewData]));
            }
        }
        if(array_key_exists("questionId",$parameters)){
            $questionId = $parameters['questionId'];
            $questionId = HelperServices::sanitize_int($questionId);
            $parametersQ = ['questionId' => $questionId];

            $result = $this->executeQuery("questions","questionId",$parametersQ);
            $viewData['message'] = "Pregunta no encontrada";
            if(empty($result)){
                return response(view('elements.generalpages.page404',["viewData"=>$viewData]));
            }
        }

        return $next($request);
    }

    public function executeQuery($table, $parametersId ,$parameters)
    {
        $query = "SELECT * FROM {$table} WHERE id = :{$parametersId}";
        return \DB::select( \DB::raw($query), $parameters);
    }
}
