<?php
$allQuestions = "";
?>
<div class="row">
    <!-- NEW WIDGET START -->
    <!-- multiple_choice_comments -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>
                <!-- widget content -->
                <div class="widget-body smart-form"
                     id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <fieldset>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table adjustTableContent">
                                            <tbody>
                                            <?php
                                            foreach ($config["subQuestions"] as $key => $value){
                                            $allQuestions = $allQuestions . '|' .$key;
                                            ?>
                                            <tr>
                                                <td>
                                                    <label class="checkbox inlineBlock">
                                                        <input type="checkbox"
                                                               id="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key; ?>]"
                                                                <?php
                                                                    if(!empty($resultQuestion['keepAnswered']) &&
                                                                        !empty($resultQuestion['keepAnswered'][$key."_comment"])){ ?>
                                                                        <?php
                                                                            echo "checked";
                                                                        ?>
                                                                    <?php } ?>
                                                                >
                                                        <i></i>
                                                        <!--<strong>
                                                            <?php //echo $value; ?>
                                                        </strong>-->
                                                            <span class="label <?php
                                                            if(!empty($resultQuestion['withoutAnswered']) &&
                                                                in_array($key."_comment", $resultQuestion['withoutAnswered'])
                                                                && $question->mandatory == "Y" &&
                                                                !empty($resultQuestion['fail'])){
                                                                echo "labelCommonError";
                                                            }
                                                            ?>"><?php echo $value; ?>
                                                        </span>
                                                    </label>
                                                </td>
                                                <td>
                                                    <label class="input inlineBlock">
                                                        <input type="text"
                                                               id="question_<?php echo $question->id; ?>[answers][<?php echo $key.'_comment'; ?>]"
                                                               name="question_<?php echo $question->id; ?>[answers][<?php echo $key.'_comment'; ?>]"
                                                               class="input-sm" value="<?php if(!empty($resultQuestion['keepAnswered']) && !empty($resultQuestion['keepAnswered'][$key."_comment"])){ ?> <?php echo trim($resultQuestion['keepAnswered'][$key."_comment"]); ?> <?php } ?>">
                                                    </label>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                            @if(!empty($config["anotherOption"]) && $config["anotherOption"] == "dd")
                                                <tr>
                                                    <td >
                                                        <label class="checkbox inlineBlock">
                                                            <input type="checkbox"
                                                                   id="question_<?php echo $question->id; ?>[answers][<?php echo 0; ?>]"
                                                                   name="question_<?php echo $question->id; ?>[answers][<?php echo 0; ?>]"
                                                                   value="0">
                                                            <i></i><strong>
                                                                Otro:
                                                            </strong></label>
                                                            <input type="text"
                                                                   id="question_<?php echo $question->id; ?>[answers][another]"
                                                                   name="question_<?php echo $question->id; ?>[answers][another]"
                                                                   class="input-sm inlineBlock">
                                                        <label class="input inlineBlock">
                                                            <input type="text"
                                                                   id="question_<?php echo $question->id; ?>[answers][anotherInput]"
                                                                   name="question_<?php echo $question->id; ?>[answers][anotherInput]"
                                                                   class="input-sm">
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="input inlineBlock">
                                                            <input type="text"
                                                                   id="question_<?php echo $question->id; ?>[answers][anotherInput]"
                                                                   name="question_<?php echo $question->id; ?>[answers][anotherInput]"
                                                                   class="input-sm">
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>



                        </section>
                    </fieldset>


                </div>
                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[subQuestions]"
                       id="question_<?php echo $question->id; ?>[subQuestions]"
                       value="<?php echo trim($allQuestions, '|'); ?>">
            @include('survey.public.questions.littlepartial.help')
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>