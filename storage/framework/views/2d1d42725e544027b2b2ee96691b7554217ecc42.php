<?php
    use \App\CustomLib\Services\SurveyService;
    $surveyService = new SurveyService();
    $infoSurvey = $surveyService->getData();
    $surveyData = $infoSurvey['survey'];
    $groupData = $infoSurvey['group'];
    $questionData = $infoSurvey['question'];

    $testSurveys = config('custom.testSurvey');
?>
<div class="well padding-10">
    <h5 class="margin-top-0">
        <strong>
            Grupos de preguntas
        </strong>
    </h5>
    <p>

        <?php if(!in_array($surveyData->id,$testSurveys)): ?>
            <a <?php echo Form::activesurvey($surveyData, "link","btn-primary",route('questionGroup',[$surveyData->id])); ?>>
                <i class="fa fa-plus-circle"></i>Grupo
            </a>
            <a <?php echo Form::activesurvey($surveyData, "link","btn-danger pull-right",route('question',[$surveyData->id])); ?> >
                <i class="fa fa-plus-circle"></i>Pregunta
            </a>
        <?php endif; ?>
    </p>
    <div class="row">
        <div class="col-sm-12">
            <div class="dd" id="groupQuestions">
                <ol class="dd-list dd-nodrag">
                    <?php
                    if(!empty($surveyData->groups)){
                    $groups = $surveyData->groups;
                    foreach ($groups as $group){
                    ?>
                    <li class="dd-item dd3-item "
                        data-id="<?php echo e($group->id); ?>">

                        <?php
                        $amountQuestions = $group->questions ? $group->questions : 0;
                        ?>
                        <?php
                        if(count($amountQuestions) > 0){
                        ?>

                        <?php
                        }
                        ?>
                        <div class="dd3-handle">
                            Drag
                        </div>
                        <div class="dd3-content <?php
                        if(!empty($groupData) &&
                            $groupData->id == $group->id){
                            echo " bg-color-teal txt-color-white ";
                        }
                        ?>">
                            <u>
                                <a href="<?php echo e(route('groupDetail',['surveyId'=>$surveyData->id,'groupId'=>$group->id])); ?>" class="<?php
                                if(!empty($groupData) &&
                                    $groupData->id == $group->id){
                                    echo " txt-color-white ";
                                }
                                ?>">
                                    <?php echo e($group->name); ?>

                                </a>
                            </u>
                            <a href="javascript:void(0);"
                               class="badge pull-right bg-color-green"
                               rel="tooltip"
                               data-placement="bottom"
                               data-original-title="Número de preguntas">
                                <i>
                                    <?php
                                        echo count($amountQuestions);
                                    ?>
                                </i>
                            </a>
                        </div>
                        <?php
                        if(count($amountQuestions) > 0){
                        ?>
                        <ol class="dd-list" style="">
                            <?php
                                foreach ($group->questions as $question){?>
                                <li class="dd-item">
                                    <div class="dd3-handle">
                                        Drag
                                    </div>
                                    <div class="dd3-content <?php
                                    if(!empty($questionData) &&
                                        $questionData->id == $question->id){
                                        echo " bg-color-blueDark  txt-color-white ";
                                    }
                                    ?>">
                                        <a href="<?php echo e(route('questionDetail',['surveyId'=>$surveyData->id,'groupId'=>$group->id, 'questionId'=>$question->id])); ?>" rel="popover-hover" data-placement="right" data-original-title="<span class='txtBlack'> Título de la pregunta </span>" data-html="true" data-content="<div class='txtBlack'><?php echo strip_tags($question->title); ?></div>" class="<?php
                                        if(!empty($questionData) &&
                                            $questionData->id == $question->id){
                                            echo " txt-color-white ";
                                        }
                                        ?>" >
                                            <strong>
                                                <?php
                                                $questionsType = config('custom.questionsType');
                                                    echo $questionsType[$question->type];
                                                ?>
                                            </strong>
                                        </a>
                                    </div>
                                </li>
                            <?php
                                }
                            ?>
                        </ol>
                        <?php
                        }
                        ?>
                    </li>
                    <?php
                    }
                    }
                    ?>
                </ol>
            </div>
        </div>
    </div>
</div>


