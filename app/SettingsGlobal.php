<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $stg_name
 * @property string $stg_value
 */
class SettingsGlobal extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'settings_global';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'stg_name';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public  $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['stg_value'];

}
