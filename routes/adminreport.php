<?php

Route::group(['prefix' => 'admin'], function () {

    Route::get('/survey/{surveyId}/viewstatistics',
        'AdminSurveys\StatisticsController@viewstatistics')
        ->name("surveystatistics.view");

    Route::get('/survey/{surveyId}/surveyexportresponse',
        'AdminSurveys\StatisticsController@viewexportresponse')
        ->name("surveyexportresponse.view");

});