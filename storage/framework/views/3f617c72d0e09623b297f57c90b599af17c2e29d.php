<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('elements.modals.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- END RIBBON -->
    <!-- MAIN CONTENT -->
    <div id="content">
        <!-- row
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa-fw fa fa-puzzle-piece"></i>
                    Index page survey tetttt
                </h1>
            </div>
        </div>-->
        <!-- end row -->

        <div class="container-fluid welcome full-page-wrapper well">

            <!-- Logo & Presentation -->
            <div class="row">
                <div class="well text-center" id="welcome-jumbotron">
                    <img alt="logo"
                         src="<?php echo url('/') ?>/img/uni-with-text.png"
                         id="lime-logo"
                         class="profile-img-card img-responsive center-block inlineBlock">
                    <br>
                    <br>
                    <h2>
                        <i class="fa-fw fa fa-home"></i>
                        <strong>
                            Bienvenido al sistema de encuesta, Gestión de Tecnología de Información y Comunicación
                        </strong>
                    </h2>
                </div>
            </div>

            <!-- Message when first start -->

            <!-- Last visited survey/question
            <div class="row text-right">
                <div class="col-lg-9 col-sm-9  ">
                    <div class="pull-right">
                                    <span id="last_survey" class="rotateHidden" style="display: none;">
                    Last visited survey:                    <a href="/monografia/limesurvey/index.php/admin/survey/sa/view/surveyid/712276" class="">Show "There are X questions in this survey":(ID:712276)</a>
                    </span>

                        <span id="last_question" class="rotateShown" style="display: inline;">
                    Last visited question:                    <a href="/monografia/limesurvey/index.php/admin/questions/sa/view/surveyid/712276/gid/6/qid/8" class="">QQ : This is my question of test</a>
                    </span>
                    </div>
                    <br><br>
                </div>
            </div>-->

            <!-- Rendering all boxes in database -->
            <div class="row text-center">
                <div class="col-md-4 col-xs-12">
                    <div class="panel panel-primary panel-clickable" id="pannel-1"
                         data-url="/monografia/limesurvey/index.php/admin/survey/sa/newsurvey"
                         style="opacity: 1; top: 0px;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Crear encuesta</h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                <a href="<?php echo e(route("survey.create")); ?>">
                                    <i class="fa fa-plus-circle"></i>
                                </a>
                            </div>
                            <br><br>
                            <a href="<?php echo e(route("survey.create")); ?>">
                                <strong>
                                    Crear una encuesta
                                </strong>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="panel panel-primary panel-clickable" id="pannel-2"
                         data-url="/monografia/limesurvey/index.php/admin/survey/sa/listsurveys" style="opacity: 1; top: 0px;">
                        <div class="panel-heading">
                            <h3 class="panel-title">Lista de encuesta</h3>
                        </div>
                        <div class="panel-body">
                            <div>
                                <a href="<?php echo e(route("survey.index")); ?>">
                                    <i class="fa fa-list"></i>
                                </a>
                            </div>
                            <br><br>
                            <a href="<?php echo e(route("survey.index")); ?>">
                                Lista de encuesta disponible
                            </a>
                        </div>
                    </div>
                </div>

                <?php
                $hasPermission = Auth::user()->checkHasPermission(['ConfigApp_viewconfigglobal']);
                if($hasPermission){
                ?>
                    <div class="col-md-4 col-xs-12">
                        <div class="panel panel-primary panel-clickable" id="pannel-3"
                             data-url="/monografia/limesurvey/index.php/admin/globalsettings"
                             style="opacity: 1; top: 0px;">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Configuración global
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div>
                                    <a href="<?php echo e(route("admin.viewconfigglobal")); ?>">
                                        <i class="fa fa-gears"></i>
                                    </a>
                                </div>
                                <br><br>
                                <a href="<?php echo e(route("admin.viewconfigglobal")); ?>">
                                    Editar configuración global
                                </a>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
    <?php $__env->startPush('scripts'); ?>
    <script src="<?php echo url('/') ?>/js/modules/SurveyModule.js"></script>
    <?php $__env->stopPush(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>