<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> @yield('title') </title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

    <link rel="icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/bootstrap.min.css">
<!--<link rel="stylesheet" type="text/css" media="screen" href="<?php //echo url('/') ?>/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-rtl.min.css">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/demo.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/helperClass.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/public/style.css">


</head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="block_error">
                        <div>
                            <h2>No encontrado!</h2>
                            <p>
                                La encuesta en la que intenta participar no parece existir. Es posible que se haya eliminado o que el enlace que le dieron no esté actualizado o sea incorrecto.
                            </p>
                            <p>
                                Para más información por favor contactese con:
                                <br>
                                <a href="mailto:test@test.test">test@test.test</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




