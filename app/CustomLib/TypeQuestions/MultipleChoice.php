<?php

namespace App\CustomLib\TypeQuestions;
use App\CustomLib\Helper\HelperServices;
use App\Question;
use Illuminate\Support\Facades\Session;

class MultipleChoice extends BaseQuestion{

    public function saveConfig( $data ){
        $jsonToSave = new \stdClass();
        $subQuestions = $data->subQuestions;
        foreach ($subQuestions as $key => $value){
            $subQuestions[$key] = $this->cleanData( $value );
        }

        $jsonToSave->anotherOption = !empty($data->anotherOption) ? "Y" : "N";
        $jsonToSave->subQuestions = $subQuestions;
        $question = Question::find( $data->question_id );
        $question->additionalData = json_encode( $jsonToSave );
        $question->save();
    }

    public function getConfig( $dataJson ){
        return json_decode( $dataJson, true );
    }

    public function validateAnswers( $partakerRequest )
    {

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message'] = "Esta pregunta es requerida";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest )
    {
        $answers = !empty($partakerRequest['answers']) ? $partakerRequest['answers'] : [];
        $subQuestions = $partakerRequest['subQuestions'];
        $subQuestions = explode("|",$subQuestions);
        $messages = [];

        $fail = false;
        $keepAnswered = [];
        $withoutAnswered = [];

        foreach ($subQuestions as $key => $value){
            if(in_array($value, array_keys($answers))){
                $answers[$value] = $this->cleanData($answers[$value]);
                if(!empty($answers[$value])){
                    $keepAnswered[$value] = trim($answers[$value]);
                } else {
                    $withoutAnswered[] = $value;
                }
            } else {
                $withoutAnswered[] = $value;
            }
        }

        if(!empty($partakerRequest['answers']['anotherInput'])){
            $anotherInput = $partakerRequest['answers']['anotherInput'];
            $keepAnswered["anotherInput"] = HelperServices::sanitize_xss_string($anotherInput);
        }

        if(!empty($withoutAnswered) &&
            !empty($partakerRequest['mandatory']) &&
            $partakerRequest['mandatory'] == "Y" &&
            count($keepAnswered) < 1){
            $fail = true;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

//        echo "<pre>";
//        print_r( $response );
//        exit;

        return $response;
    }
}
?>