<div class="row">
    <!-- NEW WIDGET START -->
    <!-- list_radio_comments -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <fieldset>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    foreach ($config["subQuestions"] as $key => $value){
                                    ?>
                                    <label class="radio">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][question]"
                                               value="<?php echo $key; ?>" <?php
                                            if(!empty($resultQuestion['keepAnswered'])){
                                                if(!empty($resultQuestion['keepAnswered']['question']) &&
                                                    $resultQuestion['keepAnswered']['question'] == $key){
                                                    echo "checked";
                                                }
                                            }
                                            ?>>
                                        <i></i><strong>
                                            <?php echo $value; ?>
                                        </strong></label>
                                    <br>
                                    <?php
                                    }
                                    ?>
                                    <?php
                                    if(!empty($question->mandatory) &&
                                    $question->mandatory == 'N'){
                                    ?>
                                    <label class="radio">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers][question]"
                                               value="notanswer"
                                        <?php
                                            if(!empty($resultQuestion)){
                                                if(!empty($resultQuestion['keepAnswered'])){
                                                    if($resultQuestion['keepAnswered'] == 'notanswer'){
                                                        echo "checked";
                                                    }
                                                }
                                            } else {
                                                echo "checked";
                                            }
                                            ?>>
                                        <i></i>Sin respuesta
                                    </label>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-md-12">
                                    <p>
                                        Por favor ingrese su comentario aquí:
                                    </p>
                                    <label class="textarea">
                                        <textarea rows="3" class="custom-scroll" name="question_<?php echo $question->id; ?>[answers][comment]"><?php if(!empty($resultQuestion['keepAnswered'])){if(!empty($resultQuestion['keepAnswered']['comment'])){?> {{$resultQuestion['keepAnswered']['comment']}}<?php }} ?></textarea>
                                    </label>
                                </div>
                            </div>
                        </section>



                    </fieldset>

                </div>
            @include('survey.public.questions.littlepartial.help')
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>