<?php
//    echo "<pre>";
//    print_r( $viewData['survey'] );
//    exit;
?>

@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Activar encuesta
                    </strong>
                </h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            <i class="fa fa-play-circle"></i>
                                            Activar encuesta
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="jumbotron message-box-error">
                                    <div class="row">
                                        <h2 class="col-md-12">
                                            <strong class="text-danger">
                                                Advertencia: ¡Lea esto detenidamente antes de continuar!
                                            </strong>
                                        </h2>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>
                                                <strong class="text-danger">
                                                    Solo debe activar una encuesta cuando esté absolutamente seguro de que la configuración de su encuesta ha finalizado y no será necesario cambiarla.
                                                </strong>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                        <h5>
                                            <strong>
                                                <u>
                                                    Una vez que se activa una encuesta, ya no puede:
                                                </u>
                                            </strong>
                                        </h5>

                                        </div>
                                        <div class="col-md-4 col-md-offset-4">
                                            <ul class="">
                                                <li>
                                                    <h5>
                                                        <strong>
                                                            Agregar o eliminar grupos
                                                        </strong>
                                                    </h5>
                                                </li>
                                                <li>
                                                    <h5>
                                                        <strong>
                                                            Agregar o eliminar preguntas
                                                        </strong>
                                                    </h5>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>
                                                <strong class="txt-color-greenDark">
                                                    Tenga en cuenta que una vez que las respuestas se hayan recopilado con esta encuesta y desee agregar o eliminar grupos / preguntas
                                                </strong>
                                            </h5>
                                            <h5>
                                                <strong class="txt-color-greenDark">
                                                    o cambiar una de las configuraciones anteriores, deberá desactivar esta encuesta.
                                                </strong>
                                            </h5>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <form method="post"
                                              action="{{ route("survey.saveactivesurvey",[$viewData['survey']->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="col-md-12">
                                                <br>
                                                <p class="text-center">
                                                    <button type="submit"
                                                            id="btnSave"
                                                            class="btn btn-success btn-lg">
                                                        <i class="fa fa-play-circle"></i> Guardar
                                                    </button>

                                                    <a class="btn btn-danger btn-lg"
                                                       href="{{ route('surveyDetail',[$viewData['survey']->id])}}">
                                                        Cancelar
                                                    </a>
                                                </p>
                                            </div>

                                            <input
                                                    type="hidden"
                                                    name="survey_id"
                                                    value="<?php echo $viewData['survey']->id; ?>">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
    @push('scripts')

    @endpush
@stop