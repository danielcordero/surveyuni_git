@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Gestionar participantes de la encuesta
                    </strong>
                </h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!--<h3>
                                        <strong>
                                            Resumen de participantes de la encuesta
                                        </strong>
                                    </h3>-->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <form action="{{ route('participants.createtokens',['surveyId'=>$viewData['survey']->id]) }}"
                                      role="form" method="post" id="frmSurvey">
                                    {{ csrf_field() }}
                                    <div class="jumbotron message-box message-box-error text-center">
                                        <h2>
                                            <strong>
                                                Crear token
                                            </strong>
                                        </h2>
                                        <p>
                                            Al hacer clic en 'Sí', se generarán tokens para todos
                                            aquellos en esta lista. ¿Continuar?
                                        </p>
                                        <br><br>
                                        <input class="btn btn-primary btn-lg"
                                               type="submit"
                                               value="Yes"
                                               style="margin-right: 30px;">
                                        <a class="btn btn-danger btn-lg"
                                           href="{{ route("participants.config",$viewData['survey']->id) }}">
                                            No
                                        </a>
                                        <br>
                                    </div>

                                    <input type="hidden"
                                           name="survey_id"
                                           id="survey_id"
                                            value="{{ $viewData['survey']->id }}">

                                </form>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
@stop