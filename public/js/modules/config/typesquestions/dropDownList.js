/**
 * Created by cesar on 12/27/2017.
 */
"use strict";
var dropDownList = new function () {

    var that = this;

    this.questionId = "";
    this.maxSq = 1;
    this.sqsAll = [];
    this.templates = {
        sortaTable: _.template('<tr id="<%= sqId %>">' +
            '<td>' +
            '<span class="cursorMove">' +
            '<i class="fa fa-bars fa-2x cursorMove" aria-hidden="true"></i>' +
            '</span>' +
            '</td>' +
            '<td><%= sqId %></td>' +
            '<td>' +
            '<label class="input fullWidth">' +
            '<input type="text" class="input-sm fullWidth" name="subQuestions[<%= sqId %>]">' +
            '</label>' +
            '</td>' +
            '<td>' +
            '<a class="btn btn-info btn-xs addTr"' +
            'data-toggle="tooltip"' +
            'data-type-btn="addTr"' +
            'data-placement="top"' +
            'title="Agregar subpreguntas">' +
            '<i class="fa fa-fw fa-plus"></i>' +
            '</a>' +

            '<a class="btn btn-danger btn-xs removeTr" style="margin-left: 4px;"' +
            'data-toggle="tooltip"' +
            'data-placement="top"' +
            'data-type-btn="removeTr"' +
            'title="Eliminar subpreguntas">' +
            '<i class="fa fa-trash-o"></i>' +
            '</a>' +
            '</td>' +
            '</tr>')
    };

    this.init = function(qId){
        this.questionId = qId;
        this.initSortTable();
    };

    this.initSortTable = function() {
        var
            nameTable = "tbSortOrder",
            table = $("#" + nameTable),
            tbody = table.find("tbody"),
            rows = tbody.find("tr");

        if(rows.length < 1){
            tbody.append(this.templates.sortaTable({sqId: 1 + '_' + that.questionId}));
        }

        if(rows.length > 0){
            rows.each(function(index, el){
                var i = $(el).attr("id").split("_")[0];
                that.sqsAll.push(i);
            });
            that.maxSq = rows.length;
        }

        tbody.sortable();
        //Action tr
        this.actionBtnOverTable(tbody);
    };

    this.actionBtnOverTable = function(tbody){
        var dataSqId = "",
            typeBtn,
            parrentTr;

        this.sqsAll.push(that.maxSq);
        tbody.on("click", ".addTr,.removeTr", function () {
            typeBtn = $(this).data("type-btn");
            parrentTr = $(this).parent().parent();

            if(typeBtn === "addTr"){
                that.maxSq = that.maxSq + 1;
                that.sqsAll.push(that.maxSq);
                parrentTr.after(that.templates.sortaTable({sqId: that.maxSq + '_' + that.questionId}));
            }

            if(typeBtn === "removeTr"){
                if(tbody.find("tr").length == 1){
                    commonModule.showPopupGeneric("error","Al menos debe haber una subpregunta");
                    // alert("Al menos debe haber una subpregunta");
                    return;
                }
                dataSqId = parrentTr.attr("id");
                that.sqsAll = commonModule.rmElementInArray(that.sqsAll, dataSqId.split("_")[0]);
                parrentTr.closest('tr').remove();
                that.reorderValueTr(dataSqId.split("_")[0]);
            }
        });
    };

    this.reorderValueTr = function(fromsqId) {
        var highsElement = [],
            lesserElement = [],
            highsElementCopy = [];

        _.each(this.sqsAll, function (el) {
            (fromsqId < el) ? highsElement.push(el) : lesserElement.push(el);
        });

        highsElementCopy = highsElement;
        highsElement = _.map(highsElement, function (el) {
            return el - 1;
        });
        this.sqsAll = _.union(highsElement, lesserElement);
        this.maxSq = _.max(this.sqsAll);

        highsElementCopy = _.map(highsElementCopy, function (el) {
            return "#" + el + "_" + that.questionId;
        });
        highsElementCopy = highsElementCopy.join(",");
        $(highsElementCopy).each(function (i, el) {
            $(el).attr("id", highsElement[i] + "_" + that.questionId);
            $(el).children('td').eq(1).html(highsElement[i] + "_" + that.questionId);
        });
    }

    this.prepareConfigToSave = function(form){
        // var
        //     jsonToSave = {},
        //     nameTable = "tbSortOrder",
        //     table = $("#" + nameTable),
        //     tbody = table.find("tbody");
        //
        // jsonToSave.subQuestions = {};
        // tbody.find("input[type='text']").each(function(i, el){
        //     var name = $(el).attr("name");
        //     jsonToSave.subQuestions[name] = Encoder.htmlEncode($(el).val());
        // });
        // jsonToSave = JSON.stringify( jsonToSave );
        // jsonToSave = Encoder.htmlEncode( jsonToSave );
        // form.append('<input type="hidden" id="config_question" name="config_question" value="'+ [jsonToSave] +'" />');
        // console.log( jsonToSave );

    };
};