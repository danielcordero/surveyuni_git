<?php if(Session::has('success')): ?>

	<div class="callout callout-success alert alert-success text-center"  
		role="alert">
	    <button type="button" 
		 	class="close" data-dismiss="alert" 
		 	aria-label="Close">
		 	<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="text-center">
			
			<strong>
				<?php echo e(Session::get('success')); ?>

			</strong>
		</h4>
    </div>

<?php endif; ?>

<?php if(Session::has('error')): ?>

	<div class="alert alert-danger callout callout-danger"
		 role="alert">
		<button type="button"
				class="close" data-dismiss="alert"
				aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="text-center">
			
			<strong>
				<?php echo e(Session::get('error')); ?>

			</strong>
		</h4>
	</div>

<?php endif; ?>

<?php if(count($errors) > 0): ?>

	<div class="alert alert-danger callout callout-danger" role="alert">
		<button type="button" 
		 	class="close" data-dismiss="alert" 
		 	aria-label="Close"><span aria-hidden="true">&times;</span>
		</button>
		<strong>Errores:</strong>
		<ul>
		<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<li><?php echo e($error); ?></li>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
		</ul>
	</div>

<?php endif; ?>