<?php $__env->startSection('content'); ?>

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Gestionar participantes de la encuesta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <?php echo $__env->make('participant.elements.topBottons', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            Resumen de participantes de la encuesta
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <strong>
                                            Resumen de participantes de la encuesta
                                        </strong>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Total de registros
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo e($viewData['survey']->participants->count()); ?>

                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Total de invitaciones enviadas
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo e($viewData['survey']->amountSent); ?>

                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Total de encuestas completadas
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo e($viewData['survey']->amountCompleted); ?>

                                                    </strong>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>