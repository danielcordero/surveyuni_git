<?php

Form::macro('activesurvey',function($survey, $type, $btnCss = ' btn-primary ', $redirectTo){

    $attrs = 'class=" btn ' .$btnCss;
    if($type == "link"){
        if(!empty($survey->active) && $survey->active == "N"){
            $route = $redirectTo;
            $attrs .= ' "';
        } else {
            $attrs .= ' opacity65 "';
            $attrs .= ' rel="tooltip" data-placement="bottom" data-original-title="Está encuesta está actualmente activa"';
            $route = "javascript:void(0);";
        }
        $attrs .= ' href="'.$route.'"';
    }
    return $attrs;
});


Form::macro('myInputText', function ($name, $label, $errors)
{
    if($errors->first($name)) {
        $comp = '<div class="form-group has-error">';
    } else {
        $comp = '<div class="form-group">';
    }
    $comp .= Form::label($name, $label);
    $comp .= Form::text($name, old($name), ['class' => 'form-control']);
    if($errors->first($name))
    {
        $comp .= '<span class="help-block">'.$errors->first($name).'</span>';
    }
    $comp .= '</div>';
    return $comp;
});