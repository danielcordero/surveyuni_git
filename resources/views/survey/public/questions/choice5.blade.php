<?php
//    echo "<pre>";
//    print_r($resultQuestion['keepAnswered']);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- choice5 -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif


                    <section>
                        <br>
                        <div class="inline-group">
                            <label class="radio">
                                <input type="radio"
                                       name="question_<?php echo $question->id; ?>[answers]"
                                        value="1" <?php
                                    if(!empty($resultQuestion['keepAnswered'])){
                                        if($resultQuestion['keepAnswered'] == '1'){
                                            echo "checked";
                                        }
                                    }
                                    ?>>
                                <i></i>1
                            </label>
                            <label class="radio">
                                <input type="radio"
                                       name="question_<?php echo $question->id; ?>[answers]"
                                       value="2" <?php
                                    if(!empty($resultQuestion['keepAnswered'])){
                                        if($resultQuestion['keepAnswered'] == '2'){
                                            echo "checked";
                                        }
                                    }
                                    ?>>
                                <i></i>2
                            </label>
                            <label class="radio">
                                <input type="radio"
                                       name="question_<?php echo $question->id; ?>[answers]"
                                       value="3" <?php
                                    if(!empty($resultQuestion['keepAnswered'])){
                                        if($resultQuestion['keepAnswered'] == '3'){
                                            echo "checked";
                                        }
                                    }
                                    ?>>
                                <i></i>3
                            </label>
                            <label class="radio">
                                <input type="radio"
                                       name="question_<?php echo $question->id; ?>[answers]"
                                       value="4" <?php
                                    if(!empty($resultQuestion['keepAnswered'])){
                                        if($resultQuestion['keepAnswered'] == '4'){
                                            echo "checked";
                                        }
                                    }
                                    ?>>
                                <i></i>4
                            </label>
                            <label class="radio">
                                <input type="radio"
                                       name="question_<?php echo $question->id; ?>[answers]"
                                       value="5" <?php
                                    if(!empty($resultQuestion['keepAnswered'])){
                                        if($resultQuestion['keepAnswered'] == '5'){
                                            echo "checked";
                                        }
                                    }
                                    ?>>
                                <i></i>5
                            </label>

                            <?php
                                if(!empty($question->mandatory) &&
                                    $question->mandatory == 'N'){
                            ?>
                                <label class="radio">
                                    <input type="radio"
                                           name="question_<?php echo $question->id; ?>[answers]"
                                           value="notanswer"
                                            <?php
                                            if(!empty($resultQuestion)){
                                                if(!empty($resultQuestion['keepAnswered'])){
                                                    if($resultQuestion['keepAnswered'] == 'notanswer'){
                                                        echo "checked";
                                                    }
                                                }
                                            } else {
                                                echo "checked";
                                            }
                                            ?>>
                                    <i></i>Sin respuesta
                                </label>
                            <?php
                                }
                            ?>
                        </div>
                    </section>

                </div>
                @include('survey.public.questions.littlepartial.help')

                @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>