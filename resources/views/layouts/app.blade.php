<?php
Auth::user()->checkHasPermission(['ConfigApp_viewconfigglobal']);
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title> Sistema de gestión de encuesta </title>
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
      <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">

      <link rel="icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">
      <link rel="shortcut icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">

      @include('elements.layout.assetcss')

      @stack('css')

   </head>
   {{--menu-on-top--}}
   <body class="<?php echo !empty($viewDataApp["showMenuQuestions"]) ? 'menu-on-top' : ''; ?>">
      <!-- #HEADER -->
      <header id="header">
         <div id="logo-group">
            <!-- PLACE YOUR LOGO HERE -->
            <span id="logo">
               <a href="{{ route('admin.viewindex') }}">
                  <img src="<?php echo url('/') ?>/img/uni.png" class="img-responsive" alt="UNI">
               </a>
            </span>
            <!-- END LOGO PLACEHOLDER -->
            <!-- Note: The activity badge color changes when clicked and resets the number to 0
               Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
            <!--<span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>-->
            <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
            <div class="ajax-dropdown">
               <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
               <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-default">
                  <input type="radio" name="activity" id="ajax/notify/mail.html">
                  Msgs (14) </label>
                  <label class="btn btn-default">
                  <input type="radio" name="activity" id="ajax/notify/notifications.html">
                  notify (3) </label>
                  <label class="btn btn-default">
                  <input type="radio" name="activity" id="ajax/notify/tasks.html">
                  Tasks (4) </label>
               </div>
               <!-- notification content -->
               <div class="ajax-notifications custom-scroll">
                  <div class="alert alert-transparent">
                     <h4>Click a button to show messages here</h4>
                     This blank page message helps protect your privacy, or you can show the first message here automatically.
                  </div>
                  <i class="fa fa-lock fa-4x fa-border"></i>
               </div>
               <span> Last updated on: 12/12/2013 9:43AM
                  <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
                  <i class="fa fa-refresh"></i>
                  </button>
               </span>
               <!-- end footer -->
            </div>
            <!-- END AJAX-DROPDOWN -->
         </div>
         <!-- #PROJECTS: projects dropdown -->
         <div class="project-context hidden-xs">
            <!-- <span class="label">Projects:</span>
            <span class="project-selector dropdown-toggle" data-toggle="dropdown">Recent projects <i class="fa fa-angle-down"></i></span> -->
            <!-- Suggestion: populate this list with fetch and push technique -->
            <!-- <ul class="dropdown-menu">
               <li>
                  <a href="javascript:void(0);">Online e-merchant management system - attaching integration with the iOS</a>
               </li>
               <li>
                  <a href="javascript:void(0);">Notes on pipeline upgradee</a>
               </li>
               <li>
                  <a href="javascript:void(0);">Assesment Report for merchant account</a>
               </li>
               <li class="divider"></li>
               <li>
                  <a href="javascript:void(0);"><i class="fa fa-power-off"></i> Clear</a>
               </li>
            </ul> -->
            <!-- end dropdown-menu-->
         </div>
         <!-- end projects dropdown -->
         <!-- #TOGGLE LAYOUT BUTTONS -->
         <!-- pulled right: nav area -->
         <div class="pull-right">
            <!-- collapse menu button -->
            <div id="hide-menu" class="btn-header pull-right">
               <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
            </div>
            <!-- end collapse menu -->
            <!-- #MOBILE -->
            <!-- Top menu profile link : this shows only when top menu is active -->
            <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
               <li class="">
                  <!--<a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">-->
                  <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                  <img src="https://www.mastermagazine.info/termino/wp-content/uploads/Usuario-Icono.jpg"
                       alt="" class="online" />
                  </a>
                  <ul class="dropdown-menu pull-right">
                     <!--<li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                     </li>-->
                     <!--<li class="divider"></li>
                     <li>
                        <a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                     </li>
                     <li class="divider"></li>
                     <li>
                        <a href="javascript:void(0);"
                           class="padding-10 padding-top-0 padding-bottom-0"
                           data-action="toggleShortcut">
                           <i class="fa fa-arrow-down"></i><u>S</u>hortcut
                        </a>
                     </li>-->
                     <li class="divider"></li>
                     <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                     </li>
                     <li class="divider"></li>
                     <li>

                        <a href="{{ route('logout') }}" class="padding-10 padding-top-5 padding-bottom-5"
                           onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                           <i class="fa fa-sign-out fa-lg"></i> <strong><u>S</u>alir</strong>
                        </a>
                        <form id="logout-form"
                              action="{{ route('logout') }}"
                              method="POST" style="display: none;">
                           {{ csrf_field() }}
                        </form>

                        {{--<a href="login.html"--}}
                           {{--class="padding-10 padding-top-5 padding-bottom-5"--}}
                           {{--data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong>--}}
                        {{--</a>--}}
                     </li>
                  </ul>
               </li>
            </ul>
            <!-- logout button -->
            <div id="logout" class="btn-header transparent pull-right">
               <span>
                  <a href="{{ route('logout') }}"
                     class="padding-10 padding-top-5 padding-bottom-5"
                     onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                     <i class="fa fa-sign-out" style="position: relative;top: -8px;"></i>
                  </a>
                  {{--<form id="logout-form"--}}
                        {{--action="{{ route('logout') }}"--}}
                        {{--method="POST" style="display: none;">--}}
                     {{--{{ csrf_field() }}--}}
                  {{--</form>--}}

                  {{--<a href="login.html"--}}
                     {{--title="Sign Out"--}}
                     {{--data-action="userLogout"--}}
                     {{--data-logout-msg="You can improve your security further after logging out by closing this opened browser">--}}
                     {{--<i class="fa fa-sign-out"></i>--}}
                  {{--</a>--}}
               </span>
            </div>
            <!-- end logout button -->
            <!-- search mobile button (this is hidden till mobile view port) -->
            <div id="search-mobile" class="btn-header transparent pull-right">
               <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
            </div>
            <!-- fullscreen button -->
            <div id="fullscreen" class="btn-header transparent pull-right">
               <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
            </div>
            <!-- end fullscreen button -->
         </div>
         <!-- end pulled right: nav area -->
      </header>
      <!-- END HEADER -->
      <!-- #NAVIGATION -->
      @include('elements.layout.sidebarMenu')
      <!-- END NAVIGATION -->
      <!-- MAIN PANEL -->
      <div id="main" role="main">
         <!-- RIBBON -->
         <div id="ribbon" class="<?php echo !empty($viewDataApp["showMenuQuestions"]) ? 'hideElement' : ''; ?>">
            <span class="ribbon-button-alignment"> 
            <!-- <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
            </span>  -->
            </span>
         </div>
         @include('elements.view._message')
         @yield('content')
         <!-- END RIBBON -->
         <!-- MAIN CONTENT -->
      </div>
      <!-- END MAIN PANEL -->
      <!-- PAGE FOOTER -->
      <div class="page-footer">
         <div class="row">
            <div class="col-xs-12 col-sm-6">
               <span class="txt-color-white">Sistema de gestión de encuesta ©2017 - <?php echo date("Y");  ?></span>
            </div>
            <!-- <div class="col-xs-6 col-sm-6 text-right hidden-xs">
               <div class="txt-color-white inline-block">
                  <i class="txt-color-blueLight hidden-mobile">Last account activity <i class="fa fa-clock-o"></i> <strong>52 mins ago &nbsp;</strong> </i>
                  <div class="btn-group dropup">
                     <button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
                     <i class="fa fa-link"></i> <span class="caret"></span>
                     </button>
                     <ul class="dropdown-menu pull-right text-left">
                        <li>
                           <div class="padding-5">
                              <p class="txt-color-darken font-sm no-margin">Download Progress</p>
                              <div class="progress progress-micro no-margin">
                                 <div class="progress-bar progress-bar-success" style="width: 50%;"></div>
                              </div>
                           </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                           <div class="padding-5">
                              <p class="txt-color-darken font-sm no-margin">Server Load</p>
                              <div class="progress progress-micro no-margin">
                                 <div class="progress-bar progress-bar-success" style="width: 20%;"></div>
                              </div>
                           </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                           <div class="padding-5">
                              <p class="txt-color-darken font-sm no-margin">Memory Load <span class="text-danger">*critical*</span></p>
                              <div class="progress progress-micro no-margin">
                                 <div class="progress-bar progress-bar-danger" style="width: 70%;"></div>
                              </div>
                           </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                           <div class="padding-5">
                              <button class="btn btn-block btn-default">refresh</button>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </div> -->
         </div>
      </div>
      <!-- END PAGE FOOTER -->
      <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
         Note: These tiles are completely responsive,
         you can add as many as you like
         -->
      <div id="shortcut">
         <ul>
            <!--<li>
               <a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
            </li>
            <li>
               <a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
            </li>
            <li>
               <a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
            </li>
            <li>
               <a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
            </li>
            <li>
               <a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
            </li>-->

            <li>
               <a href="profile.html"
                  class="jarvismetro-tile big-cubes selected bg-color-pinkDark">
                  <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>Ver mi perfil</span></span>
               </a>
            </li>
         </ul>
      </div>

      @include('elements.layout.assetjavascript')
      @stack('scripts')
      
   </body>
</html>