<div class="modal fade"
     id="mdConfirm"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center" id="myModalLabel">
                    <strong>
                        ¿Está seguro que desea eliminar este elemento?
                    </strong>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <i class="fa fa-warning" style="font-size: 100px;color: #850000;"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            <strong>
                                ¡No podrás recuperar este elemento!
                            </strong>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger btn-ok">
                    <i class="fa fa-trash-o"></i>&nbsp; Eliminar elemento
                </a>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{--<div class="modal-dialog demo-modal">--}}
    {{--<div class="modal-content">--}}
        {{--<div class="modal-header">--}}
            {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">--}}
                {{--×--}}
            {{--</button>--}}
            {{--<h4 class="modal-title">Modal title</h4>--}}
        {{--</div>--}}
        {{--<div class="modal-body">--}}
            {{--<p>--}}
                {{--One fine body…--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--<div class="modal-footer">--}}
            {{--<button type="button" class="btn btn-default" data-dismiss="modal">--}}
                {{--Close--}}
            {{--</button>--}}
            {{--<button type="button" class="btn btn-primary">--}}
                {{--Save changes--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}