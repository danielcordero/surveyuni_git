<?php

//use \App\CustomLib\TypeQuestions\BaseQuestion;
//$helperHtml = new HelperHtml();
namespace App\CustomLib\Helper;
//use \App\CustomLib\Helper\HelperHtml;
use \stdClass;

//echo "<pre>";
//$helperHtml = new HelperHtml();
//print_r( $helperHtml );
//exit;


if (empty($viewData)) {
    $viewData['survey'] = new \stdClass();
}

$existSurveyId = !empty($viewData['survey']) && !empty($viewData['survey']->id);

?>
<section id="content">

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa-fw fa fa-puzzle-piece"></i>
                <strong>
                    <?php
                    echo !empty($existSurveyId) ? "Editar" : "Crear";
                    ?>
                    encuesta (Configuración)
                </strong>
            </h1>
        </div>
    </div>

    <section id="widget-grid" class="">
        <div class="well">

            <div class="row">
                <div class="col-md-12">
                    <p class="text-right">
                        <button type="button" id="btnSave"
                                class="btn btn-primary openMainModal">
                            <i class="fa fa-save"></i> Guardar
                        </button>
                        <a class="btn btn-danger"
                           href="{{ route('survey.index') }}">
                            Cancelar
                            <span class="glyphicon glyphicon-arrow-right"></span>
                        </a>
                    </p>
                </div>
            </div>

            <hr>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Básico</a></li>
                <!--<li><a data-toggle="tab" href="#menu1">Avanzada</a></li>-->
            </ul>

            @if(!empty($viewData['survey']) && !empty($viewData['survey']->id))
                <form action="{{ route('survey.update',['surveyId'=>$viewData['survey']->id]) }}"
                      role="form" method="post" id="frmSurvey">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    @else
                        <form action="{{ route('survey.store') }}"
                              method="post" id="frmSurvey" class="">
                            {{ csrf_field() }}
                            @endif

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <br>
                        @include('survey.elements.tags.basicConfig')
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <br>
                        {{--@include('survey.elements.tags.pagesConfig')--}}
                    </div>
                </div><button type="submit" class="hide"></button>
                @if(!empty($viewData['survey']) && !empty($viewData['survey']->id))
                    <input type="hidden"
                           name="survey_id"
                           id="survey_id"
                           value="{{$viewData['survey']->id}}">
                @endif
            </form>
        </div>
    </section>
</section>
@push('scripts')

@endpush