<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Closure;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'survey/{surveyId}',
    ];

    public function handle($request, Closure $next)
    {
        foreach($this->except as $route) {
            if(strpos($request->route()->uri,$route) !== false){
                return $next($request);
            }
        }
        return parent::handle($request, $next);
    }

}
