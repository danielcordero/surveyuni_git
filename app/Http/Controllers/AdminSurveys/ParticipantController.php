<?php

namespace App\Http\Controllers\AdminSurveys;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Http\Requests\CsvImportRequest;
use App\{
    CustomLib\Services\SurveyService, Survey, Participant
};
use App\CustomLib\Services\{EmailService,SurveySettingsServices};
use App\CustomLib\Libraries\CSecurityManager;
use App\CustomLib\StatusCodes;
use App\CustomLib\Services\{ParticipantService,SurveyPublic};
use App\CustomLib\Helper\HelperServices;
use Illuminate\Support\Facades\Auth;
use App\CustomLib\GenericMessages;
use Storage;
use Session;


class ParticipantController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckOwnSurvey');
        //$this->middleware('CheckPermission',['except'=>'setroles']);
    }

    public function viewconfig(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyId = HelperServices::sanitize_int($surveyId);
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['survey']->amountSent = ParticipantService::getAmountSent( $surveyId );
        $viewData['survey']->amountCompleted = ParticipantService::getAmountCompleted( $surveyId );

        return view('participant.viewconfig')
            ->with('viewData',$viewData);
    }

    public function viewadd(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyId = HelperServices::sanitize_int($surveyId);
        $viewData['survey'] = Survey::find( $surveyId );
        return view('participant.viewadd')
            ->with('viewData',$viewData);
    }

    public function viewaddcustomfields(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyId = HelperServices::sanitize_int($surveyId);
        $survey = Survey::find( $surveyId );
        $viewData['survey'] = $survey;
        $additionalData = SurveyService::getConfigData($survey->additionalData);
        if(!empty($additionalData) && !empty($additionalData->customFields)){
            $viewData['customFields'] = (array)$additionalData->customFields;
        }

        return view('participant.viewaddcustomfields')
            ->with('viewData',$viewData);
    }

    public function createcustomfields(Request $request)
    {
        $surveyId = $request->survey_id;
        $surveyId = HelperServices::sanitize_int($surveyId);
        $survey = Survey::find($surveyId);
        $customfieldsToSaved = $request->data['customfields'];
        if(!empty($request->data['mandatoryfield'])){
            $mandatoryfieldKey = array_keys($request->data['mandatoryfield']);
        }
        $customfields = [];
        if(!empty($customfieldsToSaved)){
            $namesFields = array_values($customfieldsToSaved);
            $namesFields = HelperServices::lowerCaseArray($namesFields);

            if(!HelperServices::array_has_dupes($namesFields)){
                foreach ($customfieldsToSaved as $key => $value){
                    $customfield = [];
                    $customfield['id'] = $key;
                    $value = explode(' ',trim($value));
                    $customfield['namefield'] = $value[0];
                    if(!empty($mandatoryfieldKey) && in_array($key, $mandatoryfieldKey)){
                        $customfield['isRequired'] = true;
                    } else {
                        $customfield['isRequired'] = false;
                    }
                    $customfields[$key] = $customfield;
                }
            } else {
                Session::flash('error', GenericMessages::$all['duplicateField']);
                return redirect()->route('participants.viewaddcustomfields',[$surveyId]);
            }
        }

        $additionalData = SurveyService::getConfigData($survey->additionalData);
        $additionalData->customFields = $customfields;
        $survey->additionalData = json_encode($additionalData);
        $survey->save();

        Session::flash('success', GenericMessages::$all['successOperation']);
        return redirect()->route('participants.viewaddcustomfields',[$surveyId]);
    }

    public function viewgeneratetoken(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyId = HelperServices::sanitize_int($surveyId);
        $viewData['survey'] = Survey::find( $surveyId );
        return view('participant.viewgeneratetoken')
            ->with('viewData',$viewData);
    }

    public function createtokens(Request $request)
    {
        ini_set('max_execution_time', 300);
        $tokenLength = 15;
        $surveyId = $request->survey_id;
        $tkresult = ParticipantService::getWithoutTokenBy( $surveyId );
        $cSecurityManager = new CSecurityManager;

        $newtokencount = 0;
        $invalidtokencount=0;
        $existingtokens = [];
        foreach ($tkresult as $tkrow)
        {

            $bIsValidToken = false;
            while ($bIsValidToken == false && $invalidtokencount<50)
            {
                $newtoken = $cSecurityManager->generateRandomString($tokenLength);
                if (!isset($existingtokens[$newtoken]))
                {
                    $existingtokens[$newtoken] = true;
                    $bIsValidToken = true;
                    $invalidtokencount=0;
                }
                else
                {
                    $invalidtokencount++;
                }
            }
            if($bIsValidToken)
            {
                $newtoken = str_replace("~","",$newtoken);
                $itresult = ParticipantService::updateToken($tkrow->spid, $newtoken);
                $newtokencount++;
            }
            else
            {
                break;
            }
        }
        Session::flash('success', GenericMessages::$all['successOperation']);
        return redirect()->route('participants.listAll',[$surveyId]);
    }


    public function addEdit(Request $request)
    {
        $parameters = $request->toArray();
        $parameters['email'] = $parameters['pemail'];
        $parameters['firstname'] = $parameters['pfirstname'];
        $parameters['lastname'] = $parameters['plastname'];
        $parameters['token'] = ParticipantService::generateToken();

        if(!ParticipantService::hasValidEmail( $parameters['email'] )){
            Session::flash('error', GenericMessages::$all['invalidEmail']);
            return redirect()->route('participants.add',[$request->survey_id]);
        }

        $participantInfo = ParticipantService::addEdit($parameters);
        if(!empty($participantInfo)){
            $result = ParticipantService::getSurvey($request->survey_id, $participantInfo->id);
            if(!empty($result)){
                Session::flash('error', GenericMessages::$all['emailExistSurvey']);
            } else {
                $result = ParticipantService::setToSurvey($request->survey_id, $participantInfo->id, $parameters);
                if(!empty($result)){
                    SurveyService::setNotAllInvitationSent($request->survey_id);
                    Session::flash('success', GenericMessages::$all['successOperation']);
                }
            }
            return redirect()->route('participants.listAll',[$request->survey_id]);
        }

        Session::flash('error', GenericMessages::$all['failOperation']);
        $surveyId = (int)$request->surveyId;
        $viewData['survey'] = Survey::find( $surveyId );
        return view('participant.viewadd')->with('viewData',$viewData);
    }

    public function getInfo(Request $request)
    {
        $info = ParticipantService::getOneBySurvey($request->surveyId,$request->participantId);
        return response()->json($info,StatusCodes::HTTP_OK);
    }

    public function updateone(Request $request)
    {
        $response = new \stdClass();
        $response->result = 1;
        $request->pvdatefrom = !empty($request->pvdatefrom) ? $request->pvdatefrom : NULL;
        $request->pvdateuntil = !empty($request->pvdateuntil) ? $request->pvdatefrom : NULL;
        //if(!empty($request->pvdatefrom) && !empty($request->pvdatefrom)){}
        $string = "UPDATE 
                    survey_participant 
                    SET survey_participant.firstname = '{$request->pfirstname}',
                    survey_participant.lastname = '{$request->plastname}'
                    WHERE survey_participant.id = {$request->survey_participant_id}";

        $result = \DB::statement($string);
        $response->result = $result;
        return response()->json($response,StatusCodes::HTTP_OK);
    }

    public function listAll(Request $request)
    {
        $amountPerPage = 15;
        $surveyId = (int)$request->surveyId;
        $surveyId = HelperServices::sanitize_int($surveyId);
        $parameters = $request->toArray();
        $surveyPublic = new SurveyPublic();
        $viewData = $surveyPublic->getConfig( $surveyId );
        $viewData['participants'] = ParticipantService::filter($surveyId, $request, $amountPerPage);
//        if( count($parameters) > 1){
//            $viewData['participants'] = ParticipantService::filter($surveyId, $request, $amountPerPage);
//        } else {
//            $viewData['participants'] = $viewData['survey']->participants()->paginate($amountPerPage);
//        }

        return view('participant.list')->with('viewData',$viewData);
    }

    public function viewupload(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $surveyId = HelperServices::sanitize_int($surveyId);
        $viewData['survey'] = SurveyService::getInfoBydId( $surveyId );
        return view('participant.viewupload')->with('viewData',$viewData);
    }

    public function destroySurveyParticipant(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $result = true;

        if(!empty($request->survey_participant)){
            $survey_participant = (int)$request->survey_participant;
            $result = \DB::table('survey_participant')->delete($survey_participant);
        }

        if(!empty($request->idstodelete)){
            $survey_participant = $request->idstodelete;
            $survey_participant_ids = explode(",",$survey_participant);

            $result = \DB::table('survey_participant')
                ->whereIn("id",$survey_participant_ids)
                ->delete();
        }

        if($result){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else {
            Session::flash('error', GenericMessages::$all['failOperation']);
        }

        return redirect()->route('participants.listAll',[$surveyId]);
    }

    public function destroyParticipantAll(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $result = true;

        \DB::table('survey_participant')
            ->where("survey_id",$surveyId)
            ->delete();

        Session::flash('success', GenericMessages::$all['successOperation']);
        return redirect()->route('participants.listAll',[$surveyId]);
    }

    public function uploadfile(Request $request)
    {
        ini_set('max_execution_time', 300);

        //CsvImportRequest $requestCSV
        if($request->isMethod('post')) {

            $survey = SurveyService::getInfoBydId( $request->survey_id );
            //Merge fields required with custom fields required
            if(!empty($survey->customFields)){
                $customsFieldsRequired = $customsFields = [];
                foreach ($survey->customFields as $key => $value){
                    $customsFields[] = $value->namefield;
                    if(!empty($value->isRequired)){
                        $customsFieldsRequired[] = $value->namefield;
                    }
                }
            }

            if(!$request->hasFile('csv_file')){
                Session::flash('error', GenericMessages::$all['invalidFile']);
                return redirect()->route('participants.viewupload',[$request->surveyId]);
            }
            $extensionFile = $request->file("csv_file")->getClientOriginalExtension();
            if($extensionFile != "csv"){
                Session::flash('error', GenericMessages::$all['invalidFile']);
                return redirect()->route('participants.viewupload',[$request->surveyId]);
            }

            $participantService = new ParticipantService;
            $objectResponse = new \stdClass();
            $file = $request->file('csv_file');
            $path = $file->getRealPath();
            $extension = $file->getClientOriginalExtension();
            $fileName = $file->getClientOriginalName();

            // This allows to read file with MAC line endings too
            @ini_set('auto_detect_line_endings', true);
            $aListArray = file($path);
            $aFilterDuplicateFields = ['firstname', 'lastname', 'email'];
            $csvcharset = "auto";
            $separator = "auto";
            $filterblankemail = 1;
            $allowinvalidemail = 0;
            $showwarningtoken = 0;
            $filterduplicatetoken = 1;
            $iRecordCount = 0;
            $aFirstLine = $aModelErrorLis = $aDuplicateList = $aInvalidEmailList = [];
            $aMissingAttrFieldName = $aInvalideAttrFieldName = [];

            $iRecordImported = $iRecordCount = $iRecordOk = $iInvalidEmailCount = 0;
            $headerFaild = false;

            foreach ($aListArray as $buffer) {
                $buffer = @mb_convert_encoding($buffer, "UTF-8", $csvcharset);
                $aAllowedFieldNames = HelperServices::getTableColumns('participants');

                if ($iRecordCount == 0) {
                    // Parse first line (header) from CSV
                    $buffer = HelperServices::removeBOM($buffer);
                    //We allow all field except id because this one is really not needed
                    if (($kTid = array_search('id', $aAllowedFieldNames)) !== false) {
                        unset($aAllowedFieldNames[$kTid]);
                    }

                    $separator = $participantService->getSeparator($separator, $buffer);
                    //Read current line on file
                    $aFirstLine = str_getcsv($buffer, $separator, '"');
                    $aFirstLine = array_map('trim', $aFirstLine);
                    $aIgnoredColumns = [];
                    $countHeaderMatch = 0;

                    if(!empty($customsFieldsRequired)){
                        $aFilterDuplicateFields = array_merge($aFilterDuplicateFields, $customsFieldsRequired);
                    }

                    foreach ($aFirstLine as $index => $header) {
                        if (in_array($header, $aFilterDuplicateFields)) {
                            $countHeaderMatch++;
                        }
                    }

                    if(count($aFilterDuplicateFields) != $countHeaderMatch){
                        $headerFaild = true;
                        break;
                    }


//                    foreach ($aFirstLine as $index => $sFirstName) {
//                        $aFirstLine[$index] = preg_replace("/(.*) <[^,]*>$/", "$1", $sFirstName);
//                        $sFirstName = $aFirstLine[$index];
//
//                        if (!in_array($sFirstName, $aAllowedFieldNames)) {
//                            $aIgnoredColumns[] = $sFirstName;
//                        }
//                    }
                }
                else
                {
                    //Read current line on file
                    $line = str_getcsv($buffer, $separator, '"');
                    if (count($aFirstLine) != count($line)) {
                        $aInvalidFormatList[] = sprintf("Line %s", $iRecordCount);
                        $iRecordCount++;
                        continue;
                    }

                    $aWriteArray = array_combine($aFirstLine, $line);
                    $bDuplicateFound = false;
                    $bInvalidEmail = false;
                    $aWriteArray['email'] = !empty($aWriteArray['email']) ? trim($aWriteArray['email']) : "";
                    $aWriteArray['firstname'] = !empty($aWriteArray['firstname']) ? trim($aWriteArray['firstname']) : "";
                    $aWriteArray['lastname'] = !empty($aWriteArray['lastname']) ? trim($aWriteArray['lastname']) : "";

                    if ($filterduplicatetoken) {
                        $result = ParticipantService::filterDuplicateToken($aWriteArray, ['firstname', 'lastname', 'email']);

                        if (!empty($result)) {
                            $bDuplicateFound = true;
                            $aDuplicateList[] = sprintf("Linea %s : %s %s (%s)",
                                $iRecordCount,
                                $aWriteArray['firstname'], $aWriteArray['lastname'], $aWriteArray['email']);
                            $otherParameters['firstname'] = $aWriteArray['firstname'];
                            $otherParameters['lastname'] = $aWriteArray['lastname'];
                            if(!empty($customsFields)){
                                $otherParameters['customFields'] = [];
                                foreach ($customsFields as $key => $value){
                                    if(!empty($aWriteArray[$value])){
                                        $otherParameters['customFields'][$value] = $aWriteArray[$value];
                                    } else {
                                        $otherParameters['customFields'][$value] = "";
                                    }
                                }
                            }

                            $surveyParticipan["survey_id"] = $request->survey_id;
                            $surveyParticipan["participant_id"] = $result->id;
                            $resultExist = \DB::table('survey_participant')->where($surveyParticipan)->first();
                            if(!empty($resultExist)){

                                $bDuplicateFound = true;
                                $aDuplicateList[] = sprintf("Linea %s : %s %s (%s)",
                                    $iRecordCount,
                                    $aWriteArray['firstname'], $aWriteArray['lastname'], $aWriteArray['email']);

                            } else {
                                ParticipantService::setToSurvey((int)$request->survey_id, $result->id, $otherParameters);
                            }
                        }
                    }

                    //treat blank emails
                    if (!$bDuplicateFound && $aWriteArray['email'] != '') {
                        $aEmailAddress = preg_split("/(,|;)/", $aWriteArray['email']);
                        foreach ($aEmailAddress as $sEmailAddress) {
                            if (!ParticipantService::hasValidEmail($sEmailAddress)) {
                                $bInvalidEmail = true;
                                $aInvalidEmailList[] = sprintf("Line %s : %s %s (%s)",
                                    $iRecordCount,
                                    $aWriteArray['firstname'],
                                    $aWriteArray['lastname'],
                                    $aWriteArray['email']);
                            }
                        }
                    }

                    if (!$bDuplicateFound && !$bInvalidEmail) {
                        // Some default value : to be moved to Token model rules in future release ?
                        // But think we have to accept invalid email etc ... then use specific scenario
                        $newParticipant = [];
                        foreach ($aAllowedFieldNames as $key => $value) {
                            if(array_key_exists($value,$aWriteArray)){
                                $newParticipant["{$value}"] = $aWriteArray[$value];
                            }
                        }
                        $newParticipant["created_by"] = Auth::id();
                        try{
                            $lastId = \DB::table('participants')->insertGetId($newParticipant);
                            if(!$lastId || empty($lastId))
                            {
                                $aModelErrorList[] =  "Error" . $iRecordCount;
                            }
                            else
                            {
                                $iRecordImported++;
                                $otherParameters['firstname'] = $aWriteArray['firstname'];
                                $otherParameters['lastname'] = $aWriteArray['lastname'];
                                if(!empty($customsFields)){
                                    $otherParameters['customFields'] = [];
                                    foreach ($customsFields as $key => $value){
                                        if(!empty($aWriteArray[$value])){
                                            $otherParameters['customFields'][$value] = $aWriteArray[$value];
                                        } else {
                                            $otherParameters['customFields'][$value] = "";
                                        }
                                    }
                                }
                                ParticipantService::setToSurvey((int)$request->survey_id, $lastId, $otherParameters);
                            }

                        }catch(\Illuminate\Database\QueryException $ex){
                            $aModelErrorList[] =  "Error" . $iRecordCount;
                        }
                    }
                    $iRecordOk++;
                }
                $iRecordCount++;
            }
            $iRecordCount = $iRecordCount - 1;
            unlink($path);

            //Header file required
            if(!empty($headerFaild)){
                Session::flash('error', GenericMessages::$all['invalidHeaderFile']);
                return redirect()->route('participants.viewupload',[$request->surveyId]);
            }

            if(!empty($iRecordOk)){
                SurveyService::setNotAllInvitationSent($request->surveyId);
            }

            $viewData['aTokenListArray'] = $aListArray;// Big array in memory, just for success ?
            $viewData['iRecordImported'] = $iRecordImported;
            $viewData['iRecordOk'] = $iRecordOk;
            $viewData['iRecordCount'] = $iRecordCount < 0 ? 0 : $iRecordCount;
            $viewData['aDuplicateList'] = $aDuplicateList;
            $viewData['aInvalidEmailList'] = $aInvalidEmailList;

            $viewData['survey'] = Survey::find((int)$request->survey_id);
            return view('participant.viewresultupload')->with('viewData',$viewData);
        }
        else
        {
            return redirect()->route('participants.viewupload',[$request->surveyId]);
        }
    }

    public function sendSomeInvitation(Request $request){
        $surveyInfo = Survey::find( $request->surveyId );
        $keySession = 'some_invitation_'.$request->surveyId;
        if(!empty($surveyInfo->active) && $surveyInfo->active == "Y"){
            Session::put($keySession, $request->idsInvitations);
        } else {
            Session::forget($keySession);
        }
        return redirect()->route('email.viewsendinvitation',[$request->surveyId]);

    }

    public function sendSomeRemainder(Request $request){
        $surveyInfo = Survey::find( $request->surveyId );
        $keySession = 'some_remainder_'.$request->surveyId;
        if(!empty($surveyInfo->active) && $surveyInfo->active == "Y"){
            Session::put($keySession, $request->idsRemainder);
        } else {
            Session::forget($keySession);
        }
        return redirect()->route('email.viewsendremind',[$request->surveyId]);
    }

}