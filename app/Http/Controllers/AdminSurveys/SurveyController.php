<?php

namespace App\Http\Controllers\AdminSurveys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\GenericMessages;
use App\CustomLib\Services\{SurveyPublic,SurveyService,SurveySettingsServices,GroupService};
use App\CustomLib\Helper\HelperServices;
use App\Survey;
use App\SurveySettings;
use Session;
use File;
use Auth;

class SurveyController extends Controller
{
    private $excet = ['setroles','index','viewindex','redirectpreviewmode'];

    public function __construct()
    {
        $this->middleware('auth');
        /*$excet = $this->excet;
        $this->middleware('auth');*/
    }

    public function index(Request $request)
    {
        $perPage = 10;
        $currentUser = Auth::user();
        $parameters = $request->toArray();

        if( !empty($parameters) ){
            $surveyService = new SurveyService;
            $surveys = $surveyService->filter($request);
            $surveys = $surveys->paginate($perPage);
        } else {
            if(!empty($currentUser->is_super_admin)){
                $surveys = Survey::orderBy('created_at', 'DESC')->paginate($perPage);
            } else {
                $surveys = Survey::where('user_id',$currentUser->id)
                    ->orderBy('created_at', 'DESC')->paginate($perPage);
            }

        }

        $viewData["surveys"] = $surveys;
        return view('survey.list')->with("viewData", $viewData);
    }


    public function create()
    {
        return view('survey.create');
    }

    public function store(Request $request)
    {
        $survey = new Survey();
        $surveySettings = new SurveySettings();
        $survey->user_id = \Auth::user()->id;
        $survey->title = HelperServices::fixCKeditorText($request->txtTitle);
        $survey->description = HelperServices::fixCKeditorText($request->txADescription);
        $survey->welcometext = HelperServices::fixCKeditorText($request->txAWelcomeMessage);
        $survey->endtext     = HelperServices::fixCKeditorText($request->txAEndMessage);
        $survey->save();
        $lastId = $survey->id;
        SurveyService::saveConfig($lastId, $request);
        $defaultTemplates = SurveySettingsServices::getDefaultTemplates();
        $surveySettings->survey_survey_id = $lastId;
        $surveySettings->survey_title = HelperServices::fixCKeditorText($request->txtTitle);
        $surveySettings->survey_description = HelperServices::fixCKeditorText($request->txADescription);
        $surveySettings->survey_welcometext = HelperServices::fixCKeditorText($request->txAWelcomeMessage);
        $surveySettings->survey_endtext = HelperServices::fixCKeditorText($request->txAEndMessage);
        $surveySettings->survey_email_invite_subj = $defaultTemplates['admin_detailed_notification_subject'];
        $surveySettings->email_admin_responses = $defaultTemplates['admin_detailed_notification'];
        $surveySettings->email_admin_notification = $defaultTemplates['admin_notification'];
        $surveySettings->email_admin_notification_subj = $defaultTemplates['admin_notification_subject'];
        $surveySettings->survey_email_confirm_subj = $defaultTemplates['confirmation_subject'];
        $surveySettings->survey_email_confirm = $defaultTemplates['confirmation'];
        $surveySettings->survey_email_invite_subj = $defaultTemplates['invitation_subject'];
        $surveySettings->survey_email_invite = $defaultTemplates['invitation'];
        $surveySettings->survey_email_remind_subj = $defaultTemplates['reminder_subject'];
        $surveySettings->survey_email_remind = $defaultTemplates['reminder'];
        $surveySettings->survey_email_register_subj = $defaultTemplates['registration_subject'];
        $surveySettings->survey_email_register = $defaultTemplates['registration'];
        $surveySettings->save();

        Session::flash('success', GenericMessages::$all['successOperation']);
        return redirect()->route('survey.index');
    }

    public function show($surveyId = "")
    {
        $surveyId = !empty($surveyId) ? $surveyId : 1;
        $viewData['survey'] =  SurveyService::getInfoBydId($surveyId);

        return view('survey.edit')->with('viewData', $viewData);
    }

    public function edit($surveyId = "")
    {
        
    }


    public function update(Request $request, $id)
    {
        $survey = Survey::find($id);
        $survey->user_id = \Auth::user()->id;
        $survey->title = $request->txtTitle; 
        $survey->description = $request->txADescription; 
        $survey->welcometext = $request->txAWelcomeMessage; 
        $survey->endtext     = $request->txAEndMessage;
        $lastId = $survey->save();
        SurveyService::saveConfig($id, $request);
        Session::flash('success', GenericMessages::$all['successOperation']);
        return redirect()->route('survey.index');
    }

    public function destroy($surveyId)
    {
        $success = true;
        \DB::beginTransaction();
        try {
            $surveyId = (int)$surveyId;
            $surveyPublic = new SurveyPublic();
            $surveyConfig = $surveyPublic->getConfig( $surveyId );
            \DB::table('questions')->whereIn('id',$surveyConfig['questionsIds'])->delete();
            \DB::table('groups')->whereIn('id', $surveyConfig['groupsIds'])->delete();
            $surveyConfig['survey']->delete();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }

        return redirect()->route('survey.index');
    }

    public function questions(){
        return view('survey.questions.manage');
    }

    public function viewindex(){
        return view('survey.viewindex');
    }

    public function viewactivesurvey(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['surveysetting'] = $viewData['survey']->surveysetting;
        return view('survey.viewactivesurvey')->with('viewData',$viewData);
    }

    public function saveactivesurvey(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $survey = Survey::find( $surveyId );
        $survey->active = "Y";
        if($survey->save()){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }
        return redirect()->route('surveyDetail',['surveyId'=>$surveyId]);
    }

    public function redirectpreviewmode(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $url = HelperServices::addQStoRoute('public.index', [$surveyId], ['mode'=>"preview"]);
        return redirect()->away($url);
    }

    public function savedesactivesurvey(Request $request)
    {
        $surveyId = (int)$request->surveyId;
        $survey = Survey::find( $surveyId );
        $survey->active = "N";
        if($survey->save()){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }
        return redirect()->route('surveyDetail',['surveyId'=>$surveyId]);
    }

    public function clonesu(Request $request)
    {
        $fullConfig = SurveyService::getFullConfig( $request->surveyId );

        $isOkEverything = true;
        try {

            $survey = new Survey();
            $survey->user_id = \Auth::user()->id;
            $survey->title =  ("(Copia ".date("Y-m-d").")").$fullConfig['survey'][0]->title;
            $survey->description = $fullConfig['survey'][0]->description;
            $survey->welcometext = $fullConfig['survey'][0]->welcometext;
            $survey->endtext     = $fullConfig['survey'][0]->endtext;
            $survey->additionalData = $fullConfig['survey'][0]->additionalData;
            if(!$survey->save()){
                throw new \Exception("Hubo un error al ejecutar está acción");
            }
            $lastId = $survey->id;
            $surveySettings = new SurveySettings();
            $surveySettings->survey_survey_id = $lastId;
            $surveySettings->survey_title = ("(Copia ".date("Y-m-d").")").$fullConfig['survey'][0]->title;
            $surveySettings->survey_description = $fullConfig['survey'][0]->description;
            $surveySettings->survey_welcometext = $fullConfig['survey'][0]->endtext;
            $surveySettings->survey_endtext = $fullConfig['survey'][0]->endtext;
            $surveySettings->survey_email_invite_subj = $fullConfig['survey'][0]->survey_email_invite_subj;
            $surveySettings->email_admin_responses = $fullConfig['survey'][0]->email_admin_responses;
            $surveySettings->email_admin_notification = $fullConfig['survey'][0]->email_admin_notification;
            $surveySettings->email_admin_notification_subj = $fullConfig['survey'][0]->email_admin_notification_subj;
            $surveySettings->survey_email_confirm_subj = $fullConfig['survey'][0]->survey_email_confirm_subj;
            $surveySettings->survey_email_confirm = $fullConfig['survey'][0]->survey_email_confirm;
            $surveySettings->survey_email_invite_subj = $fullConfig['survey'][0]->survey_email_invite_subj;
            $surveySettings->survey_email_invite = $fullConfig['survey'][0]->survey_email_invite;
            $surveySettings->survey_email_remind_subj = $fullConfig['survey'][0]->survey_email_remind_subj;
            $surveySettings->survey_email_remind = $fullConfig['survey'][0]->survey_email_remind;
            $surveySettings->survey_email_register_subj = $fullConfig['survey'][0]->survey_email_register_subj;
            $surveySettings->survey_email_register = $fullConfig['survey'][0]->survey_email_register;
            if(!$surveySettings->save()){
                throw new \Exception("Hubo un error al ejecutar está acción");
            }


            $allRecords = [];
            foreach( $fullConfig['groups'] as $group ) {
                $record['survey_id'] = $lastId;
                $record['name'] = $group->name;
                $record['title'] = $group->title;
                $record['description'] = $group->description;
                $record['sortorder'] = $group->sortorder;
                $record['created_by'] = $group->created_by;
                array_push($allRecords, $record);
            }


            if(HelperServices::insertMultiple($allRecords, 'groups')){
                throw new \Exception("Hubo un error al ejecutar está acción");
            }


            $newGroups = GroupService::getBySurveyRaw($lastId);
            $groupLoop = 0;
            foreach ($fullConfig['groupQuestions'] as $group){
                $allRecords = [];
                foreach ($group as $question){
                    $record = [];
                    $record['sid'] = $lastId;
                    $record['gid'] = $newGroups[$groupLoop]->id;
                    $record['type'] = $question->type;
                    $record['title'] = $question->title;
                    $record['help'] = $question->help;
                    $record['mandatory'] = $question->mandatory;
                    $record['hide'] = $question->hide;
                    $record['show_help'] = $question->show_help;
                    $record['sortorder'] = $question->sortorder;
                    $record['created_by'] = $question->created_by;
                    $record['additionalData'] = $question->additionalData;
                    array_push($allRecords, $record);
                }
                if(HelperServices::insertMultiple($allRecords, 'questions')){
                    throw new \Exception("Hubo un error al ejecutar está acción");
                }
                $groupLoop++;
            }
            \DB::commit();

        } catch (\Exception $e) {
            $isOkEverything = false;
            \DB::rollback();
        }

        if($isOkEverything){
            Session::flash('success', GenericMessages::$all['surveyclonedsuccessfully']);
            return redirect()->route('surveyDetail',['surveyId'=>$lastId]);
        } else {
            Session::flash('error', GenericMessages::$all['surveyclonedfailed']);
            return redirect()->route('surveyDetail',['surveyId'=>$request->surveyId]);
        }

    }
}