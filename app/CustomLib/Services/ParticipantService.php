<?php

namespace App\CustomLib\Services;

use Illuminate\Support\Facades\Auth;
use Mso\IdnaConvert\IdnaConvert;
use App\{Survey, Participant,SettingsGlobal};
use App\CustomLib\Helper\HelperServices;
use App\CustomLib\Libraries\CSecurityManager;

class ParticipantService{

    public static function addEdit($data)
    {
        $info = Participant::where('email',$data['email'])->first();

        if(empty($info)){
            $newParticipant = new Participant();
            $newParticipant->firstname = $data['firstname'];
            $newParticipant->lastname = $data['lastname'];
            $newParticipant->email = $data['email'];
            $newParticipant->created_by = Auth::user()->id;
            $newParticipant->save();
            $lastId = $newParticipant->id;
        }

        $id = !empty($lastId) ? $lastId : $info->id;
        return Participant::find($id);
    }

    public static function hasValidEmail($email)
    {
        $oIdnConverter = new IdnaConvert();
        $email = $oIdnConverter->encode($email);
        $bResult = filter_var($email, FILTER_VALIDATE_EMAIL);
        if ($bResult!==false)
        {
            return true;
        }
        return false;
    }

    public function getSeparator($typeSeparator, $bufferData)
    {
        switch ($typeSeparator)
        {
            case 'comma':
                $sSeparator = ',';
                break;
            case 'separator':
                $sSeparator = ';';
                break;
            default:
                $comma = substr_count($bufferData, ',');
                $semicolon = substr_count($bufferData, ';');
                $sSeparator = $semicolon > $comma ? ';' : ',';
        }
        return $sSeparator;
    }

    public static function getProgressBarStatus($participant, $groupsIds)
    {
        if(!empty($participant->completed) && $participant->completed == "Y"){
            return 100;
        }

        $additionalData = json_decode($participant->additionalData);
        $additionalData = !empty($additionalData) ? $additionalData : [];
        $currentGroup = !empty($additionalData) ? $additionalData->currentGroup : 0;

        $statusProgressBar = $currentGroup / count($groupsIds);
        return $statusProgressBar * 100;
    }

    public static function filterDuplicateToken($record, $aFilterDuplicateFields)
    {
        $filterDuplicates = [];
        foreach ($aFilterDuplicateFields as $field) {
            if (!empty($record[$field])) {
                $filterDuplicate = ["{$field}","=",$record[$field]];
                $filterDuplicates[] = $filterDuplicate;
            }
        }
        return \DB::table('participants')->where($filterDuplicates)->first();
    }

    public static function setToSurvey($surveyId, $participantId, $parameters = [])
    {
        $surveyParticipan["survey_id"] = $surveyId;
        $surveyParticipan["participant_id"] = $participantId;
        $result = \DB::table('survey_participant')->where($surveyParticipan)->first();
        $setToArray = [];
        if(empty($result)){
            if(!empty($parameters)){
                $surveyParticipan['firstname'] = !empty($parameters['firstname']) ? $parameters['firstname'] : null;
                $surveyParticipan['lastname'] = !empty($parameters['lastname']) ? $parameters['lastname'] : null;
                $surveyParticipan['validfrom'] = !empty($parameters['validfrom']) ? $parameters['validfrom'] : null;
                $surveyParticipan['validuntil'] = !empty($parameters['validuntil']) ? $parameters['validuntil'] : null;
                $surveyParticipan['token'] = !empty($parameters['token']) ? $parameters['token'] : null;
                $surveyParticipan['customFields'] = !empty($parameters['customFields']) ? json_encode($parameters['customFields']) : null;
            }
            $setToArray = \DB::table('survey_participant')->insertGetId($surveyParticipan);
        }
        return $setToArray;
    }

    public static function getSurvey($surveyId, $participantId)
    {
        $surveyParticipan["survey_id"] = $surveyId;
        $surveyParticipan["participant_id"] = $participantId;
        $result = \DB::table('survey_participant')->where($surveyParticipan)->first();
        return empty($result) ? [] : (array)$result;
    }

    public  static function getAllCompleted($surveyId)
    {
        $parameters = [
            'surveyId' => $surveyId,
        ];
        $query = "SELECT * FROM survey_participant WHERE survey_id = :surveyId AND completed = 'Y'";
        $result = \DB::select( \DB::raw($query), $parameters);

        return $result;
    }

    public static function filter($surveyId, $request, $amountPerPage = 10)
    {
        $result = Participant::leftJoin('survey_participant',
            'participants.id', '=', 'survey_participant.participant_id')
            ->where('survey_participant.survey_id', '=', $surveyId);

            if ($request->has('fparticipantid') && !empty($request->fparticipantid)) {
                $ids = array_map('trim', explode(',', $request->fparticipantid));
                $result->whereIn('participants.id', $ids);
            }

            if ($request->has('ffirstname') && !empty($request->ffirstname)) {
                $result->where('participants.firstname', 'like', '%' . $request->ffirstname . '%');
            }

            if ($request->has('flastname') && !empty($request->flastname)) {
                $result->where('participants.lastname', 'like', '%' . $request->flastname . '%');
            }

            if ($request->has('fparticipantemail') && !empty($request->fparticipantemail)) {
                $result->where('participants.email', 'like', '%' . $request->fparticipantemail . '%');
            }

            if ($request->has('typeinvitation') && !empty($request->typeinvitation)) {
                $result->where('survey_participant.sent', $request->typeinvitation);
            }

            if ($request->has('typeremind') && !empty($request->typeremind)) {
                $result->where('survey_participant.remindersent', $request->typeremind);
            }

            if ($request->has('fStatus') && !empty($request->fStatus)) {
                $result->where('survey_participant.completed', $request->fStatus);
            }
//        $r = $result->paginate($amountPerPage)->toArray();
//            echo "<pre>";
//            print_r($r['data'][0]);
//            exit;
        return $result->paginate($amountPerPage);
    }

    public static function getOneBySurvey($surveyId, $participantId)
    {
        $surveyParticipant = \DB::select(\DB::raw("SELECT * FROM survey_participant WHERE id = '$participantId'"));
        if(!empty($surveyParticipant)){
            $surveyParticipant = (array)$surveyParticipant[0];
            $participantInfo = Participant::find($surveyParticipant['participant_id']);
            $surveyParticipant['firstname'] = $participantInfo->firstname;
            $surveyParticipant['lastname'] = $participantInfo->lastname;
            $surveyParticipant['email'] = $participantInfo->email;
        }

        return $surveyParticipant;

//        $users = DB::table('survey_participant')
//            ->select(DB::raw("
//              name,
//              surname,
//              (CASE WHEN (gender = 1) THEN 'M' ELSE 'F' END) as gender_text"));
//
//
//        $result = Participant::leftJoin('survey_participant',
//            'participants.id', '=', 'survey_participant.participant_id')
//            ->where([
//                ['survey_participant.survey_id', '=', $surveyId],
//                ['survey_participant.participant_id', '=', $participantId],
//                ])->get();
//
//        echo "<pre>";
//        print_r( $result->toArray() );
//        exit;
    }

    public static function prepareDataToList(&$participant)
    {

        $currentParticipanId = !empty($participant->pivot->id) ? $participant->pivot->id : $participant->id;
        $validfrom = !empty($participant->pivot->validfrom) ? $participant->pivot->validfrom : $participant->validfrom;
        $validuntil = !empty($participant->pivot->validuntil) ? $participant->pivot->validuntil : $participant->validuntil;
        $validfrom = !empty($validfrom) ? date('Y-m-d', strtotime($validfrom)) : "";
        $validuntil = !empty($validuntil) ? date('Y-m-d', strtotime($validuntil)) : "";
        $participant->validfrom = $validfrom;
        $participant->validuntil = $validuntil;
        $participant->currentParticipanId = $currentParticipanId;
        $participant->token = !empty($participant->pivot->token) ? $participant->pivot->token : $participant->token;
        $participant->firstname = !empty($participant->pivot->firstname) ? $participant->pivot->firstname : $participant->firstname;
        $participant->lastname = !empty($participant->pivot->lastname) ? $participant->pivot->lastname : $participant->lastname;
        $participant->sent = !empty($participant->pivot->sent) ? $participant->pivot->sent : $participant->sent;
        $participant->remindersent = !empty($participant->pivot->remindersent) ? $participant->pivot->remindersent : $participant->remindersent;
        $participant->completed = !empty($participant->pivot->completed) ? $participant->pivot->completed : $participant->completed;

        $participant->invitationDate = !empty($participant->pivot->invitationDate) ? $participant->pivot->invitationDate : $participant->invitationDate;
        $participant->reminderDate = !empty($participant->pivot->reminderDate) ? $participant->pivot->reminderDate : $participant->reminderDate;
        $participant->completedDate = !empty($participant->pivot->completedDate) ? $participant->pivot->completedDate : $participant->completedDate;

        if(!empty($participant->pivot->customFields)){
            $customFields = $participant->pivot->customFields;
            $customFields = json_decode($customFields);
            if(!empty($customFields)){
                foreach ($customFields as $key => $value){
                    $participant->{$key} = $value;
                }
            }
        }
        return $participant;
    }

    public static function getWithoutTokenBy($surveyId)
    {
        $result = \DB::select("SELECT sp.id as spid,p.firstname,p.lastname,p.email,sp.token FROM participants p 
	                inner join survey_participant sp on 
                    p.id = sp.participant_id 
                    where sp.survey_id = '{$surveyId}' and sp.token is null or sp.token=''");
        return $result;
    }

    public static function updateToken($spId, $token)
    {
        return \DB::statement("UPDATE survey_participant SET token = '{$token}' where id = {$spId}");
    }

    public static function getUninvited($surveyId, $toArray = true)
    {
        $surveyId = HelperServices::sanitize_int($surveyId);
        $parameters = [
            'surveyId' => $surveyId
        ];

        $maxemails = SettingsGlobal::find("maxemails");

        if(empty($maxemails)){
            $defaultConfig = config('globalconfig');
            $maxemails = !empty($defaultConfig["maxemails"]) ? $defaultConfig["maxemails"] : 0;
            $maxemails = HelperServices::sanitize_int($maxemails);
        } else {
            $maxemails = $maxemails->stg_value;
        }

        if(!empty($maxemails) && $maxemails > 15){
            $maxemails = 15;
        }

        if(empty($maxemails)){
            $maxemails = 15;
        }

        $parameters['limit'] = $maxemails;

//        $query = "SELECT * FROM participants AS p
//		INNER JOIN survey_participant AS sp ON p.id = sp.participant_id
//        WHERE survey_id = :surveyId AND (completed = 'N' OR completed = '') AND sent = 'N' LIMIT :limit";

        $query = "SELECT * FROM participants AS p 
		INNER JOIN survey_participant AS sp ON p.id = sp.participant_id 
        WHERE survey_id = :surveyId AND sent = 'N' LIMIT :limit";

        $result = \DB::select( \DB::raw($query), $parameters);

        if($toArray){
            $result = json_decode(json_encode($result), true);
        }

        return $result;
    }

    public static function getSomeUninvited($surveyId, $participantsIds, $toArray = true)
    {
        $surveyId = HelperServices::sanitize_int($surveyId);
        $participantsIds = explode(",", $participantsIds);
        $participantsIds = array_map("App\CustomLib\Helper\HelperServices::sanitize_int",$participantsIds);
        $participantsIds = implode(',',$participantsIds);

        $parameters = [
            'surveyId' => $surveyId,
            'participantsIds' => $participantsIds
        ];

        $maxemails = SettingsGlobal::find("maxemails");

        if(empty($maxemails)){
            $defaultConfig = config('globalconfig');
            $maxemails = !empty($defaultConfig["maxemails"]) ? $defaultConfig["maxemails"] : 0;
            $maxemails = HelperServices::sanitize_int($maxemails);
        } else {
            $maxemails = $maxemails->stg_value;
        }

        if(!empty($maxemails) && $maxemails > 15){
            $maxemails = 15;
        }

        if(empty($maxemails)){
            $maxemails = 15;
        }

        $parameters['limit'] = $maxemails;

        $query = "SELECT * FROM participants AS p 
        INNER JOIN survey_participant AS sp ON p.id = sp.participant_id 
        WHERE survey_id = {$surveyId} AND sent = 'N' AND sp.id in ($participantsIds) LIMIT $maxemails;";

        $result = \DB::select($query);
        if($toArray){
            $result = json_decode(json_encode($result), true);
        }
        return  (array) $result;
    }

    public static function getUnanswered($surveyId, $toArray = true)
    {
        $surveyId = HelperServices::sanitize_int($surveyId);
        $parameters = [
            'surveyId' => $surveyId
        ];

        $maxemails = SettingsGlobal::find("maxemails");

        if(empty($maxemails)){
            $defaultConfig = config('globalconfig');
            $maxemails = !empty($defaultConfig["maxemails"]) ? $defaultConfig["maxemails"] : 0;
            $maxemails = HelperServices::sanitize_int($maxemails);
        } else {
            $maxemails = $maxemails->stg_value;
        }

        if(empty($maxemails)){
            $maxemails = 50;
        }

        if(!empty($maxemails) && $maxemails > 50){
            $maxemails = 50;
        }

        $parameters['limit'] = $maxemails;

        $query = "SELECT * FROM participants AS p 
		INNER JOIN survey_participant AS sp ON p.id = sp.participant_id 
        WHERE survey_id = :surveyId AND (completed = 'N' OR completed = '') AND sent = 'Y' LIMIT :limit";

        $result = \DB::select( \DB::raw($query), $parameters);

        if($toArray){
            $result = json_decode(json_encode($result), true);
        }

        return $result;
    }

    public static function getSomeUnanswered($surveyId, $participantsIds, $toArray = true)
    {
        $surveyId = HelperServices::sanitize_int($surveyId);
        $participantsIds = explode(",", $participantsIds);
        $participantsIds = array_map("App\CustomLib\Helper\HelperServices::sanitize_int",$participantsIds);
        $participantsIds = implode(',',$participantsIds);

        $parameters = [
            'surveyId' => $surveyId
        ];

        $maxemails = SettingsGlobal::find("maxemails");

        if(empty($maxemails)){
            $defaultConfig = config('globalconfig');
            $maxemails = !empty($defaultConfig["maxemails"]) ? $defaultConfig["maxemails"] : 0;
            $maxemails = HelperServices::sanitize_int($maxemails);
        } else {
            $maxemails = $maxemails->stg_value;
        }

        if(empty($maxemails)){
            $maxemails = 50;
        }

        if(!empty($maxemails) && $maxemails > 50){
            $maxemails = 50;
        }

        $parameters['limit'] = $maxemails;

//        $query = "SELECT * FROM participants AS p
//		INNER JOIN survey_participant AS sp ON p.id = sp.participant_id
//        WHERE survey_id = :surveyId AND (completed = 'N' OR completed = '') AND sent = 'Y' LIMIT :limit";

        $query = "SELECT * FROM participants AS p 
		INNER JOIN survey_participant AS sp ON p.id = sp.participant_id 
        WHERE survey_id = {$surveyId} AND (completed = 'N' OR completed = '') AND sent = 'Y' AND sp.id in ($participantsIds) LIMIT $maxemails;";

        $result = \DB::select($query);
        if($toArray){
            $result = json_decode(json_encode($result), true);
        }


        return $result;
    }

    public static function setSendInvitation($participantId)
    {
        self::setSendInvitationDate( $participantId );
        $query = "UPDATE survey_participant
            SET sent = 'Y' 
            WHERE id = {$participantId}";

        return \DB::statement($query);
    }

    public static function setSendInvitationDate($participantId)
    {
        $date = date('Y-m-d H:i:s');
        $query = "UPDATE survey_participant
            SET invitationDate = '{$date}' 
            WHERE id = {$participantId}";
        return \DB::statement($query);
    }

    public static function setSendRemind($participantId)
    {
        self::setSendRemindDate( $participantId );
        $query = "UPDATE survey_participant
            SET remindersent = 'Y' 
            WHERE id = {$participantId}";

        return \DB::statement($query);
    }

    public static function setSendRemindDate($participantId)
    {
        $date = date('Y-m-d H:i:s');
        $query = "UPDATE survey_participant
            SET reminderDate = '{$date}'
            WHERE id = {$participantId}";

        return \DB::statement($query);
    }

    public static function setSurveyCompleted($participantId)
    {
        self::setSurveyCompletedDate( $participantId );
        $query = "UPDATE survey_participant
            SET completed = 'Y' 
            WHERE id = {$participantId}";

        return \DB::statement($query);
    }

    public static function setSurveyCompletedDate($participantId)
    {
        $date = date('Y-m-d H:i:s');
        $query = "UPDATE survey_participant
            SET completedDate = '{$date}' 
            WHERE id = {$participantId}";

        return \DB::statement($query);
    }

    public static function getAmountSent( $surveyId )
    {
        $parameters = [
            'surveyId' => $surveyId
        ];

//        $query = "SELECT count(*) as AmountSent FROM participants AS p
//		INNER JOIN survey_participant AS sp ON p.id = sp.participant_id
//        WHERE survey_id = :surveyId AND (completed = 'N' OR completed = '') AND sent = 'Y'";
        $query = "SELECT count(*) as AmountSent FROM participants AS p 
            INNER JOIN survey_participant AS sp ON p.id = sp.participant_id 
            WHERE survey_id = :surveyId AND sent = 'Y'";

        $result = \DB::select( \DB::raw($query), $parameters);

        if(empty( $result )){
            return 0;
        }
        return $result[0]->AmountSent;
    }

    public static function getAmountCompleted( $surveyId )
    {
        $parameters = [
            'surveyId' => $surveyId
        ];

        $query = "SELECT count(*) as AmountSent FROM participants AS p 
		INNER JOIN survey_participant AS sp ON p.id = sp.participant_id 
        WHERE survey_id = :surveyId AND completed = 'Y'";

        $result = \DB::select( \DB::raw($query), $parameters);

        if(empty( $result )){
            return 0;
        }
        return $result[0]->AmountSent;
    }

    public static function generateToken()
    {
        $tokenLength = 15;
        $cSecurityManager = new CSecurityManager;
        $invalidtokencount=0;
        $bIsValidToken = false;
        $newtoken = "";
        while ($bIsValidToken == false && $invalidtokencount<50)
        {
            $newtoken = $cSecurityManager->generateRandomString($tokenLength);
            if (!isset($existingtokens[$newtoken]))
            {
                $existingtokens[$newtoken] = true;
                $bIsValidToken = true;
                $invalidtokencount=0;
            }
            else
            {
                $invalidtokencount++;
            }
        }
        return str_replace("~","",$newtoken);
    }

    /*
     * $arraySubQuestion = It is used for questions type array
     * */
    public static function processQuestion($question, $participants, $arraySubQuestion = null)
    {

//        echo "<pre>";
//        print_r($arraySubQuestion);
//        echo "<br>";
//        echo "<pre>";
//        print_r( $question->toArray() );
//        echo "<br>";
//        echo "<pre>";
//        print_r( $participants );
////        print_r( $question->toArray() );
//        exit;

        $configQuestion = json_decode($question->additionalData, true);

//        echo "<pre>";
////        print_r( $participants );
//        //print_r( $question->toArray() );
//        $test = json_decode( $participants[0]->additionalData, true);
//        echo "<pre>";
//        print_r( $test );
//        exit;
//
//        print_r( $participants );
//        echo "<pre>";
//        print_r( $configQuestion );
//        exit;

        $data = [];
        switch ($question->type){
            case "AN":
            case "AC":
            case "AISD":
            case "AYN":
            case "A10":
            case "A5":
            case "A":
                $loopElements = [];
                switch ($question->type){
                    case "AISD":
                        $loopElements = ["increase"=>"Aumentar","same"=>"Mismo","decrease"=>"Disminuir"];
                        break;
                    case "AYN":
                        $loopElements = ["yes"=>"Si","no"=>"No","uncertain"=>"No sé"];
                        break;
                    case "A":
                        $loopElements = $configQuestion['subQuestionsC'];
                        break;
                    case "AC":
                        $loopElements = $configQuestion['subQuestionsR'];
                        break;
                    case "AN":
                    case "A10":
                        $loopElements = [1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10];
                        break;
                    case "A5":
//                        echo ""
                        $loopElements = [1=>1,2=>2,3=>3,4=>4,5=>5];
                        break;
                }
                foreach ($loopElements as $key => $value){
                    $dataob = !empty($data[$key]) ? $data[$key] : self::buildObjectToChart($value, $value);
                    $data[$key] = self::findRightAnswers($question, $participants, $key, $dataob, $arraySubQuestion);
                }
                break;
            case "C5":
                for( $i = 1; $i <= 5; $i++ ){
                    $dataob = !empty($data[$i]) ? $data[$i] : self::buildObjectToChart($i, $i);
                    $data[$i] = self::findRightAnswers($question, $participants, $i, $dataob);
                }
                break;
            case "OMC":
//            case "NM":
                if(!empty($configQuestion)){
                    foreach ($configQuestion['subQuestions'] as $key => $value){
                        $dataob = !empty($data[$key]) ? $data[$key] : self::buildObjectToChart($value, $value);
                        if($question->type == "OMC"){
                            $key = $key."_comment";
                        }
                        $data[$key] = self::findRightAnswers($question, $participants, $key, $dataob);
                    }
                }
                break;
            case "YN":
                $answers = ["no"=>"No","yes"=>"Si"];
                foreach ($answers as $key => $value){
                    $dataob = !empty($data[$key]) ? $data[$key] : self::buildObjectToChart($value, $value);
                    $data[$key] = self::findRightAnswers($question, $participants, $key, $dataob);
                }
                break;
            case "T":
                $answers = ["male"=>"male","female"=>"female"];
                foreach ($answers as $key => $value){
                    $dataob = !empty($data[$key]) ? $data[$key] : self::buildObjectToChart($value, $value);
                    $data[$key] = self::findRightAnswers($question, $participants, $key, $dataob);
                }
                break;
            case "OM":
            case "DDL":
            case "LR":
            case "LRC":
                if(!empty($configQuestion)){
                    foreach ($configQuestion['subQuestions'] as $key => $value){
                        $dataob = !empty($data[$key]) ? $data[$key] : self::buildObjectToChart($value, $value);
                        $data[$key] = self::findRightAnswers($question, $participants, $key, $dataob);
                    }
                }
            break;
        }
        return $data;
    }

    public static function findRightAnswers( $question, $participants, $answerToEvaluate ,$dataob, $arraySubQuestion = null)
    {
        foreach ($participants as $participant) {
            $statusSurvey = json_decode($participant->additionalData, true);
            if(!empty($statusSurvey) && !empty($statusSurvey['answers'])){
                switch ($question->type){
                    case "AN":
                    case "AISD":
                    case "AYN":
                    case "A":
                    case "AC":
                    case "A5":
                    case "A10":
                        $statusQuestion = self::getFullStatusInSurvey($statusSurvey['answers'], $question->id);

//                        echo "<pre>";
//                        print_r($answerToEvaluate);
//                        print_r($statusQuestion);
//                        print_r($statusQuestion['keepAnswered'][$arraySubQuestion]);
//                        exit;

                        if(!empty($statusQuestion['keepAnswered'])){
                            if(!empty($statusQuestion['keepAnswered'][$arraySubQuestion]) &&
                                $statusQuestion['keepAnswered'][$arraySubQuestion] == $answerToEvaluate){
                                $dataob = self::increaseAmountAnswer($dataob);
                            }
                        }
                        break;
                    case "C5":
                        $statusQuestion = self::getFullStatusInSurvey($statusSurvey['answers'], $question->id);
                        if(!empty($statusQuestion['keepAnswered']) &&
                            $statusQuestion['keepAnswered'] == $answerToEvaluate){
                            $dataob = self::increaseAmountAnswer($dataob);
                        }
                        break;
                    case "OM":
                    case "OMC":
//                    case "NM":
                        $statusQuestion = self::getFullStatusInSurvey($statusSurvey['answers'], $question->id);
                        if(!empty($statusQuestion['keepAnswered'])){
                              if(!empty($statusQuestion['keepAnswered'][$answerToEvaluate])){
                                  $dataob = self::increaseAmountAnswer($dataob);
                              }
                        }
                        break;
                    case "YN":
                    case "T":
                    case "DDL":
                    case "LR":
                    case "LRC":
                        $statusQuestion = self::getFullStatusInSurvey($statusSurvey['answers'], $question->id);

                        if(!empty($statusQuestion['keepAnswered'])){
                            if($statusQuestion['keepAnswered'] == $answerToEvaluate ||
                                (!empty($statusQuestion['keepAnswered']['question'])) &&
                                $statusQuestion['keepAnswered']['question'] == $answerToEvaluate){
                                $dataob = self::increaseAmountAnswer($dataob);
                            }
                        }
                        break;
                }
            }
        }
        return $dataob;
    }

    public static function getFullStatusInSurvey($surveyStatus , $surveyId )
    {
        return !empty($surveyStatus[$surveyId]) ? $surveyStatus[$surveyId] :[];
    }

    public static function buildObjectToChart( $name , $drilldown)
    {
        $dataob = new \stdClass();
        $dataob->name = $name;
        $dataob->drilldown = $drilldown;
        return $dataob;
    }

    public static function increaseAmountAnswer( $dataob )
    {
        if(!empty($dataob->y)){
            $dataob->y = $dataob->y + 1;
        } else {
            $dataob->y = 1;
        }
        return $dataob;
    }

}?>