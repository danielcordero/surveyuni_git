<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permissions = Auth::user()->getAllPermissions();
        $action = $request->route()->getAction();
        $pathControllerAction = $action['controller'];
        $controllerAction = explode("\\",$pathControllerAction);

        if (strpos($controllerAction[3], '@') !== false) {
            $controllerAction = explode("@",$controllerAction[3]);
        } else {
            $controllerAction = explode("@",$controllerAction[4]);
        }
        $hasPermission = false;

        foreach ($permissions as $permission) {
            $permissionFull = $permission->name;
            $permissionFull = explode("_",$permissionFull);
            $permissionFull[0] = $permissionFull[0].'Controller';
            if($permissionFull[0] == $controllerAction[0] 
            && $permissionFull[1] == $controllerAction[1]){
                $hasPermission = true;                
            }
        }

        if(empty($hasPermission) &&
            empty(Auth::user()->is_super_admin)){
            abort('401');
        }

        return $next($request);
    }
}
