@extends('layouts.app')
@section('content')

   <div id="content">
      <!-- row -->
      <div class="row">
         <!-- col -->
         <div class="col-xs-12 col-sm-7 col-md-7">
            <h1 class="page-title txt-color-blueDark">
               <!-- PAGE HEADER -->
               <i class="fa-fw fa fa-puzzle-piece"></i> 
               Edición de roles y permisos para el usuario
               <span class="label label-success colorWhite">
               <?php echo $viewData['user']->name ?>
               </span>
               <span class="label label-primary colorWhite">
               <?php echo $viewData['user']->email ?>
               </span>
            </h1>
         </div>
      </div>
      <section id="widget-grid" class="">
         <!-- row -->
         <div >
            
         </div>
         <div class="row">
            <div class="col-md-12">
               <p class="text-right">
                  <button id="btnSaveRolesPermissions" 
                     class="btn btn-primary openMainModal">
                     <i class="fa fa-save"></i>
                     Guardar
                  </button>
                 <a class="btn btn-default" href="{{ route('adminuser') }}">
                   Cancelar
                 </a>
               </p>
            </div>
            <div class="col-md-12">
               <div class="jarviswidget jarviswidget-color-darken" id="wid-id-16" data-widget-sortable="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-deletebutton="false" style="" role="widget">
                  <header role="heading">
                     <span class="widget-icon"> <i class="fa fa-lock"></i> </span>
                     <h2><strong>Roles</strong> <i>Usuario</i> </h2>
                     <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                  </header>
                  
                  <div role="content">
                     <div class="jarviswidget-editbox">
                     </div>
                     <div class="widget-body">
                        <div class="table-responsive">
                           <form action="{{ route('setroles') }}" class="smart-form" 
                                    method="post" id="frmRolesPermissions">
                              <table class="table table-bordered">
                                 <thead>
                                    <th>
                                       Name
                                    </th>
                                    <th>
                                       Descripcion
                                    </th>
                                    <th>
                                       Aplicar Role
                                    </th>
                                    <th>Permisos</th>
                                 </thead>
                                 <tbody>
                                       @foreach($viewData['roles'] as $rol)
                                       <tr class="trCheckbox" >
                                          <td>
                                             <strong>
                                             {{ $rol->name }}
                                             </strong>
                                          </td>
                                          <td>{{ $rol->description }}</td>
                                          <td>
                                             <label class="checkbox">
                                                <input type="checkbox" 
                                                   <?php
                                                      $userPermissions = $viewData['user']->myPermissions;
                                                      $permissions = $rol->permissions->pluck('name')->toArray();
                                                      $userPermissionCurrentRol = [];
                                                      
                                                      foreach ($rol->permissions as $permission) {
                                                         if(in_array($permission->name, $userPermissions)){
                                                            $userPermissionCurrentRol[] = $permission->name;
                                                         }
                                                      }
                                                      
                                                      $hasAllPermission = false;
                                                      $userAmountPermissionRol = count($userPermissionCurrentRol);
                                                      
                                                      if($userAmountPermissionRol > 0){
                                                        if($userAmountPermissionRol == count($permissions)){
                                                            $hasAllPermission = true;
                                                        }
                                                      }
                                                      echo !empty($hasAllPermission) ? 'checked' : '';
                                                      ?>
                                                   class="inlineBlock checkboxbtn"
                                                   value="<?php echo $rol->id; ?>"
                                                   name="roles[]" />
                                                   <i></i>
                                             </label>
                                          </td>
                                          <td>
                                             <div class="table-responsive">
                                                <table class="table table-bordered">
                                                   <thead>
                                                      <th>Descripcion</th>
                                                      <th>Permiso</th>
                                                   </thead>
                                                   <tbody>
                                                      @foreach($rol->permissions as $permission)
                                                      <tr class="trCheckbox_child">
                                                         <td>
                                                            <strong>
                                                            {{ $permission->description }}
                                                            </strong>
                                                         </td>
                                                         <td>
                                                            <label class="checkbox">
                                                               <input type="checkbox" 
                                                               <?php
                                                                  echo in_array($permission->name, $viewData['user']->myPermissions) ? 'checked' : '';
                                                                  ?>
                                                               class="inlineBlock checkboxbtn"
                                                               name="<?php 
                                                                     $currentRol = trim(preg_replace('/\s\s+/','',$rol->name));
                                                                     echo 'permissions'.'['.$rol->id.']'.'[]'; 
                                                                  ?>"
                                                               value="<?php echo $permission->name; ?>"
                                                               >
                                                               <i></i>
                                                            </label>
                                                         </td>
                                                      </tr>
                                                      @endforeach
                                                   </tbody>
                                                </table>
                                             </div>
                                          </td>
                                       </tr>
                                       @endforeach
                                       {{ csrf_field() }}
                                       <input type="hidden" 
                                          name="userId" 
                                          value="<?php echo $viewData['user']->id; ?>">
                                 </tbody>
                              </table>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12">
            </div>
         </div>
      </section>
   </div>

@push('scripts')
   <script src="<?php echo url('/') ?>/js/modules/RolModule.js"></script>
@endpush
@stop