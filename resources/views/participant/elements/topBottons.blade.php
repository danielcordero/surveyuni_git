<div class="row">
    <div class="col-md-12">
        <ul class="demo-btns">
            <li>
                <a href="{{ route('surveyDetail',[$viewData['survey']->id])}}"
                   class="btn bg-color-orange txt-color-white"
                        rel="tooltip"
                        data-placement="top"
                        data-original-title="Regresar detalle de la encuesta">
                        <span class="glyphicon glyphicon-arrow-left"></span>
                        Regresar detalle de la encuesta
                </a>
            </li>
            <li>
                <a href="{{ route('participants.listAll',['surveyId'=>$viewData['survey']->id]) }}"
                   class="btn btn-primary">
                    <span class="glyphicon glyphicon-list-alt"></span>
                    Mostrar participantes
                </a>
            </li>
            <li>
                <label class="select">
                    <div class="dropdown">
                        <button class="btn btn-default txt-color-white bg-color-greenDark dropdown-toggle"
                                type="button"
                                data-toggle="dropdown">
                            <span class="glyphicon glyphicon-plus"></span>
                            Crear ....

                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('participants.viewaddcustomfields',['surveyId'=>$viewData['survey']->id]) }}">
                                    <i class="fa fa-gear"></i>
                                    Administar campos personalizados
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('participants.add',['surveyId'=>$viewData['survey']->id]) }}">
                                    <span class="glyphicon glyphicon-plus"></span>
                                    Agregar participantes
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <small>Importar participantes desde:</small>
                            <li>
                                <a href="{{ route('participants.viewupload',['surveyId'=>$viewData['survey']->id]) }}">
                                    <i class="fa fa-upload"></i>
                                    Archivos CSV
                                </a>
                            </li>
                        </ul>
                    </div>
                </label>
            </li>
            <li>
                <label class="select">
                    <div class="dropdown">
                        <button class="btn btn-default txt-color-white bg-color-greenDark dropdown-toggle"
                                type="button"
                                data-toggle="dropdown">
                            <span class="glyphicon glyphicon-plus"></span>
                            Invitaciones y recordatorios

                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('email.viewsendinvitation',['surveyId'=>$viewData['survey']->id]) }}">
                                    <i class="fa fa-envelope-square"></i>
                                    Enviar correo de invitación
                                </a>
                                <a href="{{ route('email.viewsendremind',['surveyId'=>$viewData['survey']->id]) }}">
                                    <i class="fa fa-envelope"></i>
                                    Enviar correo de recordatorio
                                </a>
                                <!--<a href="{{ route('email.viewtemplates',['surveyId'=>$viewData['survey']->id]) }}">
                                    <span class="fa fa-envelope-o"></span>
                                    Editar plantillas de correo.
                                </a>-->
                            </li>
                        </ul>
                    </div>
                </label>
            </li>

            <!--<li>
                <a href="#"
                   class="btn bg-color-greenDark txt-color-white" target="_blank">
                    <i class="fa fa-gear"></i>
                    Exportar
                </a>
            </li>-->

            <li>
                <a href="{{ route('participants.viewgeneratetoken',['surveyId'=>$viewData['survey']->id]) }}"
                   class="btn bg-color-red txt-color-white">
                    <i class="fa fa-gear"></i>
                    Generar tokens
                </a>
            </li>

            <!--<li>
                <button data-remove-participant="all"
                        class="btn btn-danger btnDelete btnRemoveParticipants"
                        role="button"
                        data-placement="top"
                        data-question-id="7" title="Eliminar">
                    <i class="fa fa-trash-o">
                    </i>
                    Eliminar todo los participantes
                </button>

                <form method="post" role="form" id="frDelete_7" class="hideElement frDelete_7"
                      action="http://localhost:83/monografia/mainproject/laravel5.5/mono2017/public/admin/survey-delete/7">
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="form-group">
                        <input type="hidden" name="_token"
                               value="NmQMG5WmMkbFK79kqiQYxl5vIihXb6IMaiVInIMp">
                    </div>
                    <input type="submit" value="Enviar">
                </form>
            </li>-->
        </ul>
    </div>
</div>