<?php

namespace App\Http\Controllers\AdminSurveys;

use App\CustomLib\TypeQuestions\BaseQuestion;
use App\Http\Controllers\Controller;
use App\CustomLib\GenericMessages;
use Illuminate\Http\Request;
use App\Survey;
use App\Group;
use App\Question;
use Session;

class ConfigQuestionController extends Controller
{
    public $pathNamespace = "\\App\\CustomLib\\TypeQuestions\\";

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckOwnSurvey');
        $this->middleware('CheckExistResource');
//        $this->middleware('CheckPermission');
    }

    public function config( Request $request ){

        $surveyId = (int)$request->surveyId;
        $groupId = (int)$request->groupId;
        $questionId = (int)$request->questionId;
        $surveyInfo = Survey::find( $surveyId );
        $groupInfo = Group::find($groupId);
        $questionInfo = Question::with('group')->find($questionId);
        $viewData['survey'] = $surveyInfo;
        $viewData['group'] = $groupInfo;
        $viewData['question'] = $questionInfo;

        $questionsType = config('custom.questionsTypeTemplate');
        $questionType = $questionsType[$questionInfo->type];

        $baseQuestion = new BaseQuestion;
        $addData = $baseQuestion->getConfigBase( $questionType, $questionInfo->additionalData );
        $viewData['config'] = $addData;

        return view('survey.questions.configQuestion')->with('viewData',$viewData);
    }

    public function store( Request $request ){

        $questionType = $request->question_type;

        $baseQuestion = new BaseQuestion;
        $baseQuestion->saveConfigBase( $questionType, $request);

        $parameters = [
            'surveyId'=>$request->survey_id,
            'groupId'=>$request->group_id,
            'questionId'=>$request->question_id,
        ];

        Session::flash('success', GenericMessages::$all['successOperation']);
        return redirect()->route('configQuestion',$parameters);
    }
}
