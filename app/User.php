<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','is_super_admin',
    ];

    public function checkHasPermission($permissions)
    {
        $myPermissions = $this->getAllPermissions();

        $hasPermission = false;
        foreach ($myPermissions as $permission) {
            $permissionFull = $permission->name;
            $permissionFull = explode("_",$permissionFull);

            foreach ($permissions as $permissio){
                $permissio = explode("_",$permissio);
                if($permissionFull[1] == $permissio[1]){
                    $hasPermission = true;
                }
            }
        }

        if(!empty($this->is_super_admin)){
            $hasPermission = true;
        }

        return $hasPermission;
    }
}

