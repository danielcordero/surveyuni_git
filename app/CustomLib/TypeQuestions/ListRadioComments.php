<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;

class ListRadioComments extends BaseQuestion{

    public function saveConfig( $data ){
        $jsonToSave = new \stdClass();
        $subQuestions = $data->subQuestions;
        foreach ($subQuestions as $key => $value){
            $subQuestions[$key] = $this->cleanData( $value );
        }

        $jsonToSave->subQuestions = $subQuestions;
        $question = Question::find( $data->question_id );
        $question->additionalData = json_encode( $jsonToSave );
        $question->save();
    }

    public function getConfig( $dataJson ){
        return json_decode( $dataJson, true );
    }

    public function validateAnswers( $partakerRequest ){

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message']= "Esta pregunta es requerida";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest ){

        $answers = !empty($partakerRequest['answers']) ? $partakerRequest['answers'] : [];
        $messages = [];

        $fail = false;
        $keepAnswered = [];
        $withoutAnswered = [];

        $answers['question'] = !empty($answers['question']) ? $this->cleanData($answers['question']) : "";
        $answers['comment'] = !empty($answers['comment']) ? $this->cleanData($answers['comment']) : "";
        if((empty($answers['comment']) ||
            empty($answers['question'])) &&
            !empty($partakerRequest['mandatory']) &&
            $partakerRequest['mandatory'] == "Y"){
            $fail = true;
            $messages[] = "Por favor agregue un comentario";
        }

        if(!empty($answers['comment'])){
            $keepAnswered['comment'] = $answers['comment'];
        }

        if(!empty($answers['question'])){
            $keepAnswered['question'] = $answers['question'];
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

//        echo "<pre>";
//        print_r( $response );
//        exit;

        return $response;
    }

}
?>