<?php
    $allQuestions = "";
//    echo "<pre>";
//    print_r($resultQuestion);
//    exit;
?>
<div class="row">
    <!-- NEW WIDGET START -->
    <!-- array -->
    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            <?php echo $__env->make("survey.public.questions.littlepartial.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <!--<header class="headerWidget">
                <h2>
                    <?php if($question->mandatory == "Y"): ?>
                        <small class="text-danger fa fa-asterisk small redAsterisk"
                               aria-hidden="true">
                        </small>
                    <?php endif; ?>
                    <strong>
                        <?php
                            echo strip_tags($question->title);
                        ?>
                        <?php
                            $questionsType = config('custom.questionsType');
                            echo $questionsType[$question->type];
                        ?>
                    </strong>
                </h2>
            </header>-->

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    <?php if(!empty($resultQuestion) && !empty($resultQuestion['fail']) ): ?>
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> <?php echo e($resultQuestion['message']); ?>

                            <?php if(!empty($resultQuestion['messages'])): ?>
                                <br>
                                <?php $__currentLoopData = $resultQuestion['messages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> <?php echo e($value); ?>

                                    <br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Fila / Columna</th>
                                <?php
                                    foreach ($config["subQuestionsC"] as $key => $value){
                                        ?>
                                    <th class="text-center">
                                        <?php
                                            echo $value;
                                        ?>
                                    </th>
                                <?php
                                    }
                                ?>
                                <?php
                                    if(!empty($question->mandatory) && $question->mandatory == "N"){
                                ?>
                                <th class="text-center">
                                    Sin respuesta
                                </th>
                                <?php
                                    }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                foreach ($config["subQuestionsR"] as $key1 => $value1){
                                        $allQuestions = $allQuestions . '|' .$key1;
                                    ?>
                                <tr>
                                    <th>
                                        <span class="label <?php
                                                if(!empty($resultQuestion['withoutAnswered']) &&
                                                    in_array($key1, $resultQuestion['withoutAnswered'])
                                                    && $question->mandatory == "Y"){
                                                    echo "labelCommonError";
                                                }
                                        ?>">
                                            <?php
                                                echo $value1;
                                            ?>
                                        </span>
                                    </th>
                                    <?php
                                        foreach ($config["subQuestionsC"] as $key2 => $value2){
                                        ?>
                                        <td class="text-center">
                                            <label class="radio inline">
                                                <input type="radio"
                                                       name="question_<?php echo $question->id; ?>[answers][<?php echo $key1; ?>]"
                                                       value="<?php echo $key2;//$key1.'|'.$key2; ?>"
                                                        <?php
                                                            if(!empty($resultQuestion['keepAnswered']) &&
                                                                !empty($resultQuestion['keepAnswered'][$key1])){
                                                                if($resultQuestion['keepAnswered'][$key1] == $key2){
                                                                    echo "checked";
                                                                }
                                                            }
                                                            ?>>
                                                <i></i><strong>
                                                </strong>
                                            </label>
                                        </td>
                                        <?php
                                        }
                                    ?>
                                    <?php
                                    if(!empty($question->mandatory) && $question->mandatory == "N"){
                                    ?>
                                    <td class="text-center">
                                        <label class="radio inline">
                                            <input type="radio"
                                                   name="question_<?php echo $question->id; ?>[answers][<?php echo $key1; ?>]"
                                                   value="notanswer" <?php
                                                    if(!empty($resultQuestion)){
                                                        if(!empty($resultQuestion['keepAnswered']) &&
                                                            !empty($resultQuestion['keepAnswered'][$key1])){
                                                            if($resultQuestion['keepAnswered'][$key1] == 'notanswer'){
                                                                echo "checked";
                                                            }
                                                        }
                                                    } else {
                                                        echo "checked";
                                                    }
                                                ?>>
                                            <i></i><strong>
                                            </strong>
                                        </label>
                                    </td>
                                    <?php
                                    }
                                    ?>
                                </tr>
                            <?php
                                    }
                            ?>
                            </tbody>
                        </table>

                    </div>


                </div>

                <?php echo $__env->make('survey.public.questions.littlepartial.help', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[subQuestions]"
                       id="question_<?php echo $question->id; ?>[subQuestions]"
                       value="<?php echo trim($allQuestions, '|'); ?>">
            <?php echo $__env->make('survey.public.questions.commonHidden', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                <!-- end widget content -->
            </div>
            <!-- end widget div -->
        </div>
    </article>
</div>