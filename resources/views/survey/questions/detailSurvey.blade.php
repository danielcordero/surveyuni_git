@extends('layouts.app')
@section('content')
    @include('elements.modals.confirm')

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Detalle de la encuesta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">

        @include('survey.questions.elements.topBarBreadcrumbs')

            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-3">
                    @include('survey.questions.elements.treeGroupQuestions')
                </div>
                <div class="col-sm-9">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        @include('survey.questions.elements.topBarAction')
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            Resumen de la encuesta
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><strong>
                                            Configuración de la encuesta
                                            (
                                            <?php
                                            echo $viewData['survey']->title;
                                            ?>
                                            <span class="badge bg-color-red">
                                                <?php
                                                echo $viewData['survey']->id;
                                                ?></span>)
                                        </strong>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Introducción

                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php echo strip_tags($viewData['survey']->welcometext); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Descripción
                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php echo strip_tags($viewData['survey']->description); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Mensaje de despedida
                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php echo strip_tags($viewData['survey']->endtext); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Fecha de creación
                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php
                                                        echo date('Y-m-d', strtotime($viewData['survey']->created_at));
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Fecha de inicio
                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php
                                                        //startdate
                                                        if(!empty($viewData['survey']->startdate)){
                                                            echo date('Y-m-d', strtotime($viewData['survey']->startdate));
                                                        }
                                                        //echo $viewData['survey']->create_at;
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Fecha de caducidad
                                                    </strong>
                                                </td>
                                                <td>
                                                    <?php
                                                        if(!empty($viewData['survey']->expiresdate)){
                                                            echo date('Y-m-d', strtotime($viewData['survey']->expiresdate));
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Número de preguntas/secciones
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo $viewData['survey']->totalQuestions; ?>/
                                                            <?php echo $viewData['survey']->totalGroups; ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>

                    @if(!empty($viewData['survey']->groups))
                        <form action="{{ route('storeSurveyConfig',['surveyId'=>$viewData['survey']->id] )}}"
                              role="form" method="post"
                              class="currentForm">
                            {{ csrf_field() }}
                            <article class="well">
                                <div class="inbox-info-bar">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h3>
                                                <strong>
                                                    Ordernar grupos de preguntas
                                                </strong>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <br>
                                    <div class="col-sm-12">
                                        <div class="panel panel-default">
                                            <div class="table-responsive">
                                                <table class="table table-striped"
                                                           id="sortorderGroup">
                                                        <thead>
                                                        <tr>
                                                            <th>Posición</th>
                                                            <th>Código</th>
                                                            <th>
                                                                Título del grupo de pregunta
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        $groups = $viewData['survey']->groups;
                                                        $loopI = 0;
                                                        foreach ($groups as $group){
                                                        $loopI = $loopI + 1;
                                                        ?>
                                                        <tr id="<?php echo $group->id; ?>" class="all-scroll rowHover">
                                                            <td>
                                                                <input type="hidden"
                                                                       value="{{$group->id}}"
                                                                       name="groups[<?php echo $group->id; ?>]">
                                                                <span class="cursorMove">
                                                                <i class="fa fa-bars fa-2x cursorMove" aria-hidden="true"></i>
                                                            </span>
                                                            </td>
                                                            <td>
                                                                <strong>
                                                                    <?php echo $group->id; ?>
                                                                </strong>
                                                            </td>
                                                            <td>
                                                                {!! $group->title !!}
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>


                            <input type="hidden"
                                   value="{{$viewData['survey']->id}}"
                                   name="survey_id">
                            <input type="submit" value="Enviar" class="hideElement" />

                        </form>
                    @else

                        <article class="well">
                            <div class="inbox-info-bar">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4>
                                            <strong>
                                                Está encuesta aún no tiene grupo de pregunta
                                            </strong>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </article>
                    @endif
                </div>
            </div>
        </section>
    </div>


    @push('scripts')
    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/QuestionsModule.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/SurveyModule.js"></script>
    @endpush
@stop