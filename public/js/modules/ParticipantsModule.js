/**
 * Created by cesar on 1/14/2018.
 */

var ParticipantsModule = new function(){
    var that = this;
    this.dataTableInstance;
    this.idsSelected;

    this.init = function(){
        this.loadInitialElement();
    };

    this.loadInitialElement = function(){
        $('#mdConfirm').modal({ show: false});
        this.loadConfigDataTable();
        this.eventHandler();
        this.removeAction();
        this.loadConfigDateFields();
        this.sendSomeEmails();
    };

    this.loadConfigDateFields = function(){
        if($('#startdate').length > 0){
            $('#startdate').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {},
                beforeShow: function() {
                    setTimeout(function(){
                        $('.ui-datepicker').css('z-index', 99999999999999);
                    }, 0);
                }
            });
        }

        if($('#expires').length > 0){
            $('#expires').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {},
                beforeShow: function() {
                    setTimeout(function(){
                        $('.ui-datepicker').css('z-index', 99999999999999);
                    }, 0);
                }
            });
        }

        $(".btnClearDate").on("click", function(e){
            $("#"+$(this).data("type-date")).val("");
        });

        $("#btnSaveDateConfig").on("click", function(e){
            if(that.isValidaDateForm()){
                var parameters = {
                    startdate:$("#startdate").val(),
                    expiresdate:$("#expires").val(),
                    surveyId: $("#survey_id").val()
                };
                ajaxRequest.saveDateConfig(parameters).then(function(response){
                    if(response.status){
                        commonModule.showPopupGeneric("success","La configuración se ha guardado correctamente");
                        location.reload();
                    } else {
                        commonModule.showPopupGeneric("error",response.msg);
                    }
                });
            }
            e.preventDefault();
        });
    };

    this.sendSomeEmails = function(){
        var typeRequest = "", idsSelected;
        $('.sendSomeEmails').on("click", function(e){
            typeRequest = $(this).data("execute-action");
            console.log( typeRequest );
            idsSelected = that.getPartipantsOverTable();
            debugger;
            if(idsSelected){
                switch(typeRequest){
                    case "email_reminder":
                        $("#idsInvitations").val(idsSelected);
                        $("#fmSendSomeRemainder").trigger('submit');
                        break;
                    case "email_invitations":
                        $("#idsInvitations").val(idsSelected);
                        $("#fmSendSomeInvitation").trigger('submit');
                        break;
                }
            }
        });
    };

    this.isValidaDateForm = function(){
        var fieldDateStart = $("#startdate").val();
        var fieldDateEnd = $("#expires").val();
        var isValid = true;

        if(fieldDateStart == "" || fieldDateEnd == ""){
            commonModule.showPopupGeneric("error","Hay campos vacíos");
            return;
        }

        if(fieldDateStart != "" || fieldDateEnd != ""){
            if(!commonModule.isDateStartLow(fieldDateStart, fieldDateEnd)){
                commonModule.showPopupGeneric("error","Fecha de caducidad debe ser mayor");
                isValid = false;
            }
        }

        return isValid;
    };

    this.eventHandler = function(){
        $("#btnResetForm").on("click",function(event){
            commonModule.cleanForm($("#frmFilterparticipante")[0]);
        });

    };

    this.removeAction = function(){
        var typeOfAction = "";
        $('.btnRemoveParticipants').on("click", function(e){
            $('#mdConfirm').modal('show');
            typeOfAction = $(this).data("remove-participant");
            switch(typeOfAction){
                case "one":
                    that.idsSelected = $(this).data("survey-participant-id");
                    break;
                case "several":
                    that.idsSelected = that.getPartipantsOverTable();
                    $("#idstodelete").val(that.idsSelected);
                    break;
            }

            $('#mdConfirm').find('.btn-ok').on("click", function(e){
                switch(typeOfAction){
                    case "one":
                        $("#frDelete_" + that.idsSelected).find('[type="submit"]').trigger("click");
                        break;
                    case "several":
                        $("#fmActionTable").trigger('submit');
                        break;
                    case "all":
                        $("#fmActionTableAll").trigger('submit');
                        break;
                }
                e.preventDefault();
            });
            e.preventDefault();
        });
    };

    this.getPartipantsOverTable = function(){
        var participantIds = [];

        this.dataTableInstance.$('input[type="checkbox"]:checked').each(function (index, element) {
            participantIds.push($(element).val());
        });
        return participantIds;
    };

    this.loadConfigDataTable = function(){
        var rows_selected = [];

        var table = $('#participan').DataTable({
            "bInfo" : false,
            searching: false,
            paging: false,
            ordering: false,
            'columnDefs': [{
                'targets': 0,
                'searchable':false,
                'orderable':false,
                'width':'1%',
                'className': 'dt-body-center',
                'render': function (data, type, full, meta){

                    return '<input type="checkbox" value="'+full[2]+'">';
                }
            }],
            'order': [1, 'asc'],
            'rowCallback': function(row, data, dataIndex){
                // Get row ID
                var rowId = data[0];

                // If row ID is in the list of selected row IDs
                if($.inArray(rowId, rows_selected) !== -1){
                    $(row).find('input[type="checkbox"]').prop('checked', true);
                    $(row).addClass('selected');
                }
            }
        });

        this.dataTableInstance = table;

        // Handle click on checkbox
        $('#participan tbody').on('click', 'input[type="checkbox"]', function(e){
            var $row = $(this).closest('tr');

            // Get row data
            var data = table.row($row).data();

            // Get row ID
            var rowId = data[0];

            // Determine whether row ID is in the list of selected row IDs
            var index = $.inArray(rowId, rows_selected);

            // If checkbox is checked and row ID is not in list of selected row IDs
            if(this.checked && index === -1){
                rows_selected.push(rowId);

                // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            } else if (!this.checked && index !== -1){
                rows_selected.splice(index, 1);
            }

            if(this.checked){
                $row.addClass('selected');
            } else {
                $row.removeClass('selected');
            }

            // Update state of "Select all" control
            that.updateDataTableSelectAllCtrl(table);

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

        // Handle click on table cells with checkboxes
        //$('#participan').on('click', 'tbody td, thead th:first-child', function(e){
        $('#participan').on('click', 'checkbox', function(e){
            $(this).parent().find('input[type="checkbox"]').trigger('click');
        });

        // Handle click on "Select all" control
        $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
            if(this.checked){
                $('#participan tbody input[type="checkbox"]:not(:checked)').trigger('click');
            } else {
                $('#participan tbody input[type="checkbox"]:checked').trigger('click');
            }

            // Prevent click event from propagating to parent
            e.stopPropagation();
        });

        // Handle table draw event
        table.on('draw', function(){
            // Update state of "Select all" control
            that.updateDataTableSelectAllCtrl(table);
        });

        // $(".executeAction").on("click", function(e){
        //     var executeAction = $(this).data("execute-action");
        //     switch(executeAction){
        //         case "send_email_invitations":
        //             break;
        //         case "send_email_reminder":
        //             break;
        //         case "delete_participants":
        //             break;
        //     }
        //     $("#fmActionTable").trigger('submit');
        // });

        //Handle form submission event
       $('#fmActionTable').on('submit', function(e){
           var form = this;
           var participantIds = [];
           table.$('input[type="checkbox"]:checked').each(function (index, element) {
               participantIds.push($(element).val());
           });
           $("#idstodelete").val(participantIds);
           // e.preventDefault();
       });

    };

    this.updateDataTableSelectAllCtrl = function(table){
        var $table             = table.table().node();
        var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
        var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
        var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

        // If none of the checkboxes are checked
        if($chkbox_checked.length === 0){
            chkbox_select_all.checked = false;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }

            // If all of the checkboxes are checked
        } else if ($chkbox_checked.length === $chkbox_all.length){
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = false;
            }

            // If some of the checkboxes are checked
        } else {
            chkbox_select_all.checked = true;
            if('indeterminate' in chkbox_select_all){
                chkbox_select_all.indeterminate = true;
            }
        }
    };

    $(document).ready(function(){
        console.log("Element loaded");
        that.init();
    });
};