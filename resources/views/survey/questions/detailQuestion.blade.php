@extends('layouts.app')
@section('content')


    @include('elements.modals.confirm')

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Detalle de la pregunta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            @include('survey.questions.elements.topBarBreadcrumbs')
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-3">
                    @include('survey.questions.elements.treeGroupQuestions')
                </div>
                <div class="col-sm-9">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <?php
                                            $testSurveys = config('custom.testSurvey');
                                        ?>

                                        <ul class="demo-btns">

                                            @if(!in_array($viewData['survey']->id,$testSurveys))
                                                @if(!empty($viewData['survey']->active) &&
                                                $viewData['survey']->active == "N")

                                                    <li>
                                                        <a href="{{ route('editQuestion',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id,'questionId'=>$viewData['question']->id])}}"
                                                           class="btn btn-primary">
                                                            <i class="fa fa-pencil"></i>
                                                            Editar pregunta
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('configQuestion',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id,'questionId'=>$viewData['question']->id])}}"
                                                           class="btn btn-primary bg-color-blueDark">
                                                            <i class="fa fa-fw fa-gear"></i>
                                                            Configurar
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <button
                                                                data-toggle="modal"
                                                                data-target="#mdConfirm"
                                                                class="btn btn-danger btnDelete"
                                                                role="button"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                data-question-id="{{ $viewData['question']->id }}"
                                                                title="Eliminar">
                                                            <i class="fa fa-trash-o">
                                                            </i>
                                                            Eliminar pregunta
                                                        </button>

                                                        <form method="post" role="form" id="<?php echo "frDelete_" . $viewData['question']->id; ?>"
                                                              class="hideElement <?php echo "frDelete_" . $viewData['question']->id; ?>"
                                                              action="{{ route('questionDelete',[$viewData['question']->id])}}">
                                                            {{ method_field('DELETE') }}
                                                            <div class="form-group">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            </div>
                                                            <input type="submit" value="Enviar"  />
                                                        </form>
                                                    </li>
                                                @endif
                                            @endif

                                            <li>
                                                <a href="{{ route('survey.redirectpreviewmode',[$viewData['survey']->id])}}"
                                                   class="btn bg-color-orange txt-color-white" target="_blank">
                                                    <i class="fa fa-eye"></i>
                                                    Vista previa de encuesta
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ route('groupviewpreviewpage',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id])}}"
                                                   target="_blank"
                                                   class="btn bg-color-teal txt-color-white">
                                                    <i class="fa fa-eye"></i>
                                                    Vista previa de grupo
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ route('questionviewpreviewpage',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id,'questionId'=>$viewData['question']->id])}}" target="_blank"
                                                   class="btn bg-color-blueDark txt-color-white">
                                                    <i class="fa fa-eye"></i>
                                                    Vista previa de pregunta
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            Resumen de la pregunta
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <br>
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Sección
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <em>
                                                            <?php echo strip_tags($viewData['question']->group->name); ?>
                                                        </em>
                                                    </strong>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Título de la pregunta
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo strip_tags($viewData['question']->title); ?>
                                                    </strong>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Texto de ayuda
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo strip_tags($viewData['question']->help); ?>
                                                    </strong>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Código
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo strip_tags($viewData['question']->id); ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Tipo
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php
                                                        $questionsType = config('custom.questionsType');
                                                        echo $questionsType[$viewData['question']->type];
                                                        ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>
                                                        Obligatoria
                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php
                                                            $isMandatory = !empty($viewData['question']->mandatory) &&
                                                                $viewData['question']->mandatory != "N";

                                                            echo $isMandatory ? "Si" : "No";
                                                        ?>
                                                    </strong>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>


    @push('scripts')
    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/QuestionsModule.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/SurveyModule.js"></script>
    @endpush
@stop