/**
 * Created by cesar on 11/7/2017.
 */
'use strict';
var GroupsModule = new function(){
    var that = this;

    this.init = function(){
        this.handlerEvent();
        this.loadInitialElement();
    };

    this.loadInitialElement = function(){
        if($("#sortorderQuestions").length){
            $("#sortorderQuestions").find("tbody").sortable();
        }

        this.initckEditors();
    };

    this.initckEditors = function(){

        var ckeditorOptions = {};
        var idElement = "";
        $(".smCkEditor").each(function(index, element){
            idElement = $(element).attr("id");
            ckeditorOptions.LimeReplacementFieldsAction = "newsurvey";
            ckeditorOptions.LimeReplacementFieldsType = idElement;
            $("#"+idElement).initcKeditor(ckeditorOptions);
        });
    };

    this.handlerEvent = function(){
        $('#mdConfirm').on('show.bs.modal', function(e) {
            var currentSurvey = $(e.relatedTarget).data('survey-id');
            //$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            $(this).find('.btn-ok').on("click", function(e){
                e.preventDefault();
                $("#frDelete_" + currentSurvey).find('[type="submit"]').trigger("click");
            });
        });
    };

    $(document).ready(function(){
        console.log("Element loaded");
        that.init();
    });
};