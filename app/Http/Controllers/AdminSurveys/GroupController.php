<?php

namespace App\Http\Controllers\AdminSurveys;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CustomLib\GenericMessages;
use App\CustomLib\Services\GroupService;
use App\CustomLib\Helper\HelperServices;
use App\Group;
use App\Survey;
use Session;

class GroupController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckOwnSurvey');
        $this->middleware('CheckExistResource');
        //$this->middleware('CheckPermission',['except'=>'setroles']);
    }

    public function destroy( $groupId )
    {
        $success = true;
        \DB::beginTransaction();
        try {
            $groupId = (int)$groupId;
            $groupInfo = Group::find($groupId);
            $surveyId = (int)$groupInfo->survey_id;
            $groups = GroupService::getByOrderCondition($surveyId, $groupInfo->sortorder, false);
            if(!empty($groups)){
                if(!GroupService::changeSortOrder($groups, false)){
                    throw new Exception();
                }
            }
            $groupInfo->delete();

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }
        return redirect()->route('surveyDetail',[$surveyId]);
    }

    public function saveConfig(Request $request){

        $success = true;
        \DB::beginTransaction();
        try{
            if(!empty($request->questions)){
                $questions = $request->questions;
                $newArrayQuestion = [];
                $newArrayQuestions = [];
                $counter = 0;
                foreach($questions as $key => $question)
                {
                    $counter = $counter + 1;
                    $newArrayQuestion['id'] = $key;
                    $newArrayQuestion['sortorder'] = $counter;
                    array_push($newArrayQuestions, $newArrayQuestion);
                }
                $result = HelperServices::updateBulk('questions',$newArrayQuestions,'id');
                if(empty($result)){
                    throw new Exception();
                }
            }
            \DB::commit();
        }catch (\Exception $e){
            \DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }

        $parameters = [
            'surveyId'=>$request->survey_id,
            'groupId'=>$request->group_id
        ];
        return redirect()->route('groupDetail',$parameters);
    }

    public function viewPreview($surveyId, $groupId)
    {
        $groupId = HelperServices::sanitize_int( $groupId );
        $groupService = new GroupService();
        $group = $groupService->getById( $groupId );
        $viewData['groupInfo'] = $group;

        return view('survey.public.grouppreview')
                ->with('viewData',$viewData);
    }
}
