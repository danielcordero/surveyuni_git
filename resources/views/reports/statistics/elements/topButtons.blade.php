<ul class="demo-btns">

    <li>
        <a href="{{ route('surveyDetail',[$viewData['survey']->id])}}"
           class="btn bg-color-orange txt-color-white"
           rel="tooltip"
           data-placement="top"
           data-original-title="Regresar detalle de la encuesta">
            <span class="glyphicon glyphicon-arrow-left"></span>
            Regresar detalle de la encuesta
        </a>
    </li>

    <li>
        <a href="{{ route('survey.redirectpreviewmode',[$viewData['survey']->id])}}"
           class="btn bg-color-greenDark txt-color-white" target="_blank">
            <i class="fa fa-eye"></i>
            Previsualizar encuesta
        </a>
    </li>
    <li>
        <a href="{{ route('participants.config',[$viewData['survey']->id])}}"
           class="btn bg-color-greenDark txt-color-white">
            <i class="fa fa-user"></i>
            Participantes de la encuesta
        </a>
    </li>

    <li>
        <a href="{{ route('surveyexportresponse.view',[$viewData['survey']->id])}}"
           class="btn bg-color-blueDark txt-color-white">
            <i class="fa fa-download"></i>
            Exportar respuestas
        </a>
    </li>

    <li>
        <a href="{{ route('surveystatistics.view',[$viewData['survey']->id])}}"
           class="btn bg-color-orange txt-color-white">
            <i class="fa fa-eye"></i>
            Ver respuestas
        </a>
    </li>

</ul>