'use strict';
var customModal = {};
(function(){

	this.setTemplate = function(template, callBack){
		this.open();
		$.get(template, function( view ){
			$("#layerModal__content").html(view);
			if(callBack){
				callBack();
			}
		});
	};

	this.open = function(){
		$("#layerModal").show();
		$("#layerModal__container").show();
		$("#layerModal__content").html($("#cargador_empresa").html());
	};

	this.close = function(){
		$("#layerModal").hide();
		$("#layerModal__container").hide();
		$("#layerModal__content").html("");
	};

	this.closeEvent = function(){
		$(document).on("click",".layerModal, .closeModal", function(){
			//Hide layers modal
			$("#layerModal").hide();
			$("#layerModal__container").hide();
			$("#layerModal__content").html("");
		});
	};

	return this;
}).call(customModal);