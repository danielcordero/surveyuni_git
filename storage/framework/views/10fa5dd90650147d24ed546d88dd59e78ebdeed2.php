<?php
//echo "<pre>";
//print_r($viewDataApp);
//exit;
?>
<div class="row">
    <div class="col-md-12">
        <div class="well">
            <div class="row">
                <div class="col-md-9 col-xs-12">
                    <ol class="breadcrumb">
                        <li>
                            <strong>
                                <u>
                                    <i>
                                        <a href="<?php echo e(route('surveyDetail',[$viewData['survey']->id])); ?>"
                                           rel="popover-hover"
                                           data-placement="right"
                                           data-original-title="<span class='txtBlack'> Ir al detalle de la encuesta </span>"
                                           data-html="true"
                                           data-content="<div class='txtBlack'><?php echo strip_tags($viewData['survey']->title); ?></div>"
                                        style="color: #c79121;">
                                            <?php
                                                $pureText = strip_tags($viewData['survey']->title);
                                                echo substr($pureText, 0, 50) . '...';
                                            ?>
                                        </a>
                                    </i>
                                </u>
                            </strong>
                        </li>
                        <?php
                        if(!empty($viewData['group'])){?>

                        <li>
                            <strong>
                                <u>
                                    <i>
                                        <a href="<?php echo e(route('groupDetail',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id])); ?>"
                                           rel="popover-hover"
                                           data-placement="right"
                                           data-original-title="<span class='txtBlack'> Ir al detalle del grupo de pregunta </span>"
                                           data-html="true"
                                           data-content="<div class='txtBlack'><?php echo strip_tags($viewData['group']->name); ?></div>"
                                        style="color:#4c4f53">
                                            <?php
                                            $pureText = strip_tags($viewData['group']->name);
                                            echo substr($pureText, 0, 50) . '...';
                                            ?>
                                        </a>
                                    </i>
                                </u>
                            </strong>
                        </li>
                        <?php
                        if(!empty($viewData['question'])){?>
                        <li>
                            <strong>
                                <u>
                                    <i>
                                        <a href="<?php echo e(route('questionDetail',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id, 'questionId'=>$viewData['question']->id])); ?>"
                                           rel="popover-hover"
                                           data-placement="right"
                                           data-original-title="<span class='txtBlack'> Ir al detalla de la pregunta </span>"
                                           data-html="true"
                                           data-content="<div class='txtBlack'><?php echo strip_tags($viewData['group']->name); ?></div>">
                                            <?php
                                            $pureText = strip_tags($viewData['question']->title);
                                            echo substr($pureText, 0, 50) . '...';
                                            ?>
                                        </a>
                                    </i>
                                </u>
                            </strong>
                        </li>
                        <?php } ?>
                        <?php
                        }
                        ?>
                    </ol>
                </div>
                <div class="col-md-3 col-xs-12">
                    <p class="text-right">
                        <?php if(!empty($viewDataApp['btnSubmitQuestion'])): ?>

                            <button type="button" id="btnSave"
                                    class="btn btn-primary openMainModal">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        <?php endif; ?>

                        <a class="btn btn-default"
                           href="<?php echo e(route('survey.index')); ?>">
                            Volver a la lista de encuesta
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>