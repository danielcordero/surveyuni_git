<?php

namespace App\CustomLib\Services;

use App\User;
use Illuminate\Support\Facades\Auth;
use App\CustomLib\Helper\HelperServices;

class UserService
{
	private $currentUser;

	public function __construct($user = null){
		if(empty($user)){
			$this->currentUser = Auth::user();
		}
	}

	public function getPermissionsByRoles(){
		$permissions = $this->getPermissionsByRolesO($this->currentUser);
		if(empty($permissions)){
			$permissions = [];
		}
		return $permissions;
	}
	
	public function getPermissionsByRolesO($user = null){
		if(empty($user)){
			$user = Auth::user();
		}

		$permissions = $user->getAllPermissions();
		$permissions = $permissions->pluck('name');
		if(empty($permissions->toArray())){
			$permissions = [];
		}
		return $permissions;
	}

	public function getRoles($user = null){
		if(empty($user)){
			$user = Auth::user();
		}

	}

	public function getRolesO($user = null){
		// if(empty($user)){
		// 	$user = Auth::user();
		// }

		// $roles = $user->getRoleNames()->pluck('name');
		// if(empty($roles->toArray())){
		// 	$roles = 
		// }
	}

    public function filter($filter)
    {
//        echo $filter->fnombre;
//        exit;
        $userFilter = User::query();

        if ($filter->has('fnombre') && !empty($filter->fnombre)) {
            $userFilter->where('name','like' , '%' . $filter->input('fnombre') . '%');
        }

        if ($filter->has('femail') && !empty($filter->femail) ) {
            $userFilter->where('email', $filter->input('femail'));
        }

//        if ($filter->has('fStatus') && !empty($filter->fStatus)) {
//            $userFilter->where('active', $filter->input('fStatus'));
//        }

        if (($filter->has('fdateStart') && !empty($filter->fdateStart)) ||
            ($filter->has('fdateEnd') || !empty($filter->fdateEnd))) {
            $dateStart = $filter->fdateStart;
            $fdateEnd = $filter->fdateEnd;
            if(!HelperServices::checkFormatDateYMD($dateStart) &&
                HelperServices::checkFormatDateYMD($fdateEnd)){
                $dateStart = $fdateEnd;
            }

            if(!HelperServices::checkFormatDateYMD($fdateEnd) &&
                HelperServices::checkFormatDateYMD($dateStart)){
                $fdateEnd = $dateStart;
            }

            if(HelperServices::checkFormatDateYMD($fdateEnd) &&
                HelperServices::checkFormatDateYMD($dateStart)){

                //$userFilter->whereRaw('DATE(created_at) = ?', $filter->input('fdateCrated'));
                $userFilter->whereBetween('created_at',[$dateStart, $fdateEnd]);
            }
        }

        $userFilter->orderBy('created_at', 'DESC');
        return $userFilter;
    }
}

?>