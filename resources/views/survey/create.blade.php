@extends('layouts.app')
@section('content')

    @include('survey.elements.newEdit')

    @push('scripts')

    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/SurveyModule.js"></script>

    @endpush
@stop