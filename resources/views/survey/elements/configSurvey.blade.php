<div class="jarviswidget"
     data-widget-editbutton="false"
     data-widget-custombutton="false"
     data-widget-deletebutton="false"
     data-widget-sortable="false"
     data-widget-collapsed="false"
     data-widget-fullscreenbutton="false">

    <header role="heading">
        <h2>
            <strong>
                Notificación y gestión de datos
            </strong>
        </h2>
        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
    </header>

    <!-- widget div-->
    <div role="content">
        <div class="widget-body">

            <div class="form-horizontal">

                <!-- email basic to -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="emailnotificationto">Send basic admin
                        notification email to:</label>
                    <div class="col-sm-7">
                        <input size="70" class="form-control" type="text" name="emailnotificationto"
                               id="emailnotificationto"></div>
                </div>

                <!-- email detail to  -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="emailresponseto">Send detailed admin notification
                        email to:</label>
                    <div class="col-sm-7">
                        <input size="70" class="form-control" type="text" name="emailresponseto"
                               id="emailresponseto"></div>
                </div>

                <!-- Date Stamp -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="datestamp">Date stamp:</label>
                    <div class="col-sm-7">


                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="datestamp"
                                               class="onoffswitch-checkbox"
                                               id="ytdatestamp">
                                        <label class="onoffswitch-label"
                                               for="ytdatestamp">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Save IP Address -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="ipaddr">Save IP address:</label>
                    <div class="col-sm-7">


                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="ipaddr"
                                               class="onoffswitch-checkbox"
                                               id="ytipaddr">
                                        <label class="onoffswitch-label"
                                               for="ytipaddr">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Save referrer URL -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="refurl">Save referrer URL:</label>
                    <div class="col-sm-7">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="refurl"
                                               class="onoffswitch-checkbox"
                                               id="ytrefurl">
                                        <label class="onoffswitch-label"
                                               for="ytrefurl">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Save timings -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="savetimings">Save timings:</label>
                    <div class="col-sm-7">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="savetimings"
                                               class="onoffswitch-checkbox"
                                               id="ytsavetimings">
                                        <label class="onoffswitch-label"
                                               for="ytsavetimings">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Enable assessment mode -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="assessments">Enable assessment mode:</label>
                    <div class="col-sm-7">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="assessments"
                                               class="onoffswitch-checkbox"
                                               id="ytassessments">
                                        <label class="onoffswitch-label"
                                               for="ytassessments">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>


                    </div>
                </div>

                <!-- Participant may save and resume  -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="allowsave">Participant may save and resume
                        later:</label>
                    <div class="col-sm-7">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="allowsave"
                                               class="onoffswitch-checkbox"
                                               id="ytallowsave">
                                        <label class="onoffswitch-label"
                                               for="ytallowsave">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>


                    </div>
                </div>

                <!-- GoogleAnalytics settings to be used -->
                <div class="form-group">
                    <label class="col-sm-5 control-label" for="googleanalyticsapikeysetting">
                        Google Analytics settings: </label>
                    <div class="col-sm-7">
                        <div class="btn-group" id="googleanalyticsapikeysetting" data-toggle="buttons">
                            <label class="btn btn-default active"><input name="googleanalyticsapikeysetting"
                                                                         id="googleanalyticsapikeysetting_opt1"
                                                                         value="N" checked="checked"
                                                                         type="radio">None</label>
                            <label class="btn btn-default"><input name="googleanalyticsapikeysetting"
                                                                  id="googleanalyticsapikeysetting_opt2"
                                                                  value="Y" type="radio">Use settings
                                below</label>
                            <label class="btn btn-default"><input name="googleanalyticsapikeysetting"
                                                                  id="googleanalyticsapikeysetting_opt3"
                                                                  value="G" type="radio">Use global
                                settings</label>
                        </div>
                    </div>
                </div>
                <!-- Google Analytics -->
                <div class="form-group" style="display: none;">
                    <label class="col-sm-5 control-label" for="googleanalyticsapikey">Google Analytics API
                        key:</label>
                    <div class="col-sm-7">
                        <input size="20" type="text" name="googleanalyticsapikey" id="googleanalyticsapikey"
                               disabled="disabled"></div>
                </div>
                <!-- Google Analytics style -->
                <div class="form-group" style="display: none;">
                    <label class="col-sm-5 control-label" for="googleanalyticsstyle">Google Analytics
                        style:</label>
                    <div class="col-sm-7">
                        <div class="btn-group" id="googleanalyticsstyle" data-toggle="buttons">
                            <label class="btn btn-default active disabled"><input name="googleanalyticsstyle"
                                                                                  id="googleanalyticsstyle_opt1"
                                                                                  value="0" checked="checked"
                                                                                  type="radio">Off</label>
                            <label class="btn btn-default disabled"><input name="googleanalyticsstyle"
                                                                           id="googleanalyticsstyle_opt2"
                                                                           value="1"
                                                                           type="radio">Default</label>
                            <label class="btn btn-default disabled"><input name="googleanalyticsstyle"
                                                                           id="googleanalyticsstyle_opt3"
                                                                           value="2" type="radio">Survey-SID/Group</label>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="jarviswidget"
     data-widget-editbutton="false"
     data-widget-custombutton="false"
     data-widget-deletebutton="false"
     data-widget-sortable="false"
     data-widget-collapsed="false"
     data-widget-fullscreenbutton="false">

    <header role="heading">
        <h2>
            <strong>
                Tokens
            </strong>
        </h2>
        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
    </header>

    <!-- widget div-->
    <div role="content">
        <div class="widget-body">

            <div class="form-horizontal">
                <!-- Anonymized responses -->
                <div class="form-group">
                    <label class="col-sm-6 control-label" for="anonymized"
                           title="If you set 'Yes' then no link will exist between token table and survey responses table. You won't be able to identify responses by their token.">
                        Anonymized responses:
                        <script type="text/javascript"><!--
                            function alertPrivacy() {
                                if ($('#tokenanswerspersistence').is(':checked') == true) {
                                    $('#alertPrivacy1').modal();
                                    document.getElementById('anonymized').value = '0';
                                }
                                else if ($('#anonymized').is(':checked') == true) {
                                    $('#alertPrivacy2').modal();
                                }
                            }
                            //--></script>
                    </label>
                    <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="anonymized"
                                               class="onoffswitch-checkbox"
                                               id="ytanonymized">
                                        <label class="onoffswitch-label"
                                               for="ytanonymized">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Enable token-based response persistence -->
                <div class="form-group">
                    <label class="col-sm-6 control-label" for="tokenanswerspersistence"
                           title="With non-anonymized responses (and the token table field 'Uses left' set to 1) if the participant closes the survey and opens it again (by using the survey link) his previous answers will be reloaded.">
                        Enable token-based response persistence: </label>
                    <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="tokenanswerspersistence"
                                               class="onoffswitch-checkbox"
                                               id="yttokenanswerspersistence">
                                        <label class="onoffswitch-label"
                                               for="yttokenanswerspersistence">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Allow multiple responses or update responses with one token -->
                <div class="form-group">
                    <label class="col-sm-6 control-label" for="alloweditaftercompletion"
                           title="If token-based response persistence is enabled a participant can update his response after completion, else a participant can add new responses without restriction.">
                        Allow multiple responses or update responses with one token: </label>
                    <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="alloweditaftercompletion"
                                               class="onoffswitch-checkbox"
                                               id="ytalloweditaftercompletion">
                                        <label class="onoffswitch-label"
                                               for="ytalloweditaftercompletion">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Allow public registration -->
                <div class="form-group">
                    <label class="col-sm-6 control-label" for="allowregister">Allow public registration:</label>
                    <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="allowregister"
                                               class="onoffswitch-checkbox"
                                               id="ytallowregister">
                                        <label class="onoffswitch-label"
                                               for="ytallowregister">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Use HTML format for token emails -->
                <div class="form-group">
                    <label class="col-sm-6 control-label" for="htmlemail">Use HTML format for token
                        emails:</label>
                    <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="htmlemail"
                                               class="onoffswitch-checkbox"
                                               id="ythtmlemail">
                                        <label class="onoffswitch-label"
                                               for="ythtmlemail">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!-- Send confirmation emails -->
                <div class="form-group">
                    <label class="col-sm-6 control-label" for="sendconfirmation">Send confirmation
                        emails:</label>
                    <div class="col-sm-6">

                                <span class="onoffswitch">
                                        <input type="checkbox"
                                               name="sendconfirmation"
                                               class="onoffswitch-checkbox"
                                               id="ytsendconfirmation">
                                        <label class="onoffswitch-label"
                                               for="ytsendconfirmation">
                                    <span class="onoffswitch-inner"
                                          data-swchon-text="ON"
                                          data-swchoff-text="OFF"></span>
                                    <span class="onoffswitch-switch"></span></label>
                                </span>

                    </div>
                </div>

                <!--  Set token length to -->
                <div class="form-group">
                    <label class="col-sm-6 control-label" for="tokenlength">Set token length to:</label>
                    <div class="col-sm-6">
                        <input type="text" value="15" name="tokenlength" id="tokenlength" size="4" maxlength="2"
                               onkeypress="return goodchars(event,'0123456789')" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>