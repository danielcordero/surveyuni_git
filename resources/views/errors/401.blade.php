{{-- \resources\views\errors\401.blade.php --}}
@extends('layouts.app')

@section('content')
    <div class='col-lg-4 col-lg-offset-4'>
        <h1>
            <strong>
            <center>
                401<br>
                ACCESO DENEGADO, NO TIENE PERMISO PARA ACCEDER A ESTA VISTA</center>
            </strong>
        </h1>
    </div>

@endsection