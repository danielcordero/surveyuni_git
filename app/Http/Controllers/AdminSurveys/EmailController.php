<?php

namespace App\Http\Controllers\AdminSurveys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\Services\{
    EmailService, ParticipantService, SurveySettingsServices,SurveyService
};
use App\{SettingsGlobal,Survey};
use App\CustomLib\GenericMessages;
use App\CustomLib\Helper\{HelperServices,Template};
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Helper\Helper;


class EmailController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckOwnSurvey');
        $this->middleware('CheckExistResource');
    }

    public function viewtemplates(Request $request)
    {
        $defaultTemplates = SurveySettingsServices::getDefaultTemplates();
        $tagsStructure = EmailService::getTabStructure($defaultTemplates);

        $surveyId = (int)$request->surveyId;
        $viewData['tagsStructure'] = $tagsStructure;
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['surveysetting'] = $viewData['survey']->surveysetting;

        return view('email.templates')->with('viewData',$viewData);
    }

    public function updatetemplates(Request $request)
    {
        $surveyId = (int)$request->survey_id;
        $surveyconfigId = (int)$request->surveyconfig_id;
        $result = SurveySettingsServices::update($request, $surveyconfigId);
        if(!empty($result)){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }
        return redirect()->route('email.viewtemplates',[$surveyId]);
    }

    public function validatetemplate(Request $request)
    {

        $surveyId = HelperServices::sanitize_int($request->surveyId);
        $sType = HelperServices::sanitize_xss_string($request->type);

        $aTypeAttributes=array(
            'invitation'=>array(
                'subject'=>array(
                    'attribute'=>'survey_email_invite_subj',
                    'title'=>('Invitation email subject'),
                ),
                'message'=>array(
                    'attribute'=>'survey_email_invite',
                    'title'=>('Invitation email body'),
                ),
            ),
            'reminder'=>array(
                'subject'=>array(
                    'attribute'=>'survey_email_remind_subj',
                    'title'=>('Reminder email subject'),
                ),
                'message'=>array(
                    'attribute'=>'survey_email_remind',
                    'title'=>('Reminder email body'),
                ),
            ),
            'confirmation'=>array(
                'subject'=>array(
                    'attribute'=>'survey_email_confirm_subj',
                    'title'=>('Confirmation email subject'),
                ),
                'message'=>array(
                    'attribute'=>'survey_email_confirm',
                    'title'=>('Confirmation email body'),
                ),
            ),
            'registration'=>array(
                'subject'=>array(
                    'attribute'=>'survey_email_register_subj',
                    'title'=>('Registration email subject'),
                ),
                'message'=>array(
                    'attribute'=>'survey_email_register',
                    'title'=>('Registration email body'),
                ),
            ),
            'admin_notification'=>array(
                'subject'=>array(
                    'attribute'=>'email_admin_notification_subj',
                    'title'=>('Basic admin notification subject'),
                ),
                'message'=>array(
                    'attribute'=>'email_admin_notification',
                    'title'=>('Basic admin notification body'),
                ),
            ),
            'admin_detailed_notification'=>array(
                'subject'=>array(
                    'attribute'=>'email_admin_responses_subj',
                    'title'=>('Detailed admin notification subject'),
                ),
                'message'=>array(
                    'attribute'=>'email_admin_responses',
                    'title'=>('Detailed admin notification body'),
                ),
            ),
        );
        $surveyInfo = Survey::find($surveyId);
        $aReplacement=array(
            'ADMINNAME'=> $surveyInfo->surveysetting->admin,
            'ADMINEMAIL'=> $surveyInfo->surveysetting->adminemail,
        );

        $aReplacement["SURVEYNAME"] = "Name of the survey";
        $aReplacement["SURVEYDESCRIPTION"] =  "Description of the survey";
        $aReplacement["TOKEN"] = "Token code for this participant";
        $aReplacement["TOKEN:EMAIL"] = "Email from the token";
        $aReplacement["TOKEN:FIRSTNAME"] = "First name from token";
        $aReplacement["TOKEN:LASTNAME"] = "Last name from token";
        $aReplacement["TOKEN:TOKEN"] = "Token code for this participant";
        $aReplacement["TOKEN:LANGUAGE"] = "language of token";

        switch ($sType)
        {
            case 'invitation' :
            case 'reminder' :
            case 'registration' :
                // Replaced when sending email (registration too ?)
                $aReplacement["EMAIL"] = ("Email from the token");
                $aReplacement["FIRSTNAME"] = ("First name from token");
                $aReplacement["LASTNAME"] = ("Last name from token");
                $aReplacement["LANGUAGE"] = ("language of token");
                $aReplacement["OPTOUTURL"] = ("URL for a respondent to opt-out of this survey");
                $aReplacement["OPTINURL"] = ("URL for a respondent to opt-in to this survey");
                $aReplacement["SURVEYURL"] = ("URL of the survey");
                break;
            case 'confirmation' :
                $aReplacement["EMAIL"] = ("Email from the token");
                $aReplacement["FIRSTNAME"] = ("First name from token");
                $aReplacement["LASTNAME"] = ("Last name from token");
                $aReplacement["SURVEYURL"] = ("URL of the survey");
                break;
            case 'admin_notification' :
            case 'admin_detailed_notification' :
                $aReplacement["RELOADURL"] = ("Reload URL");
                $aReplacement["VIEWRESPONSEURL"] = ("View response URL");
                $aReplacement["EDITRESPONSEURL"] = ("Edit response URL");
                $aReplacement["STATISTICSURL"] = ("Statistics URL");
                $aReplacement["ANSWERTABLE"] = ("Answers from this response");
                // $moveResult = LimeExpressionManager::NavigateForwards(); // Seems OK without, nut need $LEM::StartSurvey
                break;
            default:
                throw new CHttpException(400,gT('Invalid type.'));
                break;
        }

        $aData=array();
        $aExpressions=array();
        foreach($aTypeAttributes[$sType] as $key=>$aAttribute)
        {
            $sAttribute=$aAttribute['attribute'];
            // Email send do : templatereplace + ReplaceField to the Templatereplace done : we need 2 in one
            // $LEM::ProcessString($oSurveyLanguage->$sAttribute,null,$aReplacement,false,1,1,false,false,true); // This way : ProcessString don't replace coreReplacements
            $aExpressions[$key]=array(
                'title'=>$aAttribute['title'],
                //'expression'=> $this->getHtmlExpression($surveyInfo->{$sAttribute},$aReplacement,__METHOD__),
            );
        }
    }


    public function viewsendinvitation(Request $request)
    {

        $defaultTemplates = SurveySettingsServices::getDefaultTemplates();
        $tagsStructure = EmailService::getTabStructure($defaultTemplates);

        $surveyId = (int)$request->surveyId;
        $viewData['tagsStructure'] = $tagsStructure;
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['surveysetting'] = $viewData['survey']->surveysetting;

        return view('email.viewsendinvitation')->with('viewData',$viewData);
    }

    public function viewsendremind(Request $request)
    {
        $defaultTemplates = SurveySettingsServices::getDefaultTemplates();
        $tagsStructure = EmailService::getTabStructure($defaultTemplates);

        $surveyId = (int)$request->surveyId;
        $viewData['tagsStructure'] = $tagsStructure;
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['surveysetting'] = $viewData['survey']->surveysetting;

        return view('email.viewsendremind')->with('viewData',$viewData);
    }

    public function sendinvitation(Request $request)
    {
        $surveyId = (int)$request->survey_id;
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['surveysetting'] = $viewData['survey']->surveysetting;

        $outputMessage = "";
        $bSendError = false;

        $participantsSelected = Session::get('some_invitation_'.$surveyId);

        if(!empty($participantsSelected)){
            $participants = ParticipantService::getSomeUninvited($request->surveyId, $participantsSelected);

        } else {
            $participants = ParticipantService::getUninvited($surveyId, true);
            $amountSent = ParticipantService::getAmountSent( $surveyId );

            if(empty($amountSent) && count($participants) > 0){
                SurveyService::setFirstBlockSent( $surveyId );
            }

            if($viewData['survey']->participants->count() == $amountSent){
                SurveyService::setAllInvitationSent( $surveyId );
            }
        }

        $tpl = new Template();
        foreach ($participants as $participant)
        {
            $messageHtml = $request->survey_email_invite;
            $subject = $request->survey_email_invite_subj;
            //Change character set, or at least try
            //https://stackoverflow.com/questions/7979567/php-convert-any-string-to-utf-8-without-knowing-the-original-character-set-or
            $messageHtml = iconv(mb_detect_encoding($messageHtml, mb_detect_order(), true), "UTF-8", $messageHtml);
            $subject = iconv(mb_detect_encoding($subject, mb_detect_order(), true), "UTF-8", $subject);

            $fieldsarray = [];
            $fieldsarray["SURVEYNAME"] = $viewData['survey']->title;
            $fieldsarray["SURVEYDESCRIPTION"] = strip_tags($viewData['survey']->description);
            $fieldsarray["ADMINNAME"] = SettingsGlobal::find("siteadminname")->stg_value;
            $fieldsarray["ADMINEMAIL"] = SettingsGlobal::find("siteadminemail")->stg_value;

            foreach ($participant as $attribute => $value)
            {
                $fieldsarray[strtoupper($attribute)] = $value;
            }

            $fieldsarray["OPTOUTURL"] = HelperServices::addQStoRoute('public.index', [$surveyId], ['token'=>$participant['token']]);
            $fieldsarray["OPTINURL"] = HelperServices::addQStoRoute('public.index', [$surveyId], ['token'=>$participant['token']]);
            $fieldsarray["SURVEYURL"] = HelperServices::addQStoRoute('public.index', [$surveyId], ['token'=>$participant['token']]);

            foreach(array('OPTOUT', 'OPTIN', 'SURVEY') as $key)
            {
                $url = $fieldsarray["{$key}URL"];
                $fieldsarray["{$key}URL"] = "<a href='{$url}'>" . htmlspecialchars($url) . '</a>';
                $subject = str_replace("@@{$key}URL@@", $url, $subject);
                $messageHtml = str_replace("@@{$key}URL@@", $url, $messageHtml);
            }

            // Include class file and instantiate.
            $tpl->assign($fieldsarray);

            // Parse the template file
            $messageHtml = $tpl->display($messageHtml);
            $parameters['emailParticipant'] = $participant['email'];
            $parameters['messageHtml'] = $messageHtml;
            $parameters['subject'] = $subject;

            $result = EmailService::sendInvitation( $parameters );
            if(empty($result['success'])){
                $bSendError = true;
                $outputMessage .= htmlspecialchars("El correo para {$participant['firstname']} {$participant['lastname']} fallo {$result['message']}");
            } else {
                ParticipantService::setSendInvitation($participant['id']);
            }
        }

        $viewData['isThereError'] = $bSendError;
        $viewData['tokenoutput'] = $outputMessage;
        if(!$bSendError){
            $viewData['tokenoutput'].="<strong class='result success text-success'>Todos los correos electrónicos fueron enviados.<strong>";
        } else {
            $viewData['tokenoutput'] .= "<br><strong class='result warning text-warning textRed'> No todos los correos electrónicos fueron enviados: <strong><ul class='list-unstyled'>";
        }
        return view('email.emailpostinvitation')->with('viewData',$viewData);
    }

    public function sendremind(Request $request)
    {
        $surveyId = (int)$request->survey_id;
        $viewData['survey'] = Survey::find( $surveyId );
        $viewData['surveysetting'] = $viewData['survey']->surveysetting;
        $fieldsarray = [];
        $outputMessage = "";
        $bSendError = false;

        $participantsSelected = Session::get('some_remainder_'.$surveyId);
        if(!empty($participantsSelected)){
            $participants = ParticipantService::getSomeUnanswered($surveyId, $participantsSelected);

        } else {
            $participants = ParticipantService::getUnanswered($surveyId, true);
            ///$surveyInfo = Survey::find( $surveysIds[$i] );
            $amountSent = ParticipantService::getAmountSent( $surveyId );
            //if the amount of participant is equal to amountsent
            //then set sent at all

            if(!empty($viewData['surveysetting']->participants)){
                if($viewData['surveysetting']->participants->count() == $amountSent){
                    SurveyService::setAllInvitationSent( $surveyId );
                }
            }
        }

        $tpl = new Template();
        foreach ($participants as $participant)
        {
            $messageHtml = $request->survey_email_remind;
            $subject = $request->survey_email_remind_subj;

            //Change character set, or at least try
            //https://stackoverflow.com/questions/7979567/php-convert-any-string-to-utf-8-without-knowing-the-original-character-set-or
            $messageHtml = iconv(mb_detect_encoding($messageHtml, mb_detect_order(), true), "UTF-8", $messageHtml);
            $subject = iconv(mb_detect_encoding($subject, mb_detect_order(), true), "UTF-8", $subject);
//            $messageHtml = HelperServices::force_utf8($messageHtml);
//            $subject = HelperServices::force_utf8($subject);

            $fieldsarray["SURVEYNAME"] = $viewData['survey']->title;
            $fieldsarray["SURVEYDESCRIPTION"] = strip_tags($viewData['survey']->description);
            $fieldsarray["ADMINNAME"] = SettingsGlobal::find("siteadminname")->stg_value;
            $fieldsarray["ADMINEMAIL"] = SettingsGlobal::find("siteadminemail")->stg_value;

            foreach ($participant as $attribute => $value)
            {
                $fieldsarray[strtoupper($attribute)] = $value;
            }

            $fieldsarray["OPTOUTURL"] = HelperServices::addQStoRoute('public.index', [$surveyId], ['token'=>$participant['token']]);
            $fieldsarray["OPTINURL"] = HelperServices::addQStoRoute('public.index', [$surveyId], ['token'=>$participant['token']]);
            $fieldsarray["SURVEYURL"] = HelperServices::addQStoRoute('public.index', [$surveyId], ['token'=>$participant['token']]);

            foreach(array('OPTOUT', 'OPTIN', 'SURVEY') as $key)
            {
                $url = $fieldsarray["{$key}URL"];
                $fieldsarray["{$key}URL"] = "<a href='{$url}'>" . htmlspecialchars($url) . '</a>';
                $subject = str_replace("@@{$key}URL@@", $url, $subject);
                $messageHtml = str_replace("@@{$key}URL@@", $url, $messageHtml);
            }

            // Include class file and instantiate.
            $tpl->assign($fieldsarray);

            // Parse the template file
            $messageHtml = $tpl->display($messageHtml);

            $parameters['emailParticipant'] = $participant['email'];
            $parameters['messageHtml'] = $messageHtml;
            $parameters['subject'] = $subject;
//            $outputMessage .= htmlspecialchars("El correo para {$participant['firstname']} {$participant['lastname']} fallo");

            $result = EmailService::sendInvitation( $parameters );
            if(empty($result['success'])){
                $bSendError = true;
                $outputMessage .= htmlspecialchars("El correo para {$participant['firstname']} {$participant['lastname']} fallo {$result['message']}");
            } else {
                ParticipantService::setSendRemind($participant['id']);
            }
        }

        $viewData['isThereError'] = $bSendError;
        $viewData['tokenoutput'] = $outputMessage;
        if(!$bSendError){
            $viewData['tokenoutput'].="<strong class='result success text-success'>Todos los correos electrónicos fueron enviados.<strong>";
        } else {
            $viewData['tokenoutput'] .= "<br><strong class='result warning text-warning textRed'> No todos los correos electrónicos fueron enviados: <strong><ul class='list-unstyled'>";
        }

        return view('email.emailpostremind')->with('viewData',$viewData);
    }

}