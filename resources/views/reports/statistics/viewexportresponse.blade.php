<?php
use \App\CustomLib\Services\ParticipantService;

?>
@extends('layouts.app')
@section('content')
    @include('elements.modals.confirm')
    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-download"></i>
                    <strong>
                        Exportar resultado de la encuesta
                    </strong>
                </h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        @include('reports.statistics.elements.topButtons')
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            <i class="fa fa-download"></i>
                                            Resultados
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <article class="col-sm-12 col-sm-12">
                                <div class="jarviswidget jarviswidget-initialHeight"
                                     id="barChartwidget"
                                     data-widget-colorbutton="false"
                                     data-widget-fullscreenbutton="false"
                                     data-widget-editbutton="false"
                                     data-widget-sortable="false"
                                     data-widget-deletebutton="false">
                                    <header style="padding-left: 8px;" class="headerWidget">
                                        <h5>
                                            <strong>
                                                Campos por defecto
                                            </strong>
                                        </h5>
                                    </header>
                                    <div>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <!-- Column control -->
                                                <input type="hidden" name="sid" value="162655">
                                                <label for="colselect" class="col-sm-3 control-label">
                                                    <strong>
                                                        Seleccione campos:
                                                    </strong>
                                                </label>
                                                <div class="col-sm-9">
                                                    <select multiple="multiple"
                                                            size="20"
                                                            class="form-control"
                                                            name="colselect[]" id="colselect">

                                                        <option value="id"
                                                                selected="selected"
                                                                title="Response ID"
                                                                data-fieldname="id"
                                                                data-emcode="id">
                                                            id - Response ID
                                                        </option>
                                                        <option value="submitdate"
                                                                selected="selected"
                                                                title="Date submitted "
                                                                data-fieldname="submitdate"
                                                                data-emcode="submitdate">submitdate - Date submitted
                                                        </option>
                                                        <option value="lastpage"
                                                                selected="selected"
                                                                title="Last page "
                                                                data-fieldname="lastpage"
                                                                data-emcode="lastpage">lastpage - Last page
                                                        </option>
                                                        <option value="startlanguage"
                                                                selected="selected"
                                                                title="Start language "
                                                                data-fieldname="startlanguage"
                                                                data-emcode="startlanguage">
                                                            startlanguage - Start language
                                                        </option>

                                                        <option value="token"
                                                                selected="selected"
                                                                title="Token "
                                                                data-fieldname="token"
                                                                data-emcode="token">token - Token</option>

                                                        <option value="162655X15X87SQ001"
                                                                selected="selected"
                                                                title="Pregunta de tipo matriz (Array 1)"
                                                                data-fieldname="162655X15X87SQ001"
                                                                data-emcode="Array_SQ001">Array[SQ001] - Pregunta de tipo matriz (Array 1)</option>

                                                        <option value="162655X15X87SQ002"
                                                                selected="selected"
                                                                title="Pregunta de tipo matriz (Array 2)"
                                                                data-fieldname="162655X15X87SQ002"
                                                                data-emcode="Array_SQ002">Array[SQ002] - Pregunta de tipo matriz (Array 2)
                                                        </option>

                                                        <option value="162655X15X87SQ003"
                                                                selected="selected"
                                                                title="Pregunta de tipo matriz (Array 3)"
                                                                data-fieldname="162655X15X87SQ003"
                                                                data-emcode="Array_SQ003">Array[SQ003] - Pregunta de tipo matriz (Array 3)
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-8 col-sm-offset-4">
                                                    <br>
                                                    <strong id="columncount">8 de 8 columnas seleccionadas</strong>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <!-- Column control -->
                                                <div class="alert alert-info alert-dismissible" role="alert">
                                                    <button type="button"
                                                            class="close limebutton"
                                                            data-dismiss="alert" aria-label="Close"><span>×</span>
                                                    </button>
                                                    Su encuesta puede exportar datos de otros campos con cada respuesta.
                                                    Seleccione cualquier campo adicional que le gustaría exportar.
                                                </div>
                                                <div>
                                                    <label for="attribute_select" class="col-sm-4 control-label">
                                                        <strong>
                                                            Elegir otros campos
                                                        </strong>

                                                    </label>
                                                    <div class="col-sm-8" style="margin-bottom: 35px;">
                                                        <select name="attribute_select[]" multiple="" size="20" class="form-control" id="attribute_select">
                                                            <option value="first_name" id="first_name">First name</option>
                                                            <option value="last_name" id="last_name">Last name</option>
                                                            <option value="email_address" id="email_address">Email address</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
@stop