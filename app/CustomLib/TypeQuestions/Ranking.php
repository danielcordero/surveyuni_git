<?php

namespace App\CustomLib\TypeQuestions;
use App\Question;

class Ranking extends BaseQuestion{

    public function saveConfig( $data ){
        $jsonToSave = new \stdClass();
        $subQuestions = $data->subQuestions;
        foreach ($subQuestions as $key => $value){
            $subQuestions[$key] = $this->cleanData( $value );
        }

        $jsonToSave->subQuestions = $subQuestions;
        $question = Question::find( $data->question_id );
        $question->additionalData = json_encode( $jsonToSave );
        $question->save();
    }

    public function getConfig( $dataJson ){
        return json_decode( $dataJson, true );
    }

    public function validateAnswers( $partakerRequest ){

        $response = $this->checkAnswers($partakerRequest);
        if($this->isMandatory($partakerRequest) && !empty($response['fail']) ){
            $response['message']= "Esta pregunta es requerida";
            $response['messages'][] = "Por favor complete todas las partes";
        }
        $response['groupId'] = $partakerRequest['groupId'];
        return $response;
    }

    public function checkAnswers( $partakerRequest ){

        $answers = !empty($partakerRequest['answers']) ? $partakerRequest['answers'] : [];
        $subQuestions = $partakerRequest['subQuestions'];
        $subQuestions = explode("|",$subQuestions);
        $messages = [];

        $fail = false;
        $keepAnswered = [];
        $withoutAnswered = [];

        foreach ($subQuestions as $key => $value){
            if(in_array($value, $answers)){
                $keepAnswered[] = $value;
            } else {
                $withoutAnswered[] = $value;
            }
        }

        if(!empty($withoutAnswered) &&
            !empty($partakerRequest['mandatory']) &&
            $partakerRequest['mandatory'] == "Y"){
            $fail = true;
        }

        $response = [];
        $response['fail'] = $fail;
        $response['keepAnswered'] = $keepAnswered;
        $response['withoutAnswered'] = $withoutAnswered;
        $response['messages'] = $messages;

        return $response;
    }

}
?>