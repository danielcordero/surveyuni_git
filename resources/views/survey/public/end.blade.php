<?php
    use \App\CustomLib\Services\SurveyPublic;
    $surveyPublic = new SurveyPublic;
?>

@extends('survey.public.layout')
@section('title',$viewData['config']['survey']->title)
@section('content')
    <form action="{{ route('public.index',['surveyId'=> $viewData['config']['survey']->id ]) }}"
          method="post">
        {{ csrf_field() }}
        <div class="row">
            <br>
            @if($surveyPublic->isModePreview())
                <div class="col-md-12 text-center titleSurvey">
                    <div class="alert alert-warning alert-dismissible fade in alert-dismissible"
                         role="alert">
                        <!--<button type="button"
                                data-dismiss="alert"
                                aria-label="Close" class="close">
                            <span aria-hidden="true">×</span></button>-->
                        <h3>
                            Está encuesta actualmente no está activa. Usted no será capaz de guardar sus respuestas.
                        </h3>
                    </div>
                </div>
            @endif

            <div class="col-md-12 text-center titleSurvey">
                <h2>
                    {!! $viewData['config']['survey']->endtext !!}
                    {{--{!! $viewData['config']['survey']->title !!}--}}
                </h2>
            </div>

            @if(!empty($viewData['config']['survey']->url))
                <div class="col-md-12 text-center titleSurvey">
                    <h2>
                        Para más información ver el enlace de abajo
                    </h2>
                    <h3>
                        <a href="{{$viewData['config']['survey']->url}}" target="_blank"
                           style="text-decoration: underline;">
                            @if(empty($viewData['config']['survey']->urldescription))
                                {{$viewData['config']['survey']->url}}
                            @else
                                {{$viewData['config']['survey']->urldescription}}
                            @endif
                        </a>
                    </h3>
                </div>
            @endif
            <!--<div class="col-md-12 text-center">
                {{--{!! $viewData['config']['survey']->description !!}--}}
            </div>-->
            <!--<div class="col-md-12 text-center">

            </div>-->
        </div>
        <!-- PRESENT THE NAVIGATOR -->
        <input name="survey_id"
               type="hidden"
               value="<?php echo $viewData['config']['survey']->id; ?>">
    </form>
@stop