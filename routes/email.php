<?php

// routes/web.php
Route::group(['prefix' => 'admin'], function () {
    //participants
    Route::get('/survey/{surveyId}/email/templates',
        'AdminSurveys\EmailController@viewtemplates')
        ->name("email.viewtemplates");

    Route::post('/survey/{surveyId}/email/templates/update',
        'AdminSurveys\EmailController@updatetemplates')
        ->name("email.updatetemplates");

    Route::get('/survey/email/validatetemplate',
        'AdminSurveys\EmailController@validatetemplate')
        ->name("email.validatetemplate");

    Route::get('/survey/{surveyId}/participants/viewsendinvitation',
        'AdminSurveys\EmailController@viewsendinvitation')
        ->name("email.viewsendinvitation");

    Route::get('/survey/{surveyId}/participants/viewsendremind',
        'AdminSurveys\EmailController@viewsendremind')
        ->name("email.viewsendremind");

    Route::post('/survey/{surveyId}/email/sendinvitation',
        'AdminSurveys\EmailController@sendinvitation')
        ->name("email.sendinvitation");

    Route::post('/survey/{surveyId}/email/sendremind',
        'AdminSurveys\EmailController@sendremind')
        ->name("email.sendremind");
});