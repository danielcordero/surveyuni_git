<?php

namespace App\Http\Controllers;

use App\CustomLib\Helper\HelperServices;
use Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\CustomLib\GenericMessages;
use Illuminate\Http\Request;
use App\User;
use Session;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('CheckPermission',['except'=>'setroles']);
        $this->middleware('CheckPermission',['except'=>['setroles','destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function manageroles( $userId )
    {   
        $user = User::where("id",$userId)->first();
        if(empty($user)){
            return redirect()->back();
            //return redirect()->back()->with('message', 'IT WORKS!');
        }

        $viewData = array();
        $permissions = array();
        $viewData['user'] = $user;
        $viewData['roles'] = Role::all();
        $userPermissions = $viewData['user']->getAllPermissions();
        $userPermissions = $userPermissions->pluck('name')->toArray();
        $viewData['user']->myPermissions = $userPermissions;

//        echo "<pre>";
//        print_r($userPermissions);
////        print_r($viewData['user']->myPermissions);
//        exit;

        return view('security.manageroles')
            ->with('viewData',$viewData);
    }

    public function setroles(Request $request){
       $requestData = $request->toArray();
       $user = User::where("id",$requestData['userId'])->first();

       if(!empty($requestData['roles'])){
            $user->roles()->sync($requestData['roles']);
       } else {
            $user->roles()->detach();
       }
       
       if(!empty($requestData['permissions'])){
           $allPermission = [];
           foreach ($requestData['permissions'] as $key => $rolePermissions) {
                $roleInf =  Role::where("id",$key)->first();
                $roleInfPermission = $roleInf->permissions()->pluck("id")->toArray();
                //if(count($roleInfPermission) != count($rolePermissions)){
                    foreach ($rolePermissions as $key => $permission) {
                        if(!in_array($permission,$allPermission)){
                            array_push($allPermission, $permission);
                        }
                    }
                //}
            }

            $user->syncPermissions($allPermission);
       } else {
            $user->permissions()->detach();
       }

       Session::flash('success', 'Cambios aplicados exitosamente');
       return redirect()->route('adminuser');
    }

    public function index()
    {
        return view('survey.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $userId )
    {
        $success = true;
        \DB::beginTransaction();
        try {
            $userId = (int)$userId;
            $user = User::where("id",$userId)->first();
            if(!empty($user)){
                $user->delete();
            } else {
                $success = false;
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $success = false;
        }

        if($success){
            Session::flash('success', GenericMessages::$all['successOperation']);
        } else{
            Session::flash('error', GenericMessages::$all['failOperation']);
        }

        return redirect()->route('adminuser');
    }

    public function addeditusersview($userId = "")
    {
        $viewData = [];
        if(!empty($userId)){
            $userId = HelperServices::sanitize_int($userId);
            $userInfo = User::find($userId);
            if(!empty($userInfo)){

                $viewData['user'] = $userInfo;
            }
        }
        return view('security.addedituser')->with('viewData', $viewData);;
    }

    public function createuser(Request $request)
    {
        $rules = [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ];

        $messages = [
            'name.required' => 'El campo nombre es requerido',
            'name.max' => 'La longitud máxima del campo nombre es 120',
            'email.required' => 'El campo correo es requerido',
            'email.email' => 'Por favor agregue un correo valido',
            'email.unique' => 'El correo que está queriendo agregar ya existe',
            'password.required' => 'El campo contraseña es requerido',
            'password.min' => 'La logintud mínima para el campo contraseña es 6',
            'password.confirmed' => 'El campo confirmación contraseña no coinciden',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);
        if(!empty($validator->errors()) && !empty($validator->errors()->toArray())){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $request['password'] = bcrypt($request['password']);
        User::create($request->only('email', 'name', 'password'));
        Session::flash('success', GenericMessages::$all['userAddedSuccessfully']);

        return redirect()->route('adminuser');
    }

    public function updateuser(Request $request)
    {
        $rules = [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$request->user_id,
            'password'=>'required|min:6|confirmed'
        ];

        $messages = [
            'name.required' => 'El campo nombre es requerido',
            'name.max' => 'La longitud máxima del campo nombre es 120',
            'email.required' => 'El campo correo es requerido',
            'email.email' => 'Por favor agregue un correo valido',
            'email.unique' => 'El correo que está queriendo agregar ya existe',
            'password.required' => 'El campo contraseña es requerido',
            'password.min' => 'La logintud mínima para el campo contraseña es 6',
            'password.confirmed' => 'El campo confirmación contraseña no coinciden',
        ];

        $validator = Validator::make($request->all(),$rules,$messages);
        if(!empty($validator->errors()) && !empty($validator->errors()->toArray())){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $userInfo = User::find($request->user_id);
        $userInfo->name = $request->name;
        $userInfo->email = $request->email;
        $userInfo->password = bcrypt($request->password);
        $userInfo->save();

        Session::flash('success', GenericMessages::$all['userUpdatedSuccessfully']);

        return redirect()->route('adminuser');
    }

}
