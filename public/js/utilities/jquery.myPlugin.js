/*! Surver
* ================
*
* @Author  César Cordero
* @Support <>
* @Email   <test@test.test>
* @version 1.1.0
* @repository 
* @license MIT <http://opensource.org/licenses/MIT>
*/

//Make sure jQuery has been loaded
if(typeof jQuery === 'undefined'){
	throw new Error('Project requires jQuery');
}

/* RowCheckBox()
 * ======
 * Adds box widget functions to boxes.
 * Adds row check 
 *
 * @Usage: $('.row').rowCheckBox(options)
 *         This plugin auto activates on any element using the `tr` class
 *         Pass any option as data-option="value"
 */

//https://stackoverflow.com/questions/13341698/javascript-plus-sign-in-front-of-function-name
+function($){
	'use strict';

	var dataKey = 'srv.rowcheckbox';
	var Default = {};

	var selector = {
		data: '.trCheckbox',
		child: '.trCheckbox_child'

	}

	var RowCheckBox = function(element, options){
		this.element = element;
		this.options = options;

		this.amountChild = this.getAmountChildren();
		this._init();
	}

	RowCheckBox.prototype.getAmountChildren = function(checked){
		var that = this;
		return this.getChildren(checked).length;	
	};

	RowCheckBox.prototype.getChildren = function(checked){
		var filter = !checked ? 'input:checkbox' : 'input:checkbox:checked';
		return $(this.element).find(selector.child).find(filter);
	}

	//private
	RowCheckBox.prototype._init = function(){
		this._setUpListeners();
	}

	RowCheckBox.prototype._setUpListeners = function(){
		var that = this;
		this.getChildren().on('change',function(){
			that.checkParentCheck();
		});

		$(this.element).find("input:checkbox:first").on('change', function(){
			if(this.checked){
				that.getChildren().prop("checked",true);
			} else {
				that.getChildren().prop("checked",false);
			}
		})
	}

	//RowCheckBox.prototype.

	RowCheckBox.prototype.checkParentCheck = function(){
		var parentCheck = $(this.element).find("input:checkbox:first");
		var childWithoutCheck = this.getAmountChildren();
		var childCheck = this.getAmountChildren(true);
		//input[type=checkbox]
		if(childWithoutCheck == childCheck){
			parentCheck.prop("checked",true);
		} else {
			parentCheck.prop("checked",false);
		}
	}

	//Plugin Definition
	//======================
	function Plugin(option){
		return this.each(function(){
			var $this = $(this);
			var data = $this.data(dataKey)

			if(!data){
				var options = $.extend({}, Default, $this.data, typeof option == 'object' && option);
				$this.data(dataKey, (data = new RowCheckBox($this,options)));

			}

			if(typeof data == 'string'){
				if(typeof data[option] == 'undefined'){
					throw new Error('No method named' + option)
				}
				data[option]()
			}

		})
	}

	var old = $.fn.rowCheckBox

	$.fn.rowCheckBox = Plugin;
	$.fn.rowCheckBox.Constructor = RowCheckBox

	//No Conflict Mode
	//===========

	$.fn.rowCheckBox.noConflict = function(){
		$.fn.rowCheckBox = old
		return this
	}


	//RowCheckBox Data API
	//====================
	$(window).on('load', function(){
		$(selector.data).each(function () {
	      	Plugin.call($(this))
	    })
	})

}(jQuery);

+function($){
	'use strict';

	var dataKey = 'smart.manageModal';
	var Default = {
		onSubmit: function(e){}
	};

	var selector = {
		data:'.asModal'
	};

	var ManageModal = function(element, options){
		var that = this;

		this.element = element;
		this.options = options;
		this.instance = {};
		this._init();
	}

	ManageModal.prototype._init = function(){
		this._setUpListeners();
	};

	ManageModal.prototype._setUpListeners = function(){
		var that = this, modal = $('#' + that.options.view);
		//this.instance = $('#' + that.options.view).modal();

		$(this.element).on('click', function(){
			modal.modal('show');
			that.instance = modal;
		});

		this._opened(modal);
	};

	ManageModal.prototype._opened = function(modal){
		var that = this, formData = {};
		modal.on('shown.bs.modal', function(e){
			modal.find('form').submit(function( event ){
				event.preventDefault();
				formData = $(this).serializeArray();
				if (that.options.onSubmit !== undefined) {
			        that.options.onSubmit(formData);
			    }
			});
		});
		return this;
	};

	ManageModal.prototype.close = function(){
		$('#' + this.options.view).modal('hide');
	};
	//Plugin Definition
	//==================

	function Plugin(option){
		return this.each(function(){
			var $this = $(this);
			var data = $this.data(dataKey);

			if(!data){
				var options = $.extend({}, Default, $this.data, typeof option == 'object' && option)
				$this.data(dataKey, (data = new ManageModal($this, options)));
			}

			if(typeof data == 'string'){
				if(typeof data[option] == 'undefined'){
					throw new Error('No method named ' + option)
				}
				data[option]()
			}

		})
	}

	var old = $.fn.manageModal;

	$.fn.manageModal = Plugin;
	$.fn.manageModal.Constructor = ManageModal

	//No Conflict Mode
	//===============

	$.fn.manageModal.noConflict = function(){
		$.fn.manageModal = old;
		return this
	}

	//ManageModal Data API
	//====================
	$(window).on('load', function(){
		$(selector.data).each(function(){
			Plugin.call($(this))
		});
	});


}(jQuery);

// +function($){
//     'use strict';
//
//     var dataKey = 'smart.initCkeditor';
//     var Default = {
//         onSubmit: function(e){}
//     };
//
//     var selector = {
//         data:'.smCkEditor'
//     };
//
//     var InitCkEditor = function(element, options){
//         this.element = element;
//         this.options = options;
//
//         this._init();
//     }
//
//     InitCkEditor.prototype._init = function(){
//
//     	var element = $(this.element);
//     	var elementId = "";
//     	if(CKEDITOR){
//             elementId = element.attr('id');
//             if(elementId){
//                 CKEDITOR.replace(elementId , {
//                     customConfig : globalValues.baseUrl+'/js/modules/admin/ckeditor-config.js'
//                     ,LimeReplacementFieldsSID : ""
//                     ,LimeReplacementFieldsGID : ""
//                     ,LimeReplacementFieldsQID : ""
//                     ,LimeReplacementFieldsType : "survey-desc"
//                     ,LimeReplacementFieldsAction : "newsurvey"
//                     ,LimeReplacementFieldsPath : globalValues.baseUrl + "/admin/survey/custom/replacementfields"
//                     ,language:'en'
//                     ,filebrowserBrowseUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
//                     ,filebrowserImageBrowseUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
//                     ,filebrowserFlashBrowseUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
//                     ,filebrowserUploadUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
//                     ,filebrowserImageUploadUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
//                     ,filebrowserFlashUploadUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
//                     ,toolbarStartupExpanded:true
//                     ,toolbar:'inline'
// 				});
// 			}
// 		}
//     };
//
//
//     //Plugin Definition
//     //==================
//
//     function Plugin(option){
//         return this.each(function(){
//             var $this = $(this);
//             var data = $this.data(dataKey);
//
//             if(!data){
//                 var options = $.extend({}, Default, $this.data, typeof option == 'object' && option)
//                 $this.data(dataKey, (data = new InitCkEditor($this, options)));
//             }
//
//             if(typeof data == 'string'){
//                 if(typeof data[option] == 'undefined'){
//                     throw new Error('No method named ' + option)
//                 }
//                 data[option]()
//             }
//
//         })
//     }
//
//     var old = $.fn.initCkEditor;
//
//     $.fn.initCkEditor = Plugin;
//     $.fn.initCkEditor.Constructor = InitCkEditor
//
//     //No Conflict Mode
//     //===============
//
//     $.fn.initCkEditor.noConflict = function(){
//         $.fn.initCkEditor = old;
//         return this
//     }
//
//     //ManageModal Data API
//     //====================
//     $(window).on('load', function(){
//         $(selector.data).each(function(){
//             Plugin.call($(this))
//         });
//     });
//
//
// }(jQuery);

+function($, window, _){

	/*jshint validthis: true */
	"use strict";

	var namespace = ".us.jquery.dynamicRow";

	function init(){
		buildRows.call(this);
        actionBtnOverTable.call(this);
		sortableRow.call(this);
	}

	function buildRows(){
        var that = this,
			rows = this.getRow(),
			tbody = this.element.find("tbody"),
            rg = this.option.rg,
			rowkey = {
        		fieldName:this.fieldName
			};

        if(rows.length < 1){
        	if(this.rowKey){
                rowkey.sqId = 1 + '_' + this.rowKey;
			} else {
                rowkey.sqId = 1;

			}
            tbody.append(this.option.templates.sortaTable(rowkey));
        }

        if(rows.length > 0){
            rows.each(function(index, el){
                //var id = $(el).attr("id").split("_")[0];
				var id = $(el).attr("id");
				var match = rg.underscore.exec(id);
				if(match.length > 0){
                    id = id.split("_")[0];
				}
                that.rowsKeys.push(id);
            });
            that.indexMax = rows.length;
        }
	}

    function actionBtnOverTable(){
        var that = this,
			dataSqId = "",
            typeBtn,
            parrentTr,
        	tbody = this.element.find("tbody"),
        	rg = this.option.rg;

        this.rowsKeys.push(that.indexMax);
        tbody.on("click", ".addTr,.removeTr", function () {
            typeBtn = $(this).data("type-btn");
            parrentTr = $(this).parent().parent();

            if(typeBtn === "addTr"){
                that.indexMax = that.indexMax + 1;
                that.rowsKeys.push(that.indexMax);
                parrentTr.after(that.option.templates.sortaTable({sqId: that.indexMax + '_' + that.rowKey,fieldName:that.fieldName}));
            }

            if(typeBtn === "removeTr"){
                if(tbody.find("tr").length == 1){
                    that.element.trigger("atleastone" + namespace);
                	console.log("Al menos debe haber una subpregunta");
                    //commonModule.showPopupGeneric("error","Al menos debe haber una subpregunta");
                    // alert("Al menos debe haber una subpregunta");
                    return;
                }
                dataSqId = parrentTr.attr("id");
                if(rg.underscore.exec(dataSqId).length > 0){
                    dataSqId = dataSqId.split("_")[0];
				}

                that.sqsAll = that.rmElementInArray(that.rowsKeys, dataSqId);
                parrentTr.closest('tr').remove();
                reorderValueTr.call(that, dataSqId);
            }
        });
    }

    function reorderValueTr(fromsqId) {
        var that = this,
			highsElement = [],
            lesserElement = [],
            highsElementCopy = [];

        _.each(this.rowsKeys, function (el) {
            (fromsqId < el) ? highsElement.push(el) : lesserElement.push(el);
        });

        highsElementCopy = highsElement;
        highsElement = _.map(highsElement, function (el) {
            return el - 1;
        });
        this.rowsKeys = _.union(highsElement, lesserElement);
        this.indexMax = _.max(this.rowsKeys);

        highsElementCopy = _.map(highsElementCopy, function (el) {
        	if(that.rowKey){
                return "#" + el + "_" + that.rowKey;
			} else {
                return "#" + el;
			}
        });
        highsElementCopy = highsElementCopy.join(",");
        this.element.find(highsElementCopy).each(function (i, el) {
            if(that.rowKey){
                $(el).attr("id", highsElement[i] + "_" + that.rowKey);
                $(el).children('td').eq(1).html(highsElement[i] + "_" + that.rowKey);
			} else {
                $(el).attr("id", highsElement[i]);
                $(el).children('td').eq(1).html(highsElement[i]);
            }
        });
    }

	function sortableRow(){
        this.element.find("tbody").sortable();
	}

	//DynamicRow PUBLIC CLASS DEFINITION
	//================

    /**
	 * Represents the jQuery dynamicRow plugin
	 *
	 * @class Grid
	 * @constructor
	 * @param element {Object} the corresponding DOM element.
	 * @param options {Object} the options to override default settings
	 * @chainable
     */

    var DynamicRow = function(element, options)
	{
		this.element = $(element);
		this.origin = this.element.clone();
		this.option = $.extend(true, {}, DynamicRow.defaults, this.element.data() , options );
		this.row = [];
		this.rowsKeys = [];
        this.indexMax = 1;
        this.rowKey = this.option.keyRow;
        this.fieldName = this.option.fieldName;
	};

    DynamicRow.prototype.getRow = function(){
        var tbody = this.element.find("tbody"),
            rows = tbody.find("tr");
		return rows;
	};

    DynamicRow.prototype.rmElementInArray = function(array, element){
        array = $.grep(array, function(value) {
            return value != element;
        });
        return array;
	};

    DynamicRow.defaults = {
        rowKey:"",
        fieldName:"",
    	rg:{
    		underscore: /_/
		},
    	templates:{
            sortaTable: _.template('<tr id="<%= sqId %>">' +
                '<td>' +
                '<span class="cursorMove">' +
                '<i class="fa fa-bars fa-2x cursorMove" aria-hidden="true"></i>' +
                '</span>' +
                '</td>' +
                '<td><%= sqId %></td>' +
                '<td>' +
                '<label class="input fullWidth">' +
                '<input type="text" class="input-sm fullWidth" name="<%= fieldName %>[<%= sqId %>]">' +
                '</label>' +
                '</td>' +
                '<td>' +
                '<a class="btn btn-info btn-xs addTr"' +
                'data-toggle="tooltip"' +
                'data-type-btn="addTr"' +
                'data-placement="top"' +
                'title="Agregar subpreguntas">' +
                '<i class="fa fa-fw fa-plus"></i>' +
                '</a>' +

                '<a class="btn btn-danger btn-xs removeTr" style="margin-left: 4px;"' +
                'data-toggle="tooltip"' +
                'data-placement="top"' +
                'data-type-btn="removeTr"' +
                'title="Eliminar subpreguntas">' +
                '<i class="fa fa-trash-o"></i>' +
                '</a>' +
                '</td>' +
                '</tr>')
        }
	};

	//DynamicRow PLUGIN DEFINITION
	//======================

	var old = $.fn.dynamicRow;

	$.fn.dynamicRow = function(option)
	{
		var args = Array.prototype.slice.call(arguments,1),
			returnValue = null,
			elements = this.each(function(index)
			{
				var $this = $(this),
					instance = $this.data(namespace),
					options = typeof option == "object" && option;

				if(!instance && option == "destroy")
				{
					return;
				}
				if(!instance)
				{
					$this.data(namespace, (instance = new DynamicRow(this, options)));
					init.call(instance);
				}

				if(typeof option === "string")
				{
					if(option.indexOf("get") === 0 && index === 0)
					{
						returnValue = instance[option].apply(instance, args);
					}
					else if(option.index("get") !== 0)
					{
						return instance[option].apply(instance, args);
					}
				}
			});

		return (typeof option == "string" && option.indexOf("get") == 0) ? returnValue : elements;
	};

	$.fn.dynamicRow.Constructor = DynamicRow;

	//DynamicRow NO CONFLICT
	//===============

	$.fn.dynamicRow.noConflict = function()
	{
		$.fn.dynamicRow = old;
		return this;
	};

	//DynamicRow DATA-API
	//==============

$("[data-toogle=\"dynamicRow\"]").dynamicRow();
}(jQuery,window, _);

+function($, window, _){
	/*jshint validthis: true */
    "use strict";
    var namespace = ".us.jquery.initcKeditor";

    function init(){
    	// debugger;
        CKEDITOR.replace(this.element.attr("id"), this.option);
    }

    /**
     * Represents the jQuery InitcKeditor plugin
     *
     * @class InitcKeditor
     * @constructor
     * @param element {Object} the corresponding DOM element.
     * @param options {Object} the options to override default settings
     * @chainable
     */

    var InitcKeditor = function(element, options)
    {
        this.element = $(element);
        this.origin = this.element.clone();
        this.option = $.extend(true, {}, InitcKeditor.defaults, this.element.data(), options);
    };

    InitcKeditor.defaults = {
        customConfig : globalValues.baseUrl+'/js/modules/admin/ckeditor-config.js'
        ,LimeReplacementFieldsSID : ""
        ,LimeReplacementFieldsGID : ""
        ,LimeReplacementFieldsQID : ""
        ,LimeReplacementFieldsType : "survey-desc"
        ,LimeReplacementFieldsAction : "newsurvey"
        ,LimeReplacementFieldsPath : globalValues.baseUrl + "/admin/survey/custom/replacementfields"
        ,language:'en'
        ,filebrowserBrowseUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
        ,filebrowserImageBrowseUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
        ,filebrowserFlashBrowseUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
        ,filebrowserUploadUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
        ,filebrowserImageUploadUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
        ,filebrowserFlashUploadUrl:'/monografia/limesurvey/index.php/admin/survey/sa/fakebrowser'
        ,toolbarStartupExpanded:true
        ,toolbar:'inline'
    };


    var old = $.fn.initcKeditor;

    $.fn.initcKeditor = function(option)
    {
        var args = Array.prototype.slice.call(arguments,1),
            returnValue = null,
            elements = this.each(function(index)
            {
                var $this = $(this),
                    instance = $this.data(namespace),
                    options = typeof option == "object" && option;

                if(!instance && option == "destroy")
                {
                    return;
                }
                if(!instance)
                {
                    $this.data(namespace, (instance = new InitcKeditor(this, options)));
                    init.call(instance);
                }

                if(typeof option === "string")
                {
                    if(option.indexOf("get") === 0 && index === 0)
                    {
                        returnValue = instance[option].apply(instance, args);
                    }
                    else if(option.index("get") !== 0)
                    {
                        return instance[option].apply(instance, args);
                    }
                }
            });

        return (typeof option == "string" && option.indexOf("get") == 0) ? returnValue : elements;
    };

    $.fn.initcKeditor.Constructor = InitcKeditor;

    $.fn.initcKeditor.noConflict = function()
    {
        $.fn.initcKeditor = old;
        return this;
    };
    $("[data-toogle=\"initcKeditor\"]")
}(jQuery, window, _);
