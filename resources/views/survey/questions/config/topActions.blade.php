<div class="row">
    <div class="col-md-12 col-xs-12">
        <ul class="demo-btns">
            <li>
                <a href="{{ route('editQuestion',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id,'questionId'=>$viewData['question']->id])}}"
                   class="btn btn-primary">
                    <i class="fa fa-pencil"></i>
                    Editar pregunta
                </a>
            </li>
            <li>
                <a href="{{ route('survey.redirectpreviewmode',[$viewData['survey']->id])}}"
                   class="btn bg-color-orange txt-color-white" target="_blank">
                    <i class="fa  fa-eye"></i>
                    Previsualizar encuesta
                </a>
            </li>

            <li>
                <a href="{{ route('groupviewpreviewpage',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id])}}"
                   target="_blank"
                   class="btn bg-color-teal txt-color-white">
                    <i class="fa  fa-eye"></i>
                    Previsualizar grupo
                </a>
            </li>

            <li>
                <a href="{{ route('questionviewpreviewpage',['surveyId'=>$viewData['survey']->id,'groupId'=>$viewData['group']->id,'questionId'=>$viewData['question']->id])}}" target="_blank"
                   class="btn bg-color-blueDark txt-color-white">
                    <i class="fa  fa-eye"></i>
                    Previsualizar pregunta
                </a>
            </li>
        </ul>
    </div>
</div>