<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/serving/invitation/survey',
    'AdminSurveys\CronJobController@servinginvitation')
        ->name("serving.invitation.survey");

//Route::match(['GET','POST'],'/survey/{surveyId}',
//    'PublicSurveys\SurveyController@index')
//    ->name("public.index");
