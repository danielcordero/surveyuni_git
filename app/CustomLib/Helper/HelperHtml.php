<?php

namespace App\CustomLib\Helper;

class HelperHtml
{

    public static function getEditor($fieldtype,$fieldname,$fieldtext, $surveyID=null,$gID=null,$qID=null,$action=null)
    {

        return self::getInlineEditor($fieldtype,$fieldname,$fieldtext, $surveyID,$gID,$qID,$action);

//        $session = &Yii::app()->session;
//
//        if ($session['htmleditormode'] &&
//            $session['htmleditormode'] == 'none')
//        {
//            return '';
//        }
//
//
//        if (!$session['htmleditormode'] ||
//            ($session['htmleditormode'] != 'inline' &&
//                $session['htmleditormode'] != 'popup') )
//        {
//            $htmleditormode = Yii::app()->getConfig('defaulthtmleditormode');
//        }
//        else
//        {
//            $htmleditormode = $session['htmleditormode'];
//        }
//        if ( $surveyID && getEmailFormat($surveyID) != 'html' && substr($fieldtype,0,6)==="email-" )// email but survey as text email
//        {
//            return '';
//        }
//
//        if ($htmleditormode == 'popup' ||
//            ( $fieldtype == 'editanswer' ||
//                $fieldtype == 'addanswer' ||
//                $fieldtype == 'editlabel' ||
//                $fieldtype == 'addlabel') && (preg_match("/^translate/",$action) == 0 ) )
//        {
//            return getPopupEditor($fieldtype,$fieldname,$fieldtext, $surveyID,$gID,$qID,$action);
//        }
//        elseif ($htmleditormode == 'inline')
//        {
//            return getInlineEditor($fieldtype,$fieldname,$fieldtext, $surveyID,$gID,$qID,$action);
//        }
//        else
//        {
//            return '';
//        }
    }

    public static function getInlineEditor($fieldtype,$fieldname,$fieldtext, $surveyID=null,$gID=null,$qID=null,$action=null)
    {

        $htmlcode = '';
        $imgopts = '';
        $toolbarname = 'inline';
        $toolbaroption="";
        $sFileBrowserAvailable='';
        $htmlformatoption="";
        $oCKeditorVarName = "oCKeditor_".str_replace("-","_",$fieldname);

        if ( ($fieldtype == 'editanswer' ||
                $fieldtype == 'addanswer' ||
                $fieldtype == 'editlabel' ||
                $fieldtype == 'addlabel') && (preg_match("/^translate/",$action) == 0) )
        {
            $toolbaroption= ",toolbarStartupExpanded:true\n"
                .",toolbar:'popup'\n"
                .",toolbarCanCollapse:false\n";
        }
        else
        {
//            $ckeditexpandtoolbar = Yii::app()->getConfig('ckeditexpandtoolbar');
//            if (!isset($ckeditexpandtoolbar) ||  $ckeditexpandtoolbar == true)
//            {
//                $toolbaroption = ",toolbarStartupExpanded:true\n"
//                    .",toolbar:'inline'\n";
//            }
        }

        if ( $fieldtype == 'email-inv' ||
            $fieldtype == 'email-reg' ||
            $fieldtype == 'email-conf'||
            $fieldtype == 'email-admin-notification'||
            $fieldtype == 'email-admin-resp'||
            $fieldtype == 'email-rem' )
        {
            $htmlformatoption = ",fullPage:true\n";
        }

//        if ($surveyID=='')
//        {
//            $sFakeBrowserURL=Yii::app()->getController()->createUrl('admin/survey/sa/fakebrowser');
//            $sFileBrowserAvailable=",filebrowserBrowseUrl:'{$sFakeBrowserURL}'
//            ,filebrowserImageBrowseUrl:'{$sFakeBrowserURL}'
//            ,filebrowserFlashBrowseUrl:'{$sFakeBrowserURL}'
//            ,filebrowserUploadUrl:'{$sFakeBrowserURL}'
//            ,filebrowserImageUploadUrl:'{$sFakeBrowserURL}'
//            ,filebrowserFlashUploadUrl:'{$sFakeBrowserURL}'";
//        }

        $htmlcode .= ""
            . "<script type=\"text/javascript\">debugger;\n"
            . "$(document).ready(
        function(){ var $oCKeditorVarName = CKEDITOR.replace('$fieldname', {
        
        LimeReplacementFieldsType : \"".$fieldtype."\"
        ,LimeReplacementFieldsSID : \"".$surveyID."\"
        ,LimeReplacementFieldsGID : \"".$gID."\"
        ,LimeReplacementFieldsQID : \"".$qID."\"
        ,LimeReplacementFieldsType : \"".$fieldtype."\"
        ,LimeReplacementFieldsAction : \"".$action."\"
        ,LimeReplacementFieldsPath : \"".''."\"
        ,language:'"."'"
            . $sFileBrowserAvailable
            . $htmlformatoption
            . $toolbaroption
            ."});

CKEDITOR.editorConfig = function( config )
{
    config.uiColor = '#FFF';
};

        \$('#$fieldname').parents('ul:eq(0)').addClass('editor-parent');
        });";


        $htmlcode.= '</script>';

        return $htmlcode;
    }
}