<?php
$allQuestions = "";
//    echo "<pre>";
//    print_r($resultQuestion);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- ranking -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>
                <!-- widget content -->
                <div class="widget-body" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <section>
                        <select multiple="multiple"
                                size="10"
                                name="question_<?php echo $question->id; ?>[answers][]"
                                id="<?php echo $question->id."_initializeDuallistbox"; ?>">
                            <?php
                            foreach ($config["subQuestions"] as $key => $value){
                            $allQuestions = $allQuestions . '|' .$key;
                            ?>
                                <option value="<?php echo $key; ?>"
                                    <?php
                                        if(!empty($resultQuestion['keepAnswered']) &&
                                            in_array($key, $resultQuestion['keepAnswered'])){
                                            echo "selected";
                                        }
                                        ?>>
                                    <?php echo $value; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </section>

                </div>
                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[subQuestions]"
                       id="question_<?php echo $question->id; ?>[subQuestions]"
                       value="<?php echo trim($allQuestions, '|'); ?>">
            @include('survey.public.questions.littlepartial.help')
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->
            </div>
            <!-- end widget div -->
        </div>
    </article>
</div>


@push('scripts')

<script>
    var initializeDuallistbox = $('#<?php echo $question->id."_initializeDuallistbox"; ?>').bootstrapDualListbox({
        nonSelectedListLabel: 'No seleccionados',
        selectedListLabel: 'Seleccionados',
        preserveSelectionOnMove: 'moved',
        infoText:"Mostrando todo {0}",
        infoTextEmpty:"Lista vacía",
        moveOnSelect: false,
        showFilterInputs: false,
        helperSelectNamePostfix: false
    });

</script>
@endpush