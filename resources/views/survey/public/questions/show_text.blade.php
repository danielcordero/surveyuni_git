<div class="row">
    <!-- NEW WIDGET START -->
    <!-- show_text -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget edit box -->
                <div class="jarviswidget-editbox">
                    <!-- This area used as dropdown edit box -->
                    <input class="form-control" type="text">
                    <span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>

                </div>
                <!-- end widget edit box -->

                <!-- widget content -->
                <div class="widget-body">



                </div>
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>