<aside id="left-panel">
   <div class="login-info">
      <span>
         <!--<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">-->
         <a href="#">
         <img src="https://www.mastermagazine.info/termino/wp-content/uploads/Usuario-Icono.jpg"
              alt="me" class="online" />
         <span>
             Hola
             <?php
               echo Auth::user()->name;
             ?>
         </span>
         <!--<i class="fa fa-angle-down"></i>-->
         </a> 
      </span>
   </div>
   <nav>
      <ul>
          <?php
           $hasPermission = Auth::user()->checkHasPermission(['ConfigApp_viewconfigglobal']);
           if($hasPermission){
               ?>
             <li>
                <a href="#">
                   <i class="fa fa-gears"></i>
                   <span class="menu-item-parent">
                  Configuración
               </span>
                </a>
                <ul>
                   <li>
                      <a href="<?php echo e(route('admin.viewconfigglobal')); ?>">
                         <i class="fa fa-gear"></i>
                         Configuración
                      </a>
                   </li>
                </ul>
             </li>
         <?php
           }
          ?>
         <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-pencil-square-o">
               </i> <span class="menu-item-parent">
                  Encuesta
               </span>
            </a>
            <ul>
               <li>
                  <a href="<?php echo e(route('survey.index')); ?>">
                     <span class="glyphicon glyphicon-th-list"></span>
                     Lista
                  </a>
               </li>
               <li>
                  <a href="<?php echo e(route('survey.create')); ?>">
                     <span class="glyphicon glyphicon-list-alt"></span>
                     Crear
                  </a>
               </li>
            </ul>
         </li>
           <?php
           $hasPermission = Auth::user()->checkHasPermission(['Home_adminuser']);
           if($hasPermission){
           ?>
          <li>
             <a href="#"><i class="fa fa-lg fa-fw fa-users">
                </i> <span class="menu-item-parent">
                     Usuarios
                  </span>
             </a>
             <ul>
                <li>
                   <a href="<?php echo e(route('adminuser')); ?>">
                      <i class="fa fa-user"></i>Control de usuarios
                   </a>
                </li>
             </ul>
          </li>
           <?php
           }
           ?>
      </ul>
   </nav>
   <span class="minifyme" data-action="minifyMenu"> 
   <i class="fa fa-arrow-circle-left hit"></i> 
   </span>
</aside>