<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 2/6/2018
 * Time: 6:19 PM
 */

namespace App\Http\Controllers\AdminSurveys;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\Services\{ConfigAppService};
use App\CustomLib\GenericMessages;
use App\CustomLib\Helper\{HelperServices,Mysqldump};
use Illuminate\Support\Facades\Session;
use App\{SettingsGlobal};

class ConfigAppController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckPermission');
    }

    public function viewconfigglobal(Request $request)
    {
        if($request->isMethod('post')){

            require_once(app_path().'/ThirdParty/phpmailer/PHPMailerAutoload.php');
            $credentialSMTP = [];
            $credentialSMTP['Username'] = SettingsGlobal::find("siteadminemail")->stg_value;
            $credentialSMTP['Password'] = SettingsGlobal::find("emailsmtppassword")->stg_value;
            $file = app_path() . DIRECTORY_SEPARATOR . 'backup' . DIRECTORY_SEPARATOR . date('d-m-Y-H-i-s').'dump.sql';
            $response = new \stdClass();
            $response->failExport = false;
            $response->failMail = false;
            $response->failExportMessage = "";
            $response->failMailMessage = "";

            try {

                $dump = new Mysqldump('mysql:host=localhost;dbname=' . env("DB_DATABASE", "somedefaultvalue"), env("DB_USERNAME", "somedefaultvalue"), env("DB_PASSWORD", "somedefaultvalue"));
                $dump->start($file);

                if(!empty($_POST['saveitonemail']) && $_POST['saveitonemail'] == "on"){

                    $mail = new \PHPMailer(); // create a new object
                    $mail->IsSMTP(); // enable SMTP
                    $mail->CharSet = 'UTF-8';
                    $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
                    $mail->SMTPAuth = true; // authentication enabled
                    //$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
                    $mail->Host = "smtp.gmail.com";
                    $mail->SMTPSecure = "ssl";
                    //$mail->Port = 587; // or 587
                    $mail->Port = "465";
                    $mail->IsHTML(true);
                    $mail->Username = $credentialSMTP['Username'];
                    $mail->Password = $credentialSMTP['Password'];
                    $mail->SetFrom($credentialSMTP['Username']);

                    $mail->addAddress($credentialSMTP['Username']);
                    $mail->Subject = "Backup de la database encuesta uni";
                    $mail->Body = "Backup de la database encuesta uni";
                    $mail->AddAttachment( $file );

                    if(!$mail->Send()){
                        $response->failMail = true;
                        $response->failMailtMessage = "La base de datos no se pudo enviar al correo";
                        //echo "Mailer Error: " . $mail->ErrorInfo;
                    }

//            if (!unlink($file))
//            {
//                echo ("Error deleting $file");
//            }
//            else
//            {
//                echo ("Deleted $file");
//            }
//
//            exit;

                }

            } catch (\Exception $e) {
                $response->failExport = true;
                $response->failMailMessage = "No se pudo realizar el backup, revise la configuracion de la base de datos" . " "  ."'".$e->getMessage()."'";
            }

            //if(!$response->failExport && !$response->failMail){
            if(!$response->failExport){
//            echo "<pre>";
//            print_r( $response );
//            exit;
                Session::flash("success", GenericMessages::$all['successOperation']);
            }

//        echo "<pre>";
//        print_r( $response );
//        exit;

            $viewData['response'] = $response;
            return view('config.viewconfigglobal')->with('viewData',$viewData);
        }

        if($request->isMethod('get')){

            return view('config.viewconfigglobal');
        }
    }

    public function generateBackupdatabase()
    {
        require_once(app_path().'/ThirdParty/phpmailer/PHPMailerAutoload.php');
        $credentialSMTP = [];
        $credentialSMTP['Username'] = SettingsGlobal::find("siteadminemail")->stg_value;
        $credentialSMTP['Password'] = SettingsGlobal::find("emailsmtppassword")->stg_value;
        $file = app_path() . DIRECTORY_SEPARATOR . 'backup' . DIRECTORY_SEPARATOR . date('d-m-Y-H-i-s').'dump.sql';
        $response = new \stdClass();
        $response->failExport = false;
        $response->failMail = false;
        $response->failExportMessage = "";
        $response->failMailMessage = "";

        try {

            $dump = new Mysqldump('mysql:host=localhost;dbname=surveyun', 'root', '');
            $dump->start($file);

            if(!empty($_POST['saveitonemail']) && $_POST['saveitonemail'] == "on"){

                $mail = new \PHPMailer(); // create a new object
                $mail->IsSMTP(); // enable SMTP
                $mail->CharSet = 'UTF-8';
                $mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
                $mail->SMTPAuth = true; // authentication enabled
                //$mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
                $mail->Host = "smtp.gmail.com";
                $mail->SMTPSecure = "ssl";
                //$mail->Port = 587; // or 587
                $mail->Port = "465";
                $mail->IsHTML(true);
                $mail->Username = $credentialSMTP['Username'];
                $mail->Password = $credentialSMTP['Password'];
                $mail->SetFrom($credentialSMTP['Username']);

                $mail->addAddress($credentialSMTP['Username']);
                $mail->Subject = "Backup de la database encuesta uni";
                $mail->Body = "Backup de la database encuesta uni";
                $mail->AddAttachment( $file );

                if(!$mail->Send()){
                    $response->failMail = true;
                    $response->failMailtMessage = "La base de datos no se pudo enviar al correo";
                    //echo "Mailer Error: " . $mail->ErrorInfo;
                }

//            if (!unlink($file))
//            {
//                echo ("Error deleting $file");
//            }
//            else
//            {
//                echo ("Deleted $file");
//            }
//
//            exit;

            }

        } catch (\Exception $e) {
            $response->failExport = true;
            $response->failMailMessage = "No se pudo realizar el backup, revise la configuracion de la base de datos" . " "  ."'".$e->getMessage()."'";
        }

        //if(!$response->failExport && !$response->failMail){
        if(!$response->failExport){
//            echo "<pre>";
//            print_r( $response );
//            exit;
            Session::flash("success", GenericMessages::$all['successOperation']);
        }

//        echo "<pre>";
//        print_r( $response );
//        exit;

        $viewData['response'] = $response;

        //return redirect()->route('admin.viewconfigglobal')->with('viewData',$viewData);

        return \Redirect::route('admin.viewconfigglobal')->with('viewData',$viewData);
        //return \Redirect::route('admin.viewconfigglobal')->with('viewData',$viewData);

    }

    public function saveconfigglobal(Request $request)
    {

        ConfigAppService::setGlobalSetting("siteadminemail",strip_tags($request->siteadminemail));
        ConfigAppService::setGlobalSetting("siteadminname",strip_tags($request->siteadminname));
        ConfigAppService::setGlobalSetting("emailmethod",strip_tags($request->emailmethod));
        ConfigAppService::setGlobalSetting("emailsmtphost",strip_tags($request->emailsmtphost));
        ConfigAppService::setGlobalSetting("emailsmtpuser",strip_tags($request->emailsmtpuser));
        ConfigAppService::setGlobalSetting("emailsmtppassword",strip_tags($request->emailsmtppassword));
        ConfigAppService::setGlobalSetting("emailsmtpssl",$request->emailsmtpssl);
        ConfigAppService::setGlobalSetting("emailsmtpdebug",HelperServices::sanitize_paranoid_string($request->emailsmtpdebug));
        ConfigAppService::setGlobalSetting("maxemails", HelperServices::sanitize_int($request->maxemails));
        Session::flash("success", GenericMessages::$all['successOperation']);
        return redirect()->route('admin.viewconfigglobal');
    }

}