<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 2/6/2018
 * Time: 6:18 PM
 */


// routes/web.php
Route::group(['prefix' => 'admin'], function () {

    Route::match(['GET','POST'],'/viewconfigglobal',
        'AdminSurveys\ConfigAppController@viewconfigglobal')
        ->name("admin.viewconfigglobal");

//    Route::get('/viewconfigglobal',
//        'AdminSurveys\ConfigAppController@viewconfigglobal')
//        ->name("admin.viewconfigglobal");

    Route::post('/saveconfigglobal',
        'AdminSurveys\ConfigAppController@saveconfigglobal')
        ->name("admin.saveconfigglobal");

//    Route::post('/backupdatabase',
//        'AdminSurveys\ConfigAppController@generateBackupdatabase')
//        ->name("admin.backupdatabase");

});