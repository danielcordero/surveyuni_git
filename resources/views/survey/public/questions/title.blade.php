<?php
//    echo "<pre>";
//    print_r($resultQuestion['keepAnswered']);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- title -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!--<header class="headerWidget">
                <h2>
                    @if($question->mandatory == "Y")
                        <small class="text-danger fa fa-asterisk small redAsterisk"
                               aria-hidden="true">
                        </small>
                    @endif
                    <strong>
                        <?php
                        echo strip_tags($question->title);
                        ?>
                        <?php
                        $questionsType = config('custom.questionsType');
                        echo $questionsType[$question->type];
                        ?>
                    </strong>
                </h2>
            </header>-->

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <section>
                        <fieldset>
                            <div class="form-group">
                                <div class="col-md-12 col-xs-12">
                                    <br>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default <?php
                                        if(!empty($resultQuestion['keepAnswered'])){
                                            if($resultQuestion['keepAnswered'] == 'male'){
                                                echo "active";
                                            }
                                        }
                                        ?> btn-lg">
                                            <input type="radio"
                                                   name="question_<?php echo $question->id; ?>[answers]"
                                                   value="male"
                                                   data-bv-field="gender" <?php
                                                if(!empty($resultQuestion['keepAnswered'])){
                                                    if($resultQuestion['keepAnswered'] == 'male'){
                                                        echo "checked";
                                                    }
                                                }
                                                ?>>
                                            <span class="fa fa-mars displayBlock"></span>
                                            <strong>
                                                Male
                                            </strong>
                                             </label>
                                        <label class="btn btn-default <?php
                                        if(!empty($resultQuestion['keepAnswered'])){
                                            if($resultQuestion['keepAnswered'] == 'female'){
                                                echo "active";
                                            }
                                        }
                                        ?> btn-lg">
                                            <input type="radio"
                                                   name="question_<?php echo $question->id; ?>[answers]"
                                                   value="female"
                                                   data-bv-field="gender" <?php
                                                if(!empty($resultQuestion['keepAnswered'])){
                                                    if($resultQuestion['keepAnswered'] == 'female'){
                                                        echo "checked";
                                                    }
                                                }
                                                ?>>
                                            <span class="fa fa-venus displayBlock"></span>
                                            <strong>
                                                Female
                                            </strong>
                                        </label>
                                        <?php
                                        if(!empty($question->mandatory) && $question->mandatory == "N"){
                                        ?>
                                        <label class="btn btn-default <?php
                                        if(!empty($resultQuestion)){
                                            if(!empty($resultQuestion['keepAnswered'])){
                                                if($resultQuestion['keepAnswered'] == 'notanswer'){
                                                    echo "active";
                                                }
                                            }
                                        } else {
                                            echo "active";
                                        }
                                        ?> btn-lg">
                                            <input type="radio"
                                                   name="question_<?php echo $question->id; ?>[answers]"
                                                   value="notanswer"
                                                   data-bv-field="gender" <?php
                                                if(!empty($resultQuestion)){
                                                    if(!empty($resultQuestion['keepAnswered'])){
                                                        if($resultQuestion['keepAnswered'] == 'notanswer'){
                                                            echo "checked";
                                                        }
                                                    }
                                                } else {
                                                    echo "checked";
                                                }
                                                ?>>
                                            <span class="fa fa-genderless displayBlock"></span>
                                            <strong> Sin respuesta </strong>
                                        </label>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </section>
                </div>
            @include('survey.public.questions.littlepartial.help')
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->
            </div>
            <!-- end widget div -->
        </div>
    </article>
</div>