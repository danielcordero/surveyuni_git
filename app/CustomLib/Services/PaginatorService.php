<?php

namespace App\CustomLib\Services;

class PaginatorService{

    private $limitL;
    private $limitH;
    private $currentPage;
    private $rows;

    function __construct(){
        $this->rows = 10;
        $this->currentPage = 1;
        $this->doCalculatePerPage();
    }

    public function doCalculatePerPage(){
        $this->limitL = ($this->currentPage * $this->rows) - ($this->rows);
        $this->limitH = ($this->rows);
    }

    public function getAmountRows(){
        return $this->rows;
    }

    public function getCurrentPage(){
        return $this->currentPage;
    }

    public function setAmountRows($rows){
        if(!empty($rows)){
            $this->rows = $rows;
        }
    }

    public function setCurrentPage($currentPage){
        if(!empty($currentPage)){
            $this->currentPage = $currentPage;
            $this->doCalculatePerPage();
        }
    }

    public function getPerPage(){
        $perPage = array();
        $perPage['limitL'] = $this->limitL;
        $perPage['limitH'] = $this->limitH;
        return $perPage;
    }
}?>