<?php

namespace App\CustomLib\Helper;
use Illuminate\Support\Facades\DB;

class HelperServices{

    public function __construct()
    {
//        include(app_path() . '\CustomLib\Helper\adodb-time.inc_helper.php');
//        echo "Hiiiiiiii";
//        exit;
    }

    public static function conditionalNewlineToBreak($mytext,$ishtml,$encoded='')
    {
        if ($ishtml === true)
        {
            // $mytext has been processed by gT with html mode
            // and thus \n has already been translated to &#10;
            if ($encoded == '')
            {
                $mytext=str_replace('&#10;', '<br />',$mytext);
            }
            return str_replace("\n", '<br />',$mytext);
        }
        else
        {
            return $mytext;
        }
    }

    public static function sanitize_int($integer, $min = '', $max = '')
    {
        $int = preg_replace("#[^0-9]#","",$integer);
        if((($min != '') && ($int < $min)) || (($max != '') && ($int > $max))){
            return FALSE;
        }
        if($int == ''){
            return null;
        }
        return $int;
    }

    public static function fixCKeditorText($str)
	{
	    $str = str_replace('<br type="_moz" />','',$str);
	    if ($str == "<br />" || $str == " " || $str == "&nbsp;"){
	        $str = "";
	    }
	    if (preg_match("/^[\s]+$/",$str)){
	        $str='';
	    }

	    if ($str == "\n"){
	        $str = "";
	    }

	    if (trim($str) == "&nbsp;" || trim($str)==''){ 
	    // chrome adds a single &nbsp; element to empty fckeditor fields
	        $str = "";
	    }
	    return $str;
	}

	public static function sanitize_xss_string($string)
    {
        if (isset($string))
        {
            $bad = array ('*','^','&',';','\"','(',')','%','$','?');
            return str_replace($bad, '',$string);
        }
    }

    /**
     * https://github.com/mavinoo/updateBatch
     * Update Multi fields
     * $table String
     * $value Array
     * $index String
     *
     * Example
     *
     * $table = 'users';
     * $value = [
     *      [
     *          'id' => 1,
     *          'status' => 'active',
     *          'nickname' => 'Mohammad'
     *      ] ,
     *      [
     *          'id' => 5,
     *          'status' => 'deactive',
     *          'nickname' => 'Ghanbari'
     *      ] ,
     * ];
     *
     * $index = 'id';
     *
     */
	public static function updateBulk($table, $values, $index){
        $final  = array();
        $ids    = array();
        if(!count($values))
            return false;
        if(!isset($index) AND empty($index))
            return 'Select Key for Update';
        foreach ($values as $key => $val)
        {
            $ids[] = $val[$index];
            foreach (array_keys($val) as $field)
            {
                if ($field !== $index)
                {
                    $final[$field][] = 'WHEN `'. $index .'` = "' . $val[$index] . '" THEN "' . $val[$field] . '" ';
                }
            }
        }
        $cases = '';
        foreach ($final as $k => $v)
        {
            $cases .= $k.' = (CASE '. implode("\n", $v) . "\n"
                . 'ELSE '.$k.' END), ';
        }

        $query = 'UPDATE ' . $table . ' SET '. substr($cases, 0, -2) . ' WHERE ' . $index . ' IN('.implode(',', $ids).')';
        return DB::statement($query);
    }

    /**
     * This function removes the UTF-8 Byte Order Mark from a string
     *
     * @param string $str
     * @return string
     */
    public static function removeBOM($str = "")
    {
        if(substr($str, 0 ,3 ) == pack("CCC",0xef,0xbb,0xbf)){
            $str = substr($str, 3);
        }
        return $str;
    }

    /**
     * Get definition of a table on database
     *
     * @param string $str
     * @return array
     */
    public static function getTableColumns($tableName)
    {
        $defintion = DB::getSchemaBuilder()->getColumnListing($tableName);
        if(empty($defintion)){
            $defintion = Schema::getColumnListing($tableName);
        }
        return $defintion;
    }

    public static function checkFormatDateYMD($date){
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            return true;
        } else {
            return false;
        }
    }

    public static function sanitize_paranoid_string($string, $min='', $max='')
    {
        if (isset($string))
        {
            $string = preg_replace("/[^_.a-zA-Z0-9]/", "", $string);
            $len = strlen($string);
            if((($min != '') && ($len < $min)) || (($max != '') && ($len > $max)))
                return FALSE;
            return $string;
        }
    }

    public static function qs_url($path = null, $qs = array(), $secure = null)
    {
        $url = app('url')->to($path, $secure);
        if(count($qs)){
            foreach ($qs as $key => $value){
                $qs[$key] = sprintf('%s=%s', $key, urlencode($value));
            }
            $url = sprintf('%s?%s', $url, implode("&",$qs) );
        }
        return $url;
    }

    public static function addQStoRoute($route, $parameters, $queryS)
    {
        return route($route,$parameters) . '?' . http_build_query($queryS);
    }

    /**
     * Try to convert a string to UTF-8.
     *
     * @author Thomas Scholz <http://toscho.de>
     * @param string $str String to encode
     * @param string $inputEnc Maybe the source encoding.
     *               Set to NULL if you are not sure. iconv() will fail then.
     * @return string
     */
    public static function force_utf8( $str, $inputEnc='WINDOWS-1252' )
    {
        if ( self::is_utf8( $str ) ) // Nothing to do.
            return $str;

        if ( strtoupper( $inputEnc ) === 'ISO-8859-1' )
            return utf8_encode( $str );

        if ( function_exists( 'mb_convert_encoding' ) )
            return mb_convert_encoding( $str, 'UTF-8', $inputEnc );

        if ( function_exists( 'iconv' ) )
            return iconv( $inputEnc, 'UTF-8', $str );

        // You could also just return the original string.
        trigger_error(
            'Cannot convert string to UTF-8 in file '
            . __FILE__ . ', line ' . __LINE__ . '!',
            E_USER_ERROR
        );
    }

    /**
     * Check for UTF-8 compatibility
     *
     * Regex from Martin Dürst
     * @source http://www.w3.org/International/questions/qa-forms-utf-8.en.php
     * @param string $str String to check
     * @return boolean
     */
    public static function is_utf8( $str )
    {
        return preg_match( "/^(
         [\x09\x0A\x0D\x20-\x7E]            # ASCII
       | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
       |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
       | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
       |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
       |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
       | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
       |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
      )*$/x",
            $str
        );
    }

    public static function array_has_dupes($array)
    {
        return count($array) !== count(array_unique($array));
    }

    public static function lowerCaseArray($array)
    {
        return array_map(function($element){
            return strtolower($element);
        },$array);
    }

    public static function forceDownload($filename = '',$data)
    {

        // Grab the file extension
        $x = explode('.', $filename);
        $extension = end($x);

        $mimes = Mimes::getAll();
        // Set a default mime if we can't find it
        if ( ! isset($mimes[$extension]))
        {
            $mime = 'application/octet-stream';
        }
        else
        {
            $mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
        }

        if (strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") !== FALSE)
        {
            header('Content-Type: "'.$mime.'"');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');
            header("Content-Length: ".strlen($data));
        }
        else
        {
            header('Content-Type: "'.$mime.'"');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            header("Content-Transfer-Encoding: binary");
            header('Expires: 0');
            header('Pragma: no-cache');
            header("Content-Length: ".strlen($data));
        }
        exit($data);
    }

    public static function array2csv( &$array )
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array_keys(reset($array)));
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public static function response($data, $error = "", $msg = "", $status = true){
        $response = array();
        $response['data'] = isset($data) ? $data : array();
        $response['error'] = isset($error) ? $error : "";
        $response['msg'] = isset($msg) ? $msg : "";
        $response['status'] = isset($status) ? $status : "";
        return $response;
    }

    public static function getDaysBetweenTwoDates($myDate)
    {
        $date1_ts = strtotime(date('Y-m-d'));
        $date2_ts = strtotime($myDate);
        $diff = $date2_ts - $date1_ts;
        return round($diff / 86400);
    }

    /**
     * https://gist.github.com/RuGa/5354e44883c7651fd15c
     * Mass (bulk) insert or update on duplicate for Laravel 4/5
     *
     * insertMultiple([
     *   ['id'=>1,'value'=>10],
     *   ['id'=>2,'value'=>60]
     * ]);
     *
     *
     * @param array $rows
     * @param $table
     */
    public static function insertMultiple(array $rows, $table = ''){
        $queryFailed = false;
        if(empty($table)){
            $table = \DB::getTablePrefix().with(new self)->getTable();
        }

        $first = reset($rows);

        $columns = implode( ',',
            array_map( function( $value ) { return "$value"; } , array_keys($first) )
        );

        $values = implode( ',', array_map( function( $row ) {
                return '('.implode( ',',
                        array_map( function( $value ) { return '"'.str_replace('"', '""', $value).'"'; } , $row )
                    ).')';
            } , $rows )
        );

        $updates = implode( ',',
            array_map( function( $value ) { return "$value = VALUES($value)"; } , array_keys($first) )
        );

        $sql = "INSERT INTO {$table}({$columns}) VALUES {$values}";

        $mysqli = new \mysqli("localhost",
            env("DB_USERNAME", "somedefaultvalue"),
            env("DB_PASSWORD", "somedefaultvalue"),
            env("DB_DATABASE", "somedefaultvalue"));

        $mysqli->set_charset("utf8");
        /* Comprueba la conexión */
        if ($mysqli->connect_errno) {
            $queryFailed = true;
            echo 1;
            echo $sql;
            exit;
        }

        if (!$mysqli->query($sql)) {
            $queryFailed = true;
            echo 2;
            echo $sql;
            exit;
        }
        $mysqli->close();

        return $queryFailed;

    }
}