<div class="row">
            <div class="col-md-12">
                <h6>
                    <strong>
                        <?php
                        echo strip_tags($question->question_yes_no);
                        ?>
                    </strong>

                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default  btn-sm">
                            <input type="radio" name="yesNoCondition-<?php echo $question->id; ?>" value="yes" data-bv-field="yesNo" required>
                            <strong>
                                Si
                            </strong>
                        </label>
                        <label class="btn btn-default  btn-sm">
                            <input type="radio" name="yesNoCondition-<?php echo $question->id; ?>" value="no" data-bv-field="yesNo">
                            <strong>
                                No
                            </strong>
                        </label>
                    </div>
                </h6>
            </div>
        </div>
<br>