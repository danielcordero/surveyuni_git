<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title text-center" id="myModalLabel">
					<strong>
						Crear usuario	
					</strong>
				</h4>
			</div>
			<form id="createuser" method="POST" action="<?php echo e(route('storeUser')); ?>">
					<?php echo e(csrf_field()); ?>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="category"> <strong>Nombre</strong> </label>
								<input type="text" 
									class="form-control" 
									name="name" 
									placeholder="Nombre" 
								required />
							</div>
							<div class="form-group">
								<label for="category"> <strong>Dirección de correo</strong> </label>
								<input type="email" class="form-control" 
									name="email" 
									placeholder="Dirección de correo" required />
							</div>

							<div class="form-group">
								<label for="category"> <strong>Contraseña</strong> </label>
								<input type="password" 
									class="form-control" 
									name="password" 
									placeholder="Contraseña" required />
							</div>

							<div class="form-group">
								<label for="category"> Confirmación de contraseña </label>
								<input type="password" 
									class="form-control" 
									name="password_confirmation" 
									placeholder="Confirmación de contraseña" required />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">
						<i class="fa fa-save"></i>
						Crear
					</button>
					<button type="button"
							class="btn btn-danger"
							data-dismiss="modal">
						Cancelar
					</button>
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->