<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title> <?php echo $__env->yieldContent('title'); ?> </title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <link rel="icon" href="<?php echo e(asset('img/favicon.ico')); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicon.ico')); ?>" type="image/x-icon">

    <link rel="icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/bootstrap.min.css">
<!--<link rel="stylesheet" type="text/css" media="screen" href="<?php //echo url('/') ?>/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-rtl.min.css">

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/demo.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/helperClass.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/public/style.css">


</head>
<body class="hold-transition login-page">

<div class="container-fluid">
    <div class="row bg-primary header">
        <div class="col-md-2 text-center">
            <a href="http://www.uni.edu.ni" target="_blank" class="logo-UNI">
                <img src="https://si.uni.edu.ni/NotasUNI/imgs/logo_uni_89.png" alt="UNI"></a>
        </div>
        <div class="col-md-8 text-center header-text">
            <h1 class="noMarginTopBottom">Universidad Nacional de Ingeniería</h1>
            <h2>Maestría en Gestión de Tecnologías de Información y Comunicación (GTIC)</h2>
            <h2 class="noMarginTopBottom">Sistema de gestion de encuestas online</h2>
            <!--<h2 class="noMarginTopBottom">
                Maestría en Gestión de Tecnologías de Información y Comunicación (GTIC)
            </h2>-->
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>

<div class="container">
    <?php echo $__env->yieldContent('content'); ?>
</div>


    
        
        
        
            
                
            
        
        
        
    


<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo url('/') ?>/js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="js/libs/jquery-2.1.1.min.js"><\/script>');
    }
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo url('/') ?>/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo url('/') ?>/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<!-- BOOTSTRAP JS -->
<script src="<?php echo url('/') ?>/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo url('/') ?>/js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo url('/') ?>/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo url('/') ?>/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="<?php echo url('/') ?>/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo url('/') ?>/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo url('/') ?>/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo url('/') ?>/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo url('/') ?>/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo url('/') ?>/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo url('/') ?>/js/plugin/fastclick/fastclick.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
<script src="<?php echo url('/') ?>/js/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="<?php echo url('/') ?>/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo url('/') ?>/js/speech/voicecommand.min.js"></script>

<!-- SmartChat UI : plugin -->
<script src="<?php echo url('/') ?>/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="<?php echo url('/') ?>/js/smart-chat-ui/smart.chat.manager.min.js"></script>

<script src="<?php echo url('/') ?>/js/plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<script src="<?php echo url('/') ?>/js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script>-->

<script type="text/javascript">

    $(document).ready(function() {
        pageSetUp();
    })

</script>

<script src="<?php echo url('/') ?>/js/public/surveyRuntime.js"></script>

<?php echo $__env->yieldPushContent('scripts'); ?>

</body>
</html>