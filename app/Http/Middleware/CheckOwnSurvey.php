<?php

namespace App\Http\Middleware;

use Closure;
use App\CustomLib\Helper\HelperServices;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Survey;

class CheckOwnSurvey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /*public function handle($request, Closure $next)
    {
        $surveyId = $request->route('surveyId');
        if(!empty($surveyId)){
            $where = [
                ['user_id',Auth::user()->id],
                ['id',$surveyId]
            ];
            $surveyInfo = Survey::where($where)->first();
            if(empty($surveyInfo)){
                abort('401');
            }
        }
        return $next($request);
    }*/

    public function handle($request, Closure $next)
    {
        $parameters = Route::current()->parameters();
        $viewData = [];

        if(array_key_exists("surveyId",$parameters)){
            $surveyId = $parameters['surveyId'];
            $surveyId = HelperServices::sanitize_int($surveyId);
            $parametersQ = [
                'id' => $surveyId,
                'user_id' => Auth::user()->id,
            ];

            $query = "SELECT * FROM surveys WHERE id =:id AND user_id = :user_id";
            $result = \DB::select( \DB::raw($query), $parametersQ);
            $viewData['message'] = "No tiene permiso para acceder a esta encuesta";
            if(empty($result) && empty(Auth::user()->is_super_admin)){
                return response(view('elements.generalpages.page401',
                        ["viewData"=>$viewData]));
            }
        }

        if(array_key_exists("groupId",$parameters)){
            if(!empty($parameters['surveyId']) &&
                !empty($parameters['groupId']) ){

                $surveyId = $parameters['surveyId'];
                $groupId = $parameters['groupId'];
                $surveyId = HelperServices::sanitize_int($surveyId);
                $groupId = HelperServices::sanitize_int($groupId);
                $parametersQ = [
                    'id' => $groupId,
                    'survey_id' => $surveyId
                ];
                $query = "SELECT * FROM groups WHERE id = :id AND survey_id = :survey_id";
                $result = \DB::select( \DB::raw($query), $parametersQ);
                $viewData['message'] = "Este grupo de pregunta no pertenece a esta encuesta";
                if(empty($result)){
                    return response(view('elements.generalpages.page401',["viewData"=>$viewData]));
                }
            }
        }

        if(array_key_exists("questionId",$parameters)){

            if(!empty($parameters['groupId']) &&
                !empty($parameters['questionId']) ){

                $groupId = $parameters['groupId'];
                $questionId = $parameters['questionId'];
                $groupId = HelperServices::sanitize_int($groupId);
                $questionId = HelperServices::sanitize_int($questionId);

                $parametersQ = [
                    'group_id' => $groupId,
                    'id' => $questionId
                ];
                $query = "SELECT * FROM questions WHERE id =:id AND gid = :group_id";
                $result = \DB::select( \DB::raw($query), $parametersQ);
                $viewData['message'] = "Esta pregunta no pertence a este grupo de pregunta";
                if(empty($result)){
                    return response(view('elements.generalpages.page401',["viewData"=>$viewData]));
                }
            }
        }

        return $next($request);
    }
}
