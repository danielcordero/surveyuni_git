@extends('layouts.app')
@section('content')

    @push('css')
        <link rel="stylesheet" href="<?php echo url('/') ?>/js/qtip/dist/jquery.qtip.min.css">
    @endpush
    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        <?php
                            echo !empty($viewData['question']) ? "Editar pregunta" : "Agregar una nueva pregunta";
                        ?>
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="well">
                        <div class="row">
                            <div class="col-md-9">

                                <ol class="breadcrumb">
                                    <li>
                                        <strong>
                                            <u>
                                                <i>
                                                    <a href="{{ route('surveyDetail',[$viewData['survey']->id])}}"
                                                       rel="popover-hover"
                                                       data-placement="right"
                                                       data-original-title="<span class='txtBlack'> Ir al detalle de la encuesta </span>"
                                                       data-html="true"
                                                       data-content="<div class='txtBlack'><?php echo strip_tags($viewData['survey']->title); ?></div>"
                                                       style="color: #c79121;">
                                                        <?php
                                                        $pureText = strip_tags($viewData['survey']->title);
                                                        echo substr($pureText, 0, 50) . '...';
                                                        ?>
                                                    </a>
                                                </i>
                                            </u>
                                        </strong>
                                    </li>
                                </ol>

                                
                            </div>
                            <div class="col-md-3">
                                <p class="text-right">
                                    <button type="button" id="btnSave"
                                            class="btn btn-primary openMainModal">
                                        <i class="fa fa-save"></i>
                                        Guardar
                                    </button>
                                    <a class="btn btn-default"
                                       href="{{ route('surveyDetail',[$viewData['survey']->id])}}">
                                        Cancelar
                                    </a>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-3">
                    @include('survey.questions.elements.treeGroupQuestions')
                </div>
                @include('survey.questions.elements.questionNewEdit')
            </div>
        </section>
    </div>


    @push('scripts')
    <script src="<?php echo url('/') ?>/js/qtip/dist/jquery.qtip.min.js"></script>
    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/QuestionsModule.js"></script>
    @endpush
@stop