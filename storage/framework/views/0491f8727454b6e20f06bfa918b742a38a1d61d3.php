<?php $__env->startSection('title',$viewData['config']['survey']->title); ?>
<?php $__env->startSection('content'); ?>
    <form action="<?php echo e(route('public.index',['surveyId'=> $viewData['config']['survey']->id ])); ?>" method="post">

        <div class="row">
            <div class="col-md-12 text-center titleSurvey">
                <h1>
                    <?php echo $viewData['config']['survey']->title; ?>

                </h1>
            </div>
            <div class="col-md-12 text-center">
                <?php echo $viewData['config']['survey']->description; ?>

            </div>
            <div class="col-md-12">
                <?php echo $viewData['config']['survey']->welcometext; ?>

            </div>

            <?php if(!empty($viewData['config']['survey']->showxquestions)
                && $viewData['config']['survey']->showxquestions == "Y"): ?>
                <div class="col-md-12">
                    <h3 class="text-center amountQuestions">
                        <span class="label label-primary">
                            Hay <?php echo e(count($viewData['config']['questionsIds'])); ?>

                            pregunta<?php if(count($viewData['config']['questionsIds']) > 1) echo "s" ; ?>  en está encuesta.
                        </span>
                    </h3>
                </div>
            <?php endif; ?>

        </div>
        <!-- PRESENT THE NAVIGATOR -->
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 text-right">
                <button type="submit"
                        name="currentView"
                        value="startview"
                        class="btn btn-danger btn-lg btnNext">
                    Siguiente <i class="glyphicon glyphicon-chevron-right"></i>
                </button>
            </div>
        </div>

        <input name="survey_id"
               type="hidden"
               value="<?php echo $viewData['config']['survey']->id; ?>">

        <!--<input name="current_group"
               type="hidden"
               value="0">-->
    </form>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('survey.public.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>