<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\CustomLib\StatusCodes;
use App\{Group,Question,Survey};
use App\CustomLib\Helper\HelperServices;

class AjaxController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except' => ['getSelectQuestionHtml']]);
        $this->middleware('CheckPermission',['except' => ['getSelectQuestionHtml']]);
    }
    
    public function storeUser(Request $request){

        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ]);
        $request['password'] = bcrypt($request['password']);
        $user = User::create($request->only('email', 'name', 'password'));
        return response()->json($user->toArray(),StatusCodes::HTTP_CREATED);
    }

    public function getSelectQuestionHtml(Request $request){
        $groupId = (int)$request->groupId;
        $groupInfo = Group::with([
            'questions' => function($q) {
                $q->orderBy('sortorder', 'asc');
            }
        ])->find($groupId);

        $viewData['group'] = $groupInfo;

//        echo "<pre>";
//        print_r( $viewData['group']->toArray() );
//        exit;

        $viewData['question'] = !empty($request->questionId) ? Question::find((int)$request->questionId) : array();
        return view('partials.questions.selectOption')->with('viewData',$viewData);
    }

    public function savedateconfig(Request $request){
        $isValidaDate = true;
        if (trim($request->startdate) == '' ||
            !HelperServices::checkFormatDateYMD($request->startdate)){
            $isValidaDate =  false;
        }

        if (trim($request->expiresdate) == '' ||
            !HelperServices::checkFormatDateYMD($request->expiresdate)){
            $isValidaDate =  false;
        }

        $surveyId = HelperServices::sanitize_int($request->surveyId);
        $surveyInfo = Survey::find($surveyId);
        $surveyAdditionalData = json_decode($surveyInfo->additionalData);

        if($isValidaDate){
            $surveyAdditionalData->startdate = $request->startdate;
            $surveyAdditionalData->expiresdate = $request->expiresdate;
        }

        $surveyInfo->additionalData = json_encode($surveyAdditionalData);
        $result = [];

        if($surveyInfo->save()){
            $result = HelperServices::response(array(),"","",true);
        } else {
            $result = HelperServices::response(array(),"Hubo un problema al executar está acción","",false);
        }
        return response()->json($result,StatusCodes::HTTP_OK);
    }


}
