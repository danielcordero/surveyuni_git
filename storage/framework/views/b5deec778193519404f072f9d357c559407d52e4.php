<?php
//    echo phpinfo();
//    exit;
?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <title> Login </title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="<?php echo e(asset('img/favicon.png')); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicon.png')); ?>" type="image/x-icon">

    <link rel="icon" href="<?php echo e(asset('img/favicon.ico')); ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo e(asset('img/favicon.ico')); ?>" type="image/x-icon">

    <link rel="icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo url('/') ?>/img/favicon.png" type="image/x-icon">


    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo url('/') ?>/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo url('/') ?>/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/smartadmin-rtl.min.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/mystyle.css">

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo url('/') ?>/css/demo.min.css">

    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body class="hold-transition login-page">

<div class="container-fluid">
    <div class="row bg-primary header">
        <div class="hidden-xs col-md-2">
            <a href="http://www.uni.edu.ni" target="_blank" class="logo-UNI">
                <img src="https://si.uni.edu.ni/NotasUNI/imgs/logo_uni_89.png" alt="UNI"></a>
        </div>
        <div class="col-md-8 text-center header-text">
            <h1 class="noMarginTopBottom">Universidad Nacional de Ingeniería</h1>
            <!-- <h2>Maestría en Gestión de Tecnologías de Información y Comunicación (GTIC)</h2> -->
            <h3 class="noMarginTopBottom">Sistema de gestion de encuestas online</h3>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4 login-box  col-centered">
            <?php echo $__env->make('elements.view._message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="well no-padding">
                <form
                        action="login" method="post"
                        id="login-form"
                        class="smart-form client-form"
                        novalidate="novalidate">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <header>
                        <strong>
                            Login
                        </strong>
                    </header>

                    <fieldset>

                        <section>
                            <label class="label">Email</label>
                            <label class="input"> <i class="icon-append fa fa-user"></i>
                                <input type="email" name="email">
                                <b class="tooltip tooltip-top-right">
                                    <i class="fa fa-user txt-color-teal">
                                    </i> Por favor ingrese su correo</b>
                            </label>
                        </section>

                        <section>
                            <label class="label">Contraseña</label>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="password" name="password">
                                <b class="tooltip tooltip-top-right">
                                    <i class="fa fa-lock txt-color-teal"></i>
                                    Por favor ingrese su password</b>
                            </label>
                            <div class="note">
                            </div>
                        </section>

                        <section>
                            <div class="g-recaptcha" data-sitekey="<?php echo e(env('CAPTCHA_KEY')); ?>"></div>
                            <?php if($errors->has('g-recaptcha-response')): ?>
                                <span class="invalid-feedback" style="display: block;">
                                            <strong><?php echo e($errors->first('g-recaptcha-response')); ?></strong>
                                        </span>
                            <?php endif; ?>

                        </section>

                        <section>
                            <label class="checkbox">
                                <input type="checkbox" name="remember" checked="">
                                <i></i>Recordar</label>
                        </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Acceder
                        </button>
                    </footer>
                </form>

            </div>
        </div>
    </div>
</div>
<div class="container-fluid overContent-fixedBottom fullWidth">
    <div class="row bg-primary footer">
        <div class="hidden-xs col-md-2">
        </div>
        <div class="col-md-8 text-center header-text">
            <p class="noMarginTopBottom">
                Universidad Nacional de Ingeniería (UNI)
            </p>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>



<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="js/libs/jquery-2.1.1.min.js"><\/script>');
    }
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?php echo url('/') ?>/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?php echo url('/') ?>/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<!-- BOOTSTRAP JS -->
<script src="<?php echo url('/') ?>/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo url('/') ?>/js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo url('/') ?>/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo url('/') ?>/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="<?php echo url('/') ?>/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="<?php echo url('/') ?>/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?php echo url('/') ?>/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo url('/') ?>/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo url('/') ?>/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo url('/') ?>/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo url('/') ?>/js/plugin/fastclick/fastclick.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
<script src="<?php echo url('/') ?>/js/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="<?php echo url('/') ?>/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<?php echo url('/') ?>/js/speech/voicecommand.min.js"></script>

<!-- SmartChat UI : plugin -->
<script src="<?php echo url('/') ?>/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="<?php echo url('/') ?>/js/smart-chat-ui/smart.chat.manager.min.js"></script>

<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script>-->

<script type="text/javascript">

    $(document).ready(function () {
        pageSetUp();
    })

</script>



</body>
</html>