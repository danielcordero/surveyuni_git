<?php

namespace App\CustomLib\Services;

use App\Group;
use App\CustomLib\Helper\HelperServices;
use App\Survey;

class GroupService{
    private $pagination = 1;

    public  function getById($groupId){
        return Group::with([
            'questions' => function($q){
                $q->orderBy('sortorder', 'asc');
            }
        ])->find($groupId);
    }

    public  function setPagination($page){
        $this->pagination = $page;
    }

    public static function getByOrderCondition($surveyId, $order, $isLess = true ,$isArray = true){

        $survey = Survey::with([
            'groups' => function($q) use ($isLess, $order){
                if($isLess){
                    $q->where(
                        "groups.sortorder","<",$order
                    );
                } else {
                    $q->where(
                        "groups.sortorder",">",$order
                    );
                }
            }
        ])->find($surveyId);

        if($isArray){
            $result = $survey->groups->toArray();
        } else {
            $result = $survey->groups;
        }
        return $result;
    }

    public static function changeSortOrder($groups, $isPlus = true){
        $newArrayQuestion = [];
        $newArrayQuestions = [];
        foreach($groups as $key => $group)
        {
            $sortOrder = $groups[$key]['sortorder'];
            if($isPlus){
                $sortOrder = $sortOrder + 1;
            } else {
                $sortOrder = $sortOrder - 1;
            }

            $groups[$key]['sortorder'] = $sortOrder;
            $newArrayQuestion['id'] = $groups[$key]['id'];
            $newArrayQuestion['sortorder'] = $sortOrder;
            array_push($newArrayQuestions, $newArrayQuestion);
        }

        return HelperServices::updateBulk('groups',$newArrayQuestions,'id');
    }

    public static function getBySurveyRaw( $surveyId ){

        $parameters['surveyId'] = $surveyId;
        $query = "SELECT * FROM groups AS g 
                  WHERE g.survey_id = :surveyId";

        return \DB::select( \DB::raw($query), $parameters);

    }
}
?>