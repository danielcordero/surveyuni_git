<?php

namespace App\Http\Controllers\AdminSurveys;

use App\CustomLib\Helper\HelperServices;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomLib\Services\{SurveyPublic,ParticipantService,SurveyService,SurveySettingsServices};
use League\Csv\Writer;
use League\Csv\CharsetConverter;
use League\Csv\Reader;

class StatisticsController extends Controller
{
    public function __construct()
    {
        $this->middleware('CheckExistResource');
    }

    public function viewstatistics($surveyId = "")
    {
        $surveyId = HelperServices::sanitize_int($surveyId);
        $surveyPublic = new SurveyPublic();
        $surveyId = !empty($surveyId) ? $surveyId : 1;
        $surveyConfig =  $surveyPublic->getConfig($surveyId);
        $viewData['survey'] = $surveyConfig['survey'];
        $participants = ParticipantService::getAllCompleted($surveyId);
        $viewData['participants'] = $participants;

        return view('reports.statistics.viewgraph')
            ->with('viewData', $viewData);
    }

    public function viewexportresponse($surveyId = "")
    {

        include(app_path() . DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.'csv'.DIRECTORY_SEPARATOR.'autoload.php');

        $surveyPublic = new SurveyPublic();
        $surveyId = HelperServices::sanitize_int($surveyId);
        $surveyConfig =  $surveyPublic->getConfig($surveyId);
        $participants = ParticipantService::getAllCompleted($surveyId);

        $titlesSurveys = [];
        $responseByQuestion = [];
        $responseAll = [];

        foreach ($participants as $participant) {
            $statusSurvey = json_decode($participant->additionalData, true);
            $titlesSurveys[] = "Nombre Participante";
            $responseByQuestion[] = $participant->firstname;
            $titlesSurveys[] = "Apellido Participante";
            $responseByQuestion[] = $participant->lastname;
            foreach ($surveyConfig['survey']->groups as $group){
                foreach ($group->questions as $question){
                    $statusQuestion = SurveyService::getFullStatusInSurvey($statusSurvey['answers'], $question->id);
                    $questionConfig = json_decode($question->additionalData, true);
                    switch ($question->type){
                        case "A":
                        case "AC":
                            $keepAnswered = $statusQuestion['keepAnswered'];
                            $outerLoop = [];
                            $innerLoop = [];

                            if($question->type == "AC"){
                                $outerLoop = $questionConfig['subQuestionsC'];
                                $innerLoop = $questionConfig['subQuestionsR'];
                            }

                            if($question->type == "A"){
                                $innerLoop = $questionConfig['subQuestionsC'];
                                $outerLoop = $questionConfig['subQuestionsR'];
                            }

                            foreach ($outerLoop as $key => $value){
                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                foreach ($keepAnswered as $key2 => $value2){
                                    if(!empty($innerLoop[$value2])){
                                        $responseByQuestion[] = $innerLoop[$value2];
                                        unset($keepAnswered[$key2]);
                                    }
                                }
                            }
                            break;
                        case "AT":
                        case "AN":
                            $keepAnswered = $statusQuestion['keepAnswered'];
                            $subQuestionsR = $questionConfig['subQuestionsR'];
                            $subQuestionsC = $questionConfig['subQuestionsC'];
                            foreach ($subQuestionsR as $key => $value){
                                foreach ($subQuestionsC as $key2 => $value2){
                                    $titlesSurveys[] = $question->title . " " . "[".$value."]" . "[".$value2."]";
                                    if(!empty($keepAnswered[$key.":".$key2])){
                                        $responseByQuestion[] = $keepAnswered[$key.":".$key2];
                                        unset($keepAnswered[$key.":".$key2]);
                                    }
                                }
                            }
                            break;
                        case "AISD":
                        case "AYN":
                            $answers = $question->type == "AYN" ?
                                ["yes"=>"Si","no"=>"No","uncertain"=>"No sé"] :
                                ["decrease"=>"Disminuir","same"=>"Mismo","increase"=>"Incrementar"];

                            $keepAnswered = $statusQuestion['keepAnswered'];
                            foreach ($questionConfig['subQuestions'] as $key => $value){
                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                if(!empty($keepAnswered[$key])){
                                    $responseByQuestion[] = $answers[$keepAnswered[$key]];
                                    unset($answers[$keepAnswered[$key]]);
                                }
                            }
                            break;
                        case "A5":
                        case "A10":
                            $keepAnswered = $statusQuestion['keepAnswered'];
                            foreach ($questionConfig['subQuestions'] as $key => $value){
                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                if(!empty($statusQuestion['keepAnswered'][$key])){
                                    $responseByQuestion[] = $statusQuestion['keepAnswered'][$key];
                                    unset($keepAnswered[$key]);
                                }
                            }

                            break;
//                        case "A":
//                            $subQuestionsR = $questionConfig['subQuestionsR'];
//                            $subQuestionsC = $questionConfig['subQuestionsC'];
//                            $keepAnswered = $statusQuestion['keepAnswered'];
//
//                            foreach ($subQuestionsR as $key => $value){
//                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
//                                foreach ($keepAnswered as $key2 => $value2){
//                                    if(!empty($subQuestionsC[$value2])){
//                                        $responseByQuestion[] = $subQuestionsC[$value2];
//                                        unset($keepAnswered[$key2]);
//                                    }
//                                }
//                            }
//
//                            break;
                        case "NM":
                            foreach ($questionConfig['subQuestions'] as $key => $value){
                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                $responseByQuestion[] = $statusQuestion['keepAnswered'][$key];
                            }
                            break;
                        case "R":
                            $keepAnswered = $statusQuestion['keepAnswered'];
                            foreach ($keepAnswered as $key => $value){
                                $titlesSurveys[] = $question->title;
                                $responseByQuestion[] = html_entity_decode($questionConfig['subQuestions'][$value]);
                            }
                            break;
                        case "DT":
                        case "N":
                            $titlesSurveys[] = $question->title;
                            if($question->type == "DT"){
                                $responseByQuestion[] = $statusQuestion['keepAnswered']['date'];
                            }

                            if($question->type == "N"){
                                $responseByQuestion[] = $statusQuestion['keepAnswered'];
                            }
                            break;
                        case "T":
                            $titlesSurveys[] = $question->title;
                            if(!empty($statusQuestion['keepAnswered']) && $statusQuestion['keepAnswered'] == "male"){
                                $responseByQuestion[] = "Masculino";
                            } else {
                                $responseByQuestion[] = "Femenino";
                            }
                            break;
                        case "YN":
                            $titlesSurveys[] = $question->title;
                            if(!empty($statusQuestion['keepAnswered']) && $statusQuestion['keepAnswered'] == "yes"){
                                $responseByQuestion[] = "Si";
                            } else {
                                $responseByQuestion[] = "No";
                            }
                            break;
                        case "TS":
                            $keepAnswered = $statusQuestion['keepAnswered'];
                            $withoutAnswered = $statusQuestion['withoutAnswered'];
                            foreach ($questionConfig['subQuestions'] as $key => $value){
                                if(!empty($keepAnswered)){
                                    foreach ($keepAnswered as $key2 => $value2){
                                        $answer = $key2;
                                        if(!empty($questionConfig['subQuestions'][$answer])){
                                            $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                            $responseByQuestion[] = $questionConfig['subQuestions'][$answer];
                                            unset($keepAnswered[$key2]);
                                            break;
                                        }
                                    }
                                } else {
                                    if(!empty($withoutAnswered)){
                                        $index = 0;
                                        foreach ($withoutAnswered as $key2 => $value2){
                                            $answer = $key2;
                                            if(!empty($questionConfig['subQuestions'][$answer])){
                                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                                $responseByQuestion[] = $questionConfig['subQuestions'][$answer];
                                                unset($withoutAnswered[$index]);
                                                break;
                                            }
                                            $index++;
                                        }
                                    }
                                }
                            }
                            break;
                        case "TFS":
                        case "TFB":
                        case "TFP":
                            $titlesSurveys[] = $question->title;
                            $responseByQuestion[] = $statusQuestion['keepAnswered'];
                            break;
                        case "OMC":

                            $keepAnswered = $statusQuestion['keepAnswered'];
                            $withoutAnswered = $statusQuestion['withoutAnswered'];
                            foreach ($questionConfig['subQuestions'] as $key => $value){
                                if(!empty($keepAnswered)){
                                    foreach ($keepAnswered as $key2 => $value2){
                                        $answer = str_replace("_comment","",$key2);
                                        if(!empty($questionConfig['subQuestions'][$answer])){
                                            $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                            $responseByQuestion[] = "Si";
                                            $titlesSurveys[] = $question->title . " " . "[".$value."]["."comentario"."]";
                                            $responseByQuestion[] = $questionConfig['subQuestions'][$answer];
                                            unset($keepAnswered[$key2]);
                                            break;
                                        }
                                    }
                                } else {
                                    if(!empty($withoutAnswered)){
                                        $index = 0;
                                        foreach ($withoutAnswered as $key2 => $value2){
                                            $answer = str_replace("_comment","",$value2);
                                            if(!empty($questionConfig['subQuestions'][$answer])){
                                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                                $responseByQuestion[] = "No";
                                                $titlesSurveys[] = $question->title . " " . "[".$value."]["."comentario"."]";
                                                $responseByQuestion[] = "";
                                                unset($withoutAnswered[$index]);
                                                break;
                                            }
                                            $index++;
                                        }
                                    }
                                }
                            }
                            break;
                        case "OM":
                            $keepAnswered = $statusQuestion['keepAnswered'];
                            $withoutAnswered = $statusQuestion['withoutAnswered'];

                            foreach ($questionConfig['subQuestions'] as $key => $value){
                                $titlesSurveys[] = $question->title . " " . "[".$value."]";
                                if(!empty($keepAnswered)){
                                    foreach ($keepAnswered as $key2 => $value2){
                                        if(!empty($questionConfig['subQuestions'][$key2])){
                                            //$responseByQuestion[] = $questionConfig['subQuestions'][$key2];
                                            $responseByQuestion[] = "Si";
                                            unset($keepAnswered[$key2]);
                                            break;
                                        }
                                    }
                                } else {
                                    if(!empty($withoutAnswered)){
                                        $index = 0;
                                        foreach ($withoutAnswered as $key2 => $value2){
                                            if(!empty($questionConfig['subQuestions'][$value2])){
//                                            $responseByQuestion[] = $questionConfig['subQuestions'][$value2];
                                                $responseByQuestion[] = "No";
                                                unset($withoutAnswered[$index]);
                                                break;
                                            }
                                            $index++;
                                        }
                                    }
                                }
                            }
                            break;
                        case "LRC":
                            $result = "";
                            if(!empty($questionConfig['subQuestions']) &&
                                !empty($questionConfig['subQuestions'][$statusQuestion['keepAnswered']['question']])){
                                $result = $questionConfig['subQuestions'][$statusQuestion['keepAnswered']['question']];
                            }
                            $responseByQuestion[] = $result;
                            $result = !empty($statusQuestion['keepAnswered']['comment']) ? $statusQuestion['keepAnswered']['comment'] : "";
                            $surveyConfig['questionsTitle'][] = "[comment]";
                            $titlesSurveys[] = $question->title;
                            $responseByQuestion[] = $result;
                            break;
                        case "C5":
                            $responseByQuestion[] = !empty($statusQuestion['keepAnswered']) ? $statusQuestion['keepAnswered'] : "";
                            $titlesSurveys[] = $question->title;
                            break;
                        case "LR":
                        case "DDL":
                            $result = "";
                            if(!empty($questionConfig['subQuestions']) &&
                                !empty($questionConfig['subQuestions'][$statusQuestion['keepAnswered']])){
                                $result = $questionConfig['subQuestions'][$statusQuestion['keepAnswered']];
                            }
                            $titlesSurveys[] = $question->title;
                            $responseByQuestion[] = $result;
                    }
                }
            }
            $responseAll[] = $responseByQuestion;
        }

        $formatter = (new CharsetConverter())
            ->inputEncoding('utf-8')
            ->outputEncoding('iso-8859-1');

        $csv = Writer::createFromFileObject(new \SplTempFileObject());
//        $csv->setOutputBOM(Reader::BOM_UTF8);
        $csv->addFormatter($formatter);

        //we detect and adjust the output BOM to be used

        //we insert the CSV header
        $csv->insertOne($titlesSurveys);

        // The PDOStatement Object implements the Traversable Interface
        // that's why Writer::insertAll can directly insert
        // the data into the CSV
        $csv->insertAll($responseAll);

        // Because you are providing the filename you don't have to
        // set the HTTP headers Writer::output can
        // directly set them for you
        // The file is downloadable
        $fileName = "data_export_survey_{$surveyId}" . date("Y-m-d") . ".csv";
//        $csv->output($fileName);

        header('content-type: text/csv; charset=utf-8');
        header('content-disposition: attachment; filename="'.$fileName);
        $csv->output();

        die;

//        $csv  = HelperServices::array2csv($responseAll);
//        $name = "data_export_survey_{$surveyId}" . date("Y-m-d") . ".csv";
//        header("Content-type: text/csv");
//        HelperServices::forceDownload( $name, $csv);
    }
}
