<div class="row">
    <article class="col-md-12 col-xs-12">
        <div class="jarviswidget jarviswidget-color-darken"
             id="wid-id-16"
             data-widget-sortable="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-editbutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-deletebutton="false" style="" role="widget">

            <header role="heading"
                    class="displayBlock">
                <span class="widget-icon"></span>
                <h2>
                    Preguntas
                </h2>
                <div class="widget-toolbar" role="menu">
                </div>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
            </header>
            <div role="content">
                <div class="widget-body no-padding smart-form">


                    <header>
                        Standard Form Header
                    </header>

                    <fieldset>
                        <div class="row">
                            <section class="col col-5">
                                <label class="toggle">
                                    <input type="checkbox" name="checkbox-toggle" checked="checked">
                                    <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                    <strong>
                                        Mostrar el nombre del grupo
                                    </strong>
                                </label>
                                <label class="toggle">
                                    <input type="checkbox" name="checkbox-toggle" checked="checked">
                                    <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                    <strong>
                                        Mostrar número de pregunta
                                    </strong>
                                </label>
                            </section>
                        </div>
                    </fieldset>

                    {{--<fieldset>--}}



                        {{--<section>--}}
                            {{--<label class="label">Small text input</label>--}}
                            {{--<label class="input">--}}
                                {{--<input type="text" class="input-sm">--}}
                            {{--</label>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Default text input with maxlength</label>--}}
                            {{--<label class="input">--}}
                                {{--<input type="text" maxlength="10">--}}
                            {{--</label>--}}
                            {{--<div class="note">--}}
                                {{--<strong>Maxlength</strong> is automatically added via the "maxlength='#'" attribute--}}
                            {{--</div>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Large text input</label>--}}
                            {{--<label class="input">--}}
                                {{--<input type="text" class="input-lg">--}}
                            {{--</label>--}}
                        {{--</section>--}}

                    {{--</fieldset>--}}

                    {{--<fieldset>--}}

                        {{--<section>--}}
                            {{--<label class="label">File input</label>--}}
                            {{--<div class="input input-file">--}}
                                {{--<span class="button"><input type="file" id="file" name="file" onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input type="text" placeholder="Include some files" readonly="">--}}
                            {{--</div>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Input with autocomlete</label>--}}
                            {{--<label class="input">--}}
                                {{--<input type="text" list="list">--}}
                                {{--<datalist id="list">--}}
                                    {{--<option value="Alexandra">Alexandra</option>--}}
                                    {{--<option value="Alice">Alice</option>--}}
                                    {{--<option value="Anastasia">Anastasia</option>--}}
                                    {{--<option value="Avelina">Avelina</option>--}}
                                    {{--<option value="Basilia">Basilia</option>--}}
                                    {{--<option value="Beatrice">Beatrice</option>--}}
                                    {{--<option value="Cassandra">Cassandra</option>--}}
                                    {{--<option value="Cecil">Cecil</option>--}}
                                    {{--<option value="Clemencia">Clemencia</option>--}}
                                    {{--<option value="Desiderata">Desiderata</option>--}}
                                    {{--<option value="Dionisia">Dionisia</option>--}}
                                    {{--<option value="Edith">Edith</option>--}}
                                    {{--<option value="Eleanora">Eleanora</option>--}}
                                    {{--<option value="Elizabeth">Elizabeth</option>--}}
                                    {{--<option value="Emma">Emma</option>--}}
                                    {{--<option value="Felicia">Felicia</option>--}}
                                    {{--<option value="Florence">Florence</option>--}}
                                    {{--<option value="Galiana">Galiana</option>--}}
                                    {{--<option value="Grecia">Grecia</option>--}}
                                    {{--<option value="Helen">Helen</option>--}}
                                    {{--<option value="Helewisa">Helewisa</option>--}}
                                    {{--<option value="Idonea">Idonea</option>--}}
                                    {{--<option value="Isabel">Isabel</option>--}}
                                    {{--<option value="Joan">Joan</option>--}}
                                    {{--<option value="Juliana">Juliana</option>--}}
                                    {{--<option value="Karla">Karla</option>--}}
                                    {{--<option value="Karyn">Karyn</option>--}}
                                    {{--<option value="Kate">Kate</option>--}}
                                    {{--<option value="Lakisha">Lakisha</option>--}}
                                    {{--<option value="Lana">Lana</option>--}}
                                    {{--<option value="Laura">Laura</option>--}}
                                    {{--<option value="Leona">Leona</option>--}}
                                    {{--<option value="Mandy">Mandy</option>--}}
                                    {{--<option value="Margaret">Margaret</option>--}}
                                    {{--<option value="Maria">Maria</option>--}}
                                    {{--<option value="Nanacy">Nanacy</option>--}}
                                    {{--<option value="Nicole">Nicole</option>--}}
                                    {{--<option value="Olga">Olga</option>--}}
                                    {{--<option value="Pamela">Pamela</option>--}}
                                    {{--<option value="Patricia">Patricia</option>--}}
                                    {{--<option value="Qiana">Qiana</option>--}}
                                    {{--<option value="Rachel">Rachel</option>--}}
                                    {{--<option value="Ramona">Ramona</option>--}}
                                    {{--<option value="Samantha">Samantha</option>--}}
                                    {{--<option value="Sandra">Sandra</option>--}}
                                    {{--<option value="Tanya">Tanya</option>--}}
                                    {{--<option value="Teresa">Teresa</option>--}}
                                    {{--<option value="Ursula">Ursula</option>--}}
                                    {{--<option value="Valerie">Valerie</option>--}}
                                    {{--<option value="Veronica">Veronica</option>--}}
                                    {{--<option value="Wilma">Wilma</option>--}}
                                    {{--<option value="Yasmin">Yasmin</option>--}}
                                    {{--<option value="Zelma">Zelma</option>--}}
                                {{--</datalist> </label>--}}
                            {{--<div class="note">--}}
                                {{--<strong>Note:</strong> works in Chrome, Firefox, Opera and IE10.--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</fieldset>--}}

                    {{--<fieldset>--}}

                        {{--<section>--}}
                            {{--<label class="label">Select Small</label>--}}
                            {{--<label class="select">--}}
                                {{--<select class="input-sm">--}}
                                    {{--<option value="0">Choose name</option>--}}
                                    {{--<option value="1">Alexandra</option>--}}
                                    {{--<option value="2">Alice</option>--}}
                                    {{--<option value="3">Anastasia</option>--}}
                                    {{--<option value="4">Avelina</option>--}}
                                {{--</select> <i></i> </label>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Select default</label>--}}
                            {{--<label class="select">--}}
                                {{--<select>--}}
                                    {{--<option value="0">Choose name</option>--}}
                                    {{--<option value="1">Alexandra</option>--}}
                                    {{--<option value="2">Alice</option>--}}
                                    {{--<option value="3">Anastasia</option>--}}
                                    {{--<option value="4">Avelina</option>--}}
                                {{--</select> <i></i> </label>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Select Large</label>--}}
                            {{--<label class="select">--}}
                                {{--<select class="input-lg">--}}
                                    {{--<option value="0">Choose name</option>--}}
                                    {{--<option value="1">Alexandra</option>--}}
                                    {{--<option value="2">Alice</option>--}}
                                    {{--<option value="3">Anastasia</option>--}}
                                    {{--<option value="4">Avelina</option>--}}
                                {{--</select> <i></i> </label>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Multiple select</label>--}}
                            {{--<label class="select select-multiple">--}}
                                {{--<select multiple="" class="custom-scroll">--}}
                                    {{--<option value="1">Alexandra</option>--}}
                                    {{--<option value="2">Alice</option>--}}
                                    {{--<option value="3">Anastasia</option>--}}
                                    {{--<option value="4">Avelina</option>--}}
                                    {{--<option value="5">Basilia</option>--}}
                                    {{--<option value="6">Beatrice</option>--}}
                                    {{--<option value="7">Cassandra</option>--}}
                                    {{--<option value="8">Clemencia</option>--}}
                                    {{--<option value="9">Desiderata</option>--}}
                                {{--</select> </label>--}}
                            {{--<div class="note">--}}
                                {{--<strong>Note:</strong> hold down the ctrl/cmd button to select multiple options.--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</fieldset>--}}

                    {{--<fieldset>--}}
                        {{--<section>--}}
                            {{--<label class="label">Textarea</label>--}}
                            {{--<label class="textarea">--}}
                                {{--<textarea rows="3" class="custom-scroll"></textarea>--}}
                            {{--</label>--}}
                            {{--<div class="note">--}}
                                {{--<strong>Note:</strong> height of the textarea depends on the rows attribute.--}}
                            {{--</div>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Textarea resizable</label>--}}
                            {{--<label class="textarea textarea-resizable">--}}
                                {{--<textarea rows="3" class="custom-scroll"></textarea>--}}
                            {{--</label>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Textarea expandable</label>--}}
                            {{--<label class="textarea textarea-expandable">--}}
                                {{--<textarea rows="3" class="custom-scroll"></textarea>--}}
                            {{--</label>--}}
                            {{--<div class="note">--}}
                                {{--<strong>Note:</strong> expands on focus.--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</fieldset>--}}

                    {{--<fieldset>--}}
                        {{--<section>--}}
                            {{--<label class="label">Columned radios</label>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col col-4">--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio" checked="checked">--}}
                                        {{--<i></i>Alexandra</label>--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Alice</label>--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Anastasia</label>--}}
                                {{--</div>--}}
                                {{--<div class="col col-4">--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Avelina</label>--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Basilia</label>--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Beatrice</label>--}}
                                {{--</div>--}}
                                {{--<div class="col col-4">--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Cassandra</label>--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Clemencia</label>--}}
                                    {{--<label class="radio">--}}
                                        {{--<input type="radio" name="radio">--}}
                                        {{--<i></i>Desiderata</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Inline radios</label>--}}
                            {{--<div class="inline-group">--}}
                                {{--<label class="radio">--}}
                                    {{--<input type="radio" name="radio-inline" checked="checked">--}}
                                    {{--<i></i>Alexandra</label>--}}
                                {{--<label class="radio">--}}
                                    {{--<input type="radio" name="radio-inline">--}}
                                    {{--<i></i>Alice</label>--}}
                                {{--<label class="radio">--}}
                                    {{--<input type="radio" name="radio-inline">--}}
                                    {{--<i></i>Anastasia</label>--}}
                                {{--<label class="radio">--}}
                                    {{--<input type="radio" name="radio-inline">--}}
                                    {{--<i></i>Avelina</label>--}}
                                {{--<label class="radio">--}}
                                    {{--<input type="radio" name="radio-inline">--}}
                                    {{--<i></i>Beatrice</label>--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</fieldset>--}}

                    {{--<fieldset>--}}
                        {{--<section>--}}
                            {{--<label class="label">Columned checkboxes</label>--}}
                            {{--<div class="row">--}}
                                {{--<div class="col col-4">--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox" checked="checked">--}}
                                        {{--<i></i>Alexandra</label>--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Alice</label>--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Anastasia</label>--}}
                                {{--</div>--}}
                                {{--<div class="col col-4">--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Avelina</label>--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Basilia</label>--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Beatrice</label>--}}
                                {{--</div>--}}
                                {{--<div class="col col-4">--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Cassandra</label>--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Clemencia</label>--}}
                                    {{--<label class="checkbox">--}}
                                        {{--<input type="checkbox" name="checkbox">--}}
                                        {{--<i></i>Desiderata</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</section>--}}

                        {{--<section>--}}
                            {{--<label class="label">Inline checkboxes</label>--}}
                            {{--<div class="inline-group">--}}
                                {{--<label class="checkbox">--}}
                                    {{--<input type="checkbox" name="checkbox-inline" checked="checked">--}}
                                    {{--<i></i>Alexandra</label>--}}
                                {{--<label class="checkbox">--}}
                                    {{--<input type="checkbox" name="checkbox-inline">--}}
                                    {{--<i></i>Alice</label>--}}
                                {{--<label class="checkbox">--}}
                                    {{--<input type="checkbox" name="checkbox-inline">--}}
                                    {{--<i></i>Anastasia</label>--}}
                                {{--<label class="checkbox">--}}
                                    {{--<input type="checkbox" name="checkbox-inline">--}}
                                    {{--<i></i>Avelina</label>--}}
                                {{--<label class="checkbox">--}}
                                    {{--<input type="checkbox" name="checkbox-inline">--}}
                                    {{--<i></i>Beatrice</label>--}}
                            {{--</div>--}}
                        {{--</section>--}}
                    {{--</fieldset>--}}

                </div>
            </div>
        </div>
    </article>
</div>