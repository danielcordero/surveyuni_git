<?php
use \App\CustomLib\TypeQuestions\BaseQuestion;
?>
@extends('survey.public.layout')
@section('title','Vista previa de la pregunta')
@section('content')
    <form>
        <div class="row">
            <br>
            <div class="col-md-12 text-center titleSurvey">
                <div class="alert alert-warning alert-dismissible fade in alert-dismissible"
                     role="alert">
                    <h3>
                        <strong>
                            Vista previa de pregunta
                        </strong>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>
                    {!! $viewData['groupInfo']->name !!}
                </h1>
            </div>
        </div>
        <?php
            $questionsType = config('custom.questionsTypeTemplate');
            $allQuestionsId = "";
        ?>
        <?php
            $question = $viewData['question'];
            $questionType = $questionsType[$question->type];
            $baseQuestion = new BaseQuestion;
            $config = $baseQuestion->getConfigBase($questionType, $question->additionalData);
            $template = 'survey.public.questions.' . $questionType;
            $resultQuestion = [];
            if(!empty($viewData['responses'])){
                if(!empty($viewData['responses'][$question->id])){
                    $resultQuestion = $viewData['responses'][$question->id];
                }
            }
        ?>
        <?php
        $allQuestionsId = $allQuestionsId . '|' . 'question_'.$question->id;
        ?>
        @include($template)
    </form>
@stop