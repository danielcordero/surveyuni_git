/**
 * Created by cesar on 3/23/2018.
 */
'use strict';

var ReportModule = new function(){
    //Private variable
    var that = this;
    this.init = function(){

        // $('#barCharTest').highcharts({
        //     chart: {
        //         type: 'column'
        //     },
        //     title: {
        //         /*text: 'Browser market shares. January, 2015 to May, 2015'*/
        //     },
        //     subtitle: {
        //         /*text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'*/
        //     },
        //     xAxis: {
        //         type: 'category'
        //     },
        //     yAxis: {
        //         title: {
        //             text: 'Total'
        //         }
        //
        //     },
        //     legend: {
        //         enabled: false
        //     },
        //     plotOptions: {
        //         series: {
        //             borderWidth: 0,
        //             dataLabels: {
        //                 enabled: true,
        //                 format: '{point.y}'
        //             }
        //         }
        //     },
        //
        //     tooltip: {
        //         headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        //         pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> total<br/>'
        //     },
        //
        //     series: [{
        //         name: 'Respuestas',
        //         colorByPoint: true,
        //         data: [{
        //             name: 'Listdropdown1',
        //             y: 1,
        //             drilldown: 'Listdropdown1'
        //         }, {
        //             name: 'Listdropdown2',
        //             y: 3,
        //             drilldown: 'Listdropdown2'
        //         }, {
        //             name: 'Listdropdown3',
        //             y: 4,
        //             drilldown: 'Listdropdown3'
        //         }]
        //     }],
        //     /*
        //     drilldown: {
        //         series: [{
        //             name: 'Microsoft Internet Explorer',
        //             id: 'Microsoft Internet Explorer',
        //             data: [
        //                 [
        //                     'v11.0',
        //                     24.13
        //                 ],
        //                 [
        //                     'v8.0',
        //                     17.2
        //                 ],
        //                 [
        //                     'v9.0',
        //                     8.11
        //                 ],
        //                 [
        //                     'v10.0',
        //                     5.33
        //                 ],
        //                 [
        //                     'v6.0',
        //                     1.06
        //                 ],
        //                 [
        //                     'v7.0',
        //                     0.5
        //                 ]
        //             ]
        //         }, {
        //             name: 'Chrome',
        //             id: 'Chrome',
        //             data: [
        //                 [
        //                     'v40.0',
        //                     5
        //                 ],
        //                 [
        //                     'v41.0',
        //                     4.32
        //                 ],
        //                 [
        //                     'v42.0',
        //                     3.68
        //                 ],
        //                 [
        //                     'v39.0',
        //                     2.96
        //                 ],
        //                 [
        //                     'v36.0',
        //                     2.53
        //                 ],
        //                 [
        //                     'v43.0',
        //                     1.45
        //                 ],
        //                 [
        //                     'v31.0',
        //                     1.24
        //                 ],
        //                 [
        //                     'v35.0',
        //                     0.85
        //                 ],
        //                 [
        //                     'v38.0',
        //                     0.6
        //                 ],
        //                 [
        //                     'v32.0',
        //                     0.55
        //                 ],
        //                 [
        //                     'v37.0',
        //                     0.38
        //                 ],
        //                 [
        //                     'v33.0',
        //                     0.19
        //                 ],
        //                 [
        //                     'v34.0',
        //                     0.14
        //                 ],
        //                 [
        //                     'v30.0',
        //                     0.14
        //                 ]
        //             ]
        //         }, {
        //             name: 'Firefox',
        //             id: 'Firefox',
        //             data: [
        //                 [
        //                     'v35',
        //                     2.76
        //                 ],
        //                 [
        //                     'v36',
        //                     2.32
        //                 ],
        //                 [
        //                     'v37',
        //                     2.31
        //                 ],
        //                 [
        //                     'v34',
        //                     1.27
        //                 ],
        //                 [
        //                     'v38',
        //                     1.02
        //                 ],
        //                 [
        //                     'v31',
        //                     0.33
        //                 ],
        //                 [
        //                     'v33',
        //                     0.22
        //                 ],
        //                 [
        //                     'v32',
        //                     0.15
        //                 ]
        //             ]
        //         }, {
        //             name: 'Safari',
        //             id: 'Safari',
        //             data: [
        //                 [
        //                     'v8.0',
        //                     2.56
        //                 ],
        //                 [
        //                     'v7.1',
        //                     0.77
        //                 ],
        //                 [
        //                     'v5.1',
        //                     0.42
        //                 ],
        //                 [
        //                     'v5.0',
        //                     0.3
        //                 ],
        //                 [
        //                     'v6.1',
        //                     0.29
        //                 ],
        //                 [
        //                     'v7.0',
        //                     0.26
        //                 ],
        //                 [
        //                     'v6.2',
        //                     0.17
        //                 ]
        //             ]
        //         }, {
        //             name: 'Opera',
        //             id: 'Opera',
        //             data: [
        //                 [
        //                     'v12.x',
        //                     0.34
        //                 ],
        //                 [
        //                     'v28',
        //                     0.24
        //                 ],
        //                 [
        //                     'v27',
        //                     0.17
        //                 ],
        //                 [
        //                     'v29',
        //                     0.16
        //                 ]
        //             ]
        //         }]
        //     }
        //     */
        // });
    };

    this.buildChart = function(element , data ){

        console.log( data );

        var chartData = [];
        for(var key in data){
            if(data.hasOwnProperty(key)){
                chartData.push(data[key]);
            }
        }

        console.log( chartData );

        $('#'+element).highcharts({
            chart: {
                type: 'column'
            },
            title: {
                /*text: 'Browser market shares. January, 2015 to May, 2015'*/
                text:""
            },
            subtitle: {
                /*text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'*/
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Total'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> total<br/>'
            },
            series: [{
                name: 'Respuestas',
                colorByPoint: true,
                data: chartData
            }]
        });
    };

    $("document").ready(function(){
        that.init();
    });
};