<div class="row">
    <article class="col-md-12 col-xs-12">
        <div class="jarviswidget jarviswidget-color-darken"
             id="wid-id-16"
             data-widget-sortable="false"
             data-widget-colorbutton="false"
             data-widget-togglebutton="false"
             data-widget-editbutton="false"
             data-widget-fullscreenbutton="false"
             data-widget-deletebutton="false" style="" role="widget">

            <header role="heading"
                    class="displayBlock">
                <span class="widget-icon"></span>
                <h2>
                </h2>
                <div class="widget-toolbar" role="menu">
                </div>
                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
            </header>
            <div role="content">
                <div class="widget-body no-padding smart-form">

                    <fieldset>
                        <section>
                            <h6>
                                <strong>Página de bienvenida</strong>
                            </h6>
                        </section>
                    </fieldset>

                    <fieldset>

                        <section class="col col-5">
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle" checked>
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mostrar página
                                </strong>
                            </label>
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle" checked>
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mensaje
                                </strong>
                            </label>
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle">
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mostrar fecha
                                </strong>
                            </label>
                        </section>

                    </fieldset>

                    <fieldset>
                        <section>
                            <h6>
                                <strong>Página de despedida</strong>
                            </h6>
                        </section>
                    </fieldset>

                    <fieldset>

                        <section class="col col-5">
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle" checked>
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mostrar página
                                </strong>
                            </label>
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle" checked>
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mensaje
                                </strong>
                            </label>
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle">
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mostrar fecha
                                </strong>
                            </label>
                        </section>

                    </fieldset>

                    <fieldset>
                        <section>
                            <h6>
                                <strong>Preguntas</strong>
                            </h6>
                        </section>
                    </fieldset>

                    <fieldset>

                        <section class="col col-5">
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle" checked>
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mostrar nombre del grupo
                                </strong>
                            </label>
                            <label class="toggle">
                                <input type="checkbox" name="checkbox-toggle" checked>
                                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                                <strong>
                                    Mostrar número de pregunta
                                </strong>
                            </label>
                        </section>

                    </fieldset>

                </div>
            </div>
        </div>
    </article>
</div>