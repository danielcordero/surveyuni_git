<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $blacklisted
 * @property int $created_by
 * @property string $created
 * @property string $modified
 */
class Participant extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'email', 'blacklisted','sent','remindersent','remindercount','completed','validfrom','validuntil','created_by', 'created', 'modified'];

    public function surveys() {
        return $this->belongsToMany('App\Survey',
                                    'survey_participant',
                                    'survey_id',
                                    'participant_id')
                    ->withPivot("id","firstname","lastname","sent","remindersent","remindercount","completed",'token','validfrom','validuntil','customFields');
    }

}
