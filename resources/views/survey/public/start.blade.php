<?php
    use \App\CustomLib\Services\SurveyPublic;
    $surveyPublic = new SurveyPublic;
?>
@extends('survey.public.layout')
@section('title',$viewData['config']['survey']->title)
@section('content')
    <form action="{{ route('public.index',['surveyId'=> $viewData['config']['survey']->id ]) }}" method="post">

        <div class="row">
            <div class="col-md-12 text-center titleSurvey">
                <h1>
                    {!! $viewData['config']['survey']->title !!}
                </h1>
            </div>
            <div class="col-md-12 text-center">
                {!! $viewData['config']['survey']->description !!}
            </div>
            <div class="col-md-12">
                {!! $viewData['config']['survey']->welcometext !!}
            </div>

            @if(!empty($viewData['config']['survey']->showxquestions)
                && $viewData['config']['survey']->showxquestions == "Y")
                <div class="col-md-12">
                    <h3 class="text-center amountQuestions">
                        <span class="label label-primary">
                            Hay {{count($viewData['config']['questionsIds'])}}
                            pregunta<?php if(count($viewData['config']['questionsIds']) > 1) echo "s" ; ?>  en está encuesta.
                        </span>
                    </h3>
                </div>
            @endif

            @if(!empty($viewData['config']['survey']->askifwantfollowsurvey)
                && $viewData['config']['survey']->askifwantfollowsurvey == "Y")
                <br>
                <br>
                <div class="row">
                    <br>
                    <div class="col-md-12">
                        <h3 class="text-center">
                            <strong>
                                ¿Doy mi consentimiento?
                            </strong>

                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default  btn-lg">
                                    <input type="radio" name="askifwantfollowsurvey" value="yes" data-bv-field="yesNo" required>
                                    <strong>
                                        Si
                                    </strong>
                                </label>
                                <label class="btn btn-default  btn-lg">
                                    <input type="radio" name="askifwantfollowsurvey" value="no" data-bv-field="yesNo">
                                    <strong>
                                        No
                                    </strong>
                                </label>
                            </div>
                        </h3>
                    </div>
                </div>
            @endif

        </div>
        <!-- PRESENT THE NAVIGATOR -->
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 text-right">
                <button type="submit"
                        name="currentView"
                        value="startview"
                        class="btn btn-danger btn-lg btnNext">
                    Siguiente <i class="glyphicon glyphicon-chevron-right"></i>
                </button>
            </div>
        </div>

        <input name="survey_id"
               type="hidden"
               value="<?php echo $viewData['config']['survey']->id; ?>">

        <!--<input name="current_group"
               type="hidden"
               value="0">-->
    </form>
@stop