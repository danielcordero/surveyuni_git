@extends('layouts.app')
@section('content')

    @include('security.elements.newEdit')
    @push('scripts')
        <script src="<?php echo url('/') ?>/js/modules/UserModule.js"></script>
    @endpush
@stop