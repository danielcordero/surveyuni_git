<?php
    use \App\CustomLib\Services\ParticipantService;

?>
@extends('layouts.app')
@section('content')
    @include('elements.modals.confirm')
    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark">
                    <span class="glyphicon glyphicon-stats"></span>
                    <strong>
                        Respuestas de la encuesta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">

        <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        @include('reports.statistics.elements.topButtons')
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            <span class="glyphicon glyphicon-stats"></span>
                                            Estadísticas
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            @push('scripts')
                            <script src="<?php echo url('/') ?>/js/plugin/highcharts/highstock.js"></script>
                                <!--<script src="<?php //echo url('/') ?>/js/plugin/highcharts/exporting.js"></script>-->
                                <!--<script src="<?php //echo url('/') ?>/js/plugin/highcharts/data.js" ></script>-->
                            <script src="<?php echo url('/') ?>/js/modules/ReportModule.js" ></script>
                            @endpush

                            <?php
                                foreach ($viewData['survey']->groups as $group){
                            ?>
                                <div class="col-sm-12">
                                    <div class="inbox-info-bar inbox-info-bar-yellowalternative">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3>
                                                    <?php
                                                        echo strip_tags($group->title);
                                                    ?>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    foreach ($group->questions as $question){
                                    //Question array type
                                    //$questionsArray = ['A','A10','A5','AN','AYN','AISD','AT','AC'];
                                    $questionsArray = ['A','A10','A5','AYN','AISD','AC','AN'];
                                    if(!in_array($question->type, $questionsArray)){
                                        $result = ParticipantService::processQuestion($question, $viewData['participants']);
                                        //Questions without chart
                                        $questionsWithoutChart = ['TFS','TFB','TFP','TS','DT','N','R','NM','AT'];
                                        $widgetData = new \stdClass();
                                        $widgetData->elementId = $question->id;
                                        $widgetData->titleElement = $question->title;
                                        //$widgetData->subTitleElement = $question->title;
                                ?>
                                    @include('reports.statistics.elements.widget')
                                    <!--
                                       <article class="col-md-4">
                                        <div class="jarviswidget"
                                             id="barChartwidget<?php //echo $question->id; ?>"
                                             data-widget-colorbutton="false"
                                             data-widget-fullscreenbutton="false"
                                             data-widget-editbutton="false"
                                             data-widget-sortable="false"
                                             data-widget-deletebutton="false">
                                            <header>
                                                <h2>
                                                    <strong>
                                                        <?php
                                //echo strip_tags($question->title);
                                ?>
                                    </strong>
                                </h2>
                            </header>
                            <div>
                                <div class="jarviswidget-editbox">
                                    <input class="form-control" type="text">
                                </div>
                                <div class="widget-body">
<?php
                                //if(in_array($question->type,$questionsWithoutChart)){
                                ?>
                                    <div class="alert alert-warning"
                                         style="margin-bottom: 363px;">
                                        No hay un gráfico simple para este tipo de pregunta
                                    </div>
<?php
                                //} else {
                                ?>
                                    <div id="barChart<?php //echo $question->id; ?>"></div>
                                                    <?php
                                //}
                                ?>

                                    </div>
                                </div>
                            </div>
                        </article>
-->

                                    @if(!in_array($question->type,$questionsWithoutChart))
                                        @push('scripts')
                                        <script>
                                            ReportModule.buildChart('barChart<?php echo $question->id; ?>',<?php echo json_encode($result); ?>);
                                        </script>
                                        @endpush
                                    @endif
                                <?php
                                    } else {
                                        $configQuestion = json_decode($question->additionalData, true);

                                        //Only for question X*Y
                                        $questionXY = ["AN"];
                                        if(in_array($question->type,$questionXY)){
                                            $loopElementsX = !empty($configQuestion['subQuestionsR']) ? $configQuestion['subQuestionsR'] : [];
                                            $loopElementsY = !empty($configQuestion['subQuestionsC']) ? $configQuestion['subQuestionsC'] : [];
                                            foreach($loopElementsX as $keyX => $valueX){
                                                foreach($loopElementsY as $keyY => $valueY){
                                                    $result = ParticipantService::processQuestion($question, $viewData['participants'], $keyX.":".$keyY);
                                                    $widgetData = new \stdClass();
                                                    $widgetData->elementId = $keyX."_".$keyY;
                                                    $widgetData->titleElement = $question->title;
                                                    $widgetData->subTitleElement = "[".$valueX."]" . "[".$valueY."]";
                                                    ?>
                                                        @include('reports.statistics.elements.widget')
                                                        @push('scripts')
                                                            <script>
                                                                ReportModule.buildChart('barChart<?php echo $widgetData->elementId; ?>',<?php echo json_encode($result); ?>);
                                                            </script>
                                                        @endpush
                                                    <?php

                                                }
                                         }

                                        } else {
                                            //subQuestions
                                            $loopElements = !empty($configQuestion['subQuestionsR']) ? $configQuestion['subQuestionsR'] : $configQuestion['subQuestions'];
                                            //subQuestionsC
                                            if($question->type == "AC"){
                                                $loopElements = !empty($configQuestion['subQuestionsC']) ? $configQuestion['subQuestionsC'] : [];
                                            }

                                            foreach($loopElements as $key => $value){
                                                $result = ParticipantService::processQuestion($question, $viewData['participants'], $key);
                                                $widgetData = new \stdClass();
                                                $widgetData->elementId = $question->id.'_'.$key;
                                                $widgetData->titleElement = $question->title;
                                                $widgetData->subTitleElement = $value;

                                            ?>
                                                @include('reports.statistics.elements.widget')
                                                @push('scripts')
                                                    <script>
                                                        ReportModule.buildChart('barChart<?php echo $question->id.'_'.$key; ?>',<?php echo json_encode($result); ?>);
                                                    </script>
                                                @endpush
                                        <?php
                                            }
                                        }
                                    }
                                }
                                ?>
                            <?php
                                }
                            ?>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
@stop