<?php
//    echo "<pre>";
//    print_r($resultQuestion['keepAnswered']);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- yes_no -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <section>
                        <fieldset>
                            <div class="form-group">
                                <div class="col-md-12 col-xs-12">
                                    <br>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default <?php
                                        if(!empty($resultQuestion['keepAnswered'])){
                                            if($resultQuestion['keepAnswered'] == 'yes'){
                                                echo "active";
                                            }
                                        }
                                        ?> btn-lg">
                                            <input type="radio"
                                                   name="question_<?php echo $question->id; ?>[answers]"
                                                   value="yes"
                                                   data-bv-field="yesNo" <?php
                                                if(!empty($resultQuestion['keepAnswered'])){
                                                    if($resultQuestion['keepAnswered'] == 'yes'){
                                                        echo "checked";
                                                    }
                                                }
                                                ?>>
                                            <strong>
                                                Si
                                            </strong>
                                             </label>
                                        <label class="btn btn-default <?php
                                        if(!empty($resultQuestion['keepAnswered'])){
                                            if($resultQuestion['keepAnswered'] == 'no'){
                                                echo "active";
                                            }
                                        }
                                        ?> btn-lg">
                                            <input type="radio"
                                                   name="question_<?php echo $question->id; ?>[answers]"
                                                   value="no"
                                                   data-bv-field="yesNo" <?php
                                                if(!empty($resultQuestion['keepAnswered'])){
                                                    if($resultQuestion['keepAnswered'] == 'no'){
                                                        echo "checked";
                                                    }
                                                }
                                                ?>>
                                            <strong>
                                                No
                                            </strong> </label>
                                        <?php
                                        if(!empty($question->mandatory) && $question->mandatory == "N"){
                                        ?>
                                        <label class="btn btn-default <?php
                                        if(!empty($resultQuestion)){
                                            if(!empty($resultQuestion['keepAnswered'])){
                                                if($resultQuestion['keepAnswered'] == 'notanswer'){
                                                    echo "active";
                                                }
                                            }
                                        } else {
                                            echo "active";
                                        }
                                        ?> btn-lg">
                                            <input type="radio"
                                                   name="question_<?php echo $question->id; ?>[answers]"
                                                   value="notanswer"
                                                   data-bv-field="yesNo"
                                            <?php
                                                if(!empty($resultQuestion)){
                                                    if(!empty($resultQuestion['keepAnswered'])){
                                                        if($resultQuestion['keepAnswered'] == 'notanswer'){
                                                            echo "checked";
                                                        }
                                                    }
                                                } else {
                                                    echo "checked";
                                                }
                                                ?>>
                                            <strong> Sin respuesta </strong>
                                        </label>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>


                            </div>
                        </fieldset>
                    </section>

                </div>
            @include('survey.public.questions.littlepartial.help')
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>