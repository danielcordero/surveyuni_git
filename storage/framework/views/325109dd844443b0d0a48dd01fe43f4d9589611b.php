<?php
    use \App\CustomLib\Services\ParticipantService;
    use \App\CustomLib\Services\SurveyService;
    use \App\CustomLib\Helper\HelperServices;
    $participantService = new ParticipantService();
    //$surveyService = new SurveyService();
?>

<?php
    //$numbersDays = HelperServices::getDaysBetweenTwoDates($viewData['survey']->expiresdate);
//    echo $viewData['survey']->expiresdate;
//    echo "<br>";
//    echo $numbersDays;
//    exit;
?>


<?php $__env->startSection('content'); ?>

    <?php $__env->startPush('css'); ?>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <?php $__env->stopPush(); ?>

    <?php echo $__env->make('elements.modals.confirm', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <div class="modal fade"
         id="mdConfigDate"
         tabindex="-1"
         role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title text-center" id="myModalLabel">
                        <strong>
                            Configurar fecha de caducidad <span class="glyphicon glyphicon-calendar"></span>
                        </strong>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <form action="">
                                <div class="row">
                                    <label class="col-sm-5 control-label" for="startdate">
                                        <strong>
                                            Inicio de la encuesta:
                                        </strong>
                                        <!--Start date/time:-->
                                    </label>
                                    <div class="col-sm-4 has-feedback">
                                        <div id="startdate_datetimepicker" class="input-group date">
                                            <input id="startdate"
                                                   class="form-control"
                                                   type="text"
                                                   name="startdate"
                                                   readonly="" value="<?php echo e($viewData['survey']->startdate); ?>">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-2 has-feedback">
                                        <a class="btn btn-info btnClearDate" data-type-date="startdate" href="javascript:void(0);">
                                            Limpiar
                                        </a>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <label class="col-sm-5 control-label" for="expires">
                                        <strong>
                                            Fecha de caducidad:
                                        </strong>
                                    </label>
                                    <div class="col-sm-4 has-feedback">
                                        <div id="expires_datetimepicker" class="input-group date">
                                            <input id="expires"
                                                   class="form-control"
                                                   type="text"
                                                   value="<?php echo e($viewData['survey']->expiresdate); ?>"
                                                   name="expires" readonly="">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 has-feedback">
                                        <a class="btn btn-info btnClearDate" data-type-date="expires" href="javascript:void(0);">
                                            Limpiar
                                        </a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary btn-ok" id="btnSaveDateConfig">
                        <i class="fa fa-save"></i>&nbsp;Guardar
                    </a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancelar
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Gestionar participantes de la encuesta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <?php echo $__env->make('participant.elements.topBottons', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                        <div class="inbox-info-bar">
                            <div class="row"></div>
                        </div>
                        <br>

                        <div class="row">

                            <div class="col-md-12">

                                <?php if(empty($viewData['survey']->expiresdate)): ?>
                                    <div class="alert alert-info">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <h3 class="no-margin text-center">
                                                    <strong>
                                                        Está encuesta no tiene fecha de caducidad
                                                    </strong>
                                                    <button data-toggle="modal" data-target="#mdConfigDate" class="btn btn-primary " role="button"
                                                            rel="tooltip" data-placement="top" data-original-title="Configurar fecha de caducidad">
                                                        Configurar fecha
                                                        <i class="fa fa-gear fa-spin"></i>
                                                    </button>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <?php $numbersDays = HelperServices::getDaysBetweenTwoDates($viewData['survey']->expiresdate); ?>
                                    <div class="alert alert-<?php echo empty($numbersDays) || $numbersDays < 0 ? "danger" : "success";  ?>">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <h3 class="no-margin text-center">
                                                    <strong>
                                                        <?php
                                                        if(empty($numbersDays) || $numbersDays < 0){
                                                        echo "Está encuesta ha expirado";
                                                        } else {
                                                        echo "Queda";
                                                        echo $numbersDays == 1 ? " " : "n ";
                                                        echo $numbersDays;
                                                        echo $numbersDays == 1 ? " día" : " días";
                                                        echo " para que expire está encuesta";
                                                        }
                                                        ?>
                                                    </strong>
                                                    <button data-toggle="modal" data-target="#mdConfigDate" class="btn btn-primary " role="button"
                                                            rel="tooltip" data-placement="top" data-original-title="Configurar fecha de caducidad">
                                                        Configurar fecha
                                                        <i class="fa fa-gear fa-spin"></i>
                                                    </button>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="col-md-12">

                                <div class="jarviswidget"
                                     data-widget-editbutton="false"
                                     data-widget-custombutton="false"
                                     data-widget-deletebutton="false"
                                     data-widget-sortable="false"
                                     data-widget-collapsed="true"
                                     data-widget-fullscreenbutton="false"
                                     data-widget-attstyle="jarviswidget-color-greenDark">

                                    <header role="heading">
                                        <h2>
                                            <strong>
                                                Detalle de la encuesta
                                            </strong>
                                            <span class="glyphicon glyphicon-file"></span>
                                        </h2>
                                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                    </header>

                                    <!-- widget div-->
                                    <div role="content">

                                        <!-- widget edit box -->
                                        <div class="jarviswidget-editbox">
                                            <!-- This area used as dropdown edit box -->
                                            <input class="form-control" type="text">
                                        </div>
                                        <!-- end widget edit box -->

                                        <!-- widget content -->
                                        <div class="widget-body">

                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                Titulo
                                                            </strong>
                                                        </td>
                                                        <td>
                                                            <strong>
                                                                <?php echo e($viewData['survey']->title); ?>

                                                            </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                Fecha de inicio
                                                            </strong>
                                                        </td>
                                                        <td>
                                                            <strong>
                                                                <?php echo e($viewData['survey']->startdate); ?>

                                                            </strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                Fecha de caducidad
                                                            </strong>
                                                        </td>
                                                        <td>
                                                            <strong>
                                                                <?php echo e($viewData['survey']->expiresdate); ?>

                                                            </strong>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>

                            </div>

                            <div class="col-md-12">
                                <div class="jarviswidget jarviswidget-color-green"
                                     data-widget-editbutton="false"
                                     data-widget-custombutton="false"
                                     data-widget-deletebutton="false"
                                     data-widget-sortable="false"
                                     data-widget-collapsed="false"
                                     data-widget-fullscreenbutton="false"
                                     data-widget-attstyle="jarviswidget-color-greenDark">

                                    <header role="heading">
                                        <h2>
                                            <strong>
                                                Filtro de participantes
                                            </strong>
                                            <i class="fa fa-filter"></i>
                                        </h2>
                                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                                    </header>

                                    <!-- widget div-->
                                    <div role="content">

                                        <div class="widget-body no-padding">
                                            <form id="frmFilterparticipante"
                                                  class="smart-form"
                                                  method="GET"
                                                  action="<?php echo e(route('participants.listAll',$viewData['survey']->id)); ?>">
                                                <fieldset>
                                                    <div class="row">
                                                        <!--<section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Participante Id
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input type="number"
                                                                       name="fparticipantid"
                                                                       placeholder="Separado por coma"
                                                                       value="<?php echo e(request()->input('fparticipantid')); ?>">
                                                            </label>
                                                        </section>-->
                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Primer nombre
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input type="text"
                                                                       name="ffirstname"
                                                                       placeholder="Primer nombre"
                                                                       value="<?php echo e(request()->input('ffirstname')); ?>">
                                                            </label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Apellido
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input type="text"
                                                                       name="flastname"
                                                                       placeholder="Apellido"
                                                                       value="<?php echo e(request()->input('flastname')); ?>">
                                                            </label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Correo
                                                                </strong>
                                                            </label>
                                                            <label class="input">
                                                                <input type="email"
                                                                       name="fparticipantemail"
                                                                       placeholder="Correo del participante"
                                                                       value="<?php echo e(request()->input('fparticipantemail')); ?>">
                                                            </label>
                                                        </section>

                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Invitación
                                                                </strong>
                                                            </label>
                                                            <?php
                                                                $typeinvitation = request()->input('typeinvitation');
                                                            ?>
                                                            <label class="select">
                                                                <select name="typeinvitation">
                                                                    <option value=""<?php if(empty($typeinvitation)) echo "selected";  ?>>Elija una opción</option>
                                                                    <option value="N" <?php if(!empty($typeinvitation) && $typeinvitation == "N") echo "selected"; ?>>No enviada</option>
                                                                    <option value="Y" <?php if(!empty($typeinvitation) && $typeinvitation == "Y") echo "selected"; ?>>Enviada</option>
                                                                </select> <i></i> </label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Recordatorio
                                                                </strong>
                                                            </label>
                                                            <?php
                                                                $typeremind = request()->input('typeremind');
                                                            ?>
                                                            <label class="select">
                                                                <select name="typeremind">
                                                                    <option value=""<?php if(empty($typeremind)) echo "selected";  ?>>Elija una opción</option>
                                                                    <option value="N" <?php if(!empty($typeremind) && $typeremind == "N") echo "selected"; ?>>No enviada</option>
                                                                    <option value="Y" <?php if(!empty($typeremind) && $typeremind == "Y") echo "selected"; ?>>Enviada</option>
                                                                </select> <i></i> </label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Estado
                                                                </strong>
                                                            </label>
                                                            <?php
                                                            $fStatus = request()->input('fStatus');
                                                            ?>
                                                            <label class="select">
                                                                <select name="fStatus">

                                                                    <option value=""<?php if(empty($fStatus)) echo "selected";  ?>>Elija una opción</option>
                                                                    <option value="Y" <?php if(!empty($fStatus) && $fStatus == "Y") echo "selected"; ?>>Completada</option>
                                                                    <option value="N" <?php if(!empty($fStatus) && $fStatus == "N") echo "selected"; ?>>No completada</option>

                                                                </select> <i></i> </label>
                                                        </section>

                                                        <!--<section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Estado
                                                                </strong>
                                                            </label>
                                                            <label class="select">
                                                                <select name="fStatus">
                                                                    <option value="" selected>Elija una opción</option>
                                                                    <option value="N">Inactiva</option>
                                                                    <option value="Y">Activa</option>
                                                                </select> <i></i> </label>
                                                        </section>
                                                        <section class="col col-2">
                                                            <label class="label">
                                                                <strong>
                                                                    Fecha de creación
                                                                </strong>
                                                            </label>

                                                            <label class="input"> <i class="icon-append fa fa-calendar"></i>
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text"
                                                                       name="fdateCrated"
                                                                       id="fdateCrated"
                                                                       placeholder="Fecha de creación"
                                                                       value="<?php echo e(request()->input('fdateCrated')); ?>"
                                                                       readonly>
                                                            </label>
                                                        </section>
                                                        -->
                                                    </div>
                                                    <!--<div class="row"></div>-->
                                                </fieldset>

                                                <footer class="">
                                                    <button type="submit" class="btn btn-primary pull-left">
                                                        <i class="fa fa-search"></i>
                                                        Mostrar resultados
                                                    </button>
                                                    <button
                                                            id="btnResetForm"
                                                            type="button"
                                                            class="btn btn-default pull-left bg-color-greenDark txt-color-white">
                                                        Limpiar formulario
                                                    </button>



                                                </footer>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <form id="fmActionTableAll"
                                  action="<?php echo e(route('participants.removeAll',[$viewData['survey']->id])); ?>"
                                  method="POST">
                                <?php echo e(method_field('DELETE')); ?>

                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <input type="hidden" id="idstodeleteAll" name="idstodeleteAll">
                                <input type="submit"
                                       value="send"
                                       class="hideElement" />
                            </form>

                            <form id="fmActionTable"
                                  action="<?php echo e(route('participants.removeSurvey',[$viewData['survey']->id])); ?>"
                                  method="POST">
                                <?php echo e(method_field('DELETE')); ?>

                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <input type="hidden" id="idstodelete" name="idstodelete">
                                <input type="submit"
                                       value="send"
                                       class="hideElement" />
                            </form>

                            <form id="fmSendSomeInvitation"
                                  action="<?php echo e(route('participants.sendSomeInvitation',[$viewData['survey']->id])); ?>"
                                  method="POST">
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <input type="hidden" id="idsInvitations" name="idsInvitations">
                                <input type="hidden" name="surveyId" value="<?php echo $viewData['survey']->id; ?>">
                                <input type="submit"
                                       value="send"
                                       class="hideElement" />
                            </form>

                            <form id="fmSendSomeRemainder"
                                  action="<?php echo e(route('participants.sendSomeRemainder',[$viewData['survey']->id])); ?>"
                                  method="POST">
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <input type="hidden" id="idsRemainder" name="idsRemainder">
                                <input type="hidden" name="surveyId" value="<?php echo $viewData['survey']->id; ?>">
                                <input type="submit"
                                       value="send"
                                       class="hideElement" />
                            </form>

                                <div class="col-sm-12">
                                    <label class="select">
                                        <div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                                <i class="fa fa-gear"></i>
                                                Seleccione participantes

                                                <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <small>Correos:</small>
                                                <li>
                                                    <a href="#" class="executeAction sendSomeEmails"
                                                       data-execute-action="email_invitations">
                                                        <i class="fa fa-envelope"></i>
                                                        Enviar correo de invitación
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="executeAction sendSomeEmails"
                                                       data-execute-action="email_reminder">
                                                        <span class="fa fa-envelope-o"></span>
                                                        Enviar correo de recordatorio
                                                    </a>
                                                </li>
                                                <li role="separator" class="divider"></li>
                                                <li>
                                                    <a href="#"
                                                       class="btn btn-danger txt-color-white executeAction btnRemoveParticipants"
                                                       data-remove-participant="several"
                                                       data-execute-action="delete_participants">
                                                        <i class="fa fa-trash-o"></i>
                                                        Eliminar participantes
                                                    </a>
                                                </li>
                                            </ul>



                                        </div>

                                    </label>
                                    <a href="#"
                                       class="btn bg-color-red txt-color-white pull-right btnRemoveParticipants"
                                       data-remove-participant="all"
                                       data-execute-action="delete_participants">
                                        <i class="fa fa-trash-o"></i>
                                        Eliminar todos los participantes
                                    </a>
                                </div>
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table id="participan"
                                               class="table table-striped table-bordered table-hover dt_table"
                                               width="100%">
                                            <thead>
                                            <tr>
                                                <th><input name="select_all"
                                                           value="1"
                                                           type="checkbox"></th>

                                                <th data-hide="phone,tablet">
                                                    <i class="fa fa-gear"></i>
                                                    Acciones
                                                </th>
                                                <th data-hide="phone">
                                                    Id
                                                </th>
                                                <th data-class="expand">
                                                    <i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>
                                                     Nombre
                                                </th>
                                                <th data-hide="phone">
                                                    <i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>
                                                    Apellido
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    <span class="fa fa-envelope-o"></span>
                                                    Correo electronico
                                                </th>

                                                <th data-hide="phone,tablet">
                                                    Token
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    Invitación enviada?
                                                </th>
                                                <th data-hide="phone,tablet">
                                                    Recordatorio enviado?
                                                </th>
                                                <!--
                                                <th data-hide="phone,tablet">
                                                    Contador de recordatorio
                                                </th>
                                                -->
                                                <th data-hide="phone,tablet">
                                                    Completada
                                                </th>

                                                <th data-hide="phone,tablet">
                                                    Porcentaje de avance
                                                </th>

                                                <?php
                                                $surveyInfo = SurveyService::getInfoBydId($viewData['survey']->id);
                                                if(!empty($surveyInfo->customFields)){
                                                    foreach($surveyInfo->customFields as $field){
                                                ?>
                                                    <th data-hide="phone,tablet">
                                                        <?php
                                                            echo $field->namefield;
                                                        ?>
                                                    </th>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach($viewData['participants'] as $participant){
                                                $participant = ParticipantService::prepareDataToList($participant);
                                                $currentParticipanId = !empty($participant->pivot->id) ? $participant->pivot->id : $participant->id;
                                            ?>
                                            <tr role="row" class="odd">
                                                <td>
                                                </td>

                                                <td>
                                                    <?php if(empty($participant->completed)): ?>
                                                        <!--<a href="#"
                                                           class="btn btn-info btn-xs"
                                                           role="button"
                                                           data-toggle="tooltip"
                                                           data-placement= "top"
                                                           title="Editar ">
                                                            <span class="fa fa-envelope-o"></span>
                                                        </a>-->
                                                    <?php endif; ?>

                                                    <button onclick=" AddEditParticipant.setCurrentModalId(<?php echo e($currentParticipanId); ?>) "
                                                            data-toggle="modal"
                                                            data-target="#mdAddEdit_<?php echo e($currentParticipanId); ?>"
                                                            class="btn btn-info btn-xs"
                                                            role="button"
                                                            data-placement= "top"
                                                            title="Agregar preguntas"
                                                            data-survey-participant-id="<?php echo e($currentParticipanId); ?>">
                                                        <i class="fa fa-pencil "></i>
                                                    </button>

                                                    <?php echo $__env->make('participant.elements.addEditPopup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                                                    <button
                                                            class="btn btn-danger btn-xs btnDelete btnRemoveParticipants"
                                                            role="button"
                                                            data-placement="top"
                                                            data-survey-participant-id="<?php echo e($currentParticipanId); ?>"
                                                            data-remove-participant="one"
                                                            title="Eliminar">
                                                        <i class="fa fa-trash-o">
                                                        </i>
                                                    </button>

                                                    <form
                                                            method="post"
                                                            role="form"
                                                            id="frDelete_<?php echo $currentParticipanId; ?>"
                                                          class="hideElement <?php echo "frDelete_" . $currentParticipanId; ?>"
                                                          action="<?php echo e(route('participants.removeSurveyOne',[
                                                          $viewData['survey']->id,
                                                          $currentParticipanId])); ?>">
                                                        <?php echo e(method_field('DELETE')); ?>

                                                        <div class="form-group">
                                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                        </div>
                                                        <input type="submit" value="Enviar"  />
                                                    </form>

                                                </td>

                                                <td>
                                                    <?php if(!empty($participant->pivot->id)): ?>
                                                        <?php echo e($participant->pivot->id); ?>

                                                    <?php else: ?>
                                                        <?php echo e($participant->id); ?>

                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo e($participant->firstname); ?>

                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo e($participant->lastname); ?>

                                                    </strong>
                                                </td>
                                                <td>
                                                    <strong>
                                                        <?php echo e($participant->email); ?>

                                                    </strong>
                                                </td>

                                                <td>
                                                    <strong>
                                                        <?php echo e($participant->token); ?>

                                                    </strong>
                                                </td>

                                                <td>
                                                    <strong>
                                                        <?php if(!empty($participant->invitationDate)): ?>
                                                            <?php
                                                                $invitationDate = explode(" ",$participant->invitationDate);
                                                            ?>
                                                            <?php echo e($invitationDate[0]); ?>

                                                        <?php else: ?>
                                                            <i class="fa fa-minus text-warning"></i>
                                                        <?php endif; ?>
                                                    </strong>
                                                </td>

                                                <td>
                                                    <strong>
                                                        <?php if(!empty($participant->reminderDate)): ?>
                                                            <?php
                                                            $reminderDate = explode(" ",$participant->reminderDate);
                                                            ?>
                                                            <?php echo e($reminderDate[0]); ?>

                                                        <?php else: ?>
                                                            <i class="fa fa-minus text-warning"></i>
                                                        <?php endif; ?>
                                                    </strong>
                                                </td>

                                                <!--<td>
                                                    <strong>
                                                        <?php if(!empty($participant->remindercount)): ?>
                                                            <?php echo e($participant->remindercount); ?>

                                                        <?php else: ?>
                                                            0
                                                        <?php endif; ?>
                                                    </strong>
                                                </td>-->

                                                <td>
                                                    <strong>
                                                        <?php if(!empty($participant->completedDate)): ?>
                                                            <?php
                                                            $completedDate = explode(" ",$participant->completedDate);
                                                            ?>
                                                            <?php echo e($completedDate[0]); ?>

                                                        <?php else: ?>
                                                            <i class="fa fa-minus text-warning"></i>
                                                        <?php endif; ?>
                                                    </strong>
                                                </td>

                                                <td>
                                                    <?php

                                                        $progress = ParticipantService::getProgressBarStatus($participant, $viewData['groupsIds']);

                                                    ?>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-color-blue"
                                                             aria-valuetransitiongoal="<?php echo $progress; ?>"
                                                             style="width: <?php echo $progress; ?>%;" aria-valuenow="<?php echo $progress; ?>"><?php echo $progress; ?>%</div>
                                                    </div>
                                                </td>

                                                <!--<td>
                                                    <strong>
                                                        <?php echo e($participant->validfrom); ?>

                                                    </strong>
                                                </td>

                                                <td>
                                                    <strong>
                                                        <?php echo e($participant->validuntil); ?>

                                                    </strong>
                                                </td>-->

                                                <?php
                                                if(!empty($surveyInfo->customFields)){
                                                    foreach($surveyInfo->customFields as $field){
                                                ?>
                                                    <th data-hide="phone,tablet">
                                                        <?php
                                                            if(!empty($participant->{$field->namefield})){
                                                                echo $participant->{$field->namefield};
                                                            } else {
                                                                echo "";
                                                            }
                                                        ?>
                                                    </th>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <div class="dt-toolbar-footer">
                                            <div class="col-xs-12 col-sm-3">

                                            </div>
                                            <div class="col-xs-12 col-sm-6 text-center">
                                                <div class="dataTables_info inlineBlock"
                                                     id="example_info"
                                                     role="status"
                                                     aria-live="polite">
                                                    Mostrando
                                                    <span class="txt-color-darken">
                                                        <?php echo e($viewData['participants']->firstItem()); ?>

                                                    </span> a <span class="txt-color-darken">
                                                        <?php echo e($viewData['participants']->lastItem()); ?></span> de <span class="text-primary">
                                                        <?php echo e($viewData['participants']->total()); ?></span> Registros
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">

                                                <div class="dataTables_paginate paging_simple_numbers"
                                                     id="example_paginate">
                                                    <?php
                                                    echo str_replace('/?', '?', $viewData['participants']->appends(request()->query())->links() );
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <input type="hidden"
                                   value="<?php echo e($viewData['survey']->id); ?>"
                                   name="survey_id"
                                   id="survey_id"
                                   class="hideElement" />

                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>

    <?php $__env->startPush('scripts'); ?>
    <script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/ParticipantsModule.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/AddEditParticipant.js"></script>
    <?php $__env->stopPush(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>