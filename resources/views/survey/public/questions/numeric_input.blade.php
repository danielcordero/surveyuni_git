<?php
$allQuestions = "";
//    echo "<pre>";
//    print_r($resultQuestion);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- numeric_input -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>

                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                        <div <?php if(!empty($question->full_width) && $question->full_width == "Y") {
                             ?>
                             class="col-md-12"
                             <?php
                             } else {
                             ?>
                             class="col-md-6"
                        <?php
                            }?>
                        >
                            <section>
                                <br>
                                <label class="input">
                                    <input
                                            name="question_<?php echo $question->id; ?>[answers]"
                                            type="number"
                                            class="input-sm"
                                            value="<?php if(!empty($resultQuestion['keepAnswered'])){ ?>{{$resultQuestion['keepAnswered']}}<?php } ?>">
                                </label>
                            </section>
                        </div>
                </div>
            @include('survey.public.questions.littlepartial.help')
            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>