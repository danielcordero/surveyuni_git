<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\CustomLib\Services\{UserService};

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('CheckPermission',['except'=>'createuser']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('survey.list');
    }
    
    public function adminuser(Request $request)
    {
        $perPage = 10;
        $currentUser = \Auth::user();
        //$users = User::paginate(10);

        $parameters = $request->toArray();
        if( !empty($parameters) ){
            $userService = new UserService;
            $users = $userService->filter($request);
            $users = $users->paginate($perPage);

        } else {
            $users = User::orderBy('created_at', 'DESC')->paginate($perPage);
        }

        $viewData["currentUser"] = $currentUser;
        $viewData["users"] = $users;
        return view('security.adminuser')
        ->with("viewData", $viewData);

        /*

        $perPage = 10;
        $parameters = $request->toArray();
        if( !empty($parameters) ){
            $surveyService = new SurveyService;
            $surveys = $surveyService->filter($request);
            $surveys = $surveys->paginate($perPage);
        } else {
            $surveys = Survey::orderBy('created_at', 'DESC')->paginate($perPage);
        }

        $viewData["surveys"] = $surveys;
        return view('survey.list')->with("viewData", $viewData);

         * */

    }

    public function createuser(){
        return view('security.registeruser');    
    }

    public function storeUser(Request $request){

        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ]);

        $user = User::create($request->only('email', 'name', 'password')); //Retrieving only the email and password data

        //Redirect to the users.index view and display message
        return redirect()->route('adminuser')
            ->with('flash_message',
             'User successfully added.adminuser');
    }

}
