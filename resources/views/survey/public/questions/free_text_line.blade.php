<?php
$allQuestions = "";
//    echo "<pre>";
//    print_r($resultQuestion);
//    exit;
?>
<div class="row">
    <!-- NEW WIDGET START -->
    <!-- free_text_line -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>
                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                        <fieldset>
                            <div <?php if(!empty($question->full_width) && $question->full_width == "Y") {
                                 ?>
                                 class="col-md-12"
                                 <?php
                                 } else {
                                 ?>
                                 class="col-md-6"
                            <?php
                                }?>
                            >
                                <section>
                                    <?php if($question->has_condition == "Y") {
                                    ?>
                                    @include("survey.public.questions.conditions.yes_no")
                                    <?php
                                    }?>
                                </section>
                                <section id="bodyQuestion-<?php echo $question->id; ?>" class="<?php echo $question->has_condition == "Y" ? ' hideElement ' : ''; ?>">
                                    <br>
                                    <label class="input fullWidth">
                                        <input name="question_<?php echo $question->id; ?>[answers]"
                                               type="text"
                                               class="input-sm fullWidth"
                                               value="<?php if(!empty($resultQuestion['keepAnswered'])){ ?>{{$resultQuestion['keepAnswered']}}<?php } ?>">
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                </div>
            @include('survey.public.questions.littlepartial.help')

            @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>

@push('scripts')
    <script>
        var hasCondition = {!! "'" . $question->has_condition . "'"!!}
        var questionId = {!! "'" . $question->id . "'"!!};
        if(hasCondition == 'Y'){
            surveyRuntime.handleYesNoCondition(questionId);
        }
    </script>
@endpush