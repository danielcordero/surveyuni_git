<div class="modal fade"
     id="mdConfirmClone"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center" id="myModalLabel">
                    <strong>
                        ¿Está seguro que desea clonar está encuesta?
                    </strong>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <i class="fa fa-question-circle" aria-hidden="true" style="font-size: 100px;color:#3276b1;"></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            <strong>
                                Se creará una copia de configuración de está encuesta junto con el grupo preguntas que tenga
                            </strong>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary btn-ok">
                    <i class="fa fa-files-o" aria-hidden="true"></i>&nbsp; Clonar encuesta
                </a>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->