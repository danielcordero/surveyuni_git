var ajaxRequest = {};
(function(){
    'use strict';
    //Private variable
    var that = this;
    this.badStatusCode = [422,500,404];
    this.ajax = {};
    var url = {
        ajax: globalValues.baseUrl + "/",
    };

    this.errorOnAjax = function(error){
        commonModule.notifyMessage(error.status, error.responseJSON);
    };

    this.alwaysFailorSuccess = function(){
    };

    this.registerUser = function(data, cbBeforeSend){
        var promise = $.ajax({
            method: "POST",
            url: url.ajax + "admin/storeUserAjax",
            data:data,
            dataType: 'json',
            beforeSend:cbBeforeSend
        });
        return promise.fail(this.errorOnAjax).always(this.alwaysFailorSuccess);
    };

    this.getQuestionByGroup = function(data, cbBeforeSend){

        // var promise = $.get(url.ajax + "adminApi/group/" + data.groupId + "/questions");
        // return promise.fail(this.errorOnAjax).always(this.alwaysFailorSuccess);
        var promise = $.ajax({
            method: "POST",
            url: url.ajax + "adminApi/group/" + data.groupId + "/questions",
            data:data,
            dataType: 'html',
            beforeSend:cbBeforeSend
        });
        return promise.fail(this.errorOnAjax).always(this.alwaysFailorSuccess);
    };

    this.getParticipantSurvey = function(data, cbBeforeSend){
        var promise = $.ajax({
            method: "GET",
            url: url.ajax + "api/admin/survey/" + data.surveyId + "/participants/" + data.participantId,
            data:data,
            dataType: 'json',
            beforeSend:cbBeforeSend
        });
        return promise.fail(this.errorOnAjax).always(this.alwaysFailorSuccess);
    };

    this.updateOneSurveyParticipant = function(data, cbBeforeSend){
        var promise = $.ajax({
            method: "POST",
            url: url.ajax + "api/admin/survey/participant/updateone",
            data:data,
            dataType: 'json',
            beforeSend:cbBeforeSend
        });
        return promise.fail(this.errorOnAjax).always(this.alwaysFailorSuccess);
    };

    this.saveDateConfig = function(data, cbBeforeSend){
        var promise = $.ajax({
            method: "POST",
            url: url.ajax + "adminApi/survey/dateconfig",
            data:data,
            dataType: 'json',
            beforeSend:cbBeforeSend
        });
        return promise.fail(this.errorOnAjax).always(this.alwaysFailorSuccess);
    };

    return this;

}).call(ajaxRequest);