<?php
namespace App\CustomLib\Libraries;
/**
 * This class is a copy of here
 *
 * This file contains classes implementing security manager feature.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

class CSecurityManager{

    /**
     * Generate a random ASCII string. Generates only [0-9a-zA-z_~] characters which are all
     * transparent in raw URL encoding.
     * @param integer $length length of the generated string in characters.
     * @param boolean $cryptographicallyStrong set this to require cryptographically strong randomness.
     * @return string|boolean random string or false in case it cannot be generated.
     * @since 1.1.14
     */
    public function generateRandomString($length,$cryptographicallyStrong=true)
    {
        if(($randomBytes=$this->generateRandomBytes($length+2,$cryptographicallyStrong))!==false)
            return strtr($this->substr(base64_encode($randomBytes),0,$length),array('+'=>'_','/'=>'~'));
        return false;
    }

    /**
     * Generates a string of random bytes.
     * @param integer $length number of random bytes to be generated.
     * @param boolean $cryptographicallyStrong whether to fail if a cryptographically strong
     * result cannot be generated. The method attempts to read from a cryptographically strong
     * pseudorandom number generator (CS-PRNG), see
     * {@link https://en.wikipedia.org/wiki/Cryptographically_secure_pseudorandom_number_generator#Requirements Wikipedia}.
     * However, in some runtime environments, PHP has no access to a CS-PRNG, in which case
     * the method returns false if $cryptographicallyStrong is true. When $cryptographicallyStrong is false,
     * the method always returns a pseudorandom result but may fall back to using {@link generatePseudoRandomBlock}.
     * This method does not guarantee that entropy, from sources external to the CS-PRNG, was mixed into
     * the CS-PRNG state between each successive call. The caller can therefore expect non-blocking
     * behavior, unlike, for example, reading from /dev/random on Linux, see
     * {@link http://eprint.iacr.org/2006/086.pdf Gutterman et al 2006}.
     * @return boolean|string generated random binary string or false on failure.
     * @since 1.1.14
     */
    public function generateRandomBytes($length,$cryptographicallyStrong=true)
    {
        $bytes='';
        if(function_exists('openssl_random_pseudo_bytes'))
        {
            $bytes=openssl_random_pseudo_bytes($length,$strong);
            if($this->strlen($bytes)>=$length && ($strong || !$cryptographicallyStrong))
                return $this->substr($bytes,0,$length);
        }

        if(function_exists('mcrypt_create_iv') &&
            ($bytes=mcrypt_create_iv($length, MCRYPT_DEV_URANDOM))!==false &&
            $this->strlen($bytes)>=$length)
        {
            return $this->substr($bytes,0,$length);
        }

        if(($file=@fopen('/dev/urandom','rb'))!==false &&
            ($bytes=@fread($file,$length))!==false &&
            (fclose($file) || true) &&
            $this->strlen($bytes)>=$length)
        {
            return $this->substr($bytes,0,$length);
        }

        $i=0;
        while($this->strlen($bytes)<$length &&
            ($byte=$this->generateSessionRandomBlock())!==false &&
            ++$i<3)
        {
            $bytes.=$byte;
        }
        if($this->strlen($bytes)>=$length)
            return $this->substr($bytes,0,$length);

        if ($cryptographicallyStrong)
            return false;

        while($this->strlen($bytes)<$length)
            $bytes.=$this->generatePseudoRandomBlock();
        return $this->substr($bytes,0,$length);
    }

    /**
     * Returns the length of the given string.
     * If available uses the multibyte string function mb_strlen.
     * @param string $string the string being measured for length
     * @return integer the length of the string
     */
    private function strlen($string)
    {
        return !empty($this->_mbstring) ? mb_strlen($string,'8bit') : strlen($string);
    }

    /**
     * Returns the portion of string specified by the start and length parameters.
     * If available uses the multibyte string function mb_substr
     * @param string $string the input string. Must be one character or longer.
     * @param integer $start the starting position
     * @param integer $length the desired portion length
     * @return string the extracted part of string, or FALSE on failure or an empty string.
     */
    private function substr($string,$start,$length)
    {
        return !empty($this->_mbstring) ? mb_substr($string,$start,$length,'8bit') : substr($string,$start,$length);
    }

    /**
     * Get random bytes from the system entropy source via PHP session manager.
     * @return boolean|string 20-byte random binary string or false on error.
     * @since 1.1.14
     */
    public function generateSessionRandomBlock()
    {
        ini_set('session.entropy_length',20);
        if(ini_get('session.entropy_length')!=20)
            return false;

        // These calls are (supposed to be, according to PHP manual) safe even if
        // there is already an active session for the calling script.
        @session_start();
        @session_regenerate_id();

        $bytes=session_id();
        if(!$bytes)
            return false;

        // $bytes has 20 bytes of entropy but the session manager converts the binary
        // random bytes into something readable. We have to convert that back.
        // SHA-1 should do it without losing entropy.
        return sha1($bytes,true);
    }

    /**
     * Generate a pseudo random block of data using several sources. On some systems this may be a bit
     * better than PHP's {@link mt_rand} built-in function, which is not really random.
     * @return string of 64 pseudo random bytes.
     * @since 1.1.14
     */
    public function generatePseudoRandomBlock()
    {
        $bytes='';

        if (function_exists('openssl_random_pseudo_bytes')
            && ($bytes=openssl_random_pseudo_bytes(512))!==false
            && $this->strlen($bytes)>=512)
        {
            return $this->substr($bytes,0,512);
        }

        for($i=0;$i<32;++$i)
            $bytes.=pack('S',mt_rand(0,0xffff));

        // On UNIX and UNIX-like operating systems the numerical values in `ps`, `uptime` and `iostat`
        // ought to be fairly unpredictable. Gather the non-zero digits from those.
        foreach(array('ps','uptime','iostat') as $command) {
            @exec($command,$commandResult,$retVal);
            if(is_array($commandResult) && !empty($commandResult) && $retVal==0)
                $bytes.=preg_replace('/[^1-9]/','',implode('',$commandResult));
        }

        // Gather the current time's microsecond part. Note: this is only a source of entropy on
        // the first call! If multiple calls are made, the entropy is only as much as the
        // randomness in the time between calls.
        $bytes.=$this->substr(microtime(),2,6);

        // Concatenate everything gathered, mix it with sha512. hash() is part of PHP core and
        // enabled by default but it can be disabled at compile time but we ignore that possibility here.
        return hash('sha512',$bytes,true);
    }




}