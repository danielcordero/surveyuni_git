<?php

//echo "<pre>";
//print_r($viewData);
//exit;

?>

@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Editar plantillas de correos
                    </strong>
                </h1>
            </div>
        </div>
        <!-- widget grid -->
        <section id="widget-grid" class="">
            <!-- row -->
            <div class="row">
                <!-- NEW WIDGET START -->
                <div class="col-sm-12">
                    <article class="well">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-right">
                                    <button type="button" id="btnSave"
                                            class="btn btn-primary">
                                        <i class="fa fa-save"></i> Guardar
                                    </button>
                                    <a class="btn btn-default"
                                       href="{{ route('participants.config', ['surveyId'=>$viewData['survey']->id]) }}">
                                        Cancelar
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="inbox-info-bar">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>
                                        <strong>
                                            <span class="fa fa-envelope-o"></span>
                                            Editar plantillas de correos
                                        </strong>
                                    </h3>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-12">

                                <form action="{{ route('email.updatetemplates',['surveyId'=>$viewData['survey']->id]) }}"
                                      method="post" id="frmEmailTemplates" class="">
                                    {{ csrf_field() }}
                                    <ul class="nav nav-tabs">
                                        <?php
                                        $count = 0;
                                        $state = 'active';
                                        foreach ($viewData['tagsStructure'] as $tab => $details)
                                        {
                                            echo "<li role='presentation' class='$state'><a  data-toggle='tab' href='#tab-$count'>{$details['title']}</a></li>";
                                            if($count == 0)
                                            {
                                                $state = '';
                                            }
                                            $count = $count + 1;
                                        }
                                        ?>
                                    </ul>

                                    <div class="tab-content tabsinner form-horizontal" id=''>
                                        <?php
                                        $count = 0;
                                        $active = 'active';
                                        foreach ($viewData['tagsStructure'] as $tab => $details){
                                        ?>
                                        <div id="tab-<?php echo $count; ?>" class="tab-pane fade in <?php echo empty($count) ? "active" : ""; ?>">
                                            <fieldset>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        <strong>
                                                            <?php
                                                            echo $details['subject'];
                                                            ?>

                                                        </strong>
                                                    </label>
                                                    <div class="col-md-9">
                                                        <input class="form-control"
                                                               name="<?php echo $details['field']['subject']; ?>"
                                                               id="<?php echo $details['field']['subject']; ?>"
                                                               placeholder="" value="<?php echo $viewData['surveysetting']->{$details['field']['subject']}; ?>" type="text">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        <strong>
                                                        <?php
                                                        echo $details['body'];
                                                        ?>
                                                        <!--Cuerpo de correo electrónico de invitación:-->
                                                        </strong>

                                                    </label>
                                                    <div class="col-md-9">
                                                        <label class="textarea">
                    <textarea
                            name="<?php echo $details['field']['body']; ?>"
                            id="<?php echo $details['field']['body']; ?>"
                            class="smCkEditor"><?php echo $viewData['surveysetting']->{$details['field']['body']}; ?></textarea>
                                                        </label>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">
                                                        <strong>
                                                            Acciones:
                                                        </strong>
                                                    </label>
                                                    <div class="col-md-9">
                                                    <!--<a href=" {{route("email.validatetemplate",[
                    "surveyId" => $viewData['survey']->id,
                    "sa"=>"email",
                    "type"=>$tab])}}"
                   target="dialog"
                   title="<?php echo $details['title'] ?>" class="btn btn-default">
                    Validar expresiones
                </a>-->
                                                        <?php
                                                        //                    $details['default']['body']=( $tab == 'admin_detailed_notification') ? $details['default']['body'] : HelperServices::conditionalNewlineToBreak($details['default']['body'],true) ;
                                                        ?>

                                                        <input type="button"
                                                               class="btn btn-default btn btnResetTemplate"
                                                               value="Reiniciar está plantilla"
                                                               data-value='{{ $details["default"]["body"]  }}'
                                                               data-target="<?php echo $details['field']['body']; ?>">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <?php
                                        $count = $count + 1;
                                        }
                                        ?>
                                    </div>

                                    <input type="hidden"
                                           name="survey_id"
                                           id="survey_id"
                                           value="<?php echo $viewData['survey']->id; ?>">
                                    <input type="hidden"
                                           id="surveyconfig_id"
                                           name="surveyconfig_id"
                                           value="<?php echo $viewData['surveysetting']->id; ?>">
                                </form>

                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </div>
    @push('scripts')
        <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
        <script src="<?php echo url('/') ?>/js/modules/EmailModule.js"></script>
        <script>
            EmailModule.init();
        </script>
    @endpush
@stop