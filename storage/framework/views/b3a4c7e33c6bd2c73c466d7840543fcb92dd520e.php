<div class="row">
    <!-- NEW WIDGET START -->
    <!-- list_radio -->

    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            <?php echo $__env->make("survey.public.questions.littlepartial.header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <!-- widget div-->
            <div>

                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    <?php if(!empty($resultQuestion) && !empty($resultQuestion['fail']) ): ?>
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> <?php echo e($resultQuestion['message']); ?>

                            <?php if(!empty($resultQuestion['messages'])): ?>
                                <br>
                                <?php $__currentLoopData = $resultQuestion['messages']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> <?php echo e($value); ?>

                                    <br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <fieldset>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    foreach ($config["subQuestions"] as $key => $value){
                                    ?>
                                    <label class="radio">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers]"
                                                value="<?php echo $key; ?>" <?php
                                            if(!empty($resultQuestion['keepAnswered'])){
                                                if($resultQuestion['keepAnswered'] == $key){
                                                    echo "checked";
                                                }
                                            }
                                            ?>>
                                        <i></i><strong>
                                            <?php echo $value; ?>
                                        </strong></label>
                                    <br>
                                    <?php
                                    }
                                    ?>
                                    <?php
                                    if(!empty($question->mandatory) &&
                                    $question->mandatory == 'N'){
                                    ?>
                                    <label class="radio">
                                        <input type="radio"
                                               name="question_<?php echo $question->id; ?>[answers]"
                                               value="notanswer"
                                        <?php
                                            if(!empty($resultQuestion)){
                                                if(!empty($resultQuestion['keepAnswered'])){
                                                    if($resultQuestion['keepAnswered'] == 'notanswer'){
                                                        echo "checked";
                                                    }
                                                }
                                            } else {
                                                echo "checked";
                                            }
                                            ?>>
                                        <i></i>Sin respuesta
                                    </label>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </section>

                    </fieldset>

                </div>
            <?php echo $__env->make('survey.public.questions.littlepartial.help', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php echo $__env->make('survey.public.questions.commonHidden', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- end widget content -->
            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>