<?php
$allQuestions = "";
//    echo "<pre>";
//    print_r($resultQuestion);
//    exit;
?>

<div class="row">
    <!-- NEW WIDGET START -->
    <!-- array_number -->
    <article class="col-sm-12">
        <div class="jarviswidget" id="wid-id-0">

            @include("survey.public.questions.littlepartial.header")

            <!-- widget div-->
            <div>
                <!-- widget content -->
                <div class="widget-body smart-form" id="<?php echo $question->id; ?>_question">

                    @if(!empty($resultQuestion) && !empty($resultQuestion['fail']) )
                        <div class="alert alert-danger fade in">
                            <i class="fa-fw fa fa-times"></i>
                            <strong>Error!</strong> {{$resultQuestion['message']}}
                            @if(!empty($resultQuestion['messages']))
                                <br>
                                @foreach($resultQuestion['messages'] as $key => $value)
                                    <i class="fa-fw fa fa-times"></i>
                                    <strong>Error!</strong> {{$value}}
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Fila / Columna</th>
                                <?php
                                foreach ($config["subQuestionsC"] as $key => $value){
                                ?>
                                <th class="text-center">
                                    <?php
                                    echo $value;
                                    ?>
                                </th>
                                <?php
                                }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($config["subQuestionsR"] as $key1 => $value1){
                            ?>
                            <tr>
                                <th>
                                    <span class="label <?php
                                    if(!empty($resultQuestion['withoutAnswered']) && $question->mandatory == "Y"){
                                    foreach ($config["subQuestionsC"] as $key2 => $value2){
                                            if(in_array($key1.':'.$key2, $resultQuestion['withoutAnswered'])){
                                                echo " labelCommonError ";
                                            }
                                        }
                                    }
                                    ?>">
                                        <?php
                                            echo $value1;
                                        ?>
                                    </span>
                                </th>
                                <?php
                                foreach ($config["subQuestionsC"] as $key2 => $value2){
                                $allQuestions = $allQuestions . '|' .$key1.':'.$key2;
                                ?>
                                <td class="text-center">
                                    <label class="select">
                                        <select class="input-sm"
                                                name="question_<?php echo $question->id; ?>[answers][<?php echo $key1.':'.$key2; ?>]">
                                            <option value="0">Elija una opción</option>
                                            <?php
                                                for($i = 1;$i <=10 ;$i++ ){
                                                    ?>
                                            <option value="{{$i}}" <?php
                                                if(!empty($resultQuestion['keepAnswered']) &&
                                                    !empty($resultQuestion['keepAnswered'][$key1.':'.$key2])){
                                                    if($resultQuestion['keepAnswered'][$key1.':'.$key2] == $i){
                                                        echo "selected";
                                                    }
                                                }
                                                ?>>{{$i}}</option>
                                            <?php
                                                }
                                            ?>
                                        </select><i></i>
                                    </label>
                                </td>
                                <?php
                                }
                                ?>
                            </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                @include('survey.public.questions.littlepartial.help')
                <input type="hidden"
                       name="question_<?php echo $question->id; ?>[subQuestions]"
                       id="question_<?php echo $question->id; ?>[subQuestions]"
                       value="<?php echo trim($allQuestions, '|'); ?>">
                @include('survey.public.questions.commonHidden')
                <!-- end widget content -->

            </div>
            <!-- end widget div -->

        </div>
    </article>
</div>