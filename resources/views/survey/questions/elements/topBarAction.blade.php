<?php
    $testSurveys = config('custom.testSurvey');
?>
<ul class="demo-btns">
    @if(in_array($viewData['survey']->id,$testSurveys))
        <li>
            <a href="{{ route('survey.redirectpreviewmode',[$viewData['survey']->id])}}"
               class="btn bg-color-greenDark txt-color-white" target="_blank">
                <i class="fa fa-eye"></i>
                Vista previa de la encuesta
            </a>
        </li>
    @else
        @if(!empty($viewData['survey']->active) &&
        $viewData['survey']->active == "Y")
            <li>
                <a href="{{ route('survey.savedesactivesurvey',[$viewData['survey']->id])}}"
                   class="btn btn-danger"
                   rel="tooltip"
                   data-placement="bottom" data-original-title="Detener está encuesta">
                    <i class="fa fa-stop"></i>
                    Detener está encuesta
                </a>
            </li>
        @else

            <li>
                <a href="{{ route('survey.viewactivesurvey',[$viewData['survey']->id])}}"
                   class="btn btn-primary">
                    <i class="fa fa-gear"></i>
                    <i class="fa fa-binoculars" aria-hidden="true"></i>
                    Activar encuesta
                </a>
            </li>
            <li>
                <a href="{{ route('survey.show',[$viewData['survey']->id])}}"
                   class="btn btn-primary">
                    <i class="fa fa-pencil"></i>
                    Editar encuesta
                </a>
            </li>

        @endif

        <li>
            <a href="{{ route('survey.redirectpreviewmode',[$viewData['survey']->id])}}"
               class="btn bg-color-greenDark txt-color-white" target="_blank">
                <i class="fa fa-eye"></i>
                Vista previa de la encuesta
            </a>
        </li>
        <li>
            <a href="{{ route('participants.config',[$viewData['survey']->id])}}"
               class="btn bg-color-greenDark txt-color-white">
                <i class="fa fa-user"></i>
                Participantes de la encuesta
            </a>
        </li>

        @if(!empty($viewData['survey']->active) &&
        $viewData['survey']->active == "N")
            <li>
                <button
                        data-toggle="modal"
                        data-target="#mdConfirm"
                        class="btn btn-danger btnDelete"
                        role="button"
                        data-toggle="tooltip"
                        data-placement="top"
                        data-question-id="{{ $viewData['survey']->id }}"
                        title="Eliminar">
                    <i class="fa fa-trash-o">
                    </i>
                    Eliminar encuesta
                </button>

                <form method="post" role="form" id="<?php echo "frDelete_" . $viewData['survey']->id; ?>"
                      class="hideElement <?php echo "frDelete_" . $viewData['survey']->id; ?>"
                      action="{{ route('surveyDelete',[$viewData['survey']->id])}}">
                    <!--method_field('DELETE')-->
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                    <input type="submit" value="Enviar"  />
                </form>
            </li>
        @endif

        <li>
            <a href="{{ route('surveystatistics.view',[$viewData['survey']->id])}}"
               class="btn bg-color-orange txt-color-white">
                <i class="fa fa-eye"></i>
                Ver respuestas
            </a>
        </li>

    @endif
</ul>