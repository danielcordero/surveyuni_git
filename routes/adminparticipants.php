<?php

// routes/web.php
Route::group(['prefix' => 'admin'], function () {
    //participants
    Route::get('/survey/{surveyId}/participants/viewconfig',
        'AdminSurveys\ParticipantController@viewconfig')
            ->name("participants.config");

    Route::match(['post', 'get'],'/survey/{surveyId}/participants',
        'AdminSurveys\ParticipantController@listAll')
        ->name("participants.listAll");

    Route::get('/survey/{surveyId}/participants/viewupload',
        'AdminSurveys\ParticipantController@viewupload')
        ->name("participants.viewupload");

    Route::match(['post', 'get'],'/survey/{surveyId}/participants/upload',
        'AdminSurveys\ParticipantController@uploadfile')
        ->name("participants.upload");

    Route::delete('/survey/{surveyId}/survey_participant/{survey_participant?}',
        'AdminSurveys\ParticipantController@destroySurveyParticipant')
        ->name("participants.removeSurveyOne");

    Route::delete('/survey/{surveyId}/survey_participant',
        'AdminSurveys\ParticipantController@destroySurveyParticipant')
        ->name("participants.removeSurvey");

    Route::delete('/survey/{surveyId}/survey_participant_all',
        'AdminSurveys\ParticipantController@destroyParticipantAll')
        ->name("participants.removeAll");

    Route::get('/survey/{surveyId}/participants/add',
        'AdminSurveys\ParticipantController@viewadd')
        ->name("participants.add");

    Route::post('/survey/{surveyId}/participants/addEdit',
        'AdminSurveys\ParticipantController@addEdit')
        ->name("participants.addEdit");

    Route::get('/survey/{surveyId}/participants/addcustomfields',
        'AdminSurveys\ParticipantController@viewaddcustomfields')
        ->name("participants.viewaddcustomfields");

    Route::post('/survey/{surveyId}/participants/createcustomfields',
        'AdminSurveys\ParticipantController@createcustomfields')
        ->name("participants.createcustomfields");

    Route::get('/survey/custom/replacementfields',
        'AdminSurveys\ReplacementFieldsController@index')
        ->name("replacementfields");

    Route::get('/survey/{surveyId}/participants/viewgeneratetoken',
        'AdminSurveys\ParticipantController@viewgeneratetoken')
        ->name("participants.viewgeneratetoken");

    Route::post('/survey/{surveyId}/participants/createtokens',
        'AdminSurveys\ParticipantController@createtokens')
        ->name("participants.createtokens");

    Route::post('/survey/{surveyId}/participants/sendSomeRemainder',
        'AdminSurveys\ParticipantController@sendSomeRemainder')
        ->name("participants.sendSomeRemainder");

    Route::post('/survey/{surveyId}/participants/sendSomeInvitation',
        'AdminSurveys\ParticipantController@sendSomeInvitation')
        ->name("participants.sendSomeInvitation");
});

Route::group(['prefix' => 'api/admin'], function () {
    Route::get('/survey/{surveyId}/participants/{participantId}',
        'AdminSurveys\ParticipantController@getInfo');

    Route::post('/survey/participant/updateone',
        'AdminSurveys\ParticipantController@updateone');
});