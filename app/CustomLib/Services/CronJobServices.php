<?php
/**
 * Created by PhpStorm.
 * User: cesar
 * Date: 3/30/2018
 * Time: 5:30 PM
 */
namespace App\CustomLib\Services;

use App\CustomLib\Helper\{HelperServices,Template};
use App\{Survey, SettingsGlobal};

class CronJobServices
{
    public static function sendInvetations()
    {
        $surveys = SurveyService::getSeveralByLimit();
        $surveysCronCompleted = [];
        if(!empty( $surveys )){
            $tpl = new Template();
            $surveysIds = array_column($surveys, 'id');
            for($i = 0; $i < count($surveysIds) ; $i++){

                $surveyInfo = Survey::find( $surveysIds[$i] );
                $amountSent = ParticipantService::getAmountSent( $surveysIds[$i] );
                //if the amount of participant is equal to amountsent
                //then set sent at all
                if($surveyInfo->participants->count() == $amountSent){
                    SurveyService::setAllInvitationSent( $surveysIds[$i] );
                } else {
                    $participants = ParticipantService::getUninvited($surveysIds[$i], true);
                    if(!empty($participants)){
                        $viewData['survey'] = $surveyInfo;
                        $viewData['surveysetting'] = $viewData['survey']->surveysetting;

                        foreach ($participants as $participant)
                        {
                            $messageHtml = $viewData['surveysetting']->survey_email_invite;
                            $subject = $viewData['surveysetting']->survey_email_invite_subj;
                            //Change character set, or at least try
                            //https://stackoverflow.com/questions/7979567/php-convert-any-string-to-utf-8-without-knowing-the-original-character-set-or
                            $messageHtml = iconv(mb_detect_encoding($messageHtml, mb_detect_order(), true), "UTF-8", $messageHtml);
                            $subject = iconv(mb_detect_encoding($subject, mb_detect_order(), true), "UTF-8", $subject);

                            $fieldsarray = [];
                            $fieldsarray["SURVEYNAME"] = $viewData['survey']->title;
                            $fieldsarray["SURVEYDESCRIPTION"] = strip_tags($viewData['survey']->description);
                            $fieldsarray["ADMINNAME"] = SettingsGlobal::find("siteadminname")->stg_value;
                            $fieldsarray["ADMINEMAIL"] = SettingsGlobal::find("siteadminemail")->stg_value;

                            foreach ($participant as $attribute => $value)
                            {
                                $fieldsarray[strtoupper($attribute)] = $value;
                            }

                            $fieldsarray["OPTOUTURL"]
                                = HelperServices::addQStoRoute(
                                'public.index',
                                [$surveysIds[$i]], ['token'=>$participant['token']]
                            );

                            $fieldsarray["OPTINURL"]
                                = HelperServices::addQStoRoute(
                                'public.index',
                                [$surveysIds[$i]], ['token'=>$participant['token']]
                            );

                            $fieldsarray["SURVEYURL"]
                                = HelperServices::addQStoRoute(
                                'public.index',
                                [$surveysIds[$i]],
                                ['token'=>$participant['token']]
                            );

                            foreach(array('OPTOUT', 'OPTIN', 'SURVEY') as $key)
                            {
                                $url = $fieldsarray["{$key}URL"];
                                $fieldsarray["{$key}URL"] = "<a href='{$url}'>" . htmlspecialchars($url) . '</a>';
                                $subject = str_replace("@@{$key}URL@@", $url, $subject);
                                $messageHtml = str_replace("@@{$key}URL@@", $url, $messageHtml);
                            }

                            // Include class file and instantiate.
                            $tpl->assign($fieldsarray);

                            // Parse the template file
                            $messageHtml = $tpl->display($messageHtml);
                            $parameters['emailParticipant'] = $participant['email'];
                            $parameters['messageHtml'] = $messageHtml;
                            $parameters['subject'] = $subject;

                            ParticipantService::setSendInvitation($participant['id']);

                            /*$result = EmailService::sendInvitation( $parameters );
                            if(!empty($result['success'])){
                                ParticipantService::setSendInvitation($participant['id']);
                            }*/
                        }
                    }

                    $amountSent = ParticipantService::getAmountSent( $surveysIds[$i] );
                    if($surveyInfo->participants->count() == $amountSent){
                        SurveyService::setAllInvitationSent( $surveysIds[$i] );
                    }
                }
            }
        }
    }
}