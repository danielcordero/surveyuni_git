/**
 * Created by cesar on 11/7/2017.
 */
'use strict';
var QuestionsModule = new function(){
    var that = this;

    this.init = function(){
        this.loadElement();
        this.handlerEvent();
    };

    this.handlerEvent = function(){
        var ulTypeQuestion = $("#selectQuestion");
        var slgroupsQuestion = $("#groupsQuestion");
        var questionId = $("#question_id");

        $("#btnSave").on("click", function(e){
            //$(".currentForm").trigger('submit');
            $(".currentForm").find('[type="submit"]').trigger("click");
        });

        if(ulTypeQuestion.length){
            var text = "";
            $("#selectQuestion li a").on("click", function (e) {
                e.preventDefault();
                text = $(this).text();
                $("#btnSelectQuestion").html(text + '&nbsp;<span class="caret"></span>');
                $("#questionType").val($(this).data("value"));
            });
        }

        slgroupsQuestion.on("change", function(e){
            if(this.value){
                ajaxRequest.getQuestionByGroup({groupId:this.value,questionId:questionId.val()})
                    .then(function(response){
                        if(response){
                            $("#positionQ").replaceWith(response);
                        }

                    //name="groupsQuestion" id="groupsQuestion" required
                    //(data, id, name ,classAttr, required){
                    //name="positionQ" required id="positionQ"
                });
            }
        });

        this.removeAction();
    };

    this.loadElement = function(){
        if(pageSetUp){
            pageSetUp();
        }
        if($('#groupQuestions').length){
            $('#groupQuestions').nestable({
                group : 1,
            });
        }

        this.initckEditors();
        this.loadQtip();
    };

    this.loadQtip = function(){

        $("#question_type_button .questionType")
            .each(function(index,element){
            $(element).qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: getToolTip($(element).data("value")),
                position: {
                    my : 'center right',
                    at: 'center left',
                    target: $('label[for=question_type]'),
                    viewport: $(window),
                    adjust: {
                        x: 0
                    }
                }
            });
        });

        function getToolTip( type ){
            var imgulr = globalValues.baseUrl + "/img/screeshot/all/";
            return "<img src='" + imgulr + type +".png' />";
        }

        $('.questionType').on('mouseenter', function(e){
            //alert($(this).attr('class'));
            $('.questionType').qtip('hide');
            $(this).qtip('option', 'position.target', $(this).qtip('show'));
        });

        $('.questionType').on('mouseleave', function(e){
            $(this).qtip('hide');
        });
    };

    this.initckEditors = function(){

        var ckeditorOptions = {};
        var idElement = "";
        $(".smCkEditor").each(function(index, element){
            idElement = $(element).attr("id");
            ckeditorOptions.LimeReplacementFieldsAction = "";
            ckeditorOptions.LimeReplacementFieldsType = idElement;
            $("#"+idElement).initcKeditor(ckeditorOptions);
        });
    };

    this.removeAction = function(){
        $('#mdConfirm').on('show.bs.modal', function(e) {

            var currentQuestionId = $(e.relatedTarget).data('question-id');
            //$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            $(this).find('.btn-ok').on("click", function(e){
                e.preventDefault();
                $("#frDelete_" + currentQuestionId).find('[type="submit"]').trigger("click");
            });
        });
    };

    $(document).ready(function(){
        that.init();
    });
};