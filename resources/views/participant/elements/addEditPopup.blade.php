<div class="modal fade"
     id="mdAddEdit_{{$currentParticipanId}}"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <form method="post"
                  id="frmParticipanNewEditModal_{{$currentParticipanId}}"
                  class="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title text-center" id="myModalLabel">
                    <strong>
                        Editar participante de la encuesta
                    </strong>
                </h4>
            </div>
                <div class="modal-body smart-form">
                    <div class="row">

                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">
                                        <strong>
                                            Email:
                                        </strong>
                                    </label>
                                    <label class="input">
                                        <h5>
                                            <strong>
                                                {{$participant->email}}
                                            </strong>
                                        </h5>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">
                                        <strong>
                                            Token:
                                        </strong>
                                    </label>
                                    <label class="input">
                                        <input type="text"
                                               name="ptoken"
                                               placeholder=""
                                               class="valid"
                                               value="{{$participant->token}}"
                                               disabled>
                                    </label>
                                </section>

                            </div>

                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">
                                        <strong>
                                            Primer nombre:
                                        </strong>
                                    </label>
                                    <label class="input">
                                        <input type="text"
                                               name="pfirstname"
                                               placeholder="Primer nombre"
                                                value="{{$participant->firstname}}">
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">
                                        <strong>
                                            Apellido:
                                        </strong>
                                    </label>
                                    <label class="input">
                                        <input type="text"
                                               name="plastname"
                                               placeholder="Apellido"
                                               value="{{$participant->lastname}}">
                                    </label>
                                </section>
                            </div>

                            <!--<div class="row">
                                <section class="col col-6">
                                    <label class="label">
                                        <strong>
                                            Fecha válida desde:
                                        </strong>
                                    </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-calendar"></i>
                                        <input type="text"
                                               name="pvdatefrom"
                                               id="pvdatefrom_{{$currentParticipanId}}"
                                               placeholder="Fecha válida desde"
                                               value="{{$participant->validfrom}}"
                                               readonly
                                        >
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">
                                        <strong>
                                            Fecha válida hasta:
                                        </strong>
                                    </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-calendar"></i>
                                        <input type="text"
                                               name="pvdateuntil"
                                               id="pvdateuntil_{{$currentParticipanId}}"
                                               placeholder="Fecha válida hasta"
                                               value="{{$participant->validuntil}}"
                                               readonly
                                        >
                                    </label>
                                </section>
                            </div>-->

                        </fieldset>

                        <input type="hidden"
                               value="{{$currentParticipanId}}"
                               name="survey_participant_id"
                               class="hideElement" />

                        <input type="hidden"
                               value="{{$viewData['survey']->id}}"
                               name="survey_id"
                               class="hideElement" />

                        <input type="submit"
                               value="send"
                               class="hideElement" />

                    </div>
                </div>
            <div class="modal-footer">
                <button type="submit"
                        class="btn btn-primary btn-ok">
                    Guardar cambios
                </button>
                <!--<a class="btn btn-primary btn-ok">

                </a>-->
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancelar
                </button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@push('scripts')

<script>
    $('#pvdatefrom_{{$currentParticipanId}}').datepicker({
        dateFormat : 'yy-mm-dd',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {}
    });

    $('#pvdateuntil_{{$currentParticipanId}}').datepicker({
        dateFormat : 'yy-mm-dd',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {}
    });
</script>

@endpush
