@if (Session::has('success'))

	<div class="callout callout-success alert alert-success text-center"  
		role="alert">
	    <button type="button" 
		 	class="close" data-dismiss="alert" 
		 	aria-label="Close">
		 	<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="text-center">
			{{--<strong>Success:</strong> --}}
			<strong>
				{{ Session::get('success') }}
			</strong>
		</h4>
    </div>

@endif

@if (Session::has('error'))

	<div class="alert alert-danger callout callout-danger"
		 role="alert">
		<button type="button"
				class="close" data-dismiss="alert"
				aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="text-center">
			{{--<strong>Error:</strong> --}}
			<strong>
				{{ Session::get('error') }}
			</strong>
		</h4>
	</div>

@endif

@if (count($errors) > 0)

	<div class="alert alert-danger callout callout-danger" role="alert">
		<button type="button" 
		 	class="close" data-dismiss="alert" 
		 	aria-label="Close"><span aria-hidden="true">&times;</span>
		</button>
		<strong>Errores:</strong>
		<ul>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach  
		</ul>
	</div>

@endif