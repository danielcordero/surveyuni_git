@extends('layouts.app')
@section('content')

    <!-- MAIN CONTENT -->
    <div id="content">

        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                <h1 class="page-title txt-color-blueDark"><i class="fa fa-puzzle-piece fa-fw "></i>
                    <strong>
                        Grupo de pregunta
                    </strong>
                </h1>
            </div>
        </div>

        <!-- widget grid -->
        <section id="widget-grid" class="">
            @include('survey.questions.elements.topBarBreadcrumbs')

            <!-- row -->
            <div class="row">
                <div class="col-sm-3">
                    @include('survey.questions.elements.treeGroupQuestions')
                </div>
                <!-- NEW WIDGET START -->
                <article class="col-sm-9">
                    <div class="jarviswidget jarviswidget-color-darken"
                         id="wid-id-16" data-widget-sortable="false"
                         data-widget-colorbutton="false"
                         data-widget-togglebutton="false"
                         data-widget-editbutton="false"
                         data-widget-fullscreenbutton="false"
                         data-widget-deletebutton="false" style="" role="widget">

                        <header role="heading">
                            <h2>
                                <strong>
                                    Agregar un grupo de preguntas
                                </strong>
                            </h2>
                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                        </header>

                        <!-- widget div-->
                        <div role="content">

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body smart-form">
                                @include('survey.questions.elements.groupNewEdit')
                            </div>
                            <!-- end widget content -->
                        </div>
                        <!-- end widget div -->

                    </div>
                </article>
            </div>
        </section>
    </div>


    @push('scripts')
    <script src="<?php echo url('/') ?>/js/plugin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo url('/') ?>/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>
    <script src="<?php echo url('/') ?>/js/modules/QuestionsModule.js"></script>
    @endpush
@stop