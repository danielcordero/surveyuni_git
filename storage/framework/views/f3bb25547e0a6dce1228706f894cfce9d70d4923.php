<?php
    if(!empty($question->show_help) &&
        $question->show_help == "Y"){
//    if(1){
?>
    <fieldset class="questionHelp">
        <section>
            <div class="col-md-12">
                <a href="javascript:void(0);"
                   rel="tooltip"
                   data-placement="bottom"
                   data-original-title="<?php echo strip_tags($question->help); ?>">
                                        <span class="txt-color-blueLight">
                                            <i class="fa fa-question-circle"></i>
                                            <em>
                                                <?php
                                                echo strip_tags($question->help);
                                                ?>
                                            </em>
                                        </span>
                </a>
            </div>
        </section>
        <br>
    </fieldset>
<?php
    }
?>